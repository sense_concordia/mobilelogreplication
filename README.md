# Mobile Log Replication Package

This repository contains all the scripts and data that were used in our paper: Studying the Characteristics of Logging Practices in Mobile Apps: A Case Study on F-Droid.

## Usage
The structure of the package is divided into three parts corresponding to the different RQs in our paper. Each RQ folder contains `code` that we used, and `data` that we got.

## RQ1: What are the characteristics of mobile logging practices?

### Code Usage

1. Follow the instruction in [`code/LogDetector/README.md`](./RQ1/code/LogDetector/README.md) to download F-Droid apps and detect log changes.

2. Run `code/LogDetector/bin/getmetrics.py` to get the logs' metric data.

3. Run `code/LogDetector/sql/get_log_updates.sql` to query log updates from database.

4. Run the method `get_deleted_inside_method_log_ages_of_repo` in `code/LogDetector/logdetector/metrics.py` to calculate lifespan of logs in a specific app. 

5. Run `code/LogDetector/sql/get_added_and_deleted_logs.sql` to get all added and deleted logs.

6. Run `code/check_log_pair.py` to get log changes that are falsely categorized into addition and deletion pair because of file renaming.


### Data Usage

1. The `data/database` includes data that we collected: `repo.csv`,`commit.csv` and `log.csv`. You can import them to a SQL database for further query.

2. The `data/log_lifespan` includes the lifespan of logs of our eight target apps.

3. The `data/log_sample_multiline_log.csv` is the sample that we used to identify the percentage of logging statements that occupy more than one line in the source code.


## RQ2: What are the rationales of mobile logging?

### Code Usage

1. Follow the instruction in [`RQ1/code/LogDetector/README.md`](./RQ1/code/LogDetector/README.md) to download F-Droid apps and detect log changes.

2. Run `RQ1/code/LogDetector/sql/export_added_logs.sql` to export recently added logging statements into a csv file.

3. Run `RQ1/code/LogDetector/bin/get_email_candidate_logs.py` to send query to developers about the rationale of adding a logging statement.

### Coding Guide

When categorizing the rationale of a logging statement, we follow the coding guide `code/CodingGuide.pdf`.


### Data Usage

1. The `data/Firehouse interview/sent.csv` includes the logging statements that we sent emails to developers.
2. The `data/Firehouse interview/responses.csv` includes the responses that we got from developers. Note that their authors are not public available with respect to privacy. Please send us a [request](mailto:ze_yi@encs.concordia.ca) for further detail.
3. The `data/Manual study` includes the data we used for manual study for rationales. The `MobileAddReasonResult.csv` contains 384 logging statements that we sampled and their corresponding rationales. The Java files that the sampled logging statements belong to are located in `sample_added_log_files`.


## RQ3: How large is the performance impact of mobile logging?

### Code Usage

### Step 1. Collect the data

Connect phone with computer. Make sure the [developer options](https://developer.android.com/studio/debug/dev-options) is on.

For collecting the battery consumption, when connected to the computer via usb cable, the phone would start charging, which may affect the result. Therefore, we use [Battery Charge Limit](https://play.google.com/store/apps/details?id=com.slash.batterychargelimit) to disable the usb charging before running our tests.

#### 1. Unit test

We take `nextcloud` as an example, the procedure is similar for the other apps.

1. Open the directory `code/collect`.
2. Edit  `perfsys_unit.py` , set the variable `package` to `nextcloud`.
3. Collect response time:  edit `test_unit_nextcloud.sh`,  set the variable `IsMonitorResponseTime` in  to `true` and `IsMonitorCPU` to `false`, then run `test_unit_nextcloud.sh`.
4. Collect cpu usage: edit `test_unit_nextcloud.sh`, set the variable `IsMonitorResponseTime` to `false` and `IsMonitorCPU` to `true`, then run `test_unit_nextcloud.sh`.

#### 2. Instrumented test

We take `wordpress` as an example, the procedure is similar for the other apps.

1. Open the directory `code/collect`.
2. Edit  `perfsys_ins.py`, set the variable `package` to `org.wordpress.android`,
3. Edit `test_ins.sh`, set the group variable to be app specific (e.g., `APP` to `org.wordpress.android`).
3. Collect response time: edit `test_ins.sh`, set the variable `IsMonitorResponseTime` to `true`, `IsMonitorCPU` to `false` and `IsMonitorBattery` to `false`, then run `test_ins.sh`.
4. Collect battery consumption: edit `test_ins.sh`, set the variable `IsMonitorResponseTime` to `false`, `IsMonitorCPU` to `false` and `IsMonitorBattery` to `true`, then run `test_ins.sh`.
5. Collect cpu usage: Add 15 seconds waiting before and after the test, edit `test_ins.sh`, set the variable `IsMonitorResponseTime` to `false`, `IsMonitorCPU` to `true` and `IsMonitorBattery` to `false`, then run `test_ins.sh`.


For necessary log experiment, take `cgeo` as an example, the procedure is similar for the other apps:

Edit  `test_ins.sh`, change `TEST_CONFIG`  to `cgeo_ins_necessary.log`, then run the same process above.

#### 3. Espresso test

Follow the Android [setup instruction](https://developer.android.com/training/testing/ui-testing/espresso-testing#setup) before running espresso test.

We take `wordpress` as an example, the procedure is similar for the other three apps.

1. Open the directory `code/collect`.
2. Edit  `perfsys_espresso.py`, set the variable `package`  to `org.wordpress.android`.
3. Edit `test_espresso.sh`, set the group variable to be app specific (e.g., `APP` to `org.wordpress.android`). The three options `IsMonitorBattery`, `IsMonitorResponseTime` and `IsMonitorCPU` in `test_espresso.sh` have the same usage as the instrumented test above. Then run `test_espresso.sh`.

### Step 2. Analyze the data

Put the collected data under `code/analyze/data`, the structure of the directory can be referred to `data/espresso/test_data`,  `data/unit/test_data` and `data/instrumented/test_data`.

#### 1. Unit test

1. Copy `code/analyze/merge_unit.sh` to an app data directory which contains the collected data, run the script to generate csv files for further analysis.
2. Edit `code/analyze/stats_difference.R`, set `test_type` to `unit`, set `dir` to the app data directory, set `has_necessarylog` to `False`.
3.  Run the R script, the result named `stats_diff.csv` would be located in the app data directory.

#### 2. Instrumented test

1. Copy `code/analyze/merge_instrumented.sh` to an app data directory which contains the collected data, run the script to generate csv files for further analysis.
2. Edit `code/analyze/stats_difference.R`, set `test_type` to `instrumented`, set `dir` to the app data directory, set `has_necessarylog` according to specific app.
3. Run the R script, the result named `stats_diff.csv` and `stats_diff_necessary.csv` would be located in the app data directory.
   
#### 3. Espresso test

1. Copy `code/analyze/merge_espresso.sh` to an app data directory which contains the collected data, run the script to generate csv files for further analysis.
2.  Edit `code/analyze/stats_difference.R`, set `test_type` to `espresso`, set `dir` to the app data directory, set `has_necessarylog` according to specific app.
3. Run the R script, the result named `stats_diff.csv` and `stats_diff_necessary.csv` would be located in the app data directory.
   

### Data Usage

The `data` directory includes data for `espresso_test` and `unit_test` and `instrumented_test`.

For each type of test:

- `log_output` contains the log messages generated when enabling all logging statements and only enabling the necessary ones.

- `test_data` contains the metrics(response time, CPU and battery consumption) we collected when enabling all logs, disabling all logs, and enabling necessary logs. It also contains the Wilcoxon test result.

- `test_files` contains the test files that we used in the experiment. Note that the pre-operations such as log in and download files should be done before running these tests.
