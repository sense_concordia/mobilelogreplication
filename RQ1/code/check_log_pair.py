#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng

import csv

if __name__ == '__main__':
    result = {}
    commit = {}
    last_commit_id = ''
    last_log_content = ''
    last_change_type = ''
    last_file_path = ''
    with open('./added_and_deleted_logs.csv', encoding='utf-8') as f:
        reader = csv.reader(f)
        for row in reader:
            file_path = row[2]
            commit_id = row[-2]
            change_type = row[4].split('_')[0]
            content = row[5]

            if last_commit_id != commit_id:
                last_log_content = ''
                last_change_type = ''
                last_commit_id = commit_id
            else:
                if last_log_content == content and last_file_path != file_path and last_change_type != change_type:
                    print(commit_id, content, change_type)

                last_log_content = content
                last_change_type = change_type
                last_file_path = file_path
