# LogDetector
This repository gets log characteristics.
Currently mining [F-Droid](https://f-droid.org/) projects, but you can do it on any Java-based projects.

## Installation

#### Step 1. Get fdroid repo list

- git clone `fdroidserver`

        git clone https://gitlab.com/fdroid/fdroidserver.git
        export PATH="\$PATH:\$PWD/fdroidserver"

- git clone `fdroiddata`

        git clone https://gitlab.com/fdroid/fdroiddata.git

- replace the content of `fdroidserver/fdroidserver/readmeta.py` with `bin/fdroidserver_readmeta`'s content. You can modify if needed.

- get `fdroid_github.csv`

        cd fdroiddata
        fdroid readmeta


#### Step 2. Install dependencies

- Install [PostgreSQL](https://www.postgresql.org)

- Install [srcML](http://www.srcml.org)

- Install [cloc](https://github.com/AlDanial/cloc), `version >= 1.74`

- Install [pipenv](https://github.com/pypa/pipenv)

- `pipenv install`

#### Step 3. Setup config

- Setup PostgreSQL

- Generate [GitHub API access token](https://github.com/settings/tokens)

- Copy `logdetector/config-template.py` to `logdetector/config.py`, change `config.py` with your own setting.

- Open terminal, run `git config --global diff.renameLimit 999999` to enable git detect renaming when too many files modified.

- Setup `EMAIL_SENDER` and `EMAIL_CC` to your email address which is used to send emails to developers.

- Config email password by following the instruction of [yagmail](https://github.com/kootenpv/yagmail)

#### Step 4. Initialise database

- Run `bin/fdroid.py`, change the `fdroid_csv_path` in main if needed.

## Start detecting
    run `bin/detect.py`
