copy (select commit.repo_fk, commit.commit_id, commit.author_name, commit.author_email, commit.authored_date, commit.message, log.file_path, log.embed_method, log.change_type, log.content, log.content_update_from
from log join commit on commit.id = log.commit_fk
where author_email not like '%noreply.github.com%' and commit.authored_date > '2018-07-11' and commit.authored_date < '2018-07-18' and log.change_type like 'ADD%') To './data/added_logs_from_07-11_to_07-18.csv' With CSV HEADER;
