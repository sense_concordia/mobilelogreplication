-- Get verbosity_change log updates count
select count(*) from log join commit on commit.id=log.commit_fk join repo on commit.repo_fk = repo.url where log.update_type like '%UPDATED_VERBOSITY%' and repo.sloc > 713;

select count(*) from log join commit on commit.id=log.commit_fk where log.update_type like '%UPDATED_VERBOSITY%' and commit.repo_fk like '%nextcloud/android%';

-- Get error related log updates count
select count(*) from log join commit on commit.id=log.commit_fk join repo on commit.repo_fk = repo.url where log.update_type like '%UPDATED_VERBOSITY%' and repo.sloc > 713 and (log.update_type like '%UPDATED_VERBOSITY_ERROR_TO_%' or log.update_type like '%UPDATED_VERBOSITY_%_TO_ERROR%');

select count(*) from log join commit on commit.id=log.commit_fk where (log.update_type like '%UPDATED_VERBOSITY_ERROR_TO_%' or log.update_type like '%UPDATED_VERBOSITY_%_TO_ERROR%') and commit.repo_fk like '%nextcloud/android%';


-- Get non error related log updates count
select count(*) from log join commit on commit.id=log.commit_fk join repo on commit.repo_fk = repo.url where log.update_type like '%UPDATED_VERBOSITY%' and repo.sloc > 713  and log.update_type not like '%UPDATED_VERBOSITY_ERROR_TO_%' and log.update_type not like '%UPDATED_VERBOSITY_%_TO_ERROR%';

select count(*) from log join commit on commit.id=log.commit_fk where log.update_type like '%UPDATED_VERBOSITY%' and log.update_type not like '%UPDATED_VERBOSITY_ERROR_TO_%' and log.update_type not like '%UPDATED_VERBOSITY_%_TO_ERROR%' and commit.repo_fk like '%nextcloud/android%';

-- Get log updates from error to non-error count
select count(*) from log join commit on commit.id=log.commit_fk join repo on commit.repo_fk = repo.url where log.update_type like '%UPDATED_VERBOSITY%' and repo.sloc > 713 and log.update_type like '%UPDATED_VERBOSITY_ERROR_TO_%';

select count(*) from log join commit on commit.id=log.commit_fk where log.update_type like '%UPDATED_VERBOSITY_ERROR_TO_%' and commit.repo_fk like '%nextcloud/android%';


-- Get log updates from non-error to error count
select count(*) from log join commit on commit.id=log.commit_fk join repo on commit.repo_fk = repo.url where log.update_type like '%UPDATED_VERBOSITY%' and repo.sloc > 713 and log.update_type like '%UPDATED_VERBOSITY_%_TO_ERROR%';

select count(*) from log join commit on commit.id=log.commit_fk where log.update_type like '%UPDATED_VERBOSITY_%_TO_ERROR%' and commit.repo_fk like '%nextcloud/android%';



-- Get log updates count
select count(*) from log where log.change_type='UPDATED';



