select log.*, commit.repo_fk, commit.commit_id, commit.message from log
join commit on commit.id = log.commit_fk
where log.change_type like 'ADDED_WITH_FILE' OR log.change_type like 'DELETED_WITH_FILE'
order by commit.commit_id, log.content, log.file_path, log.change_type