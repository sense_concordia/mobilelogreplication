#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng


class LaTexWriter(object):
    label = None
    title = None
    header = None
    content = None
    data = None
    escaped_header = None
    escaped_data = None
    max_col_width = None

    def __init__(self, label, title, header, data):
        self.label = label
        self.title = title
        self.header = header
        self.data = data

    def append_line(self, line):
        if self.content is None:
            self.content = line
        else:
            self.content += line
        self.content += '\n'

    def print(self):
        self.content = None
        self.escaped_header = [self._get_escape_text(x) for x in self.header]
        self.escaped_data = [[self._get_escape_text(x) for x in line] for line in self.data]
        self.max_col_width = self._get_max_col_width()

        self.append_line(r'\begin{table}[tbh]')
        self.append_line(r'\centering')
        self.append_line(r'\caption{' + self.title + '}')
        self.append_line(r'\label{' + self.label + '}')
        self.append_line(r'\resizebox{\linewidth}{!}')
        self.append_line(r'{')
        self.append_line(r'\begin{tabular}')
        self.append_line(r'{|' + 'c|' * len(self.header) + r'}')
        self.append_line(r'\hline')
        self._print_header()
        self._print_data()
        self.append_line(r'\end{tabular}')
        self.append_line(r'}')
        self.append_line(r'\end{table}')
        print(self.content)

    def _print_header(self):
        line = ''
        for i in range(len(self.escaped_header)):
            align_format = '{:>' + str(self.max_col_width[i] + 3) + '}'
            line = line + align_format.format(self.escaped_header[i]) + ' &'
        if line.endswith('&'):
            line = line[:-1]
        line += r'\\ \hline'
        self.append_line(line)

    def _print_data(self):
        for data_line in self.escaped_data:
            line = ''
            for i in range(len(data_line)):
                element = data_line[i]
                if self._is_digit(element):
                    element = self._get_format_digit(element)
                align_format = '{:>' + str(self.max_col_width[i] + 3) + '}'
                line = line + align_format.format(element) + ' &'
            if line.endswith('&'):
                line = line[:-1]
            line += r'\\ \hline'
            self.append_line(line)

    def _get_format_digit(self, text):
        if not self._is_digit(text):
            raise TypeError('Not digit')
        if '.' in text:
            integer = int(text.split('.')[0])
            float_value = text.split('.')[1]
            integer_str = "{:,}".format(integer)
            return integer_str + '.' + float_value
        else:
            return "{:,}".format(int(text))

    def _is_digit(self, text):
        return str(text).replace('.', '', 1).isdigit()

    def _get_max_col_width(self):
        max_col_width = []
        for i in range(len(self.escaped_data[0])):
            max_element_width = max(len(self.escaped_data[j][i]) for j in range(len(self.escaped_data)))
            if max_element_width < len(self.escaped_header[i]):
                max_element_width = len(self.escaped_header[i])
            max_col_width.append(max_element_width)
        return max_col_width

    def _get_escape_text(self, text):
        text = str(text).replace('\\', r'\textbackslash')
        special_char_dict = {
            '#': r'\#',
            '$': r'\$',
            '^': r'\^',
            '&': r'\&',
            '_': r'\_',
            '{': r'\{',
            '}': r'\}',
            '~': r'\~',
            '%': r'\%'
        }
        special_charset = special_char_dict.keys()
        for c in special_charset:
            text = text.replace(c, special_char_dict[c])

        return text

