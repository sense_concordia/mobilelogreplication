package com.todoroo.astrid.tags;

import java.util.LinkedHashSet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.timsu.astrid.R;
import com.todoroo.andlib.data.TodorooCursor;
import com.todoroo.andlib.sql.Criterion;
import com.todoroo.astrid.activity.TaskEditActivity.TaskEditControlSet;
import com.todoroo.astrid.model.Metadata;
import com.todoroo.astrid.model.Task;
import com.todoroo.astrid.tags.TagService.Tag;

/**
 * Control set to manage adding and removing tags
 *
 * @author Tim Su <tim@todoroo.com>
 *
 */
public final class TagsControlSet implements TaskEditControlSet {

    // --- instance variables

    private final TagService tagService = TagService.getInstance();
    private final Tag[] allTags;
    private final LinearLayout tagsContainer;
    private final Activity activity;

    public TagsControlSet(Activity activity, int tagsContainer) {
        allTags = tagService.getGroupedTags(TagService.GROUPED_TAGS_BY_SIZE, Criterion.all);
        this.activity = activity;
        this.tagsContainer = (LinearLayout) activity.findViewById(tagsContainer);
    }

    @Override
    public void readFromTask(Task task) {
        System.err.println("TAGS loading... old size = " + tagsContainer.getChildCount());
        tagsContainer.removeAllViews();

        TodorooCursor<Metadata> cursor = tagService.getTags(task.getId());
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
                addTag(cursor.get(TagService.TAG));
        } finally {
            cursor.close();
        }
        if(tagsContainer.getChildCount() == 0)
            addTag(""); //$NON-NLS-1$
        System.err.println("TAGS loaded ");
    }

    @Override
    public void writeToModel(Task task) {
        // this is a case where we're asked to save but the UI was not yet populated
        if(tagsContainer.getChildCount() == 0)
            return;

        LinkedHashSet<String> tags = new LinkedHashSet<String>();

        for(int i = 0; i < tagsContainer.getChildCount(); i++) {
            TextView tagName = (TextView)tagsContainer.getChildAt(i).findViewById(R.id.text1);
            if(tagName.getText().length() == 0)
                continue;
            tags.add(tagName.getText().toString());
        }

        tagService.synchronizeTags(task.getId(), tags);
        System.err.println("TAGS saved " + tags);
    }

    /** Adds a tag to the tag field */
    boolean addTag(String tagName) {
        System.err.println("TAG ADDING ui " + tagName);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View tagItem = inflater.inflate(R.layout.tag_edit_row, null);
        tagsContainer.addView(tagItem);

        final AutoCompleteTextView textView = (AutoCompleteTextView)tagItem.
            findViewById(R.id.text1);
        textView.setText(tagName);
        ArrayAdapter<Tag> tagsAdapter =
            new ArrayAdapter<Tag>(activity,
                    android.R.layout.simple_dropdown_item_1line, allTags);
        textView.setAdapter(tagsAdapter);

        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                View lastItem = tagsContainer.getChildAt(tagsContainer.getChildCount()-1);
                TextView lastText = (TextView) lastItem.findViewById(R.id.text1);
                if(lastText.getText().length() != 0) {
                    addTag(""); //$NON-NLS-1$
                }
            }
        });

        /*textView.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView arg0, int actionId, KeyEvent arg2) {
                if(actionId != EditorInfo.IME_NULL)
                    return false;
                View lastItem = tagsContainer.getChildAt(tagsContainer.getChildCount()-1);
                TextView lastText = (TextView) lastItem.findViewById(R.id.text1);
                if(lastText.getText().length() != 0) {
                    addTag(""); //$NON-NLS-1$
                }
                return true;
            }
        });*/

        ImageButton reminderRemoveButton;
        reminderRemoveButton = (ImageButton)tagItem.findViewById(R.id.button1);
        reminderRemoveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(tagsContainer.getChildCount() > 0)
                    tagsContainer.removeView(tagItem);
                else
                    textView.setText(""); //$NON-NLS-1$
            }
        });

        return true;
    }
}