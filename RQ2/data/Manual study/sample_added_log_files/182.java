package com.example.lexinproject.ui;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lexinproject.IFacade;
import com.example.lexinproject.R;
import com.example.lexinproject.XmlUtils;
import com.example.lexinproject.data.Language;
import com.example.lexinproject.data.LanguagesAdapter2;
import com.example.lexinproject.providers.TranslateClient;
import com.example.lexinproject.providers.Translator;

public class PreviewFragment extends Fragment implements IFacade {
	private static final String STATE_TYPE_POS = "type_pos";
	private static final String STATE_LANGUAGE_POS = "language_pos";
	private static final String STATE_TRANSLATE_VALUE = "translate_value";
	private static final String EMPTY_BODY = "";
	private static final String TAG = "PreviewFragment";
	public static final String ARG_SECTION_NUMBER = "section_number";

	private Translator mTranslationFactory;
	private Language mLanguage = null;
	private String mType = null;
	private Spinner mSpinnerLanguages;
	private Spinner mSpinnerType;
	private WebView mWebView;
	private MultiAutoCompleteTextView mTextView;

	/**
	 * Create a new instance of PreviewFragment
	 */
	public static PreviewFragment newInstance() {
		PreviewFragment f = new PreviewFragment();
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			final Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		Log.d(TAG, "onCreateView >>");
		View view = inflater.inflate(R.layout.preview_fragment, container,
				false);
		mTranslationFactory = new Translator(this);
		mSpinnerLanguages = (Spinner) view.findViewById(R.id.spinner1);
		mSpinnerType = (Spinner) view.findViewById(R.id.spinner2);
		mWebView = (WebView) view.findViewById(R.id.webView1);
		mTextView = ((MultiAutoCompleteTextView) view
				.findViewById(R.id.multiAutoCompleteTextView1));
		final LanguagesAdapter2 la = new LanguagesAdapter2(getActivity(),
				mTranslationFactory.getLanguages());
		mSpinnerLanguages.setAdapter(la);

		Button okBtn = (Button) view.findViewById(R.id.button2);
		okBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				doTranslate();
			}
		});
		Button cancelButton = (Button) view.findViewById(R.id.button1);
		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mTranslationFactory.stop();
			}
		});
		mSpinnerLanguages
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						final Language selectedItem = (Language) parent
								.getItemAtPosition(position);
						Log.d(TAG, "onItemSelected " + selectedItem.getName());
						mLanguage = selectedItem;
						String[] types = mTranslationFactory
								.getTypes(mLanguage);
						ArrayAdapter adapter = new ArrayAdapter(getActivity(),
								android.R.layout.simple_spinner_dropdown_item,
								types);
						mSpinnerType.setAdapter(adapter);
						if (savedInstanceState != null
								&& savedInstanceState
										.containsKey(STATE_TYPE_POS)) {
							mSpinnerType.setSelection(savedInstanceState
									.getInt(STATE_TYPE_POS));
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
					}

				});
		mSpinnerType.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				final String selectedItem = (String) parent
						.getItemAtPosition(position);
				Log.d(TAG, "onItemSelected " + selectedItem);
				mType = selectedItem;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		if (savedInstanceState != null) {
			Log.i(TAG, "savedInstanceState available");
			if (savedInstanceState.containsKey(STATE_TRANSLATE_VALUE)) {
				mTextView.setText(savedInstanceState
						.getString(STATE_TRANSLATE_VALUE));
			} else if (savedInstanceState.containsKey(STATE_LANGUAGE_POS)) {
				mSpinnerLanguages.setSelection(savedInstanceState
						.getInt(STATE_LANGUAGE_POS));
			}
			// FIXME : type is set onchange
			/*
			 * else if (savedInstanceState.containsKey(STATE_TYPE_POS)) {
			 * mSpinnerType.setSelection(savedInstanceState
			 * .getInt(STATE_TYPE_POS)); }
			 */
		}
		Log.d(TAG, "onCreateView <<");
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(TAG, "onSaveInstanceState >>");

		outState.putString(STATE_TRANSLATE_VALUE, mTextView.getText()
				.toString());
		outState.putInt(STATE_LANGUAGE_POS,
				mSpinnerLanguages.getSelectedItemPosition());
		outState.putInt(STATE_TYPE_POS, mSpinnerType.getSelectedItemPosition());

		Log.d(TAG, "onSaveInstanceState <<");
	}

	private void doTranslate() {
		showOnWebView(EMPTY_BODY);
		final String value = mTextView.getText().toString();
		// TODO:
		if (mLanguage != null && mType != null && (!TextUtils.isEmpty(value))) {
			mTranslationFactory.translate(value, mType, mLanguage);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mTranslationFactory.stop();
	}

	@Override
	public void startProgress() {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Log.d(TAG, "startProgress >>");
					Button okBtn = (Button) getActivity().findViewById(
							R.id.button2);
					okBtn.setEnabled(false);
					getActivity().setProgressBarIndeterminateVisibility(true);
					Log.d(TAG, "startProgress <<");
				}
			});
		}
	}

	@Override
	public void stopProgress() {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Log.d(TAG, "stopProgress >>");
					Button okBtn = (Button) getActivity().findViewById(
							R.id.button2);
					okBtn.setEnabled(true);
					getActivity().setProgressBarIndeterminateVisibility(false);
					Log.d(TAG, "stopProgress <<");
				}
			});
		}
	}

	@Override
	public void loadUrl(final TranslateClient client, final String url) {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Log.d(TAG, "loadUrl " + url);
					client.loadUrl(url);
					Log.d(TAG, "loadUrl <<");
				}
			});
		}
	}

	@Override
	public void showOnWebView(final String content) {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Log.d(TAG, "showOnWebView size " + content.length() + ">>");
					final String translateHtml = XmlUtils.toHtml(content);
					/*
					 * EditText textView = (EditText)
					 * findViewById(R.id.editText1);
					 * textView.setMovementMethod(new
					 * ScrollingMovementMethod());
					 * //textView.setInputType(InputType.TYPE_NULL);
					 * textView.setText
					 * (/*ranslateHtmlHtml.fromHtml(translateHtml));
					 */
					WebView webView = (WebView) getActivity().findViewById(
							R.id.webView1);
					if (webView != null) {
						webView.loadData(translateHtml,
								"text/html; charset=utf-8", "UTF-8");
					}
					Log.d(TAG, "showOnWebView <<");
				}
			});
		}
	}

	@Override
	public void onTranslateFinished(final TranslateClient client,
			final String content) {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Log.d(TAG, "onTranslateFinished size "
							+ ((content == null) ? "Null" : content.length())
							+ " >>");
					client.stopLoadUrl();
					showOnWebView(content);
					Log.d(TAG, "onTranslateFinished <<");
				}
			});
			stopProgress();
		}
	}

	@Override
	public void stopLoadUrl(final TranslateClient client) {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					Log.d(TAG, "stopLoadUrl >>");
					stopProgress();
					client.stopLoadUrl();
					Log.d(TAG, "stopLoadUrl <<");
				}
			});
		}
	}
}
