/*
 * ttrss-reader-fork for Android
 * 
 * Copyright (C) 2010 N. Braden.
 * Copyright (C) 2009-2010 J. Devauchelle.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

package org.ttrssreader.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.ttrssreader.controllers.Controller;
import org.ttrssreader.gui.activities.AboutActivity;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Utils {
    
    /**
     * Supported extensions of imagefiles, see http://developer.android.com/guide/appendix/media-formats.html
     */
    public static final String[] IMAGE_EXTENSIONS = { "jpeg", "jpg", "gif", "png", "bmp" };
    
    /**
     * Supported extensions of mediafiles, see http://developer.android.com/guide/appendix/media-formats.html
     */
    public static final String[] MEDIA_EXTENSIONS = { "3gp", "mp4", "m4a", "aac", "mp3", "mid", "xmf", "mxmf", "rtttl",
            "rtx", "ota", "imy", "ogg", "wav" };
    
    /**
     * The TAG for Log-Output
     */
    public static final String TAG = "ttrss";
    
    /**
     * Time to wait before starting the background-update from the activities
     */
    public static final int WAIT = 100;
    
    /**
     * Vibrate-Time for vibration when end of list is reached
     */
    public static final long SHORT_VIBRATE = 50;
    
    /**
     * The time after which data will be fetched again from the server if asked for the data
     */
    public static int UPDATE_TIME = 120000;
    
    /**
     * Path on sdcard to store files (DB, Certificates, ...)
     */
    public static final String SDCARD_PATH = "/Android/data/org.ttrssreader/files/";
    
    public static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 1024 * 10);
        StringBuilder sb = new StringBuilder();
        
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    public static boolean newVersionInstalled(Activity a) {
        String thisVersion = getVersion(a);
        String lastVersionRun = Controller.getInstance().getLastVersionRun();
        Controller.getInstance().setLastVersionRun(thisVersion);
        
        if (thisVersion.equals(lastVersionRun)) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Retrieves the packaged version of the application
     * 
     * @param a
     *            - The Activity to retrieve the current version
     * @return the version-string
     */
    public static String getVersion(Activity a) {
        String result = "";
        try {
            PackageManager manager = a.getPackageManager();
            PackageInfo info = manager.getPackageInfo(a.getPackageName(), 0);
            result = info.versionName;
        } catch (NameNotFoundException e) {
            Log.w(AboutActivity.class.toString(), "Unable to get application version: " + e.getMessage());
            result = "";
        }
        return result;
    }
    
    /**
     * Checks if the option to work offline is set or if the data-connection isn't established, else returns true. If we
     * are about to connect it waits for maximum 2 seconds and then returns the network state without waiting anymore.
     * 
     * @param cm
     * @return
     */
    public static boolean isOnline(ConnectivityManager cm) {
        if (Controller.getInstance().isWorkOffline()) {
            return false;
        } else if (cm == null) {
            return false;
        }
        
        NetworkInfo info = cm.getActiveNetworkInfo();
        
        if (info == null) {
            return false;
        }
        
        synchronized (Utils.class) {
            int wait = 0;
            while (info.isConnectedOrConnecting() && !info.isConnected()) {
                try {
                    wait += 100;
                    Utils.class.wait(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                
                if (wait > 2000) {
                    break;
                }
            }
        }
        
        return info.isConnected();
    }
    
    public static String getCachedImageUrl(String url) {
        ImageCache cache = Controller.getInstance().getImageCache(null);
        
        if (cache != null && cache.containsKey(url)) {
            String localUrl = "file://" + cache.getDiskCacheDirectory() + File.separator + cache.getFileNameForKey(url);
            
            Log.d(Utils.TAG, String.format("Url: %s - Cache-Url:  %s", url, localUrl));
            return localUrl;
        }
        return null;
    }
    
    public static String findImageUrl(String html) {
        Pattern p = Pattern.compile("<img.+src=\"([^\"]*)\".*/>", Pattern.CASE_INSENSITIVE);
        
        int i = html.indexOf("<img");
        Matcher m = p.matcher(html.substring(i, html.length()));
        
        if (m.find()) {
            return m.group(1);
        }
        return null;
    }
    
    public static Set<String> findAllImageUrls(String html) {
        Set<String> ret = new LinkedHashSet<String>();
        if (html == null || html.length() < 10) {
            return ret;
        }
        
        Pattern p = Pattern.compile("<img.+src=\"([^\"]*)\".*/>", Pattern.CASE_INSENSITIVE);
        
        for (int i = 0; i < html.length();) {
            i = html.indexOf("<img", i);
            if (i == -1) {
                break;
            }
            Matcher m = p.matcher(html.substring(i, html.length()));
            
            if (m.find()) {
                ret.add(m.group(1));
                i += m.group(1).length();
            } else {
                break;
            }
        }
        return ret;
    }
    
    public static String injectCachedImages(String html) {
        if (html == null || html.length() < 10) {
            return html;
        }
        
        for (String url : findAllImageUrls(html)) {
            
            String localUrl = getCachedImageUrl(url);
            if (localUrl != null) {
                html = html.replace(url, localUrl);
            }
        }
        return html;
    }
    
    public static void downloadImage(String downloadUrl, File file) {
        FileOutputStream fos = null;
        try {
            if (file.exists() && file.length() <= 1) {
                file.delete();
            }
            Log.d(Utils.TAG, "Writing image to file: " + file.getAbsolutePath());
            URL url = new URL(downloadUrl);
            file.createNewFile();
            fos = new FileOutputStream(file);
            
            URLConnection ucon = url.openConnection();
            InputStream is = ucon.getInputStream();
            int size = 1024 * 1024;
            byte[] buf = new byte[size];
            int byteRead;
            int byteWritten = 0;
            while (((byteRead = is.read(buf)) != -1)) {
                fos.write(buf, 0, byteRead);
                byteWritten += byteRead;
                if (byteWritten > 1024 * 1024 * 1024) { // Should be 1MB. It's late.
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                }
            }
        }
        
    }
    
    public static long getFolderSize(File folder) {
        long size = 0;
        
        File[] list = folder.listFiles();
        for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
                size += getFolderSize(list[i]);
            } else {
                size += list[i].length();
            }
        }
        return size;
    }
}
