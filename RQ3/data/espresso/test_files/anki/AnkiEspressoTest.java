package com.ichi2.anki;


import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AnkiEspressoTest {

//    @BeforeClass
//    public static void beforeMethod() {
//        try {
//            Thread.sleep(15000);
//        } catch (Exception e) {
//
//        }
//    }
//
//    @AfterClass
//    public static void afterMethod() {
//        try {
//            Thread.sleep(15000);
//        } catch (Exception e) {
//
//        }
//    }

    @Rule
    public ActivityTestRule<IntentHandler> mActivityTestRule = new ActivityTestRule<>(IntentHandler.class);


    @Test
    public void ankiEspressoTest() {
        ViewInteraction viewInteraction = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.add_deck_action), withContentDescription("Create deck"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                0),
                        isDisplayed()));
        floatingActionButton.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction editText = onView(
                childAtPosition(
                        childAtPosition(
                                withId(R.id.customViewFrame),
                                0),
                        0));
        editText.perform(scrollTo(), click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction editText2 = onView(
                childAtPosition(
                        childAtPosition(
                                withId(R.id.customViewFrame),
                                0),
                        0));
        editText2.perform(scrollTo(), replaceText("a"), closeSoftKeyboard());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction mDButton = onView(
                allOf(withId(R.id.buttonDefaultPositive), withText("OK"),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                4),
                        isDisplayed()));
        mDButton.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction viewInteraction2 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.add_note_action), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                2),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction fieldEditText = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        1),
                                0)));
        fieldEditText.perform(scrollTo(), click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction fieldEditText2 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        1),
                                0)));
        fieldEditText2.perform(scrollTo(), replaceText("a"), closeSoftKeyboard());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction fieldEditText3 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Back"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        3),
                                0)));
        fieldEditText3.perform(scrollTo(), replaceText("a"), closeSoftKeyboard());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.action_save), withContentDescription("Save"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        3),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.deckpicker_view),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction navigationMenuItemView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        2),
                        isDisplayed()));
        navigationMenuItemView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.title), withText("Change display order"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.support.v7.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        appCompatTextView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout = onData(anything())
                .inAdapterView(allOf(withId(R.id.contentListView),
                        childAtPosition(
                                withId(R.id.contentListViewFrame),
                                0)))
                .atPosition(2);
        linearLayout.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction navigationMenuItemView2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        3),
                        isDisplayed()));
        navigationMenuItemView2.perform(click());

        ViewInteraction textView = onView(
                allOf(withText("FORECAST"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                1),
                        isDisplayed()));
        textView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView2 = onView(
                allOf(withText("REVIEW COUNT"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                2),
                        isDisplayed()));
        textView2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView3 = onView(
                allOf(withText("INTERVALS"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                4),
                        isDisplayed()));
        textView3.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView4 = onView(
                allOf(withText("HOURLY BREAKDOWN"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                5),
                        isDisplayed()));
        textView4.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView5 = onView(
                allOf(withText("WEEKLY BREAKDOWN"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                6),
                        isDisplayed()));
        textView5.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView6 = onView(
                allOf(withText("ANSWER BUTTONS"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                7),
                        isDisplayed()));
        textView6.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView7 = onView(
                allOf(withText("CARD TYPES"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.sliding_tabs),
                                        0),
                                8),
                        isDisplayed()));
        textView7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton4 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton4.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction switchCompat = onView(
                allOf(withId(R.id.switch_compat), withContentDescription("Night mode"),
                        childAtPosition(
                                allOf(withId(R.id.nav_night_mode),
                                        childAtPosition(
                                                withId(R.id.design_menu_item_action_area),
                                                0)),
                                0),
                        isDisplayed()));
        switchCompat.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton5 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton5.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction navigationMenuItemView3 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        6),
                        isDisplayed()));
        navigationMenuItemView3.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout2 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(0);
        linearLayout2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout3 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(2);
        linearLayout3.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout6 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(5);
        linearLayout6.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatButton = onView(
                allOf(withId(android.R.id.button2), withText("Cancel"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                2)));
        appCompatButton.perform(scrollTo(), click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton6 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton6.perform(click());

        ViewInteraction appCompatImageButton7 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction navigationMenuItemView4 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        6),
                        isDisplayed()));
        navigationMenuItemView4.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout7 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        linearLayout7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout8 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(1);
        linearLayout8.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction appCompatCheckedTextView = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.FrameLayout")),
                                0)))
                .atPosition(1);
        appCompatCheckedTextView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton8 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton8.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton9 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton9.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction navigationMenuItemView5 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        6),
                        isDisplayed()));
        navigationMenuItemView5.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout9 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        linearLayout9.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout10 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(4);
        linearLayout10.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction appCompatCheckedTextView2 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.FrameLayout")),
                                0)))
                .atPosition(1);
        appCompatCheckedTextView2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton10 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton10.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton11 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton11.perform(click());

        ViewInteraction navigationMenuItemView6 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        6),
                        isDisplayed()));
        navigationMenuItemView6.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout12 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(3);
        linearLayout12.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout13 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout13.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton12 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton12.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatImageButton13 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton13.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction navigationMenuItemView7 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        6),
                        isDisplayed()));
        navigationMenuItemView7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout14 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(4);
        linearLayout14.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout15 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(6);
        linearLayout15.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout16 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout16.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                3)));
        appCompatButton2.perform(scrollTo(), click());

    }


    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }


            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
