package com.fsck.k9.activity;


import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.fsck.k9.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class K9EspressoTest {

//    @BeforeClass
//    public static void beforeMethod() {
//        try {
//            Thread.sleep(15000);
//        } catch (Exception e) {
//
//        }
//    }
//
//    @AfterClass
//    public static void afterMethod() {
//        try {
//            Thread.sleep(15000);
//        } catch (Exception e) {
//
//        }
//    }

    @Rule
    public ActivityTestRule<Accounts> mActivityTestRule = new ActivityTestRule<>(Accounts.class);

    @Test
    public void k9EspressoTest() {

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.compose), withContentDescription("Compose"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.widget.ActionBarContainer")),
                                        0),
                                3),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction linearLayout = onView(
                allOf(withContentDescription("Compose, Navigate up"),
                        childAtPosition(
                                allOf(withClassName(is("com.android.internal.widget.ActionBarView")),
                                        childAtPosition(
                                                withClassName(is("com.android.internal.widget.ActionBarContainer")),
                                                0)),
                                0),
                        isDisplayed()));
        linearLayout.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

//        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());


        ViewInteraction actionMenuItemView2 = onView(
                allOf(withContentDescription("More options"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.widget.ActionBarContainer")),
                                        0),
                                4),
                        isDisplayed()));

        actionMenuItemView2.perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                Matcher<View> standardConstraint = isDisplayingAtLeast(50);
                return standardConstraint;
            }

            @Override
            public String getDescription() {
                return "click more option button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView = onView(
                allOf(withId(android.R.id.title), withText("Settings"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView2 = onView(
                allOf(withId(android.R.id.title), withText("Folder settings"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout2 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        linearLayout2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(0);
        checkedTextView.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout3 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(3);
        linearLayout3.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView2 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        checkedTextView2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout4 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(4);
        linearLayout4.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView3 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        checkedTextView3.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout5 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(5);
        linearLayout5.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView4 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(3);
        checkedTextView4.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout6 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(6);
        linearLayout6.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

//        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        actionMenuItemView2.perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                Matcher<View> standardConstraint = isDisplayingAtLeast(50);
                return standardConstraint;
            }

            @Override
            public String getDescription() {
                return "click more option button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView3 = onView(
                allOf(withId(android.R.id.title), withText("Settings"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView3.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView4 = onView(
                allOf(withId(android.R.id.title), withText("Account settings"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView4.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout7 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(0);
        linearLayout7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout8 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(2);
        linearLayout8.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction button7 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                2),
                        isDisplayed()));
        button7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout9 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        linearLayout9.perform(click());

        DataInteraction linearLayout10 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout10.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView5 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        checkedTextView5.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout11 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        linearLayout11.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout12 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout12.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView6 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        checkedTextView6.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout13 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(1);
        linearLayout13.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction button8 = onView(
                allOf(withId(android.R.id.button2), withText("Cancel"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                0),
                        isDisplayed()));
        button8.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout14 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(2);
        linearLayout14.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView7 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(7);
        checkedTextView7.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout15 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(5);
        linearLayout15.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout16 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(4);
        linearLayout16.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout17 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout17.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction button9 = onView(
                allOf(withId(android.R.id.button2), withText("Cancel"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                0),
                        isDisplayed()));
        button9.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout18 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(6);
        linearLayout18.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout19 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout19.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout20 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout20.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout21 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(5);
        linearLayout21.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout22 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(7);
        linearLayout22.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

//        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        actionMenuItemView2.perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                Matcher<View> standardConstraint = isDisplayingAtLeast(50);
                return standardConstraint;
            }

            @Override
            public String getDescription() {
                return "click more option button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView5 = onView(
                allOf(withId(android.R.id.title), withText("Settings"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView5.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView6 = onView(
                allOf(withId(android.R.id.title), withText("Global settings"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView6.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout23 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(0);
        linearLayout23.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout24 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(1);
        linearLayout24.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView8 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(9);
        checkedTextView8.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout25 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(4);
        linearLayout25.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView9 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        checkedTextView9.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout26 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(5);
        linearLayout26.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView10 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(0);
        checkedTextView10.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout27 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(7);
        linearLayout27.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout28 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        linearLayout28.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout29 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout29.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout30 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(1);
        linearLayout30.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout31 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(2);
        linearLayout31.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout32 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout32.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout33 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(2);
        linearLayout33.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction button10 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                2),
                        isDisplayed()));
        button10.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout34 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout34.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout35 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(4);
        linearLayout35.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout36 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout36.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction button11 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        0),
                                2),
                        isDisplayed()));
        button11.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        DataInteraction linearLayout37 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(5);
        linearLayout37.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction linearLayout38 = onData(anything())
                .inAdapterView(allOf(withId(android.R.id.list),
                        childAtPosition(
                                withId(android.R.id.list_container),
                                0)))
                .atPosition(0);
        linearLayout38.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        DataInteraction checkedTextView11 = onData(anything())
                .inAdapterView(allOf(withClassName(is("com.android.internal.app.AlertController$RecycleListView")),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                0)))
                .atPosition(1);
        checkedTextView11.perform(click());

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        pressBack();

//        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        actionMenuItemView2.perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                Matcher<View> standardConstraint = isDisplayingAtLeast(50);
                return standardConstraint;
            }

            @Override
            public String getDescription() {
                return "click more option button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });

        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }

        ViewInteraction textView7 = onView(
                allOf(withId(android.R.id.title), withText("Folders"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("com.android.internal.view.menu.ListMenuItemView")),
                                        0),
                                0),
                        isDisplayed()));
        textView7.perform(click());


        try {
            Thread.sleep(1000);
        } catch (Exception e) {

        }
//        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

//        actionMenuItemView2.perform(new ViewAction() {
//            @Override
//            public Matcher<View> getConstraints() {
//                Matcher<View> standardConstraint = isDisplayingAtLeast(50);
//                return standardConstraint;
//            }
//
//            @Override
//            public String getDescription() {
//                return "click more option button";
//            }
//
//            @Override
//            public void perform(UiController uiController, View view) {
//                view.performClick();
//            }
//        });

        ViewInteraction linearLayout39 = onView(
                allOf(withContentDescription("K-9 Mail, Navigate up"),
                        childAtPosition(
                                allOf(withClassName(is("com.android.internal.widget.ActionBarView")),
                                        childAtPosition(
                                                withClassName(is("com.android.internal.widget.ActionBarContainer")),
                                                0)),
                                0),
                        isDisplayed()));
        linearLayout39.perform(click());


    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
