Daily stats:
  Current start time: 2018-08-09-01-29-57
  Next min deadline: 2018-08-10-01-00-00
  Next max deadline: 2018-08-10-03-00-00
  Current daily steps:
    Discharge total time: 12h 52m 8s 700ms  (from 14 steps)
    Discharge screen on time: 12h 52m 8s 700ms  (from 14 steps)
  Daily from 2018-08-08-01-41-09 to 2018-08-09-01-29-57:
    Discharge total time: 12h 13m 30s 500ms  (from 26 steps)
    Discharge screen on time: 12h 13m 30s 500ms  (from 26 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 305ms (99.9%) realtime, 1m 8s 304ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 8s 374ms realtime, 1m 8s 374ms uptime
  Discharge: 8.74 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.74 mAh
  Start clock time: 2018-08-09-11-11-04
  Screen on: 1m 8s 305ms (100.0%) 0x, Interactive: 1m 8s 305ms (100.0%)
  Screen brightnesses:
    dim 1m 8s 305ms (100.0%)
  Total full wakelock time: 1s 918ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 305ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 305ms (100.0%) 
     Cellular Sleep time:  1m 6s 943ms (98.0%)
     Cellular Idle time:   739ms (1.1%)
     Cellular Rx time:     624ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 305ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 305ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 8s 305ms (100.0%) 
     WiFi Sleep time:  1m 7s 829ms (99.3%)
     WiFi Idle time:   469ms (0.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     8ms (0.0%)
     WiFi Battery drain: 0.000686mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 306ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.60, actual drain: 0
    Screen: 5.92 Excluded from smearing
    Uid u0a130: 0.744 ( cpu=0.744 ) Including smearing: 0.925 ( proportional=0.181 )
    Uid 1000: 0.498 ( cpu=0.496 sensor=0.00190 ) Excluded from smearing
    Uid 1041: 0.437 ( cpu=0.437 ) Excluded from smearing
    Uid 0: 0.420 ( cpu=0.420 ) Excluded from smearing
    Cell standby: 0.209 ( radio=0.209 ) Excluded from smearing
    Idle: 0.186 Excluded from smearing
    Uid u0a70: 0.0436 ( cpu=0.0434 sensor=0.000126 ) Including smearing: 0.0542 ( proportional=0.0106 )
    Uid 2000: 0.0287 ( cpu=0.0287 ) Excluded from smearing
    Uid u0a47: 0.0274 ( cpu=0.0274 sensor=0.0000379 ) Excluded from smearing
    Uid u0a22: 0.0265 ( cpu=0.0217 sensor=0.00474 ) Excluded from smearing
    Uid 1013: 0.0157 ( cpu=0.0157 ) Excluded from smearing
    Uid 1046: 0.0114 ( cpu=0.0114 ) Excluded from smearing
    Uid u0a61: 0.0103 ( cpu=0.0103 ) Including smearing: 0.0129 ( proportional=0.00252 )
    Uid 1036: 0.00884 ( cpu=0.00884 ) Excluded from smearing
    Uid u0a68: 0.00726 ( cpu=0.00726 ) Including smearing: 0.00903 ( proportional=0.00177 )
    Uid u0a35: 0.00216 ( cpu=0.00216 ) Including smearing: 0.00268 ( proportional=0.000526 )
    Uid u0a40: 0.00198 ( cpu=0.00198 ) Including smearing: 0.00246 ( proportional=0.000483 )
    Uid u0a127: 0.00192 ( cpu=0.00192 ) Including smearing: 0.00239 ( proportional=0.000468 )
    Wifi: 0.00151 ( cpu=0.000820 wifi=0.000686 ) Including smearing: 0.00187 ( proportional=0.000367 )
    Uid 1040: 0.00150 ( cpu=0.00150 ) Excluded from smearing
    Uid u0a25: 0.00108 ( cpu=0.00108 ) Including smearing: 0.00135 ( proportional=0.000264 )
    Uid u0a7: 0.000906 ( cpu=0.000906 ) Excluded from smearing
    Uid 1001: 0.000752 ( cpu=0.000752 ) Excluded from smearing
    Uid u0a114: 0.000668 ( cpu=0.000668 ) Including smearing: 0.000830 ( proportional=0.000163 )
    Bluetooth: 0.000146 ( cpu=0.000146 ) Including smearing: 0.000181 ( proportional=0.0000355 )
    Uid 1027: 0.000124 ( cpu=0.000124 ) Excluded from smearing
    Uid 1021: 0.0000841 ( cpu=0.0000841 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 158ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 289ms realtime (29 times)
    XO_shutdown.ADSP: 10s 612ms realtime (5 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 7 button, 35 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock NetworkStats realtime
    Audio: 4s 608ms realtime (34 times)
    Sensor 7: 1m 8s 293ms realtime (0 times)
    Fg Service for: 1m 8s 293ms 
    Total running: 1m 8s 293ms 
    Total cpu time: u=7s 493ms s=5s 430ms 
    Total cpu time per freq: 3950 350 160 160 70 50 40 100 50 50 50 30 1480 90 20 90 10 40 40 20 40 1990 90 260 130 40 50 50 60 0 40 30 10 50 30 0 30 50 10 30 80 110 60 20 50 10 30 50 20 20 10 10 2170
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 860ms usr + 940ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 20ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 3s 880ms usr + 1s 610ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 150ms usr + 360ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 170ms usr + 320ms krn ; 0ms fg
  u0a130:
    Wake lock *launch* realtime
    Wake lock *job*/com.nextcloud.client/com.evernote.android.job.JobRescheduleService realtime
    Wake lock WindowManager: 1s 918ms full (1 times) realtime
    TOTAL wake: 1s 918ms full realtime
    Job com.nextcloud.client/com.evernote.android.job.JobRescheduleService: 3s 186ms realtime (1 times)
    Job Completions com.nextcloud.client/com.evernote.android.job.JobRescheduleService: canceled(1x)
    Video: 1s 852ms realtime (1 times)
    Foreground activities: 1m 6s 705ms realtime (14 times)
    Foreground services: 584ms realtime (2 times)
    Top for: 1m 7s 216ms 
    Fg Service for: 584ms 
    Cached for: 38ms 
    Total running: 1m 7s 838ms 
    Total cpu time: u=15s 414ms s=2s 397ms 
    Total cpu time per freq: 70 20 10 0 40 30 70 30 40 40 30 10 450 90 30 80 30 20 50 0 20 1400 100 990 330 400 350 200 150 30 100 40 40 280 30 60 120 110 20 380 630 770 550 310 100 220 220 170 100 110 70 20 8220
    Proc com.nextcloud.client:
      CPU: 0ms usr + 0ms krn ; 17s 300ms fg
      1 starts
    Apk com.nextcloud.client:
      Service com.owncloud.android.files.services.FileUploader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.owncloud.android.services.OperationsService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.files.services.FileDownloader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.evernote.android.job.JobRescheduleService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.media.MediaService:
        Created for: 124ms uptime
        Starts: 1, launches: 1
      Service com.owncloud.android.services.observer.FileObserverService:
        Created for: 1m 7s 306ms uptime
        Starts: 1, launches: 1

