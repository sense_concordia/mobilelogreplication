Daily stats:
  Current start time: 2018-08-09-01-29-57
  Next min deadline: 2018-08-10-01-00-00
  Next max deadline: 2018-08-10-03-00-00
  Current daily steps:
    Discharge total time: 12h 52m 8s 700ms  (from 14 steps)
    Discharge screen on time: 12h 52m 8s 700ms  (from 14 steps)
  Daily from 2018-08-08-01-41-09 to 2018-08-09-01-29-57:
    Discharge total time: 12h 13m 30s 500ms  (from 26 steps)
    Discharge screen on time: 12h 13m 30s 500ms  (from 26 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 438ms (99.9%) realtime, 1m 8s 438ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 8s 512ms realtime, 1m 8s 512ms uptime
  Discharge: 8.60 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.60 mAh
  Start clock time: 2018-08-09-11-03-13
  Screen on: 1m 8s 438ms (100.0%) 0x, Interactive: 1m 8s 438ms (100.0%)
  Screen brightnesses:
    dim 1m 8s 438ms (100.0%)
  Total full wakelock time: 1s 904ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 438ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 438ms (100.0%) 
     Cellular Sleep time:  1m 6s 874ms (97.7%)
     Cellular Idle time:   885ms (1.3%)
     Cellular Rx time:     679ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 438ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 438ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 8s 438ms (100.0%) 
     WiFi Sleep time:  1m 7s 903ms (99.2%)
     WiFi Idle time:   527ms (0.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     9ms (0.0%)
     WiFi Battery drain: 0.000771mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 439ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.64, actual drain: 0-35.2
    Screen: 5.93 Excluded from smearing
    Uid u0a130: 0.747 ( cpu=0.747 ) Including smearing: 0.930 ( proportional=0.184 )
    Uid 1000: 0.498 ( cpu=0.496 sensor=0.00190 ) Excluded from smearing
    Uid 1041: 0.458 ( cpu=0.458 ) Excluded from smearing
    Uid 0: 0.427 ( cpu=0.427 ) Excluded from smearing
    Cell standby: 0.209 ( radio=0.209 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid u0a70: 0.0444 ( cpu=0.0443 sensor=0.000128 ) Including smearing: 0.0553 ( proportional=0.0109 )
    Uid 2000: 0.0290 ( cpu=0.0290 ) Excluded from smearing
    Uid u0a22: 0.0242 ( cpu=0.0194 sensor=0.00475 ) Excluded from smearing
    Uid u0a47: 0.0208 ( cpu=0.0208 sensor=0.0000380 ) Excluded from smearing
    Uid 1013: 0.0159 ( cpu=0.0159 ) Excluded from smearing
    Uid u0a61: 0.0145 ( cpu=0.0145 ) Including smearing: 0.0181 ( proportional=0.00358 )
    Uid 1046: 0.0121 ( cpu=0.0121 ) Excluded from smearing
    Uid 1036: 0.00853 ( cpu=0.00853 ) Excluded from smearing
    Uid u0a68: 0.00703 ( cpu=0.00703 ) Including smearing: 0.00876 ( proportional=0.00173 )
    Wifi: 0.00185 ( cpu=0.00108 wifi=0.000771 ) Including smearing: 0.00230 ( proportional=0.000455 )
    Uid u0a40: 0.00157 ( cpu=0.00157 ) Including smearing: 0.00195 ( proportional=0.000385 )
    Uid u0a127: 0.00142 ( cpu=0.00142 ) Including smearing: 0.00178 ( proportional=0.000350 )
    Uid 1040: 0.00127 ( cpu=0.00127 ) Excluded from smearing
    Uid u0a35: 0.00116 ( cpu=0.00116 ) Including smearing: 0.00145 ( proportional=0.000287 )
    Uid u0a25: 0.00108 ( cpu=0.00108 ) Including smearing: 0.00134 ( proportional=0.000265 )
    Uid u0a7: 0.000896 ( cpu=0.000896 ) Excluded from smearing
    Uid 1001: 0.000896 ( cpu=0.000896 ) Excluded from smearing
    Uid u0a114: 0.000549 ( cpu=0.000549 ) Including smearing: 0.000684 ( proportional=0.000135 )
    Bluetooth: 0.000211 ( cpu=0.000211 ) Including smearing: 0.000263 ( proportional=0.0000520 )
    Uid 1021: 0.000197 ( cpu=0.000197 ) Excluded from smearing
    Uid 1019: 0.000126 ( cpu=0.000126 ) Excluded from smearing
    Uid u0a120: 0.0000840 ( cpu=0.0000840 ) Including smearing: 0.000105 ( proportional=0.0000207 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 255ms realtime (5 times)
    XO_shutdown.MPSS: 1m 7s 341ms realtime (32 times)
    XO_shutdown.ADSP: 10s 712ms realtime (4 times)
    wlan.Active: 55ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 7 button, 35 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 670ms realtime (34 times)
    Sensor 7: 1m 8s 426ms realtime (0 times)
    Fg Service for: 1m 8s 427ms 
    Total running: 1m 8s 427ms 
    Total cpu time: u=7s 404ms s=5s 556ms 
    Total cpu time per freq: 3250 470 220 120 60 70 70 70 60 50 40 10 1850 90 60 60 20 20 30 40 30 2540 70 200 100 30 50 30 40 0 60 10 40 80 20 10 50 0 30 50 160 70 80 40 30 40 20 40 20 30 0 10 1990
    Proc android.hardware.memtrack@1.0-service:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 340ms usr + 1s 260ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 5s 620ms usr + 2s 740ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 210ms usr + 460ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 140ms usr + 280ms krn ; 0ms fg
  u0a130:
    Wake lock *launch* realtime
    Wake lock *job*/com.nextcloud.client/com.evernote.android.job.JobRescheduleService realtime
    Wake lock WindowManager: 1s 904ms full (1 times) realtime
    TOTAL wake: 1s 904ms full realtime
    Job com.nextcloud.client/com.evernote.android.job.JobRescheduleService: 3s 178ms realtime (1 times)
    Job Completions com.nextcloud.client/com.evernote.android.job.JobRescheduleService: canceled(1x)
    Video: 1s 834ms realtime (1 times)
    Foreground activities: 1m 6s 843ms realtime (14 times)
    Foreground services: 562ms realtime (2 times)
    Top for: 1m 7s 334ms 
    Fg Service for: 562ms 
    Cached for: 33ms 
    Total running: 1m 7s 929ms 
    Total cpu time: u=15s 460ms s=2s 387ms 
    Total cpu time per freq: 30 10 10 0 20 60 40 50 60 40 10 20 400 110 30 60 30 10 20 0 20 1500 100 1020 390 320 320 220 180 160 120 50 50 260 60 50 70 50 60 460 680 600 440 240 140 160 160 250 140 100 60 60 8260
    Proc com.nextcloud.client:
      CPU: 5s 430ms usr + 960ms krn ; 17s 350ms fg
      1 starts
    Apk com.nextcloud.client:
      Service com.owncloud.android.files.services.FileUploader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.owncloud.android.services.OperationsService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.files.services.FileDownloader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.evernote.android.job.JobRescheduleService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.media.MediaService:
        Created for: 137ms uptime
        Starts: 1, launches: 1
      Service com.owncloud.android.services.observer.FileObserverService:
        Created for: 1m 7s 416ms uptime
        Starts: 1, launches: 1

