Daily stats:
  Current start time: 2018-08-09-01-29-57
  Next min deadline: 2018-08-10-01-00-00
  Next max deadline: 2018-08-10-03-00-00
  Current daily steps:
    Discharge total time: 13h 11m 37s 700ms  (from 12 steps)
    Discharge screen on time: 13h 11m 37s 700ms  (from 12 steps)
  Daily from 2018-08-08-01-41-09 to 2018-08-09-01-29-57:
    Discharge total time: 12h 13m 30s 500ms  (from 26 steps)
    Discharge screen on time: 12h 13m 30s 500ms  (from 26 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 624ms (99.9%) realtime, 1m 8s 624ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 8s 702ms realtime, 1m 8s 702ms uptime
  Discharge: 9.01 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 9.01 mAh
  Start clock time: 2018-08-09-10-51-25
  Screen on: 1m 8s 624ms (100.0%) 0x, Interactive: 1m 8s 624ms (100.0%)
  Screen brightnesses:
    dim 1m 8s 624ms (100.0%)
  Total full wakelock time: 1s 909ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 624ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 624ms (100.0%) 
     Cellular Sleep time:  1m 7s 208ms (97.9%)
     Cellular Idle time:   759ms (1.1%)
     Cellular Rx time:     657ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 624ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 624ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 8s 624ms (100.0%) 
     WiFi Sleep time:  1m 8s 95ms (99.2%)
     WiFi Idle time:   521ms (0.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     8ms (0.0%)
     WiFi Battery drain: 0.000700mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 625ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.85, actual drain: 0
    Screen: 5.94 Excluded from smearing
    Uid u0a130: 0.750 ( cpu=0.750 ) Including smearing: 0.954 ( proportional=0.204 )
    Uid 1000: 0.512 ( cpu=0.510 sensor=0.00191 ) Excluded from smearing
    Uid 1041: 0.477 ( cpu=0.477 ) Excluded from smearing
    Uid 0: 0.423 ( cpu=0.423 ) Excluded from smearing
    Cell standby: 0.210 ( radio=0.210 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid u0a22: 0.110 ( cpu=0.105 sensor=0.00476 ) Excluded from smearing
    Uid 1036: 0.0811 ( cpu=0.0811 ) Excluded from smearing
    Uid u0a70: 0.0457 ( cpu=0.0455 sensor=0.000130 ) Including smearing: 0.0581 ( proportional=0.0125 )
    Uid 2000: 0.0308 ( cpu=0.0308 ) Excluded from smearing
    Uid u0a47: 0.0230 ( cpu=0.0229 sensor=0.0000381 ) Excluded from smearing
    Uid 1013: 0.0150 ( cpu=0.0150 ) Excluded from smearing
    Uid u0a61: 0.0141 ( cpu=0.0141 ) Including smearing: 0.0179 ( proportional=0.00384 )
    Uid 1046: 0.0121 ( cpu=0.0121 ) Excluded from smearing
    Uid u0a68: 0.00663 ( cpu=0.00663 ) Including smearing: 0.00844 ( proportional=0.00181 )
    Uid u0a35: 0.00203 ( cpu=0.00203 ) Including smearing: 0.00258 ( proportional=0.000553 )
    Uid u0a40: 0.00193 ( cpu=0.00193 ) Including smearing: 0.00246 ( proportional=0.000527 )
    Uid u0a127: 0.00178 ( cpu=0.00178 ) Including smearing: 0.00226 ( proportional=0.000485 )
    Wifi: 0.00138 ( cpu=0.000680 wifi=0.000700 ) Including smearing: 0.00176 ( proportional=0.000376 )
    Uid 1040: 0.00125 ( cpu=0.00125 ) Excluded from smearing
    Uid u0a7: 0.000862 ( cpu=0.000862 ) Excluded from smearing
    Uid 1001: 0.000711 ( cpu=0.000711 ) Excluded from smearing
    Uid u0a25: 0.000667 ( cpu=0.000667 ) Including smearing: 0.000848 ( proportional=0.000182 )
    Uid u0a114: 0.000333 ( cpu=0.000333 ) Including smearing: 0.000423 ( proportional=0.0000907 )
    Uid 1021: 0.000195 ( cpu=0.000195 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 486ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 639ms realtime (28 times)
    XO_shutdown.ADSP: 10s 654ms realtime (4 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 7 button, 35 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 746ms realtime (34 times)
    Sensor 7: 1m 8s 612ms realtime (0 times)
    Fg Service for: 1m 8s 612ms 
    Total running: 1m 8s 612ms 
    Total cpu time: u=7s 250ms s=6s 123ms 
    Total cpu time per freq: 3280 550 150 90 90 50 40 40 30 50 20 30 1690 90 70 80 80 50 40 50 40 2160 170 90 120 70 90 70 60 40 80 20 10 100 20 10 30 70 10 60 80 80 130 50 50 50 20 30 80 10 20 0 1920
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 310ms usr + 620ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 1s 900ms usr + 1s 40ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 80ms usr + 240ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 200ms usr + 290ms krn ; 0ms fg
  u0a130:
    Wake lock *launch* realtime
    Wake lock *job*/com.nextcloud.client/com.evernote.android.job.JobRescheduleService realtime
    Wake lock WindowManager: 1s 909ms full (1 times) realtime
    TOTAL wake: 1s 909ms full realtime
    Job com.nextcloud.client/com.evernote.android.job.JobRescheduleService: 3s 185ms realtime (1 times)
    Job Completions com.nextcloud.client/com.evernote.android.job.JobRescheduleService: canceled(1x)
    Video: 1s 832ms realtime (1 times)
    Foreground activities: 1m 7s 27ms realtime (14 times)
    Foreground services: 548ms realtime (2 times)
    Top for: 1m 7s 516ms 
    Fg Service for: 548ms 
    Cached for: 38ms 
    Total running: 1m 8s 102ms 
    Total cpu time: u=15s 370ms s=2s 603ms 
    Total cpu time per freq: 40 0 10 10 20 30 40 30 30 40 30 20 390 70 70 40 70 30 20 10 20 1430 100 1060 410 410 330 280 180 130 110 50 70 320 20 100 50 110 30 380 660 700 460 250 150 120 220 220 160 110 30 20 8080
    Proc com.nextcloud.client:
      CPU: 0ms usr + 0ms krn ; 17s 500ms fg
      1 starts
    Apk com.nextcloud.client:
      Service com.owncloud.android.files.services.FileUploader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.owncloud.android.services.OperationsService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.files.services.FileDownloader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.evernote.android.job.JobRescheduleService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.media.MediaService:
        Created for: 146ms uptime
        Starts: 1, launches: 1
      Service com.owncloud.android.services.observer.FileObserverService:
        Created for: 1m 7s 592ms uptime
        Starts: 1, launches: 1

