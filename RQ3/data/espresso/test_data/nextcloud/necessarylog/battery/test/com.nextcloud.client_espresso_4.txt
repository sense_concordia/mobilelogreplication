Daily stats:
  Current start time: 2018-08-09-01-29-57
  Next min deadline: 2018-08-10-01-00-00
  Next max deadline: 2018-08-10-03-00-00
  Current daily steps:
    Discharge total time: 12h 0m 7s 100ms  (from 40 steps)
    Discharge screen on time: 12h 0m 7s 100ms  (from 40 steps)
    Charge total time: 11h 35m 23s 500ms  (from 7 steps)
    Charge screen on time: 11h 35m 23s 500ms  (from 7 steps)
  Daily from 2018-08-08-01-41-09 to 2018-08-09-01-29-57:
    Discharge total time: 12h 13m 30s 500ms  (from 26 steps)
    Discharge screen on time: 12h 13m 30s 500ms  (from 26 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 570ms (99.9%) realtime, 1m 8s 570ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 8s 663ms realtime, 1m 8s 663ms uptime
  Discharge: 7.40 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 7.40 mAh
  Start clock time: 2018-08-09-15-17-58
  Screen on: 1m 8s 570ms (100.0%) 0x, Interactive: 1m 8s 570ms (100.0%)
  Screen brightnesses:
    dim 1m 8s 570ms (100.0%)
  Total full wakelock time: 1s 947ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 570ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 570ms (100.0%) 
     Cellular Sleep time:  1m 7s 670ms (98.7%)
     Cellular Idle time:   249ms (0.4%)
     Cellular Rx time:     652ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 570ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 570ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 8s 570ms (100.0%) 
     WiFi Sleep time:  1m 8s 571ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 571ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.48, actual drain: 0
    Screen: 5.94 Excluded from smearing
    Uid u0a130: 0.744 ( cpu=0.744 ) Including smearing: 0.916 ( proportional=0.171 )
    Uid 1041: 0.465 ( cpu=0.465 ) Excluded from smearing
    Uid 1000: 0.421 ( cpu=0.419 sensor=0.00190 ) Excluded from smearing
    Uid 0: 0.386 ( cpu=0.386 ) Excluded from smearing
    Cell standby: 0.210 ( radio=0.210 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid 2000: 0.0310 ( cpu=0.0310 ) Excluded from smearing
    Uid u0a47: 0.0215 ( cpu=0.0215 sensor=0.0000381 ) Excluded from smearing
    Uid 1013: 0.0158 ( cpu=0.0158 ) Excluded from smearing
    Uid 1046: 0.0115 ( cpu=0.0115 ) Excluded from smearing
    Uid u0a22: 0.0107 ( cpu=0.00595 sensor=0.00476 ) Excluded from smearing
    Uid 1036: 0.00952 ( cpu=0.00952 ) Excluded from smearing
    Uid u0a68: 0.00670 ( cpu=0.00670 ) Including smearing: 0.00824 ( proportional=0.00154 )
    Uid u0a35: 0.00610 ( cpu=0.00610 ) Including smearing: 0.00750 ( proportional=0.00140 )
    Uid u0a127: 0.00462 ( cpu=0.00462 ) Including smearing: 0.00568 ( proportional=0.00106 )
    Uid u0a114: 0.00191 ( cpu=0.00191 ) Including smearing: 0.00235 ( proportional=0.000440 )
    Uid 1040: 0.00150 ( cpu=0.00150 ) Excluded from smearing
    Uid u0a7: 0.00115 ( cpu=0.00115 ) Excluded from smearing
    Bluetooth: 0.000665 ( cpu=0.000665 ) Including smearing: 0.000818 ( proportional=0.000153 )
    Uid 1001: 0.000621 ( cpu=0.000621 ) Excluded from smearing
    Wifi: 0.000533 ( cpu=0.000533 ) Including smearing: 0.000656 ( proportional=0.000123 )
    Uid u0a40: 0.000332 ( cpu=0.000332 ) Including smearing: 0.000408 ( proportional=0.0000763 )
    Uid u0a25: 0.000290 ( cpu=0.000290 ) Including smearing: 0.000357 ( proportional=0.0000668 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 417ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 469ms realtime (28 times)
    XO_shutdown.ADSP: 10s 785ms realtime (4 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 7 button, 35 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 654ms realtime (34 times)
    Sensor 7: 1m 8s 555ms realtime (0 times)
    Fg Service for: 1m 8s 556ms 
    Total running: 1m 8s 556ms 
    Total cpu time: u=6s 170ms s=4s 20ms 
    Total cpu time per freq: 1460 190 210 80 30 70 60 30 30 90 70 30 2000 90 100 110 30 40 50 40 70 2060 30 60 40 30 40 20 30 40 30 20 30 20 10 10 10 60 40 60 60 60 90 30 20 10 70 40 30 30 0 10 1870
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 230ms usr + 1s 160ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 4s 970ms usr + 1s 980ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 150ms usr + 290ms krn ; 0ms fg
  u0a130:
    Wake lock *launch* realtime
    Wake lock *job*/com.nextcloud.client/com.evernote.android.job.JobRescheduleService realtime
    Wake lock WindowManager: 1s 947ms full (1 times) realtime
    TOTAL wake: 1s 947ms full realtime
    Job com.nextcloud.client/com.evernote.android.job.JobRescheduleService: 3s 204ms realtime (1 times)
    Job Completions com.nextcloud.client/com.evernote.android.job.JobRescheduleService: canceled(1x)
    Video: 1s 872ms realtime (1 times)
    Foreground activities: 1m 6s 788ms realtime (14 times)
    Foreground services: 637ms realtime (2 times)
    Top for: 1m 7s 306ms 
    Fg Service for: 637ms 
    Cached for: 39ms 
    Total running: 1m 7s 982ms 
    Total cpu time: u=15s 337ms s=2s 550ms 
    Total cpu time per freq: 60 0 10 10 30 30 60 40 30 40 60 30 390 50 50 30 10 40 20 20 30 1360 90 1090 390 300 320 230 150 70 100 50 50 350 50 50 10 70 60 490 500 680 500 260 70 80 280 250 210 190 20 40 8190
    Proc com.nextcloud.client:
      CPU: 0ms usr + 0ms krn ; 17s 370ms fg
      1 starts
    Apk com.nextcloud.client:
      Service com.owncloud.android.files.services.FileUploader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.owncloud.android.services.OperationsService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.files.services.FileDownloader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.evernote.android.job.JobRescheduleService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.media.MediaService:
        Created for: 132ms uptime
        Starts: 1, launches: 1
      Service com.owncloud.android.services.observer.FileObserverService:
        Created for: 1m 7s 507ms uptime
        Starts: 1, launches: 1

