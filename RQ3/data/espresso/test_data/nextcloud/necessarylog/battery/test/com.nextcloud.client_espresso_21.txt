Daily stats:
  Current start time: 2018-08-09-01-29-57
  Next min deadline: 2018-08-10-01-00-00
  Next max deadline: 2018-08-10-03-00-00
  Current daily steps:
    Discharge total time: 12h 1m 40s 900ms  (from 43 steps)
    Discharge screen on time: 12h 1m 40s 900ms  (from 43 steps)
    Charge total time: 11h 35m 23s 500ms  (from 7 steps)
    Charge screen on time: 11h 35m 23s 500ms  (from 7 steps)
  Daily from 2018-08-08-01-41-09 to 2018-08-09-01-29-57:
    Discharge total time: 12h 13m 30s 500ms  (from 26 steps)
    Discharge screen on time: 12h 13m 30s 500ms  (from 26 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 488ms (99.9%) realtime, 1m 8s 488ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 8s 573ms realtime, 1m 8s 573ms uptime
  Discharge: 12.6 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 12.6 mAh
  Start clock time: 2018-08-09-15-40-16
  Screen on: 1m 8s 488ms (100.0%) 0x, Interactive: 1m 8s 488ms (100.0%)
  Screen brightnesses:
    dim 1m 8s 488ms (100.0%)
  Total full wakelock time: 1s 924ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 488ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 488ms (100.0%) 
     Cellular Sleep time:  1m 7s 579ms (98.7%)
     Cellular Idle time:   235ms (0.3%)
     Cellular Rx time:     675ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 488ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 488ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 8s 488ms (100.0%) 
     WiFi Sleep time:  1m 8s 489ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 489ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.51, actual drain: 0
    Screen: 5.93 Excluded from smearing
    Uid u0a130: 0.749 ( cpu=0.749 ) Including smearing: 0.925 ( proportional=0.176 )
    Uid 1041: 0.483 ( cpu=0.483 ) Excluded from smearing
    Uid 1000: 0.435 ( cpu=0.433 sensor=0.00190 ) Excluded from smearing
    Uid 0: 0.385 ( cpu=0.385 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.209 ( radio=0.209 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid 2000: 0.0324 ( cpu=0.0324 ) Excluded from smearing
    Uid u0a47: 0.0248 ( cpu=0.0248 sensor=0.0000380 ) Excluded from smearing
    Uid 1013: 0.0158 ( cpu=0.0158 ) Excluded from smearing
    Uid 1046: 0.0122 ( cpu=0.0122 ) Excluded from smearing
    Uid u0a22: 0.0102 ( cpu=0.00549 sensor=0.00475 ) Excluded from smearing
    Uid 1036: 0.00941 ( cpu=0.00941 ) Excluded from smearing
    Uid u0a68: 0.00688 ( cpu=0.00688 ) Including smearing: 0.00850 ( proportional=0.00162 )
    Uid u0a127: 0.00497 ( cpu=0.00497 ) Including smearing: 0.00614 ( proportional=0.00117 )
    Uid u0a61: 0.00277 ( cpu=0.00277 ) Including smearing: 0.00343 ( proportional=0.000652 )
    Uid u0a114: 0.00156 ( cpu=0.00156 ) Including smearing: 0.00193 ( proportional=0.000367 )
    Uid 1040: 0.00134 ( cpu=0.00134 ) Excluded from smearing
    Uid u0a35: 0.00103 ( cpu=0.00103 ) Including smearing: 0.00127 ( proportional=0.000243 )
    Uid u0a7: 0.000779 ( cpu=0.000779 ) Excluded from smearing
    Uid 1001: 0.000615 ( cpu=0.000615 ) Excluded from smearing
    Uid u0a25: 0.000419 ( cpu=0.000419 ) Including smearing: 0.000517 ( proportional=0.0000985 )
    Uid u0a40: 0.000293 ( cpu=0.000293 ) Including smearing: 0.000362 ( proportional=0.0000689 )
    Wifi: 0.000279 ( cpu=0.000279 ) Including smearing: 0.000344 ( proportional=0.0000656 )
    Bluetooth: 0.0000854 ( cpu=0.0000854 ) Including smearing: 0.000105 ( proportional=0.0000201 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 331ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 432ms realtime (28 times)
    XO_shutdown.ADSP: 10s 865ms realtime (4 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 7 button, 35 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 669ms realtime (34 times)
    Sensor 7: 1m 8s 478ms realtime (0 times)
    Fg Service for: 1m 8s 478ms 
    Total running: 1m 8s 478ms 
    Total cpu time: u=6s 520ms s=4s 30ms 
    Total cpu time per freq: 1470 210 100 70 80 70 50 60 40 80 80 20 2370 80 80 80 50 60 40 80 30 1730 40 70 50 30 40 30 10 10 30 10 40 20 10 50 30 20 20 80 110 60 60 50 50 20 50 40 30 10 10 20 1820
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 690ms usr + 840ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 3s 270ms usr + 1s 630ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 70ms usr + 220ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a130:
    Wake lock *launch* realtime
    Wake lock *job*/com.nextcloud.client/com.evernote.android.job.JobRescheduleService realtime
    Wake lock WindowManager: 1s 924ms full (1 times) realtime
    TOTAL wake: 1s 924ms full realtime
    Job com.nextcloud.client/com.evernote.android.job.JobRescheduleService: 3s 210ms realtime (1 times)
    Job Completions com.nextcloud.client/com.evernote.android.job.JobRescheduleService: canceled(1x)
    Video: 1s 859ms realtime (1 times)
    Foreground activities: 1m 6s 771ms realtime (14 times)
    Foreground services: 600ms realtime (2 times)
    Top for: 1m 7s 288ms 
    Fg Service for: 600ms 
    Cached for: 55ms 
    Total running: 1m 7s 943ms 
    Total cpu time: u=15s 367ms s=2s 470ms 
    Total cpu time per freq: 30 20 0 20 50 40 20 40 50 40 50 10 350 40 40 60 50 20 30 40 10 1480 130 1080 430 330 340 280 140 100 50 130 30 240 0 40 50 70 100 530 810 700 480 300 300 130 200 130 80 180 90 10 8390
    Proc com.nextcloud.client:
      CPU: 0ms usr + 0ms krn ; 17s 300ms fg
      1 starts
    Apk com.nextcloud.client:
      Service com.owncloud.android.files.services.FileUploader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.owncloud.android.services.OperationsService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.files.services.FileDownloader:
        Created for: 0ms uptime
        Starts: 0, launches: 2
      Service com.evernote.android.job.JobRescheduleService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
      Service com.owncloud.android.media.MediaService:
        Created for: 116ms uptime
        Starts: 1, launches: 1
      Service com.owncloud.android.services.observer.FileObserverService:
        Created for: 1m 7s 377ms uptime
        Starts: 1, launches: 1

