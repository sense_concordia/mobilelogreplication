Daily stats:
  Current start time: 2018-10-31-03-05-14
  Next min deadline: 2018-11-01-01-00-00
  Next max deadline: 2018-11-01-03-00-00
  Current daily steps:
    Discharge total time: 19h 2m 58s 200ms  (from 9 steps)
    Discharge screen on time: 19h 2m 58s 200ms  (from 9 steps)
    Charge total time: 11h 11m 2s 700ms  (from 18 steps)
    Charge screen off time: 8h 20m 0s 0ms  (from 4 steps)
    Charge screen on time: 11h 53m 45s 200ms  (from 13 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 27s 850ms (99.8%) realtime, 1m 27s 850ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 28s 7ms realtime, 1m 28s 7ms uptime
  Discharge: 3.23 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 3.23 mAh
  Start clock time: 2018-10-31-10-45-53
  Screen on: 1m 27s 850ms (100.0%) 0x, Interactive: 1m 27s 850ms (100.0%)
  Screen brightnesses:
    dim 1m 27s 850ms (100.0%)
  Total full wakelock time: 2s 2ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 27s 850ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 27s 850ms (100.0%) 
     Cellular Sleep time:  1m 27s 279ms (99.4%)
     Cellular Idle time:   244ms (0.3%)
     Cellular Rx time:     327ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 27s 850ms (100.0%) 
     Wifi supplicant states:
       completed 1m 27s 850ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 27s 850ms (100.0%) 
     WiFi Sleep time:  1m 27s 850ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 27s 850ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.91, actual drain: 0
    Screen: 7.61 Excluded from smearing
    Uid 1000: 0.382 ( cpu=0.379 sensor=0.00244 ) Excluded from smearing
    Cell standby: 0.268 ( radio=0.268 ) Excluded from smearing
    Uid 0: 0.245 ( cpu=0.245 wake=0.00000079 ) Excluded from smearing
    Idle: 0.239 Excluded from smearing
    Uid 1036: 0.0401 ( cpu=0.0401 ) Excluded from smearing
    Uid 2000: 0.0307 ( cpu=0.0307 ) Excluded from smearing
    Uid u0a68: 0.0244 ( cpu=0.0244 ) Including smearing: 0.0275 ( proportional=0.00310 )
    Uid u0a225: 0.0225 ( cpu=0.0225 ) Including smearing: 0.0254 ( proportional=0.00286 )
    Uid u0a47: 0.0182 ( cpu=0.0181 sensor=0.0000488 ) Excluded from smearing
    Uid u0a22: 0.0171 ( cpu=0.0171 ) Excluded from smearing
    Uid u0a127: 0.00441 ( cpu=0.00441 ) Including smearing: 0.00497 ( proportional=0.000559 )
    Uid u0a35: 0.00145 ( cpu=0.00145 ) Including smearing: 0.00164 ( proportional=0.000184 )
    Uid u0a25: 0.000953 ( cpu=0.000953 ) Including smearing: 0.00107 ( proportional=0.000121 )
    Uid u0a90: 0.000775 ( cpu=0.000775 ) Including smearing: 0.000873 ( proportional=0.0000983 )
    Uid u0a114: 0.000580 ( cpu=0.000580 ) Including smearing: 0.000653 ( proportional=0.0000735 )
    Bluetooth: 0.000580 ( cpu=0.000580 ) Including smearing: 0.000653 ( proportional=0.0000735 )
    Uid 1013: 0.000538 ( cpu=0.000538 ) Excluded from smearing
    Uid 1001: 0.000509 ( cpu=0.000509 ) Excluded from smearing
    Uid u0a40: 0.000248 ( cpu=0.000248 ) Including smearing: 0.000279 ( proportional=0.0000314 )
    Wifi: 0.000229 ( cpu=0.000229 ) Including smearing: 0.000258 ( proportional=0.0000290 )
    Uid 1053: 0.000123 ( cpu=0.000123 ) Excluded from smearing
    Uid u0a75: 0.000123 ( cpu=0.000123 ) Including smearing: 0.000139 ( proportional=0.0000156 )
    Uid u0a116: 0.000123 ( cpu=0.000123 ) Including smearing: 0.000139 ( proportional=0.0000156 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 27s 709ms realtime (2 times)
    XO_shutdown.MPSS: 1m 27s 78ms realtime (37 times)
    XO_shutdown.ADSP: 1m 27s 725ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 55 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 27s 824ms realtime (0 times)
    Fg Service for: 1m 27s 824ms 
    Total running: 1m 27s 824ms 
    Total cpu time: u=5s 904ms s=3s 307ms 
    Total cpu time per freq: 30 0 0 0 170 210 140 170 60 160 90 70 180 80 90 80 20 10 30 100 10 4150 20 60 40 30 10 10 30 10 30 0 20 110 10 0 10 0 30 10 100 110 70 60 50 0 50 20 20 30 0 0 2030
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 30ms usr + 60ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 30ms usr + 1s 10ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 5s 270ms usr + 2s 210ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 130ms usr + 230ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 20ms usr + 20ms krn ; 0ms fg
  u0a225:
    Wake lock *launch* realtime
    Wake lock WindowManager: 2s 2ms full (1 times) realtime
    TOTAL wake: 2s 2ms full realtime
    Foreground activities: 1m 26s 656ms realtime (21 times)
    Foreground services: 255ms realtime (2 times)
    Top for: 1m 27s 68ms 
    Fg Service for: 255ms 
    Cached for: 61ms 
    Total running: 1m 27s 384ms 
    Total cpu time: u=24s 86ms s=2s 930ms 
    Total cpu time per freq: 0 0 0 0 40 120 50 90 60 170 210 200 300 260 170 130 70 80 100 70 50 4460 1460 3110 1420 1120 850 860 520 330 190 230 210 2880 40 60 100 100 30 1200 1360 1990 1490 1240 770 560 560 360 230 320 60 150 23190
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc com.ichi2.anki:
      CPU: 0ms usr + 0ms krn ; 17s 510ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 243ms uptime
        Starts: 1, launches: 1

