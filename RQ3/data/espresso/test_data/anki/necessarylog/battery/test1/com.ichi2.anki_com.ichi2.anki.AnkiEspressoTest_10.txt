Daily stats:
  Current start time: 2018-10-31-03-05-14
  Next min deadline: 2018-11-01-01-00-00
  Next max deadline: 2018-11-01-03-00-00
  Current daily steps:
    Discharge total time: 18h 59m 31s 700ms  (from 6 steps)
    Discharge screen on time: 18h 59m 31s 700ms  (from 6 steps)
    Charge total time: 11h 11m 2s 700ms  (from 18 steps)
    Charge screen off time: 8h 20m 0s 0ms  (from 4 steps)
    Charge screen on time: 11h 53m 45s 200ms  (from 13 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 27s 519ms (99.8%) realtime, 1m 27s 519ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 27s 692ms realtime, 1m 27s 693ms uptime
  Discharge: 6.28 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 6.28 mAh
  Start clock time: 2018-10-31-10-13-14
  Screen on: 1m 27s 519ms (100.0%) 0x, Interactive: 1m 27s 519ms (100.0%)
  Screen brightnesses:
    dim 1m 27s 519ms (100.0%)
  Total full wakelock time: 1s 983ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 27s 519ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 27s 519ms (100.0%) 
     Cellular Sleep time:  1m 26s 931ms (99.3%)
     Cellular Idle time:   231ms (0.3%)
     Cellular Rx time:     358ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 27s 519ms (100.0%) 
     Wifi supplicant states:
       completed 1m 27s 519ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 27s 519ms (100.0%) 
     WiFi Sleep time:  1m 27s 520ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 27s 520ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.87, actual drain: 0
    Screen: 7.58 Excluded from smearing
    Uid 1000: 0.378 ( cpu=0.375 sensor=0.00243 ) Excluded from smearing
    Cell standby: 0.267 ( radio=0.267 ) Excluded from smearing
    Uid 0: 0.242 ( cpu=0.242 ) Excluded from smearing
    Idle: 0.239 Excluded from smearing
    Uid 2000: 0.0433 ( cpu=0.0433 ) Excluded from smearing
    Uid u0a68: 0.0248 ( cpu=0.0248 ) Including smearing: 0.0279 ( proportional=0.00310 )
    Uid 1036: 0.0234 ( cpu=0.0234 ) Excluded from smearing
    Uid u0a225: 0.0219 ( cpu=0.0219 ) Including smearing: 0.0247 ( proportional=0.00275 )
    Uid u0a47: 0.0190 ( cpu=0.0189 sensor=0.0000486 ) Excluded from smearing
    Uid u0a22: 0.0132 ( cpu=0.0132 ) Excluded from smearing
    Uid u0a35: 0.00584 ( cpu=0.00584 ) Including smearing: 0.00657 ( proportional=0.000732 )
    Uid u0a127: 0.00462 ( cpu=0.00462 ) Including smearing: 0.00520 ( proportional=0.000579 )
    Uid 1001: 0.000527 ( cpu=0.000527 ) Excluded from smearing
    Wifi: 0.000422 ( cpu=0.000422 ) Including smearing: 0.000475 ( proportional=0.0000529 )
    Uid u0a25: 0.000413 ( cpu=0.000413 ) Including smearing: 0.000465 ( proportional=0.0000518 )
    Uid 1013: 0.000289 ( cpu=0.000289 ) Excluded from smearing
    Uid 1053: 0.000289 ( cpu=0.000289 ) Excluded from smearing
    Uid u0a114: 0.000249 ( cpu=0.000249 ) Including smearing: 0.000280 ( proportional=0.0000312 )
    Uid u0a40: 0.000123 ( cpu=0.000123 ) Including smearing: 0.000139 ( proportional=0.0000154 )
    Uid u0a75: 0.000123 ( cpu=0.000123 ) Including smearing: 0.000139 ( proportional=0.0000154 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 27s 384ms realtime (2 times)
    XO_shutdown.MPSS: 1m 26s 793ms realtime (36 times)
    XO_shutdown.ADSP: 1m 27s 421ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 55 touch
    Wake lock *alarm* realtime
    Wake lock NetworkStats realtime
    Sensor 7: 1m 27s 495ms realtime (0 times)
    Fg Service for: 1m 27s 495ms 
    Total running: 1m 27s 495ms 
    Total cpu time: u=5s 787ms s=3s 333ms 
    Total cpu time per freq: 20 0 0 0 190 190 210 140 50 80 90 110 170 100 90 120 40 40 90 10 20 4000 10 60 60 40 40 0 10 50 0 30 10 90 30 20 0 40 0 50 110 90 90 40 50 10 20 40 10 20 0 10 1570
    Proc servicemanager:
      CPU: 20ms usr + 30ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 500ms usr + 490ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 2s 480ms usr + 890ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 50ms usr + 100ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a225:
    Wake lock *launch* realtime
    Wake lock WindowManager: 1s 983ms full (1 times) realtime
    TOTAL wake: 1s 983ms full realtime
    Foreground activities: 1m 26s 311ms realtime (21 times)
    Foreground services: 257ms realtime (2 times)
    Top for: 1m 26s 728ms 
    Fg Service for: 257ms 
    Cached for: 68ms 
    Total running: 1m 27s 53ms 
    Total cpu time: u=23s 927ms s=3s 177ms 
    Total cpu time per freq: 0 0 0 0 190 110 60 60 60 120 250 200 280 180 160 170 90 130 90 50 60 4610 1660 3250 1590 1050 870 570 500 380 160 70 100 2740 90 120 90 10 120 1140 1500 2320 1620 1180 520 580 520 420 420 230 50 20 23360
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc com.ichi2.anki:
      CPU: 0ms usr + 0ms krn ; 17s 580ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 261ms uptime
        Starts: 1, launches: 1

