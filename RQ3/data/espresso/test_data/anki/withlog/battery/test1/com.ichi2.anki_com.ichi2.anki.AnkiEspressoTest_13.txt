Daily stats:
  Current start time: 2018-10-30-03-04-08
  Next min deadline: 2018-10-31-01-00-00
  Next max deadline: 2018-10-31-03-00-00
  Current daily steps:
    Discharge total time: 16h 17m 6s 0ms  (from 51 steps)
    Discharge screen on time: 15h 12m 38s 300ms  (from 50 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 27s 719ms (99.9%) realtime, 1m 27s 719ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 27s 791ms realtime, 1m 27s 790ms uptime
  Discharge: 7.10 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 7.10 mAh
  Start clock time: 2018-10-30-15-06-47
  Screen on: 1m 27s 719ms (100.0%) 0x, Interactive: 1m 27s 719ms (100.0%)
  Screen brightnesses:
    dark 11s 947ms (13.6%)
    dim 1m 15s 772ms (86.4%)
  Total full wakelock time: 2s 1ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 27s 719ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 27s 719ms (100.0%) 
     Cellular Sleep time:  1m 26s 444ms (98.5%)
     Cellular Idle time:   947ms (1.1%)
     Cellular Rx time:     328ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 27s 718ms (100.0%) 
     Wifi supplicant states:
       completed 1m 27s 718ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 27s 718ms (100.0%) 
     WiFi Sleep time:  1m 27s 37ms (99.2%)
     WiFi Idle time:   670ms (0.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     12ms (0.0%)
     WiFi Battery drain: 0.00102mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 27s 719ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.14, actual drain: 0-35.2
    Screen: 7.24 Excluded from smearing
    Uid 1000: 0.618 ( cpu=0.615 sensor=0.00244 ) Excluded from smearing
    Uid 0: 0.375 ( cpu=0.375 ) Excluded from smearing
    Cell standby: 0.268 ( radio=0.268 ) Excluded from smearing
    Idle: 0.239 Excluded from smearing
    Uid u0a61: 0.173 ( cpu=0.173 wifi=0.000139 ) Including smearing: 0.204 ( proportional=0.0310 )
    Uid u0a22: 0.0410 ( cpu=0.0349 wifi=0.0000222 sensor=0.00609 ) Excluded from smearing
    Uid 2000: 0.0378 ( cpu=0.0378 ) Excluded from smearing
    Uid u0a47: 0.0321 ( cpu=0.0320 sensor=0.0000487 ) Excluded from smearing
    Uid u0a70: 0.0307 ( cpu=0.0306 sensor=0.000114 ) Including smearing: 0.0362 ( proportional=0.00550 )
    Uid u0a225: 0.0236 ( cpu=0.0236 ) Including smearing: 0.0278 ( proportional=0.00422 )
    Uid u0a68: 0.0228 ( cpu=0.0228 ) Including smearing: 0.0269 ( proportional=0.00409 )
    Uid 1036: 0.0137 ( cpu=0.0137 ) Excluded from smearing
    Uid u0a35: 0.00490 ( cpu=0.00490 ) Including smearing: 0.00578 ( proportional=0.000877 )
    Uid u0a40: 0.00368 ( cpu=0.00368 ) Including smearing: 0.00433 ( proportional=0.000658 )
    Uid u0a25: 0.00203 ( cpu=0.00203 ) Including smearing: 0.00239 ( proportional=0.000363 )
    Wifi: 0.00188 ( cpu=0.00102 wifi=0.000858 ) Including smearing: 0.00222 ( proportional=0.000337 )
    Uid 1001: 0.00119 ( cpu=0.00119 ) Excluded from smearing
    Uid u0a108: 0.000955 ( cpu=0.000955 ) Including smearing: 0.00113 ( proportional=0.000171 )
    Uid u0a114: 0.000600 ( cpu=0.000600 ) Including smearing: 0.000707 ( proportional=0.000107 )
    Uid u0a127: 0.000412 ( cpu=0.000412 ) Including smearing: 0.000485 ( proportional=0.0000737 )
    Uid 1041: 0.000409 ( cpu=0.000409 ) Excluded from smearing
    Uid u0a75: 0.000251 ( cpu=0.000251 ) Including smearing: 0.000296 ( proportional=0.0000450 )
    Uid 1013: 0.000238 ( cpu=0.000238 ) Excluded from smearing
    Bluetooth: 0.000236 ( cpu=0.000236 ) Including smearing: 0.000278 ( proportional=0.0000421 )
    Uid 1053: 0.000158 ( cpu=0.000158 ) Excluded from smearing
    Uid u0a116: 0.000125 ( cpu=0.000125 ) Including smearing: 0.000147 ( proportional=0.0000224 )
    Uid u0a8: 0.000118 ( cpu=0.000118 ) Including smearing: 0.000140 ( proportional=0.0000212 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 27s 460ms realtime (3 times)
    XO_shutdown.MPSS: 1m 26s 860ms realtime (39 times)
    XO_shutdown.ADSP: 1m 25s 745ms realtime (2 times)
    wlan.Active: 45ms realtime (3 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 55 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 27s 704ms realtime (0 times)
    Fg Service for: 1m 27s 705ms 
    Total running: 1m 27s 705ms 
    Total cpu time: u=9s 370ms s=6s 57ms 
    Total cpu time per freq: 0 0 0 0 0 500 1090 790 330 270 280 190 320 130 90 110 70 30 40 20 90 4630 40 70 120 50 40 20 10 20 20 10 30 100 0 10 20 0 20 20 110 130 110 100 40 20 20 60 0 20 0 10 2320
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 40ms usr + 60ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 920ms usr + 950ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 30ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 5s 490ms usr + 1s 800ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 120ms usr + 210ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 150ms usr + 150ms krn ; 0ms fg
  u0a225:
    Wake lock *launch* realtime
    Wake lock WindowManager: 2s 1ms full (1 times) realtime
    TOTAL wake: 2s 1ms full realtime
    Foreground activities: 1m 26s 473ms realtime (21 times)
    Foreground services: 270ms realtime (2 times)
    Top for: 1m 26s 949ms 
    Fg Service for: 270ms 
    Cached for: 33ms 
    Total running: 1m 27s 252ms 
    Total cpu time: u=24s 433ms s=3s 396ms 
    Total cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 120 20 20 0 0 0 0 0 0 0 0 0 30 0 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 350
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc com.ichi2.anki:
      CPU: 0ms usr + 0ms krn ; 17s 780ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 244ms uptime
        Starts: 1, launches: 1

