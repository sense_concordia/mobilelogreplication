Daily stats:
  Current start time: 2018-11-02-07-50-50
  Next min deadline: 2018-11-03-01-00-00
  Next max deadline: 2018-11-03-03-00-00
  Current daily steps:
    Discharge total time: 21h 4m 15s 400ms  (from 14 steps)
    Discharge screen on time: 21h 4m 15s 400ms  (from 14 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 4s 273ms (100.0%) realtime, 4m 4s 273ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 391ms realtime, 4m 4s 391ms uptime
  Discharge: 13.3 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 13.3 mAh
  Start clock time: 2018-11-02-20-29-14
  Screen on: 4m 4s 273ms (100.0%) 0x, Interactive: 4m 4s 273ms (100.0%)
  Screen brightnesses:
    dark 4m 4s 273ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 4s 273ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 4s 273ms (100.0%) 
     Cellular Sleep time:  4m 2s 902ms (99.4%)
     Cellular Idle time:   387ms (0.2%)
     Cellular Rx time:     984ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 4s 273ms (100.0%) 
     Wifi supplicant states:
       completed 4m 4s 273ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 4s 273ms (100.0%) 
     WiFi Sleep time:  4m 4s 274ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 4s 274ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 18.5, actual drain: 0
    Screen: 13.9 Excluded from smearing
    Uid 1000: 1.36 ( cpu=1.35 sensor=0.00678 ) Excluded from smearing
    Uid u0a200: 0.986 ( cpu=0.986 ) Including smearing: 1.17 ( proportional=0.179 )
    Cell standby: 0.746 ( radio=0.746 ) Excluded from smearing
    Idle: 0.666 Excluded from smearing
    Uid 0: 0.533 ( cpu=0.533 ) Excluded from smearing
    Uid u0a47: 0.153 ( cpu=0.153 sensor=0.000136 ) Excluded from smearing
    Uid u0a127: 0.0368 ( cpu=0.0368 ) Including smearing: 0.0434 ( proportional=0.00669 )
    Uid u0a22: 0.0261 ( cpu=0.0261 ) Excluded from smearing
    Uid u0a68: 0.0238 ( cpu=0.0238 ) Including smearing: 0.0281 ( proportional=0.00432 )
    Uid 2000: 0.0207 ( cpu=0.0207 ) Excluded from smearing
    Uid 1036: 0.0112 ( cpu=0.0112 ) Excluded from smearing
    Uid u0a90: 0.00230 ( cpu=0.00230 ) Including smearing: 0.00272 ( proportional=0.000419 )
    Uid u0a40: 0.00186 ( cpu=0.00186 ) Including smearing: 0.00220 ( proportional=0.000339 )
    Uid u0a25: 0.000876 ( cpu=0.000876 ) Including smearing: 0.00104 ( proportional=0.000159 )
    Uid 1001: 0.000743 ( cpu=0.000743 ) Excluded from smearing
    Uid u0a75: 0.000584 ( cpu=0.000584 ) Including smearing: 0.000690 ( proportional=0.000106 )
    Uid 1027: 0.000474 ( cpu=0.000474 ) Excluded from smearing
    Wifi: 0.000366 ( cpu=0.000366 ) Including smearing: 0.000433 ( proportional=0.0000666 )
    Uid u0a8: 0.000218 ( cpu=0.000218 ) Including smearing: 0.000258 ( proportional=0.0000397 )
    Uid u0a114: 0.000145 ( cpu=0.000145 ) Including smearing: 0.000171 ( proportional=0.0000264 )
    Uid u0a61: 0.000109 ( cpu=0.000109 ) Including smearing: 0.000128 ( proportional=0.0000197 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 4s 145ms realtime (2 times)
    XO_shutdown.MPSS: 4m 2s 539ms realtime (97 times)
    XO_shutdown.ADSP: 4m 4s 164ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 4s 243ms realtime (0 times)
    Fg Service for: 4m 4s 244ms 
    Total running: 4m 4s 244ms 
    Total cpu time: u=25s 144ms s=11s 827ms 
    Total cpu time per freq: 120 0 0 0 1080 690 820 840 420 530 410 280 610 340 340 390 310 210 590 290 270 8210 410 230 340 200 150 270 200 180 310 220 250 290 170 230 70 170 80 240 430 310 190 120 160 190 220 160 60 60 10 30 13180
    Proc android.hardware.memtrack@1.0-service:
      CPU: 30ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 530ms usr + 2s 260ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 40ms usr + 290ms krn ; 0ms fg
    Proc system:
      CPU: 23s 580ms usr + 7s 800ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 180ms usr + 370ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 37s 848ms realtime (27 times)
    Foreground services: 22s 345ms realtime (26 times)
    Top for: 3m 41s 367ms 
    Fg Service for: 22s 345ms 
    Cached for: 49ms 
    Total running: 4m 3s 761ms 
    Total cpu time: u=23s 53ms s=3s 917ms 
    Total cpu time per freq: 0 0 0 0 700 310 360 200 210 160 260 180 830 230 300 270 150 190 340 210 260 4120 1450 1100 1000 810 790 590 540 420 500 360 380 1080 120 160 220 200 220 900 440 340 200 330 80 150 70 190 70 40 40 50 4960
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 23s 600ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

