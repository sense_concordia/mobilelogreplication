Daily stats:
  Current start time: 2018-11-02-07-50-50
  Next min deadline: 2018-11-03-01-00-00
  Next max deadline: 2018-11-03-03-00-00
  Current daily steps:
    Discharge total time: 21h 7m 47s 500ms  (from 16 steps)
    Discharge screen on time: 21h 7m 47s 500ms  (from 16 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 4s 774ms (99.9%) realtime, 4m 4s 774ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 948ms realtime, 4m 4s 948ms uptime
  Discharge: 13.9 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 13.9 mAh
  Start clock time: 2018-11-02-20-54-44
  Screen on: 4m 4s 774ms (100.0%) 0x, Interactive: 4m 4s 774ms (100.0%)
  Screen brightnesses:
    dark 4m 4s 774ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 4s 774ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 4s 774ms (100.0%) 
     Cellular Sleep time:  4m 3s 221ms (99.4%)
     Cellular Idle time:   409ms (0.2%)
     Cellular Rx time:     1s 145ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 4s 774ms (100.0%) 
     Wifi supplicant states:
       completed 4m 4s 774ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 4s 774ms (100.0%) 
     WiFi Sleep time:  4m 4s 776ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 4s 776ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 18.5, actual drain: 0
    Screen: 14.0 Excluded from smearing
    Uid 1000: 1.34 ( cpu=1.33 sensor=0.00680 ) Excluded from smearing
    Uid u0a200: 0.985 ( cpu=0.985 ) Including smearing: 1.16 ( proportional=0.178 )
    Cell standby: 0.748 ( radio=0.748 ) Excluded from smearing
    Idle: 0.667 Excluded from smearing
    Uid 0: 0.537 ( cpu=0.537 ) Excluded from smearing
    Uid u0a47: 0.150 ( cpu=0.149 sensor=0.000136 ) Excluded from smearing
    Uid u0a127: 0.0370 ( cpu=0.0370 ) Including smearing: 0.0437 ( proportional=0.00670 )
    Uid u0a22: 0.0323 ( cpu=0.0323 ) Excluded from smearing
    Uid u0a68: 0.0241 ( cpu=0.0241 ) Including smearing: 0.0285 ( proportional=0.00436 )
    Uid 2000: 0.0195 ( cpu=0.0195 ) Excluded from smearing
    Uid 1036: 0.0106 ( cpu=0.0106 ) Excluded from smearing
    Uid u0a114: 0.00432 ( cpu=0.00432 ) Including smearing: 0.00510 ( proportional=0.000781 )
    Uid u0a90: 0.00264 ( cpu=0.00264 ) Including smearing: 0.00312 ( proportional=0.000478 )
    Uid u0a25: 0.00157 ( cpu=0.00157 ) Including smearing: 0.00185 ( proportional=0.000284 )
    Uid u0a40: 0.00157 ( cpu=0.00157 ) Including smearing: 0.00185 ( proportional=0.000284 )
    Uid 1001: 0.000597 ( cpu=0.000597 ) Excluded from smearing
    Uid u0a75: 0.000474 ( cpu=0.000474 ) Including smearing: 0.000559 ( proportional=0.0000857 )
    Uid 1027: 0.000400 ( cpu=0.000400 ) Excluded from smearing
    Wifi: 0.000378 ( cpu=0.000378 ) Including smearing: 0.000446 ( proportional=0.0000683 )
    Uid u0a8: 0.000145 ( cpu=0.000145 ) Including smearing: 0.000171 ( proportional=0.0000262 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 4s 647ms realtime (2 times)
    XO_shutdown.MPSS: 4m 2s 842ms realtime (98 times)
    XO_shutdown.ADSP: 4m 4s 673ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 4s 738ms realtime (0 times)
    Fg Service for: 4m 4s 739ms 
    Total running: 4m 4s 739ms 
    Total cpu time: u=25s 203ms s=11s 453ms 
    Total cpu time per freq: 130 0 0 0 1080 720 740 780 390 490 340 280 570 340 330 330 230 300 380 210 250 7940 430 160 220 210 250 160 170 160 260 360 290 270 240 120 220 160 170 120 300 450 110 150 120 250 170 110 100 190 0 50 12800
    Proc android.hardware.memtrack@1.0-service:
      CPU: 30ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc android.hardware.light@2.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 570ms usr + 2s 130ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 30ms usr + 290ms krn ; 0ms fg
    Proc system:
      CPU: 23s 200ms usr + 7s 210ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 150ms usr + 390ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 38s 322ms realtime (27 times)
    Foreground services: 22s 403ms realtime (26 times)
    Top for: 3m 41s 794ms 
    Fg Service for: 22s 403ms 
    Cached for: 65ms 
    Total running: 4m 4s 262ms 
    Total cpu time: u=23s 267ms s=3s 720ms 
    Total cpu time per freq: 0 0 0 0 640 370 260 260 190 210 230 200 830 320 250 280 210 210 240 260 240 4230 1430 1090 1030 780 760 790 510 390 430 410 290 1110 100 230 220 170 150 950 480 320 280 150 150 100 120 130 20 190 0 40 4840
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 23s 700ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

