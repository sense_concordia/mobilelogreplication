Daily stats:
  Current start time: 2018-11-02-07-50-50
  Next min deadline: 2018-11-03-01-00-00
  Next max deadline: 2018-11-03-03-00-00
  Current daily steps:
    Discharge total time: 20h 27m 26s 200ms  (from 10 steps)
    Discharge screen on time: 20h 27m 26s 200ms  (from 10 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 4s 625ms (99.9%) realtime, 4m 4s 626ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 801ms realtime, 4m 4s 801ms uptime
  Discharge: 13.7 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 13.7 mAh
  Start clock time: 2018-11-02-19-29-46
  Screen on: 4m 4s 625ms (100.0%) 0x, Interactive: 4m 4s 625ms (100.0%)
  Screen brightnesses:
    dark 4m 4s 625ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 4s 625ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 4s 625ms (100.0%) 
     Cellular Sleep time:  4m 3s 144ms (99.4%)
     Cellular Idle time:   543ms (0.2%)
     Cellular Rx time:     939ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 4s 625ms (100.0%) 
     Wifi supplicant states:
       completed 4m 4s 625ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 4s 625ms (100.0%) 
     WiFi Sleep time:  4m 4s 626ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 4s 626ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 18.5, actual drain: 0-35.2
    Screen: 14.0 Excluded from smearing
    Uid 1000: 1.36 ( cpu=1.36 sensor=0.00679 ) Excluded from smearing
    Uid u0a200: 0.990 ( cpu=0.990 ) Including smearing: 1.17 ( proportional=0.180 )
    Cell standby: 0.747 ( radio=0.747 ) Excluded from smearing
    Idle: 0.667 Excluded from smearing
    Uid 0: 0.538 ( cpu=0.538 wake=0.00000079 ) Excluded from smearing
    Uid u0a47: 0.148 ( cpu=0.148 sensor=0.000136 ) Excluded from smearing
    Uid u0a127: 0.0365 ( cpu=0.0365 ) Including smearing: 0.0431 ( proportional=0.00664 )
    Uid u0a68: 0.0249 ( cpu=0.0249 ) Including smearing: 0.0295 ( proportional=0.00454 )
    Uid u0a22: 0.0248 ( cpu=0.0248 ) Excluded from smearing
    Uid 2000: 0.0194 ( cpu=0.0194 ) Excluded from smearing
    Uid 1036: 0.0111 ( cpu=0.0111 ) Excluded from smearing
    Uid u0a90: 0.00234 ( cpu=0.00234 ) Including smearing: 0.00276 ( proportional=0.000426 )
    Uid u0a40: 0.00198 ( cpu=0.00198 ) Including smearing: 0.00234 ( proportional=0.000360 )
    Uid u0a25: 0.00186 ( cpu=0.00186 ) Including smearing: 0.00220 ( proportional=0.000339 )
    Uid 1001: 0.000986 ( cpu=0.000986 ) Excluded from smearing
    Wifi: 0.000952 ( cpu=0.000952 ) Including smearing: 0.00112 ( proportional=0.000173 )
    Bluetooth: 0.000937 ( cpu=0.000937 ) Including smearing: 0.00111 ( proportional=0.000171 )
    Uid u0a114: 0.000643 ( cpu=0.000643 ) Including smearing: 0.000760 ( proportional=0.000117 )
    Uid u0a75: 0.000631 ( cpu=0.000631 ) Including smearing: 0.000745 ( proportional=0.000115 )
    Uid 1027: 0.000358 ( cpu=0.000358 ) Excluded from smearing
    Uid u0a8: 0.000136 ( cpu=0.000136 ) Including smearing: 0.000161 ( proportional=0.0000248 )
    Uid u0a61: 0.000111 ( cpu=0.000111 ) Including smearing: 0.000131 ( proportional=0.0000201 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 4s 431ms realtime (7 times)
    XO_shutdown.MPSS: 4m 2s 724ms realtime (101 times)
    XO_shutdown.ADSP: 4m 4s 514ms realtime (0 times)
    wlan.Active: 83ms realtime (7 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 4s 564ms realtime (0 times)
    Fg Service for: 4m 4s 564ms 
    Total running: 4m 4s 564ms 
    Total cpu time: u=24s 916ms s=12s 140ms 
    Total cpu time per freq: 130 0 0 0 1090 850 820 800 520 520 380 180 630 420 230 270 300 390 310 350 230 8100 490 200 340 250 190 170 150 230 290 200 310 280 200 200 140 180 170 190 370 230 260 100 190 180 130 170 60 40 30 70 13620
    Proc android.hardware.memtrack@1.0-service:
      CPU: 40ms usr + 30ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 880ms usr + 2s 740ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 60ms usr + 340ms krn ; 0ms fg
    Proc system:
      CPU: 28s 950ms usr + 9s 650ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 210ms usr + 450ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 38s 132ms realtime (27 times)
    Foreground services: 22s 539ms realtime (26 times)
    Top for: 3m 41s 536ms 
    Fg Service for: 22s 539ms 
    Cached for: 49ms 
    Total running: 4m 4s 124ms 
    Total cpu time: u=23s 196ms s=3s 783ms 
    Total cpu time per freq: 10 0 0 0 570 390 300 270 180 160 210 130 760 270 190 190 310 310 230 330 190 4260 1550 1150 1040 840 710 530 570 380 410 480 370 1160 150 300 130 110 220 850 510 320 130 200 200 150 130 120 50 60 20 30 4950
    Proc de.danoeh.antennapod.debug:
      CPU: 9s 470ms usr + 1s 770ms krn ; 23s 590ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

