Daily stats:
  Current start time: 2018-11-02-07-50-50
  Next min deadline: 2018-11-03-01-00-00
  Next max deadline: 2018-11-03-03-00-00
  Current daily steps:
    Discharge total time: 20h 9m 6s 500ms  (from 11 steps)
    Discharge screen on time: 20h 9m 6s 500ms  (from 11 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 4s 749ms (100.0%) realtime, 4m 4s 749ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 838ms realtime, 4m 4s 838ms uptime
  Discharge: 13.4 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 13.4 mAh
  Start clock time: 2018-11-02-19-51-00
  Screen on: 4m 4s 749ms (100.0%) 0x, Interactive: 4m 4s 749ms (100.0%)
  Screen brightnesses:
    dark 4m 4s 749ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 4s 749ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 4s 749ms (100.0%) 
     Cellular Sleep time:  4m 3s 297ms (99.4%)
     Cellular Idle time:   417ms (0.2%)
     Cellular Rx time:     1s 37ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 4s 749ms (100.0%) 
     Wifi supplicant states:
       completed 4m 4s 749ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 4s 749ms (100.0%) 
     WiFi Sleep time:  4m 4s 751ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 4s 751ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 18.5, actual drain: 0
    Screen: 14.0 Excluded from smearing
    Uid 1000: 1.34 ( cpu=1.33 sensor=0.00680 ) Excluded from smearing
    Uid u0a200: 0.979 ( cpu=0.979 ) Including smearing: 1.16 ( proportional=0.177 )
    Cell standby: 0.748 ( radio=0.748 ) Excluded from smearing
    Idle: 0.667 Excluded from smearing
    Uid 0: 0.536 ( cpu=0.536 ) Excluded from smearing
    Uid u0a47: 0.147 ( cpu=0.147 sensor=0.000136 ) Excluded from smearing
    Uid u0a127: 0.0370 ( cpu=0.0370 ) Including smearing: 0.0437 ( proportional=0.00667 )
    Uid u0a68: 0.0234 ( cpu=0.0234 ) Including smearing: 0.0276 ( proportional=0.00421 )
    Uid u0a22: 0.0231 ( cpu=0.0231 ) Excluded from smearing
    Uid 2000: 0.0192 ( cpu=0.0192 ) Excluded from smearing
    Uid 1036: 0.0105 ( cpu=0.0105 ) Excluded from smearing
    Uid u0a90: 0.00233 ( cpu=0.00233 ) Including smearing: 0.00276 ( proportional=0.000421 )
    Uid u0a40: 0.00193 ( cpu=0.00193 ) Including smearing: 0.00228 ( proportional=0.000348 )
    Uid u0a25: 0.000619 ( cpu=0.000619 ) Including smearing: 0.000731 ( proportional=0.000112 )
    Wifi: 0.000573 ( cpu=0.000573 ) Including smearing: 0.000676 ( proportional=0.000103 )
    Uid 1001: 0.000510 ( cpu=0.000510 ) Excluded from smearing
    Uid 1027: 0.000473 ( cpu=0.000473 ) Excluded from smearing
    Uid u0a75: 0.000473 ( cpu=0.000473 ) Including smearing: 0.000559 ( proportional=0.0000853 )
    Uid u0a61: 0.000254 ( cpu=0.000254 ) Including smearing: 0.000300 ( proportional=0.0000459 )
    Uid u0a8: 0.000108 ( cpu=0.000108 ) Including smearing: 0.000128 ( proportional=0.0000195 )
    Uid u0a114: 0.000108 ( cpu=0.000108 ) Including smearing: 0.000128 ( proportional=0.0000195 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 4s 614ms realtime (2 times)
    XO_shutdown.MPSS: 4m 2s 910ms realtime (97 times)
    XO_shutdown.ADSP: 4m 4s 640ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 4s 733ms realtime (0 times)
    Fg Service for: 4m 4s 733ms 
    Total running: 4m 4s 733ms 
    Total cpu time: u=25s 364ms s=11s 273ms 
    Total cpu time per freq: 110 0 0 0 1150 720 760 910 530 490 320 200 620 280 320 380 290 260 420 370 300 8090 340 160 220 180 100 210 220 70 410 330 290 410 100 190 170 180 110 290 390 250 140 80 170 310 90 130 80 130 70 150 12850
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 570ms usr + 2s 340ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 320ms krn ; 0ms fg
    Proc system:
      CPU: 23s 900ms usr + 7s 360ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 160ms usr + 400ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 38s 307ms realtime (27 times)
    Foreground services: 22s 248ms realtime (26 times)
    Top for: 3m 41s 891ms 
    Fg Service for: 22s 248ms 
    Cached for: 93ms 
    Total running: 4m 4s 232ms 
    Total cpu time: u=23s 127ms s=3s 714ms 
    Total cpu time per freq: 10 0 0 0 670 470 270 270 160 220 190 130 720 280 380 280 220 310 320 300 170 3780 1500 1050 1090 890 500 590 460 540 640 370 270 1030 200 250 240 130 150 920 410 370 200 250 200 160 180 150 100 80 20 10 5030
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 23s 570ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

