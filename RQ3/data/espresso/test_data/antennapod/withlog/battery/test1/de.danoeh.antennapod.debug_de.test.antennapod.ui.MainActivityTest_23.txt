Daily stats:
  Current start time: 2018-11-02-07-50-50
  Next min deadline: 2018-11-03-01-00-00
  Next max deadline: 2018-11-03-03-00-00
  Current daily steps:
    Discharge total time: 21h 39m 45s 900ms  (from 2 steps)
    Discharge screen on time: 21h 39m 45s 900ms  (from 2 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 31s 681ms (99.8%) realtime, 1m 31s 680ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 31s 864ms realtime, 1m 31s 863ms uptime
  Discharge: 6.77 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 6.77 mAh
  Start clock time: 2018-11-02-18-05-07
  Screen on: 1m 31s 681ms (100.0%) 0x, Interactive: 1m 31s 681ms (100.0%)
  Screen brightnesses:
    dark 1m 31s 681ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 31s 681ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 31s 681ms (100.0%) 
     Cellular Sleep time:  1m 31s 43ms (99.3%)
     Cellular Idle time:   252ms (0.3%)
     Cellular Rx time:     387ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 31s 681ms (100.0%) 
     Wifi supplicant states:
       completed 1m 31s 681ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 31s 681ms (100.0%) 
     WiFi Sleep time:  1m 31s 683ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 31s 683ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.12, actual drain: 0
    Screen: 5.23 Excluded from smearing
    Uid u0a200: 0.678 ( cpu=0.678 ) Including smearing: 0.782 ( proportional=0.105 )
    Uid 1000: 0.368 ( cpu=0.366 sensor=0.00255 ) Excluded from smearing
    Cell standby: 0.280 ( radio=0.280 ) Excluded from smearing
    Idle: 0.250 Excluded from smearing
    Uid 0: 0.237 ( cpu=0.237 ) Excluded from smearing
    Uid u0a47: 0.0189 ( cpu=0.0189 sensor=0.0000509 ) Excluded from smearing
    Uid 2000: 0.0135 ( cpu=0.0135 ) Excluded from smearing
    Uid 1013: 0.0122 ( cpu=0.0122 ) Excluded from smearing
    Uid u0a22: 0.00931 ( cpu=0.00931 ) Excluded from smearing
    Uid u0a127: 0.00581 ( cpu=0.00581 ) Including smearing: 0.00671 ( proportional=0.000898 )
    Uid 1040: 0.00569 ( cpu=0.00569 ) Excluded from smearing
    Uid 1036: 0.00537 ( cpu=0.00537 ) Excluded from smearing
    Uid 1019: 0.00250 ( cpu=0.00250 ) Excluded from smearing
    Uid u0a61: 0.00200 ( cpu=0.00200 ) Including smearing: 0.00231 ( proportional=0.000310 )
    Uid u0a68: 0.00143 ( cpu=0.00143 ) Including smearing: 0.00165 ( proportional=0.000221 )
    Uid u0a90: 0.00105 ( cpu=0.00105 ) Including smearing: 0.00121 ( proportional=0.000162 )
    Uid u0a114: 0.000583 ( cpu=0.000583 ) Including smearing: 0.000673 ( proportional=0.0000901 )
    Uid 1001: 0.000408 ( cpu=0.000408 ) Excluded from smearing
    Wifi: 0.000348 ( cpu=0.000348 ) Including smearing: 0.000402 ( proportional=0.0000537 )
    Uid u0a40: 0.000142 ( cpu=0.000142 ) Including smearing: 0.000164 ( proportional=0.0000220 )
    Uid u0a8: 0.000106 ( cpu=0.000106 ) Including smearing: 0.000123 ( proportional=0.0000164 )
    Uid u0a25: 0.000106 ( cpu=0.000106 ) Including smearing: 0.000123 ( proportional=0.0000164 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 31s 555ms realtime (2 times)
    XO_shutdown.MPSS: 1m 30s 834ms realtime (38 times)
    XO_shutdown.ADSP: 1m 31s 582ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 3 button, 28 touch
    Wake lock *alarm* realtime
    Wake lock NetworkStats realtime
    Sensor 7: 1m 31s 668ms realtime (0 times)
    Fg Service for: 1m 31s 668ms 
    Total running: 1m 31s 668ms 
    Total cpu time: u=5s 930ms s=4s 370ms 
    Total cpu time per freq: 10 0 0 0 370 280 370 410 320 170 150 180 350 260 320 140 170 110 100 40 70 2990 150 50 70 90 60 50 50 40 20 30 20 40 60 50 70 60 20 100 40 100 40 50 20 0 50 10 10 40 0 30 1460
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 580ms usr + 1s 90ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 3s 670ms usr + 1s 880ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 80ms usr + 140ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 71ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock *launch* realtime
    Foreground activities: 1m 27s 600ms realtime (7 times)
    Foreground services: 3s 131ms realtime (4 times)
    Top for: 1m 28s 32ms 
    Fg Service for: 3s 131ms 
    Cached for: 48ms 
    Total running: 1m 31s 211ms 
    Total cpu time: u=15s 830ms s=3s 100ms 
    Total cpu time per freq: 0 0 0 0 210 130 200 130 80 130 110 240 340 290 310 110 190 80 120 130 100 2340 1050 700 680 620 620 310 190 100 230 70 210 710 130 220 150 250 120 340 560 660 500 250 180 170 80 70 70 50 30 100 4180
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 18s 180ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.download.DownloadService:
        Created for: 155ms uptime
        Starts: 1, launches: 1

