Daily stats:
  Current start time: 2018-11-02-07-50-50
  Next min deadline: 2018-11-03-01-00-00
  Next max deadline: 2018-11-03-03-00-00
  Current daily steps:
    Discharge total time: 23h 10m 55s 700ms  (from 3 steps)
    Discharge screen on time: 23h 10m 55s 700ms  (from 3 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 15s 958ms (99.9%) realtime, 1m 15s 957ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 16s 50ms realtime, 1m 16s 49ms uptime
  Discharge: 4.62 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 4.62 mAh
  Start clock time: 2018-11-02-18-21-22
  Screen on: 1m 15s 958ms (100.0%) 0x, Interactive: 1m 15s 958ms (100.0%)
  Screen brightnesses:
    dark 1m 15s 958ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 15s 958ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 15s 958ms (100.0%) 
     Cellular Sleep time:  1m 14s 698ms (98.3%)
     Cellular Idle time:   426ms (0.6%)
     Cellular Rx time:     834ms (1.1%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 15s 958ms (100.0%) 
     Wifi supplicant states:
       completed 1m 15s 958ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 15s 958ms (100.0%) 
     WiFi Sleep time:  1m 15s 959ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 15s 959ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 11.1, actual drain: 0
    Screen: 4.33 Excluded from smearing
    Uid u0a200: 2.19 ( cpu=2.19 ) Including smearing: 3.61 ( proportional=1.41 )
    Uid 1000: 1.24 ( cpu=1.24 sensor=0.00211 ) Excluded from smearing
    Uid 1041: 1.10 ( cpu=1.10 ) Excluded from smearing
    Uid 0: 0.892 ( cpu=0.892 ) Excluded from smearing
    Uid 1013: 0.382 ( cpu=0.382 ) Excluded from smearing
    Cell standby: 0.232 ( radio=0.232 ) Excluded from smearing
    Uid u0a47: 0.217 ( cpu=0.217 sensor=0.0000422 ) Excluded from smearing
    Idle: 0.207 Excluded from smearing
    Uid 1046: 0.0790 ( cpu=0.0790 ) Excluded from smearing
    Uid 1040: 0.0693 ( cpu=0.0693 ) Excluded from smearing
    Uid 1036: 0.0614 ( cpu=0.0614 ) Excluded from smearing
    Uid 2000: 0.0411 ( cpu=0.0411 ) Excluded from smearing
    Uid u0a127: 0.0275 ( cpu=0.0275 ) Including smearing: 0.0453 ( proportional=0.0177 )
    Uid u0a22: 0.0243 ( cpu=0.0243 ) Excluded from smearing
    Uid 1019: 0.0237 ( cpu=0.0237 ) Excluded from smearing
    Uid u0a40: 0.00603 ( cpu=0.00603 ) Including smearing: 0.00991 ( proportional=0.00388 )
    Uid u0a68: 0.00422 ( cpu=0.00422 ) Including smearing: 0.00694 ( proportional=0.00272 )
    Uid u0a90: 0.00219 ( cpu=0.00219 ) Including smearing: 0.00360 ( proportional=0.00141 )
    Uid u0a25: 0.000613 ( cpu=0.000613 ) Including smearing: 0.00101 ( proportional=0.000394 )
    Uid 1001: 0.000498 ( cpu=0.000498 ) Excluded from smearing
    Wifi: 0.000385 ( cpu=0.000385 ) Including smearing: 0.000633 ( proportional=0.000248 )
    Uid 1027: 0.000344 ( cpu=0.000344 ) Excluded from smearing
    Uid u0a114: 0.000267 ( cpu=0.000267 ) Including smearing: 0.000439 ( proportional=0.000172 )
    Uid u0a75: 0.000152 ( cpu=0.000152 ) Including smearing: 0.000250 ( proportional=0.0000980 )
    Bluetooth: 0.000115 ( cpu=0.000115 ) Including smearing: 0.000188 ( proportional=0.0000738 )
    Uid u0a8: 0.000114 ( cpu=0.000114 ) Including smearing: 0.000188 ( proportional=0.0000734 )
    Uid u0a61: 0.000114 ( cpu=0.000114 ) Including smearing: 0.000188 ( proportional=0.0000734 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 15s 817ms realtime (2 times)
    XO_shutdown.MPSS: 1m 15s 255ms realtime (31 times)
    XO_shutdown.ADSP: 30s 728ms realtime (8 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 15s 938ms realtime (0 times)
    Fg Service for: 1m 15s 938ms 
    Total running: 1m 15s 938ms 
    Total cpu time: u=20s 873ms s=11s 330ms 
    Total cpu time per freq: 0 0 10 0 400 740 590 370 430 270 180 120 580 220 190 240 260 250 370 330 220 8490 240 190 120 140 150 180 150 220 350 90 310 310 120 110 180 150 200 160 330 310 190 200 180 230 90 110 170 160 20 60 10930
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 100ms usr + 160ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 70ms usr + 2s 170ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 140ms krn ; 0ms fg
    Proc system:
      CPU: 12s 960ms usr + 5s 320ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 40ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 160ms usr + 320ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 423ms realtime (9 times)
    Foreground activities: 1m 8s 705ms realtime (6 times)
    Foreground services: 5s 925ms realtime (7 times)
    Top for: 1m 9s 525ms 
    Fg Service for: 5s 925ms 
    Cached for: 43ms 
    Total running: 1m 15s 493ms 
    Total cpu time: u=47s 570ms s=9s 554ms 
    Total cpu time per freq: 0 0 0 0 260 360 390 210 230 170 130 150 590 140 200 120 150 140 360 310 390 7380 1430 1020 370 580 390 310 250 290 250 150 280 2390 260 340 420 380 480 780 1370 1890 1230 930 830 750 590 1010 290 380 100 120 26430
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 17s 930ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 37s 720ms uptime
        Starts: 6, launches: 6

