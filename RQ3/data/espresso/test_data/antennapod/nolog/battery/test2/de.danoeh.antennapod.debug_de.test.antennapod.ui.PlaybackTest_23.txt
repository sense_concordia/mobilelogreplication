Daily stats:
  Current start time: 2018-10-29-08-14-30
  Next min deadline: 2018-10-30-01-00-00
  Next max deadline: 2018-10-30-03-00-00
  Current daily steps:
    Discharge total time: 3d 17h 13m 56s 600ms  (from 12 steps)
    Discharge screen on time: 18h 9m 59s 600ms  (from 10 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 16s 554ms (99.9%) realtime, 1m 16s 554ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 16s 645ms realtime, 1m 16s 645ms uptime
  Discharge: 5.29 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 5.29 mAh
  Start clock time: 2018-10-30-02-42-23
  Screen on: 1m 16s 554ms (100.0%) 0x, Interactive: 1m 16s 554ms (100.0%)
  Screen brightnesses:
    dark 1m 16s 554ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 16s 554ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 16s 554ms (100.0%) 
     Cellular Sleep time:  1m 16s 62ms (99.4%)
     Cellular Idle time:   207ms (0.3%)
     Cellular Rx time:     286ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 16s 554ms (100.0%) 
     Wifi supplicant states:
       completed 1m 16s 554ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 16s 554ms (100.0%) 
     WiFi Sleep time:  1m 16s 555ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 16s 555ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.01, actual drain: 0
    Screen: 4.37 Excluded from smearing
    Uid u0a200: 0.738 ( cpu=0.738 ) Including smearing: 0.971 ( proportional=0.233 )
    Uid 1000: 0.421 ( cpu=0.419 sensor=0.00213 ) Excluded from smearing
    Uid 1041: 0.405 ( cpu=0.405 ) Excluded from smearing
    Uid 0: 0.321 ( cpu=0.321 ) Excluded from smearing
    Cell standby: 0.234 ( radio=0.234 ) Excluded from smearing
    Idle: 0.209 Excluded from smearing
    Uid 1013: 0.0942 ( cpu=0.0942 ) Excluded from smearing
    Uid u0a47: 0.0773 ( cpu=0.0773 sensor=0.0000425 ) Excluded from smearing
    Uid 2000: 0.0411 ( cpu=0.0411 ) Excluded from smearing
    Uid 1046: 0.0255 ( cpu=0.0255 ) Excluded from smearing
    Uid 1040: 0.0226 ( cpu=0.0226 ) Excluded from smearing
    Uid 1036: 0.0198 ( cpu=0.0198 ) Excluded from smearing
    Uid u0a22: 0.0129 ( cpu=0.00755 sensor=0.00531 ) Excluded from smearing
    Uid u0a127: 0.0108 ( cpu=0.0108 ) Including smearing: 0.0142 ( proportional=0.00340 )
    Uid 1019: 0.00802 ( cpu=0.00802 ) Excluded from smearing
    Uid u0a68: 0.00171 ( cpu=0.00171 ) Including smearing: 0.00225 ( proportional=0.000540 )
    Uid u0a114: 0.00144 ( cpu=0.00144 ) Including smearing: 0.00189 ( proportional=0.000454 )
    Uid u0a40: 0.00140 ( cpu=0.00140 ) Including smearing: 0.00184 ( proportional=0.000442 )
    Uid u0a25: 0.000895 ( cpu=0.000895 ) Including smearing: 0.00118 ( proportional=0.000282 )
    Wifi: 0.000355 ( cpu=0.000355 ) Including smearing: 0.000467 ( proportional=0.000112 )
    Uid 1001: 0.000257 ( cpu=0.000257 ) Excluded from smearing
    Uid u0a61: 0.000233 ( cpu=0.000233 ) Including smearing: 0.000306 ( proportional=0.0000734 )
    Bluetooth: 0.000137 ( cpu=0.000137 ) Including smearing: 0.000180 ( proportional=0.0000432 )
    Uid 1027: 0.000116 ( cpu=0.000116 ) Excluded from smearing
    Uid u0a75: 0.000116 ( cpu=0.000116 ) Including smearing: 0.000152 ( proportional=0.0000365 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 16s 432ms realtime (2 times)
    XO_shutdown.MPSS: 1m 15s 868ms realtime (32 times)
    XO_shutdown.ADSP: 34s 74ms realtime (7 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 16s 532ms realtime (0 times)
    Fg Service for: 1m 16s 533ms 
    Total running: 1m 16s 533ms 
    Total cpu time: u=7s 83ms s=3s 740ms 
    Total cpu time per freq: 10 0 0 0 150 360 170 130 110 130 100 30 180 70 50 80 50 60 40 30 110 3240 20 70 40 30 40 30 50 30 120 50 70 120 50 60 60 90 50 120 100 90 80 110 60 60 40 70 10 30 0 10 3580
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 80ms usr + 110ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 520ms usr + 640ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 9s 330ms usr + 3s 690ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 100ms usr + 180ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 451ms realtime (9 times)
    Foreground activities: 1m 9s 309ms realtime (6 times)
    Foreground services: 5s 930ms realtime (7 times)
    Top for: 1m 10s 80ms 
    Fg Service for: 5s 930ms 
    Cached for: 78ms 
    Total running: 1m 16s 88ms 
    Total cpu time: u=15s 840ms s=3s 120ms 
    Total cpu time per freq: 0 0 0 0 80 110 50 100 40 40 100 20 270 60 20 70 70 60 60 100 70 3240 340 380 150 170 180 100 80 100 40 80 90 710 50 100 130 70 120 210 390 690 480 290 270 140 220 250 170 170 70 70 8630
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 17s 860ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 37s 769ms uptime
        Starts: 6, launches: 6

