Daily stats:
  Current start time: 2018-10-29-08-14-30
  Next min deadline: 2018-10-30-01-00-00
  Next max deadline: 2018-10-30-03-00-00
  Current daily steps:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 15s 710ms (99.8%) realtime, 1m 15s 710ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 15s 883ms realtime, 1m 15s 883ms uptime
  Discharge: 4.49 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 4.49 mAh
  Start clock time: 2018-10-30-02-52-28
  Screen on: 1m 15s 710ms (100.0%) 0x, Interactive: 1m 15s 710ms (100.0%)
  Screen brightnesses:
    dark 1m 15s 710ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 15s 710ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 15s 710ms (100.0%) 
     Cellular Sleep time:  1m 15s 130ms (99.2%)
     Cellular Idle time:   309ms (0.4%)
     Cellular Rx time:     271ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 15s 710ms (100.0%) 
     Wifi supplicant states:
       completed 1m 15s 710ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 15s 710ms (100.0%) 
     WiFi Sleep time:  1m 15s 710ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 15s 710ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 6.98, actual drain: 0-35.2
    Screen: 4.32 Excluded from smearing
    Uid u0a200: 0.751 ( cpu=0.751 ) Including smearing: 0.990 ( proportional=0.239 )
    Uid 1000: 0.431 ( cpu=0.429 sensor=0.00210 ) Excluded from smearing
    Uid 1041: 0.396 ( cpu=0.396 ) Excluded from smearing
    Uid 0: 0.317 ( cpu=0.317 ) Excluded from smearing
    Cell standby: 0.231 ( radio=0.231 ) Excluded from smearing
    Idle: 0.206 Excluded from smearing
    Uid 1013: 0.0972 ( cpu=0.0972 ) Excluded from smearing
    Uid u0a47: 0.0773 ( cpu=0.0773 sensor=0.0000420 ) Excluded from smearing
    Uid 2000: 0.0426 ( cpu=0.0426 ) Excluded from smearing
    Uid 1046: 0.0264 ( cpu=0.0264 ) Excluded from smearing
    Uid 1040: 0.0244 ( cpu=0.0244 ) Excluded from smearing
    Uid 1036: 0.0209 ( cpu=0.0209 ) Excluded from smearing
    Uid u0a22: 0.0115 ( cpu=0.00621 sensor=0.00525 ) Excluded from smearing
    Uid u0a127: 0.0105 ( cpu=0.0105 ) Including smearing: 0.0138 ( proportional=0.00333 )
    Uid 1019: 0.00872 ( cpu=0.00872 ) Excluded from smearing
    Uid u0a68: 0.00174 ( cpu=0.00174 ) Including smearing: 0.00230 ( proportional=0.000555 )
    Uid u0a40: 0.00172 ( cpu=0.00172 ) Including smearing: 0.00227 ( proportional=0.000548 )
    Uid u0a114: 0.000684 ( cpu=0.000684 ) Including smearing: 0.000901 ( proportional=0.000218 )
    Uid 1001: 0.000567 ( cpu=0.000567 ) Excluded from smearing
    Uid u0a25: 0.000365 ( cpu=0.000365 ) Including smearing: 0.000481 ( proportional=0.000116 )
    Wifi: 0.000329 ( cpu=0.000329 ) Including smearing: 0.000434 ( proportional=0.000105 )
    Uid u0a75: 0.000120 ( cpu=0.000120 ) Including smearing: 0.000158 ( proportional=0.0000382 )
    Uid 1027: 0.000114 ( cpu=0.000114 ) Excluded from smearing
    Bluetooth: 0.000102 ( cpu=0.000102 ) Including smearing: 0.000135 ( proportional=0.0000326 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 15s 540ms realtime (5 times)
    XO_shutdown.MPSS: 1m 14s 929ms realtime (34 times)
    XO_shutdown.ADSP: 33s 292ms realtime (7 times)
    wlan.Active: 59ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 15s 679ms realtime (0 times)
    Fg Service for: 1m 15s 679ms 
    Total running: 1m 15s 679ms 
    Total cpu time: u=7s 140ms s=3s 870ms 
    Total cpu time per freq: 0 0 0 0 150 250 160 130 130 110 70 50 290 70 80 90 80 60 40 90 80 3140 20 90 20 50 30 20 40 40 120 100 30 150 40 120 50 60 90 80 80 90 70 70 40 30 20 70 80 30 10 20 3860
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 70ms usr + 150ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 510ms usr + 750ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 10s 200ms usr + 3s 980ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 40ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 110ms usr + 200ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 443ms realtime (9 times)
    Foreground activities: 1m 8s 494ms realtime (6 times)
    Foreground services: 5s 915ms realtime (7 times)
    Top for: 1m 9s 309ms 
    Fg Service for: 5s 915ms 
    Cached for: 37ms 
    Total running: 1m 15s 261ms 
    Total cpu time: u=15s 913ms s=3s 57ms 
    Total cpu time per freq: 0 0 0 0 70 130 110 40 50 20 70 40 240 60 60 80 20 30 60 110 110 2750 270 420 140 170 100 40 80 90 30 30 60 740 80 100 90 70 150 250 360 610 560 310 220 210 230 310 280 160 10 10 8900
    Proc de.danoeh.antennapod.debug:
      CPU: 1s 850ms usr + 420ms krn ; 17s 810ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 37s 637ms uptime
        Starts: 6, launches: 6

