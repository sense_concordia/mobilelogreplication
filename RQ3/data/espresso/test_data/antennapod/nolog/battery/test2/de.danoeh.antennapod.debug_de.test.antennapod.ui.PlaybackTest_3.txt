Daily stats:
  Current start time: 2018-10-29-08-14-30
  Next min deadline: 2018-10-30-01-00-00
  Next max deadline: 2018-10-30-03-00-00
  Current daily steps:
    Discharge total time: 4d 17h 47m 29s 400ms  (from 9 steps)
    Discharge screen on time: 19h 17m 8s 700ms  (from 7 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 15s 894ms (99.9%) realtime, 1m 15s 893ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 15s 995ms realtime, 1m 15s 995ms uptime
  Discharge: 4.64 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 4.64 mAh
  Start clock time: 2018-10-30-02-13-36
  Screen on: 1m 15s 894ms (100.0%) 0x, Interactive: 1m 15s 894ms (100.0%)
  Screen brightnesses:
    dark 1m 15s 894ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 15s 894ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 15s 894ms (100.0%) 
     Cellular Sleep time:  1m 15s 330ms (99.3%)
     Cellular Idle time:   291ms (0.4%)
     Cellular Rx time:     274ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 15s 894ms (100.0%) 
     Wifi supplicant states:
       completed 1m 15s 894ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 15s 894ms (100.0%) 
     WiFi Sleep time:  1m 15s 895ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 15s 895ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 6.95, actual drain: 0
    Screen: 4.33 Excluded from smearing
    Uid u0a200: 0.738 ( cpu=0.738 ) Including smearing: 0.969 ( proportional=0.231 )
    Uid 1000: 0.418 ( cpu=0.416 sensor=0.00211 ) Excluded from smearing
    Uid 1041: 0.380 ( cpu=0.380 ) Excluded from smearing
    Uid 0: 0.316 ( cpu=0.316 ) Excluded from smearing
    Cell standby: 0.232 ( radio=0.232 ) Excluded from smearing
    Idle: 0.207 Excluded from smearing
    Uid 1013: 0.100 ( cpu=0.100 ) Excluded from smearing
    Uid u0a47: 0.0754 ( cpu=0.0754 sensor=0.0000421 ) Excluded from smearing
    Uid 2000: 0.0410 ( cpu=0.0410 ) Excluded from smearing
    Uid 1046: 0.0262 ( cpu=0.0262 ) Excluded from smearing
    Uid 1040: 0.0248 ( cpu=0.0248 ) Excluded from smearing
    Uid 1036: 0.0200 ( cpu=0.0200 ) Excluded from smearing
    Uid u0a22: 0.0114 ( cpu=0.00610 sensor=0.00527 ) Excluded from smearing
    Uid u0a127: 0.0110 ( cpu=0.0110 ) Including smearing: 0.0145 ( proportional=0.00345 )
    Uid 1019: 0.00870 ( cpu=0.00870 ) Excluded from smearing
    Uid u0a68: 0.00457 ( cpu=0.00457 ) Including smearing: 0.00599 ( proportional=0.00143 )
    Uid u0a40: 0.00150 ( cpu=0.00150 ) Including smearing: 0.00197 ( proportional=0.000469 )
    Uid u0a35: 0.000670 ( cpu=0.000670 ) Including smearing: 0.000879 ( proportional=0.000209 )
    Uid u0a114: 0.000596 ( cpu=0.000596 ) Including smearing: 0.000783 ( proportional=0.000186 )
    Wifi: 0.000512 ( cpu=0.000512 ) Including smearing: 0.000672 ( proportional=0.000160 )
    Uid 1001: 0.000330 ( cpu=0.000330 ) Excluded from smearing
    Uid u0a25: 0.000157 ( cpu=0.000157 ) Including smearing: 0.000206 ( proportional=0.0000490 )
    Uid u0a8: 0.0000839 ( cpu=0.0000839 ) Including smearing: 0.000110 ( proportional=0.0000262 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 15s 752ms realtime (4 times)
    XO_shutdown.MPSS: 1m 15s 144ms realtime (36 times)
    XO_shutdown.ADSP: 33s 469ms realtime (7 times)
    wlan.Active: 46ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 15s 862ms realtime (0 times)
    Fg Service for: 1m 15s 862ms 
    Total running: 1m 15s 862ms 
    Total cpu time: u=7s 100ms s=3s 720ms 
    Total cpu time per freq: 0 0 0 0 160 290 150 170 140 110 40 70 130 90 40 50 100 110 70 80 80 2950 10 130 40 30 70 20 30 40 100 60 30 100 80 30 40 60 40 50 90 110 90 100 40 40 70 70 60 30 10 10 3680
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 30ms usr + 90ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 290ms usr + 490ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 6s 0ms usr + 2s 540ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 70ms usr + 130ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 420ms realtime (9 times)
    Foreground activities: 1m 8s 692ms realtime (6 times)
    Foreground services: 5s 853ms realtime (7 times)
    Top for: 1m 9s 541ms 
    Fg Service for: 5s 853ms 
    Cached for: 56ms 
    Total running: 1m 15s 450ms 
    Total cpu time: u=15s 877ms s=3s 90ms 
    Total cpu time per freq: 0 0 0 0 60 130 80 90 50 80 50 20 230 80 30 80 70 90 80 80 80 2820 350 430 170 90 200 50 60 60 60 40 70 730 140 80 60 80 150 300 390 740 500 250 220 260 340 360 230 110 50 80 8700
    Proc de.danoeh.antennapod.debug:
      CPU: 2s 850ms usr + 710ms krn ; 17s 890ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 37s 694ms uptime
        Starts: 6, launches: 6

