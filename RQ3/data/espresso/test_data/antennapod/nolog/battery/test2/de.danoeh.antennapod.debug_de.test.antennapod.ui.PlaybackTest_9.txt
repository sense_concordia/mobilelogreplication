Daily stats:
  Current start time: 2018-10-29-08-14-30
  Next min deadline: 2018-10-30-01-00-00
  Next max deadline: 2018-10-30-03-00-00
  Current daily steps:
    Discharge total time: 4d 7h 44m 43s 900ms  (from 10 steps)
    Discharge screen on time: 18h 32m 29s 500ms  (from 8 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 16s 25ms (99.9%) realtime, 1m 16s 25ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 16s 105ms realtime, 1m 16s 104ms uptime
  Discharge: 8.74 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.74 mAh
  Start clock time: 2018-10-30-02-22-13
  Screen on: 1m 16s 25ms (100.0%) 0x, Interactive: 1m 16s 25ms (100.0%)
  Screen brightnesses:
    dark 1m 16s 25ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 16s 25ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 16s 25ms (100.0%) 
     Cellular Sleep time:  1m 15s 514ms (99.3%)
     Cellular Idle time:   235ms (0.3%)
     Cellular Rx time:     276ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 16s 25ms (100.0%) 
     Wifi supplicant states:
       completed 1m 16s 25ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 16s 25ms (100.0%) 
     WiFi Sleep time:  1m 16s 26ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 16s 26ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 6.98, actual drain: 0
    Screen: 4.34 Excluded from smearing
    Uid u0a200: 0.749 ( cpu=0.749 ) Including smearing: 0.984 ( proportional=0.235 )
    Uid 1000: 0.418 ( cpu=0.416 sensor=0.00211 ) Excluded from smearing
    Uid 1041: 0.388 ( cpu=0.388 ) Excluded from smearing
    Uid 0: 0.320 ( cpu=0.320 ) Excluded from smearing
    Cell standby: 0.232 ( radio=0.232 ) Excluded from smearing
    Idle: 0.207 Excluded from smearing
    Uid 1013: 0.102 ( cpu=0.102 ) Excluded from smearing
    Uid u0a47: 0.0765 ( cpu=0.0764 sensor=0.0000422 ) Excluded from smearing
    Uid 2000: 0.0401 ( cpu=0.0401 ) Excluded from smearing
    Uid 1046: 0.0258 ( cpu=0.0258 ) Excluded from smearing
    Uid 1040: 0.0245 ( cpu=0.0245 ) Excluded from smearing
    Uid 1036: 0.0195 ( cpu=0.0195 ) Excluded from smearing
    Uid u0a22: 0.0114 ( cpu=0.00609 sensor=0.00528 ) Excluded from smearing
    Uid u0a127: 0.0107 ( cpu=0.0107 ) Including smearing: 0.0141 ( proportional=0.00337 )
    Uid 1019: 0.00870 ( cpu=0.00870 ) Excluded from smearing
    Uid u0a68: 0.00179 ( cpu=0.00179 ) Including smearing: 0.00235 ( proportional=0.000561 )
    Uid u0a40: 0.00171 ( cpu=0.00171 ) Including smearing: 0.00224 ( proportional=0.000536 )
    Uid u0a114: 0.000543 ( cpu=0.000543 ) Including smearing: 0.000713 ( proportional=0.000170 )
    Wifi: 0.000516 ( cpu=0.000516 ) Including smearing: 0.000678 ( proportional=0.000162 )
    Uid 1001: 0.000241 ( cpu=0.000241 ) Excluded from smearing
    Uid 1027: 0.000154 ( cpu=0.000154 ) Excluded from smearing
    Uid u0a75: 0.000154 ( cpu=0.000154 ) Including smearing: 0.000203 ( proportional=0.0000484 )
    Uid u0a8: 0.000115 ( cpu=0.000115 ) Including smearing: 0.000152 ( proportional=0.0000362 )
    Uid u0a25: 0.000115 ( cpu=0.000115 ) Including smearing: 0.000152 ( proportional=0.0000362 )
    Uid u0a35: 0.0000839 ( cpu=0.0000839 ) Including smearing: 0.000110 ( proportional=0.0000263 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 15s 886ms realtime (2 times)
    XO_shutdown.MPSS: 1m 15s 321ms realtime (32 times)
    XO_shutdown.ADSP: 33s 633ms realtime (7 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 16s 10ms realtime (0 times)
    Fg Service for: 1m 16s 11ms 
    Total running: 1m 16s 11ms 
    Total cpu time: u=6s 894ms s=3s 863ms 
    Total cpu time per freq: 0 0 0 0 120 240 260 130 120 70 80 40 170 110 80 70 60 110 110 90 130 2730 30 50 30 20 30 60 70 30 40 30 60 90 40 70 60 60 50 70 100 170 100 50 70 30 40 60 50 30 10 0 3710
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 60ms usr + 130ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 390ms usr + 760ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 8s 270ms usr + 3s 540ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 30ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 80ms usr + 190ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 341ms realtime (9 times)
    Foreground activities: 1m 8s 817ms realtime (6 times)
    Foreground services: 5s 897ms realtime (7 times)
    Top for: 1m 9s 603ms 
    Fg Service for: 5s 897ms 
    Cached for: 76ms 
    Total running: 1m 15s 576ms 
    Total cpu time: u=16s 160ms s=3s 127ms 
    Total cpu time per freq: 0 0 0 0 100 110 120 110 110 50 40 40 150 130 60 30 60 70 70 100 160 2800 400 360 180 150 180 160 80 100 100 80 90 700 80 170 110 80 90 240 470 730 470 340 280 290 200 330 200 190 50 70 8280
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 18s 200ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 37s 691ms uptime
        Starts: 6, launches: 6

