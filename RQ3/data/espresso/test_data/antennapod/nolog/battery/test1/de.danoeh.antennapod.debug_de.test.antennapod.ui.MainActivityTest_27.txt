Daily stats:
  Current start time: 2018-10-29-08-14-30
  Next min deadline: 2018-10-30-01-00-00
  Next max deadline: 2018-10-30-03-00-00
  Current daily steps:
    Discharge total time: 7d 22h 9m 37s 100ms  (from 5 steps)
    Discharge screen on time: 20h 33m 34s 0ms  (from 3 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 32s 854ms (99.8%) realtime, 1m 32s 854ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 32s 995ms realtime, 1m 32s 995ms uptime
  Discharge: 6.79 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 6.79 mAh
  Start clock time: 2018-10-30-01-29-01
  Screen on: 1m 32s 854ms (100.0%) 0x, Interactive: 1m 32s 854ms (100.0%)
  Screen brightnesses:
    dark 1m 32s 854ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 32s 854ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 32s 854ms (100.0%) 
     Cellular Sleep time:  1m 31s 362ms (98.4%)
     Cellular Idle time:   363ms (0.4%)
     Cellular Rx time:     1s 129ms (1.2%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 32s 854ms (100.0%) 
     Wifi supplicant states:
       completed 1m 32s 854ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 32s 854ms (100.0%) 
     WiFi Sleep time:  1m 32s 855ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 32s 855ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.22, actual drain: 0-35.2
    Screen: 5.30 Excluded from smearing
    Uid u0a200: 0.687 ( cpu=0.687 ) Including smearing: 0.794 ( proportional=0.107 )
    Uid 1000: 0.360 ( cpu=0.357 sensor=0.00258 ) Excluded from smearing
    Cell standby: 0.284 ( radio=0.284 ) Excluded from smearing
    Idle: 0.253 Excluded from smearing
    Uid 0: 0.231 ( cpu=0.231 ) Excluded from smearing
    Uid u0a47: 0.0268 ( cpu=0.0268 sensor=0.0000515 ) Excluded from smearing
    Uid 2000: 0.0265 ( cpu=0.0265 ) Excluded from smearing
    Uid u0a22: 0.0137 ( cpu=0.00723 sensor=0.00644 ) Excluded from smearing
    Uid 1013: 0.0122 ( cpu=0.0122 ) Excluded from smearing
    Uid 1040: 0.00633 ( cpu=0.00633 ) Excluded from smearing
    Uid u0a127: 0.00614 ( cpu=0.00614 ) Including smearing: 0.00710 ( proportional=0.000953 )
    Uid 1036: 0.00589 ( cpu=0.00589 ) Excluded from smearing
    Uid u0a68: 0.00416 ( cpu=0.00416 ) Including smearing: 0.00481 ( proportional=0.000646 )
    Uid 1019: 0.00271 ( cpu=0.00271 ) Excluded from smearing
    Uid u0a35: 0.000913 ( cpu=0.000913 ) Including smearing: 0.00106 ( proportional=0.000142 )
    Uid u0a25: 0.000562 ( cpu=0.000562 ) Including smearing: 0.000649 ( proportional=0.0000871 )
    Uid u0a114: 0.000491 ( cpu=0.000491 ) Including smearing: 0.000568 ( proportional=0.0000762 )
    Wifi: 0.000354 ( cpu=0.000354 ) Including smearing: 0.000409 ( proportional=0.0000549 )
    Uid 1001: 0.000283 ( cpu=0.000283 ) Excluded from smearing
    Uid u0a40: 0.000245 ( cpu=0.000245 ) Including smearing: 0.000283 ( proportional=0.0000380 )
    Uid u0a75: 0.000104 ( cpu=0.000104 ) Including smearing: 0.000121 ( proportional=0.0000162 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 32s 699ms realtime (4 times)
    XO_shutdown.MPSS: 1m 31s 175ms realtime (40 times)
    XO_shutdown.ADSP: 1m 32s 734ms realtime (0 times)
    wlan.Active: 45ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 3 button, 28 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 32s 833ms realtime (0 times)
    Fg Service for: 1m 32s 833ms 
    Total running: 1m 32s 833ms 
    Total cpu time: u=6s 150ms s=3s 720ms 
    Total cpu time per freq: 30 0 0 0 400 350 500 450 180 150 180 160 200 180 140 190 100 140 170 80 40 2760 30 60 60 50 50 40 40 40 20 70 30 90 20 70 40 40 50 70 70 120 110 40 20 10 40 60 30 20 40 0 1460
    Proc servicemanager:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 290ms usr + 330ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 1s 960ms usr + 710ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 40ms usr + 60ms krn ; 0ms fg
  u0a200:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 53ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock *launch* realtime
    Foreground activities: 1m 28s 815ms realtime (7 times)
    Foreground services: 3s 9ms realtime (4 times)
    Top for: 1m 29s 250ms 
    Fg Service for: 3s 9ms 
    Cached for: 60ms 
    Total running: 1m 32s 319ms 
    Total cpu time: u=15s 490ms s=3s 87ms 
    Total cpu time per freq: 0 0 0 0 240 170 180 90 130 90 130 180 260 250 210 170 120 160 170 80 60 1900 810 720 790 770 520 350 150 130 90 130 130 780 150 240 220 130 200 360 720 770 390 210 180 140 200 100 60 120 10 100 4340
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 17s 970ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.download.DownloadService:
        Created for: 146ms uptime
        Starts: 1, launches: 1

