Daily stats:
  Current start time: 2018-10-30-03-04-08
  Next min deadline: 2018-10-31-01-00-00
  Next max deadline: 2018-10-31-03-00-00
  Current daily steps:
    Discharge total time: 23h 19m 58s 0ms  (from 2 steps)
    Discharge screen on time: 23h 19m 58s 0ms  (from 2 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 4s 99ms (100.0%) realtime, 4m 4s 99ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 155ms realtime, 4m 4s 154ms uptime
  Discharge: 15.7 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 15.7 mAh
  Start clock time: 2018-10-30-03-31-46
  Screen on: 4m 4s 99ms (100.0%) 0x, Interactive: 4m 4s 99ms (100.0%)
  Screen brightnesses:
    dark 4m 4s 99ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 4s 99ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 4s 99ms (100.0%) 
     Cellular Sleep time:  4m 2s 765ms (99.5%)
     Cellular Idle time:   523ms (0.2%)
     Cellular Rx time:     812ms (0.3%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 4s 99ms (100.0%) 
     Wifi supplicant states:
       completed 4m 4s 99ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 4s 99ms (100.0%) 
     WiFi Sleep time:  4m 4s 100ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 4s 101ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 18.5, actual drain: 0-35.2
    Screen: 13.9 Excluded from smearing
    Uid 1000: 1.32 ( cpu=1.32 sensor=0.00678 ) Excluded from smearing
    Uid u0a200: 0.980 ( cpu=0.980 ) Including smearing: 1.16 ( proportional=0.180 )
    Cell standby: 0.746 ( radio=0.746 ) Excluded from smearing
    Idle: 0.665 Excluded from smearing
    Uid 0: 0.537 ( cpu=0.537 ) Excluded from smearing
    Uid u0a47: 0.169 ( cpu=0.169 sensor=0.000136 ) Excluded from smearing
    Uid u0a22: 0.0464 ( cpu=0.0295 sensor=0.0169 ) Excluded from smearing
    Uid u0a127: 0.0374 ( cpu=0.0374 ) Including smearing: 0.0442 ( proportional=0.00686 )
    Uid 2000: 0.0331 ( cpu=0.0331 ) Excluded from smearing
    Uid u0a68: 0.0240 ( cpu=0.0240 ) Including smearing: 0.0284 ( proportional=0.00440 )
    Uid 1036: 0.0131 ( cpu=0.0131 ) Excluded from smearing
    Uid u0a40: 0.00157 ( cpu=0.00157 ) Including smearing: 0.00186 ( proportional=0.000289 )
    Uid u0a25: 0.00146 ( cpu=0.00146 ) Including smearing: 0.00172 ( proportional=0.000267 )
    Uid u0a35: 0.00113 ( cpu=0.00113 ) Including smearing: 0.00134 ( proportional=0.000207 )
    Bluetooth: 0.000781 ( cpu=0.000781 ) Including smearing: 0.000924 ( proportional=0.000143 )
    Uid 1001: 0.000710 ( cpu=0.000710 ) Excluded from smearing
    Wifi: 0.000574 ( cpu=0.000574 ) Including smearing: 0.000680 ( proportional=0.000105 )
    Uid 1027: 0.000510 ( cpu=0.000510 ) Excluded from smearing
    Uid u0a75: 0.000510 ( cpu=0.000510 ) Including smearing: 0.000603 ( proportional=0.0000936 )
    Uid u0a8: 0.000218 ( cpu=0.000218 ) Including smearing: 0.000258 ( proportional=0.0000400 )
    Uid u0a114: 0.000218 ( cpu=0.000218 ) Including smearing: 0.000258 ( proportional=0.0000400 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 3s 929ms realtime (5 times)
    XO_shutdown.MPSS: 4m 2s 289ms realtime (105 times)
    XO_shutdown.ADSP: 4m 3s 986ms realtime (0 times)
    wlan.Active: 56ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 4s 87ms realtime (0 times)
    Fg Service for: 4m 4s 87ms 
    Total running: 4m 4s 87ms 
    Total cpu time: u=25s 240ms s=10s 684ms 
    Total cpu time per freq: 110 10 0 0 1170 730 880 890 440 470 330 230 620 370 200 350 220 280 500 200 190 7870 150 360 280 200 190 200 310 150 230 250 270 450 180 220 110 110 60 150 320 250 230 160 110 110 150 140 120 110 40 40 13160
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 550ms usr + 1s 650ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 40ms usr + 120ms krn ; 0ms fg
    Proc system:
      CPU: 24s 440ms usr + 7s 580ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 180ms usr + 360ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 37s 950ms realtime (27 times)
    Foreground services: 22s 19ms realtime (26 times)
    Top for: 3m 41s 525ms 
    Fg Service for: 22s 19ms 
    Cached for: 64ms 
    Total running: 4m 3s 608ms 
    Total cpu time: u=22s 550ms s=3s 986ms 
    Total cpu time per freq: 0 0 0 0 570 390 250 200 50 160 180 200 640 350 260 190 260 250 400 180 140 3810 1280 1220 1020 890 750 690 520 410 420 370 310 1000 90 210 200 60 110 870 410 420 170 270 150 160 130 190 110 100 120 20 5130
    Proc de.danoeh.antennapod.debug:
      CPU: 7s 150ms usr + 1s 280ms krn ; 23s 470ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

