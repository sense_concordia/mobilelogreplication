Daily stats:
  Current start time: 2018-10-30-03-04-08
  Next min deadline: 2018-10-31-01-00-00
  Next max deadline: 2018-10-31-03-00-00
  Current daily steps:
    Discharge total time: 23h 19m 58s 0ms  (from 2 steps)
    Discharge screen on time: 23h 19m 58s 0ms  (from 2 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 4s 330ms (99.9%) realtime, 4m 4s 330ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 457ms realtime, 4m 4s 457ms uptime
  Discharge: 13.6 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 13.6 mAh
  Start clock time: 2018-10-30-03-36-01
  Screen on: 4m 4s 330ms (100.0%) 0x, Interactive: 4m 4s 330ms (100.0%)
  Screen brightnesses:
    dark 4m 4s 330ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 4s 330ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 4s 330ms (100.0%) 
     Cellular Sleep time:  4m 3s 74ms (99.5%)
     Cellular Idle time:   428ms (0.2%)
     Cellular Rx time:     828ms (0.3%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 4s 330ms (100.0%) 
     Wifi supplicant states:
       completed 4m 4s 330ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 4s 330ms (100.0%) 
     WiFi Sleep time:  4m 4s 331ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 4s 331ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 18.5, actual drain: 0
    Screen: 13.9 Excluded from smearing
    Uid 1000: 1.35 ( cpu=1.35 sensor=0.00679 ) Excluded from smearing
    Uid u0a200: 0.972 ( cpu=0.972 ) Including smearing: 1.15 ( proportional=0.179 )
    Cell standby: 0.747 ( radio=0.747 ) Excluded from smearing
    Idle: 0.666 Excluded from smearing
    Uid 0: 0.532 ( cpu=0.532 ) Excluded from smearing
    Uid u0a47: 0.167 ( cpu=0.167 sensor=0.000136 ) Excluded from smearing
    Uid u0a22: 0.0398 ( cpu=0.0228 sensor=0.0170 ) Excluded from smearing
    Uid u0a127: 0.0366 ( cpu=0.0366 ) Including smearing: 0.0433 ( proportional=0.00674 )
    Uid 2000: 0.0309 ( cpu=0.0309 ) Excluded from smearing
    Uid u0a68: 0.0244 ( cpu=0.0244 ) Including smearing: 0.0289 ( proportional=0.00450 )
    Uid 1036: 0.0134 ( cpu=0.0134 ) Excluded from smearing
    Uid u0a35: 0.00450 ( cpu=0.00450 ) Including smearing: 0.00533 ( proportional=0.000829 )
    Uid u0a40: 0.00158 ( cpu=0.00158 ) Including smearing: 0.00187 ( proportional=0.000291 )
    Uid u0a25: 0.00108 ( cpu=0.00108 ) Including smearing: 0.00128 ( proportional=0.000198 )
    Uid 1001: 0.000700 ( cpu=0.000700 ) Excluded from smearing
    Uid u0a75: 0.000477 ( cpu=0.000477 ) Including smearing: 0.000565 ( proportional=0.0000879 )
    Uid 1027: 0.000367 ( cpu=0.000367 ) Excluded from smearing
    Wifi: 0.000332 ( cpu=0.000332 ) Including smearing: 0.000394 ( proportional=0.0000612 )
    Uid u0a114: 0.000257 ( cpu=0.000257 ) Including smearing: 0.000304 ( proportional=0.0000473 )
    Uid u0a8: 0.000109 ( cpu=0.000109 ) Including smearing: 0.000130 ( proportional=0.0000202 )
    Uid u0a61: 0.0000845 ( cpu=0.0000845 ) Including smearing: 0.000100 ( proportional=0.0000156 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 4s 203ms realtime (2 times)
    XO_shutdown.MPSS: 4m 2s 716ms realtime (102 times)
    XO_shutdown.ADSP: 4m 4s 263ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 4s 297ms realtime (0 times)
    Fg Service for: 4m 4s 298ms 
    Total running: 4m 4s 298ms 
    Total cpu time: u=25s 593ms s=11s 53ms 
    Total cpu time per freq: 150 0 0 0 1200 800 650 790 410 480 290 200 660 260 380 370 300 290 390 280 230 8570 120 300 270 230 170 160 130 130 230 190 220 400 80 110 170 150 190 190 360 210 310 110 100 70 150 250 140 90 40 50 13210
    Proc android.hardware.memtrack@1.0-service:
      CPU: 40ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 470ms usr + 1s 660ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 40ms usr + 140ms krn ; 0ms fg
    Proc system:
      CPU: 24s 310ms usr + 7s 310ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 180ms usr + 390ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 38s 210ms realtime (27 times)
    Foreground services: 22s 181ms realtime (26 times)
    Top for: 3m 41s 642ms 
    Fg Service for: 22s 181ms 
    Cached for: 67ms 
    Total running: 4m 3s 890ms 
    Total cpu time: u=22s 577ms s=3s 844ms 
    Total cpu time per freq: 10 0 0 0 580 310 290 200 190 120 110 90 800 220 320 260 270 250 310 240 230 4040 1250 1210 950 840 770 600 550 400 490 220 240 1170 170 140 220 70 120 990 240 320 200 110 80 200 170 200 160 190 80 30 4910
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 23s 450ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

