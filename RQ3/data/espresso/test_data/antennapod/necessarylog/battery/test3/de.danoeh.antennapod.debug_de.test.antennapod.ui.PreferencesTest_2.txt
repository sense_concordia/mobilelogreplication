Daily stats:
  Current start time: 2018-11-03-01-23-47
  Next min deadline: 2018-11-04-01-00-00
  Next max deadline: 2018-11-04-03-00-00
  Current daily steps:
    Discharge total time: 1d 11h 9m 25s 900ms  (from 12 steps)
    Discharge screen on time: 17h 8m 29s 100ms  (from 11 steps)
    Charge total time: 11h 13m 21s 800ms  (from 10 steps)
    Charge screen doze time: 11h 13m 21s 800ms  (from 10 steps)
  Daily from 2018-11-02-07-50-50 to 2018-11-03-01-23-47:
    Discharge total time: 1d 6h 43m 29s 200ms  (from 23 steps)
    Discharge screen off time: 8d 23h 0m 42s 600ms  (from 1 steps)
    Discharge screen on time: 21h 6m 38s 600ms  (from 21 steps)
    Charge total time: 10h 25m 42s 100ms  (from 1 steps)
    Charge screen on time: 10h 25m 42s 100ms  (from 1 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 3s 629ms (100.0%) realtime, 4m 3s 629ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 3s 741ms realtime, 4m 3s 741ms uptime
  Discharge: 18.5 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 18.5 mAh
  Start clock time: 2018-11-03-21-48-02
  Screen on: 4m 3s 629ms (100.0%) 0x, Interactive: 4m 3s 629ms (100.0%)
  Screen brightnesses:
    dark 4m 3s 629ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 3s 629ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 3s 629ms (100.0%) 
     Cellular Sleep time:  4m 1s 692ms (99.2%)
     Cellular Idle time:   497ms (0.2%)
     Cellular Rx time:     1s 442ms (0.6%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 3s 629ms (100.0%) 
     Wifi supplicant states:
       completed 4m 3s 629ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 3s 629ms (100.0%) 
     WiFi Sleep time:  4m 3s 631ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 3s 632ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 19.9, actual drain: 0-35.2
    Screen: 13.9 Excluded from smearing
    Uid 1000: 1.95 ( cpu=1.95 sensor=0.00677 ) Excluded from smearing
    Uid u0a200: 0.958 ( cpu=0.958 ) Including smearing: 1.18 ( proportional=0.219 )
    Uid 0: 0.774 ( cpu=0.774 ) Excluded from smearing
    Cell standby: 0.744 ( radio=0.744 ) Excluded from smearing
    Idle: 0.664 Excluded from smearing
    Uid u0a61: 0.594 ( cpu=0.594 ) Including smearing: 0.730 ( proportional=0.136 )
    Uid u0a47: 0.152 ( cpu=0.152 sensor=0.000135 ) Excluded from smearing
    Uid u0a127: 0.0358 ( cpu=0.0358 ) Including smearing: 0.0440 ( proportional=0.00820 )
    Uid 2000: 0.0337 ( cpu=0.0337 ) Excluded from smearing
    Uid u0a22: 0.0315 ( cpu=0.0315 ) Excluded from smearing
    Uid u0a68: 0.0238 ( cpu=0.0238 ) Including smearing: 0.0293 ( proportional=0.00546 )
    Uid 1036: 0.0146 ( cpu=0.0146 ) Excluded from smearing
    Uid u0a35: 0.00601 ( cpu=0.00601 ) Including smearing: 0.00739 ( proportional=0.00138 )
    Uid u0a25: 0.00184 ( cpu=0.00184 ) Including smearing: 0.00226 ( proportional=0.000422 )
    Uid u0a114: 0.00169 ( cpu=0.00169 ) Including smearing: 0.00207 ( proportional=0.000386 )
    Uid u0a40: 0.00167 ( cpu=0.00167 ) Including smearing: 0.00205 ( proportional=0.000381 )
    Uid 1001: 0.00148 ( cpu=0.00148 ) Excluded from smearing
    Wifi: 0.000699 ( cpu=0.000699 ) Including smearing: 0.000859 ( proportional=0.000160 )
    Bluetooth: 0.000548 ( cpu=0.000548 ) Including smearing: 0.000673 ( proportional=0.000125 )
    Uid 1027: 0.000506 ( cpu=0.000506 ) Excluded from smearing
    Uid u0a75: 0.000504 ( cpu=0.000504 ) Including smearing: 0.000619 ( proportional=0.000115 )
    Uid u0a8: 0.000107 ( cpu=0.000107 ) Including smearing: 0.000131 ( proportional=0.0000244 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 3s 485ms realtime (5 times)
    XO_shutdown.MPSS: 4m 1s 442ms realtime (100 times)
    XO_shutdown.ADSP: 4m 3s 557ms realtime (0 times)
    wlan.Active: 59ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 3s 598ms realtime (0 times)
    Fg Service for: 4m 3s 599ms 
    Total running: 4m 3s 599ms 
    Total cpu time: u=33s 754ms s=20s 234ms 
    Total cpu time per freq: 1940 30 20 10 160 1500 4730 2750 1140 440 290 240 760 260 420 390 410 360 360 370 160 7900 180 250 300 200 170 260 190 200 340 210 150 360 130 110 200 170 60 140 280 320 200 120 80 110 100 240 80 130 50 10 13820
    Proc android.hardware.memtrack@1.0-service:
      CPU: 30ms usr + 40ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 40ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 740ms usr + 1s 670ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 60ms usr + 230ms krn ; 0ms fg
    Proc system:
      CPU: 25s 950ms usr + 8s 390ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 130ms usr + 330ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 37s 845ms realtime (27 times)
    Foreground services: 21s 753ms realtime (26 times)
    Top for: 3m 41s 352ms 
    Fg Service for: 21s 753ms 
    Cached for: 47ms 
    Total running: 4m 3s 152ms 
    Total cpu time: u=22s 460ms s=3s 920ms 
    Total cpu time per freq: 60 10 0 0 10 200 440 600 260 160 230 240 870 280 340 420 310 260 350 280 210 4060 1450 1100 740 740 670 420 350 250 370 430 370 880 130 270 160 320 120 840 290 270 130 220 210 210 110 230 120 40 60 40 5180
    Proc de.danoeh.antennapod.debug:
      CPU: 1s 860ms usr + 350ms krn ; 23s 380ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

