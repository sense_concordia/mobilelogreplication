Daily stats:
  Current start time: 2018-11-03-01-23-47
  Next min deadline: 2018-11-04-01-00-00
  Next max deadline: 2018-11-04-03-00-00
  Current daily steps:
    Discharge total time: 1d 4h 1m 12s 500ms  (from 21 steps)
    Discharge screen on time: 17h 45m 16s 600ms  (from 20 steps)
    Charge total time: 11h 13m 21s 800ms  (from 10 steps)
    Charge screen doze time: 11h 13m 21s 800ms  (from 10 steps)
  Daily from 2018-11-02-07-50-50 to 2018-11-03-01-23-47:
    Discharge total time: 1d 6h 43m 29s 200ms  (from 23 steps)
    Discharge screen off time: 8d 23h 0m 42s 600ms  (from 1 steps)
    Discharge screen on time: 21h 6m 38s 600ms  (from 21 steps)
    Charge total time: 10h 25m 42s 100ms  (from 1 steps)
    Charge screen on time: 10h 25m 42s 100ms  (from 1 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 4m 3s 964ms (100.0%) realtime, 4m 3s 964ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 4m 4s 67ms realtime, 4m 4s 68ms uptime
  Discharge: 13.6 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 13.6 mAh
  Start clock time: 2018-11-03-23-33-49
  Screen on: 4m 3s 964ms (100.0%) 0x, Interactive: 4m 3s 964ms (100.0%)
  Screen brightnesses:
    dark 4m 3s 964ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 4m 3s 964ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  4m 3s 964ms (100.0%) 
     Cellular Sleep time:  4m 2s 203ms (99.3%)
     Cellular Idle time:   449ms (0.2%)
     Cellular Rx time:     1s 313ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 4m 3s 964ms (100.0%) 
     Wifi supplicant states:
       completed 4m 3s 964ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 4m 3s 964ms (100.0%) 
     WiFi Sleep time:  4m 3s 965ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  4m 3s 965ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 19.9, actual drain: 0
    Screen: 13.9 Excluded from smearing
    Uid 1000: 1.98 ( cpu=1.97 sensor=0.00678 ) Excluded from smearing
    Uid u0a200: 0.971 ( cpu=0.971 ) Including smearing: 1.19 ( proportional=0.222 )
    Uid 0: 0.769 ( cpu=0.769 ) Excluded from smearing
    Cell standby: 0.745 ( radio=0.745 ) Excluded from smearing
    Idle: 0.665 Excluded from smearing
    Uid u0a61: 0.605 ( cpu=0.605 ) Including smearing: 0.743 ( proportional=0.139 )
    Uid u0a47: 0.143 ( cpu=0.143 sensor=0.000136 ) Excluded from smearing
    Uid u0a127: 0.0365 ( cpu=0.0365 ) Including smearing: 0.0449 ( proportional=0.00836 )
    Uid 2000: 0.0359 ( cpu=0.0359 ) Excluded from smearing
    Uid u0a22: 0.0290 ( cpu=0.0290 ) Excluded from smearing
    Uid u0a68: 0.0247 ( cpu=0.0247 ) Including smearing: 0.0304 ( proportional=0.00566 )
    Uid 1036: 0.0140 ( cpu=0.0140 ) Excluded from smearing
    Uid u0a35: 0.00494 ( cpu=0.00494 ) Including smearing: 0.00607 ( proportional=0.00113 )
    Uid 1001: 0.00230 ( cpu=0.00230 ) Excluded from smearing
    Uid u0a40: 0.00176 ( cpu=0.00176 ) Including smearing: 0.00217 ( proportional=0.000404 )
    Uid u0a25: 0.00131 ( cpu=0.00131 ) Including smearing: 0.00161 ( proportional=0.000300 )
    Uid u0a114: 0.000826 ( cpu=0.000826 ) Including smearing: 0.00102 ( proportional=0.000189 )
    Uid u0a75: 0.000470 ( cpu=0.000470 ) Including smearing: 0.000577 ( proportional=0.000108 )
    Wifi: 0.000358 ( cpu=0.000358 ) Including smearing: 0.000440 ( proportional=0.0000820 )
    Uid 1027: 0.000326 ( cpu=0.000326 ) Excluded from smearing
    Bluetooth: 0.000261 ( cpu=0.000261 ) Including smearing: 0.000321 ( proportional=0.0000598 )
    Uid u0a8: 0.000107 ( cpu=0.000107 ) Including smearing: 0.000131 ( proportional=0.0000244 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 4m 3s 808ms realtime (4 times)
    XO_shutdown.MPSS: 4m 1s 832ms realtime (99 times)
    XO_shutdown.ADSP: 4m 3s 831ms realtime (0 times)
    wlan.Active: 47ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 25 button, 60 touch
    Wake lock *alarm* realtime
    Sensor 7: 4m 3s 942ms realtime (0 times)
    Fg Service for: 4m 3s 942ms 
    Total running: 4m 3s 942ms 
    Total cpu time: u=33s 924ms s=20s 686ms 
    Total cpu time per freq: 1790 40 30 10 130 1420 4720 2640 990 460 300 170 680 300 310 380 360 290 460 330 300 7670 210 440 370 210 230 160 110 120 520 350 90 360 50 200 190 100 100 200 270 240 130 120 130 90 140 100 80 40 10 40 13790
    Proc android.hardware.memtrack@1.0-service:
      CPU: 30ms usr + 60ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 60ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 2s 570ms usr + 2s 540ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 70ms usr + 360ms krn ; 0ms fg
    Proc system:
      CPU: 39s 480ms usr + 12s 790ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 200ms usr + 500ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wake lock *alarm* realtime
    Wake lock *launch* realtime
    Foreground activities: 3m 38s 106ms realtime (27 times)
    Foreground services: 21s 727ms realtime (26 times)
    Top for: 3m 41s 619ms 
    Fg Service for: 21s 727ms 
    Cached for: 62ms 
    Total running: 4m 3s 408ms 
    Total cpu time: u=22s 816ms s=3s 834ms 
    Total cpu time per freq: 70 0 0 10 20 130 480 640 310 200 170 150 880 180 390 310 370 250 510 270 190 4030 1390 1220 860 710 780 630 310 330 520 380 390 930 150 370 160 180 60 810 290 400 270 280 90 130 190 150 150 90 10 30 5110
    Proc de.danoeh.antennapod.debug:
      CPU: 16s 440ms usr + 3s 60ms krn ; 23s 740ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Wakeup alarm *walarm*:de.danoeh.antennapod.debug/de.danoeh.antennapod.core.receiver.FeedUpdateReceiver: 0 times

