Daily stats:
  Current start time: 2018-11-05-03-07-57
  Next min deadline: 2018-11-06-01-00-00
  Next max deadline: 2018-11-06-03-00-00
  Current daily steps:
    Discharge total time: 16h 39m 59s 400ms  (from 29 steps)
    Discharge screen on time: 16h 39m 59s 400ms  (from 29 steps)
    Charge total time: 1d 4h 2m 57s 0ms  (from 13 steps)
    Charge screen on time: 1d 10h 10m 50s 600ms  (from 10 steps)
  Daily from 2018-11-04-04-34-30 to 2018-11-05-03-07-57:
    Discharge total time: 16h 46m 42s 300ms  (from 51 steps)
    Discharge screen on time: 16h 48m 9s 100ms  (from 50 steps)
    Charge total time: 9h 29m 26s 600ms  (from 36 steps)
    Charge screen on time: 9h 29m 26s 600ms  (from 36 steps)
  Daily from 2018-11-03-01-23-47 to 2018-11-04-04-34-30:
    Discharge total time: 1d 3h 15m 9s 600ms  (from 24 steps)
    Discharge screen on time: 18h 17m 33s 900ms  (from 23 steps)
    Charge total time: 4d 10h 1m 43s 500ms  (from 23 steps)
    Charge screen on time: 7d 10h 57m 23s 300ms  (from 13 steps)
    Charge screen doze time: 11h 13m 21s 800ms  (from 10 steps)
  Daily from 2018-11-02-07-50-50 to 2018-11-03-01-23-47:
    Discharge total time: 1d 6h 43m 29s 200ms  (from 23 steps)
    Discharge screen off time: 8d 23h 0m 42s 600ms  (from 1 steps)
    Discharge screen on time: 21h 6m 38s 600ms  (from 21 steps)
    Charge total time: 10h 25m 42s 100ms  (from 1 steps)
    Charge screen on time: 10h 25m 42s 100ms  (from 1 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 15s 968ms (100.0%) realtime, 1m 15s 968ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 15s 999ms realtime, 1m 16s 0ms uptime
  Discharge: 10.9 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 10.9 mAh
  Start clock time: 2018-11-05-20-10-36
  Screen on: 1m 15s 968ms (100.0%) 0x, Interactive: 1m 15s 968ms (100.0%)
  Screen brightnesses:
    dark 1m 15s 968ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 15s 968ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP): (no activity)
     Cellular Sleep time:  1m 15s 935ms (100.0%)
     Cellular Idle time:   34ms (0.0%)
     Cellular Rx time:     0ms (0.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states: (no activity)
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 15s 969ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 15s 969ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.32, actual drain: 0
    Screen: 4.33 Excluded from smearing
    Uid u0a200: 0.735 ( cpu=0.735 ) Including smearing: 0.985 ( proportional=0.250 )
    Uid 1000: 0.522 ( cpu=0.520 sensor=0.00211 ) Excluded from smearing
    Uid 0: 0.436 ( cpu=0.436 ) Excluded from smearing
    Uid 1041: 0.435 ( cpu=0.435 ) Excluded from smearing
    Idle: 0.207 Excluded from smearing
    Uid u0a22: 0.130 ( cpu=0.130 ) Excluded from smearing
    Uid 1013: 0.129 ( cpu=0.129 ) Excluded from smearing
    Uid u0a61: 0.0909 ( cpu=0.0909 ) Including smearing: 0.122 ( proportional=0.0309 )
    Uid u0a70: 0.0696 ( cpu=0.0687 sensor=0.000887 ) Including smearing: 0.0932 ( proportional=0.0237 )
    Uid u0a47: 0.0589 ( cpu=0.0588 sensor=0.0000422 ) Excluded from smearing
    Uid 2000: 0.0455 ( cpu=0.0455 ) Excluded from smearing
    Uid 1046: 0.0413 ( cpu=0.0413 ) Excluded from smearing
    Uid 1036: 0.0246 ( cpu=0.0246 ) Excluded from smearing
    Uid 1040: 0.0244 ( cpu=0.0244 ) Excluded from smearing
    Uid u0a40: 0.0208 ( cpu=0.0208 ) Including smearing: 0.0279 ( proportional=0.00708 )
    Uid 1019: 0.00873 ( cpu=0.00873 ) Excluded from smearing
    Uid u0a68: 0.00168 ( cpu=0.00168 ) Including smearing: 0.00225 ( proportional=0.000570 )
    Uid 1001: 0.00126 ( cpu=0.00126 ) Excluded from smearing
    Uid u0a25: 0.000609 ( cpu=0.000609 ) Including smearing: 0.000816 ( proportional=0.000207 )
    Uid 1027: 0.000113 ( cpu=0.000113 ) Excluded from smearing
    Uid u0a127: 0.000113 ( cpu=0.000113 ) Including smearing: 0.000152 ( proportional=0.0000385 )

  Resource Power Manager Stats
    XO_shutdown.MPSS: 1m 15s 854ms realtime (1 times)
    XO_shutdown.ADSP: 31s 882ms realtime (8 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 15s 961ms realtime (0 times)
    Fg Service for: 1m 15s 961ms 
    Total running: 1m 15s 961ms 
    Total cpu time: u=8s 76ms s=6s 197ms 
    Total cpu time per freq: 3290 480 230 110 100 140 110 70 50 80 60 60 1010 100 60 90 100 60 90 30 60 3120 90 300 80 80 20 40 30 20 30 20 50 90 10 50 50 30 20 60 90 120 80 30 40 20 20 60 70 20 10 0 2160
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 30ms usr + 60ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 380ms usr + 600ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 2s 900ms usr + 1s 840ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 20ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 150ms usr + 200ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 170ms usr + 270ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 392ms realtime (9 times)
    Foreground activities: 1m 8s 708ms realtime (6 times)
    Foreground services: 5s 948ms realtime (7 times)
    Top for: 1m 9s 506ms 
    Fg Service for: 5s 948ms 
    Cached for: 33ms 
    Total running: 1m 15s 487ms 
    Total cpu time: u=16s 196ms s=3s 100ms 
    Total cpu time per freq: 120 0 0 10 80 110 120 50 80 80 20 30 530 50 70 130 160 90 200 110 120 2660 100 540 220 160 170 120 150 70 90 30 100 420 70 230 150 140 60 290 500 600 410 280 290 260 190 310 210 120 60 0 8560
    Proc de.danoeh.antennapod.debug:
      CPU: 0ms usr + 0ms krn ; 18s 190ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 37s 764ms uptime
        Starts: 6, launches: 6

