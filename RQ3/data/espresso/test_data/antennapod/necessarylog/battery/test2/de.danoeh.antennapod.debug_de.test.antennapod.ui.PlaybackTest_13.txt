Daily stats:
  Current start time: 2018-11-05-03-07-57
  Next min deadline: 2018-11-06-01-00-00
  Next max deadline: 2018-11-06-03-00-00
  Current daily steps:
    Discharge total time: 16h 38m 20s 900ms  (from 30 steps)
    Discharge screen on time: 16h 38m 20s 900ms  (from 30 steps)
    Charge total time: 1d 4h 2m 57s 0ms  (from 13 steps)
    Charge screen on time: 1d 10h 10m 50s 600ms  (from 10 steps)
  Daily from 2018-11-04-04-34-30 to 2018-11-05-03-07-57:
    Discharge total time: 16h 46m 42s 300ms  (from 51 steps)
    Discharge screen on time: 16h 48m 9s 100ms  (from 50 steps)
    Charge total time: 9h 29m 26s 600ms  (from 36 steps)
    Charge screen on time: 9h 29m 26s 600ms  (from 36 steps)
  Daily from 2018-11-03-01-23-47 to 2018-11-04-04-34-30:
    Discharge total time: 1d 3h 15m 9s 600ms  (from 24 steps)
    Discharge screen on time: 18h 17m 33s 900ms  (from 23 steps)
    Charge total time: 4d 10h 1m 43s 500ms  (from 23 steps)
    Charge screen on time: 7d 10h 57m 23s 300ms  (from 13 steps)
    Charge screen doze time: 11h 13m 21s 800ms  (from 10 steps)
  Daily from 2018-11-02-07-50-50 to 2018-11-03-01-23-47:
    Discharge total time: 1d 6h 43m 29s 200ms  (from 23 steps)
    Discharge screen off time: 8d 23h 0m 42s 600ms  (from 1 steps)
    Discharge screen on time: 21h 6m 38s 600ms  (from 21 steps)
    Charge total time: 10h 25m 42s 100ms  (from 1 steps)
    Charge screen on time: 10h 25m 42s 100ms  (from 1 steps)
  Daily from 2018-11-01-07-28-51 to 2018-11-02-07-50-50:
    Discharge total time: 13h 30m 9s 800ms  (from 33 steps)
    Discharge screen on time: 13h 30m 9s 800ms  (from 33 steps)
    Charge total time: 11h 33m 12s 300ms  (from 13 steps)
    Charge screen on time: 1d 0h 55m 51s 400ms  (from 1 steps)
    Charge screen doze time: 10h 10m 59s 700ms  (from 11 steps)
  Daily from 2018-10-31-03-05-14 to 2018-11-01-07-28-51:
    Discharge total time: 13h 49m 23s 900ms  (from 63 steps)
    Discharge screen on time: 13h 49m 23s 900ms  (from 63 steps)
    Charge total time: 1d 6h 54m 20s 100ms  (from 71 steps)
    Charge screen off time: 8h 36m 22s 900ms  (from 5 steps)
    Charge screen on time: 2d 9h 11m 3s 100ms  (from 33 steps)
    Charge screen doze time: 6h 43m 34s 300ms  (from 28 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 16s 604ms (99.9%) realtime, 1m 16s 604ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 16s 645ms realtime, 1m 16s 645ms uptime
  Discharge: 5.83 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 5.83 mAh
  Start clock time: 2018-11-05-20-26-22
  Screen on: 1m 16s 604ms (100.0%) 0x, Interactive: 1m 16s 604ms (100.0%)
  Screen brightnesses:
    dark 1m 16s 604ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 16s 604ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP): (no activity)
     Cellular Sleep time:  1m 16s 555ms (99.9%)
     Cellular Idle time:   50ms (0.1%)
     Cellular Rx time:     0ms (0.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states: (no activity)
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 16s 605ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 16s 606ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.56, actual drain: 0
    Screen: 4.37 Excluded from smearing
    Uid u0a200: 0.749 ( cpu=0.749 ) Including smearing: 1.03 ( proportional=0.281 )
    Uid 1000: 0.539 ( cpu=0.537 sensor=0.00213 ) Excluded from smearing
    Uid 0: 0.435 ( cpu=0.435 ) Excluded from smearing
    Uid 1041: 0.422 ( cpu=0.422 ) Excluded from smearing
    Uid 1036: 0.235 ( cpu=0.235 ) Excluded from smearing
    Idle: 0.209 Excluded from smearing
    Uid 1013: 0.128 ( cpu=0.128 ) Excluded from smearing
    Uid u0a22: 0.127 ( cpu=0.127 ) Excluded from smearing
    Uid u0a61: 0.0769 ( cpu=0.0769 ) Including smearing: 0.106 ( proportional=0.0288 )
    Uid u0a70: 0.0704 ( cpu=0.0695 sensor=0.000875 ) Including smearing: 0.0967 ( proportional=0.0264 )
    Uid u0a47: 0.0566 ( cpu=0.0566 sensor=0.0000425 ) Excluded from smearing
    Uid 2000: 0.0439 ( cpu=0.0439 ) Excluded from smearing
    Uid 1046: 0.0399 ( cpu=0.0399 ) Excluded from smearing
    Uid 1040: 0.0248 ( cpu=0.0248 ) Excluded from smearing
    Uid u0a40: 0.0193 ( cpu=0.0193 ) Including smearing: 0.0266 ( proportional=0.00724 )
    Uid 1019: 0.00887 ( cpu=0.00887 ) Excluded from smearing
    Uid u0a68: 0.00219 ( cpu=0.00219 ) Including smearing: 0.00301 ( proportional=0.000821 )
    Uid 1001: 0.00131 ( cpu=0.00131 ) Excluded from smearing
    Uid u0a25: 0.000357 ( cpu=0.000357 ) Including smearing: 0.000491 ( proportional=0.000134 )
    Uid u0a127: 0.000249 ( cpu=0.000249 ) Including smearing: 0.000343 ( proportional=0.0000935 )

  Resource Power Manager Stats
    XO_shutdown.MPSS: 1m 16s 455ms realtime (2 times)
    XO_shutdown.ADSP: 31s 562ms realtime (8 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 6 button, 30 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 16s 596ms realtime (0 times)
    Fg Service for: 1m 16s 597ms 
    Total running: 1m 16s 597ms 
    Total cpu time: u=8s 314ms s=6s 500ms 
    Total cpu time per freq: 3270 460 130 130 150 170 100 170 60 70 40 70 1130 110 70 110 100 60 120 100 80 3300 160 350 100 60 200 40 50 20 50 20 10 110 10 80 70 50 10 90 150 70 70 20 30 60 60 40 10 60 20 0 2070
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 80ms usr + 200ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 860ms usr + 1s 290ms krn ; 0ms fg
    Proc android.hidl.allocator@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc chre:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 7s 860ms usr + 4s 350ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 30ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 240ms usr + 470ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 250ms usr + 400ms krn ; 0ms fg
    Proc android.hardware.contexthub@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a200:
    Wake lock AudioMix realtime
    Wake lock *launch* realtime
    Wake lock android.media.MediaPlayer realtime
    Audio: 21s 450ms realtime (9 times)
    Foreground activities: 1m 9s 441ms realtime (6 times)
    Foreground services: 5s 870ms realtime (7 times)
    Top for: 1m 10s 218ms 
    Fg Service for: 5s 870ms 
    Cached for: 36ms 
    Total running: 1m 16s 124ms 
    Total cpu time: u=16s 244ms s=3s 237ms 
    Total cpu time per freq: 100 10 10 0 60 100 90 120 60 40 60 50 340 70 40 80 110 50 100 130 60 2170 160 630 250 210 310 110 140 140 90 60 90 520 40 140 90 80 50 340 710 630 500 170 210 300 280 270 80 130 40 110 8840
    Proc de.danoeh.antennapod.debug:
      CPU: 1s 480ms usr + 370ms krn ; 18s 420ms fg
      1 starts
    Apk de.danoeh.antennapod.debug:
      Service de.danoeh.antennapod.core.service.playback.PlaybackService:
        Created for: 38s 613ms uptime
        Starts: 6, launches: 6

