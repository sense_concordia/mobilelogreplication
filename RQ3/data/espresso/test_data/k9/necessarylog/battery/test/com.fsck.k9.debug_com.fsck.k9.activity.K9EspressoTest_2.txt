Daily stats:
  Current start time: 2018-10-25-07-06-28
  Next min deadline: 2018-10-26-01-00-00
  Next max deadline: 2018-10-26-03-00-00
  Current daily steps:
    Discharge total time: 18h 29m 56s 100ms  (from 19 steps)
    Discharge screen on time: 18h 29m 56s 100ms  (from 19 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 45s 730ms (99.8%) realtime, 1m 45s 730ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 45s 889ms realtime, 1m 45s 889ms uptime
  Discharge: 7.41 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 7.41 mAh
  Start clock time: 2018-10-25-22-33-06
  Screen on: 1m 45s 730ms (100.0%) 0x, Interactive: 1m 45s 730ms (100.0%)
  Screen brightnesses:
    dark 1m 45s 730ms (100.0%)
  Total full wakelock time: 3s 400ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 45s 730ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 45s 730ms (100.0%) 
     Cellular Sleep time:  1m 44s 660ms (99.0%)
     Cellular Idle time:   292ms (0.3%)
     Cellular Rx time:     779ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 45s 730ms (100.0%) 
     Wifi supplicant states:
       completed 1m 45s 730ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 45s 730ms (100.0%) 
     WiFi Sleep time:  1m 45s 731ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 45s 731ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.72, actual drain: 0
    Screen: 6.03 Excluded from smearing
    Uid 1000: 0.562 ( cpu=0.559 sensor=0.00294 ) Excluded from smearing
    Uid 0: 0.359 ( cpu=0.359 ) Excluded from smearing
    Cell standby: 0.323 ( radio=0.323 ) Excluded from smearing
    Idle: 0.288 Excluded from smearing
    Uid 2000: 0.0661 ( cpu=0.0661 ) Excluded from smearing
    Uid u0a47: 0.0254 ( cpu=0.0254 sensor=0.0000587 ) Excluded from smearing
    Uid u0a22: 0.0178 ( cpu=0.0105 sensor=0.00733 ) Excluded from smearing
    Uid u0a200: 0.0135 ( cpu=0.0135 ) Including smearing: 0.0163 ( proportional=0.00288 )
    Uid u0a16: 0.00742 ( cpu=0.00742 ) Including smearing: 0.00901 ( proportional=0.00159 )
    Uid 1036: 0.00738 ( cpu=0.00738 ) Excluded from smearing
    Uid u0a68: 0.00697 ( cpu=0.00697 ) Including smearing: 0.00846 ( proportional=0.00149 )
    Uid u0a35: 0.00471 ( cpu=0.00471 ) Including smearing: 0.00571 ( proportional=0.00101 )
    Uid u0a127: 0.00413 ( cpu=0.00413 ) Including smearing: 0.00501 ( proportional=0.000884 )
    Uid u0a182: 0.00110 ( cpu=0.00110 ) Including smearing: 0.00133 ( proportional=0.000235 )
    Uid u0a114: 0.000476 ( cpu=0.000476 ) Including smearing: 0.000577 ( proportional=0.000102 )
    Wifi: 0.000396 ( cpu=0.000396 ) Including smearing: 0.000480 ( proportional=0.0000847 )
    Uid u0a40: 0.000255 ( cpu=0.000255 ) Including smearing: 0.000310 ( proportional=0.0000546 )
    Uid 1001: 0.000219 ( cpu=0.000219 ) Excluded from smearing
    Uid u0a25: 0.000146 ( cpu=0.000146 ) Including smearing: 0.000177 ( proportional=0.0000311 )
    Uid 1053: 0.000109 ( cpu=0.000109 ) Excluded from smearing
    Uid u0a61: 0.000109 ( cpu=0.000109 ) Including smearing: 0.000132 ( proportional=0.0000233 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 45s 622ms realtime (2 times)
    XO_shutdown.MPSS: 1m 44s 506ms realtime (44 times)
    XO_shutdown.ADSP: 1m 45s 664ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 15 button, 64 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 45s 678ms realtime (0 times)
    Fg Service for: 1m 45s 679ms 
    Total running: 1m 45s 679ms 
    Total cpu time: u=9s 597ms s=5s 750ms 
    Total cpu time per freq: 10 0 0 0 470 560 680 580 260 280 280 130 320 210 220 190 110 100 180 130 40 5320 30 150 90 70 60 70 50 30 40 40 30 150 30 30 10 40 40 90 380 260 140 90 60 50 50 60 30 90 10 20 1510
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.light@2.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 90ms usr + 1s 150ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 4s 30ms usr + 1s 110ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 170ms usr + 240ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 40ms usr + 20ms krn ; 0ms fg
  u0a182:
    Wake lock *launch* realtime
    Wake lock CoreService addWakeLock realtime
    Wake lock CoreReceiver getWakeLock realtime
    Wake lock WindowManager: 3s 400ms full (1 times) realtime
    Wake lock CoreService execute realtime
    Wake lock CoreService onStart realtime
    TOTAL wake: 3s 400ms full realtime
    Foreground activities: 1m 44s 478ms realtime (12 times)
    Foreground services: 290ms realtime (2 times)
    Top for: 1m 44s 954ms 
    Fg Service for: 290ms 
    Cached for: 37ms 
    Total running: 1m 45s 281ms 
    Total cpu time: u=22s 167ms s=4s 260ms 
    Total cpu time per freq: 0 0 0 0 10 30 50 40 30 120 120 110 200 170 240 160 160 100 210 210 70 1240 820 870 1000 680 590 310 270 200 120 120 60 870 80 20 10 40 30 640 330 240 120 100 70 110 220 130 70 200 10 50 4450
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc com.fsck.k9.debug:
      CPU: 0ms usr + 0ms krn ; 15s 840ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 36ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 8ms uptime
        Starts: 1, launches: 1
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

