Daily stats:
  Current start time: 2018-10-25-07-06-28
  Next min deadline: 2018-10-26-01-00-00
  Next max deadline: 2018-10-26-03-00-00
  Current daily steps:
    Discharge total time: 18h 33m 8s 400ms  (from 22 steps)
    Discharge screen on time: 18h 33m 8s 400ms  (from 22 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 45s 955ms (99.9%) realtime, 1m 45s 955ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 46s 73ms realtime, 1m 46s 72ms uptime
  Discharge: 3.23 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 3.23 mAh
  Start clock time: 2018-10-25-23-17-36
  Screen on: 1m 45s 955ms (100.0%) 0x, Interactive: 1m 45s 955ms (100.0%)
  Screen brightnesses:
    dark 1m 45s 955ms (100.0%)
  Total full wakelock time: 3s 386ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 45s 955ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 45s 955ms (100.0%) 
     Cellular Sleep time:  1m 44s 888ms (99.0%)
     Cellular Idle time:   290ms (0.3%)
     Cellular Rx time:     777ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 45s 955ms (100.0%) 
     Wifi supplicant states:
       completed 1m 45s 955ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 45s 955ms (100.0%) 
     WiFi Sleep time:  1m 45s 955ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 45s 955ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.77, actual drain: 0
    Screen: 6.04 Excluded from smearing
    Uid u0a182: 1.06 ( cpu=1.06 ) Including smearing: 1.25 ( proportional=0.191 )
    Uid 1000: 0.562 ( cpu=0.559 sensor=0.00294 ) Excluded from smearing
    Uid 0: 0.343 ( cpu=0.343 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.324 ( radio=0.324 ) Excluded from smearing
    Idle: 0.289 Excluded from smearing
    Uid 2000: 0.0597 ( cpu=0.0597 ) Excluded from smearing
    Uid u0a47: 0.0235 ( cpu=0.0234 sensor=0.0000588 ) Excluded from smearing
    Uid u0a22: 0.0169 ( cpu=0.00956 sensor=0.00735 ) Excluded from smearing
    Uid u0a200: 0.0135 ( cpu=0.0135 ) Including smearing: 0.0159 ( proportional=0.00242 )
    Uid 1036: 0.00755 ( cpu=0.00755 ) Excluded from smearing
    Uid u0a16: 0.00740 ( cpu=0.00740 ) Including smearing: 0.00873 ( proportional=0.00133 )
    Uid u0a68: 0.00707 ( cpu=0.00707 ) Including smearing: 0.00834 ( proportional=0.00127 )
    Uid u0a127: 0.00527 ( cpu=0.00527 ) Including smearing: 0.00622 ( proportional=0.000949 )
    Uid u0a35: 0.00176 ( cpu=0.00176 ) Including smearing: 0.00208 ( proportional=0.000317 )
    Uid u0a25: 0.00128 ( cpu=0.00128 ) Including smearing: 0.00151 ( proportional=0.000231 )
    Uid 1001: 0.000333 ( cpu=0.000333 ) Excluded from smearing
    Wifi: 0.000310 ( cpu=0.000310 ) Including smearing: 0.000365 ( proportional=0.0000557 )
    Uid u0a61: 0.000258 ( cpu=0.000258 ) Including smearing: 0.000304 ( proportional=0.0000464 )
    Uid u0a40: 0.000221 ( cpu=0.000221 ) Including smearing: 0.000261 ( proportional=0.0000398 )
    Uid u0a114: 0.000147 ( cpu=0.000147 ) Including smearing: 0.000173 ( proportional=0.0000264 )
    Uid 1027: 0.000110 ( cpu=0.000110 ) Excluded from smearing
    Uid 1053: 0.000110 ( cpu=0.000110 ) Excluded from smearing
    Uid u0a8: 0.000110 ( cpu=0.000110 ) Including smearing: 0.000130 ( proportional=0.0000198 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 45s 831ms realtime (2 times)
    XO_shutdown.MPSS: 1m 44s 653ms realtime (44 times)
    XO_shutdown.ADSP: 1m 45s 846ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 16 button, 64 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 45s 937ms realtime (0 times)
    Fg Service for: 1m 45s 937ms 
    Total running: 1m 45s 937ms 
    Total cpu time: u=9s 413ms s=5s 790ms 
    Total cpu time per freq: 40 0 0 10 410 660 610 540 290 370 170 140 300 110 150 150 100 120 140 70 70 5750 40 70 80 100 60 60 10 30 0 0 20 180 10 0 50 40 10 110 310 320 110 60 30 40 50 60 60 60 30 20 1500
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 270ms usr + 1s 470ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 30ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 4s 970ms usr + 1s 350ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 170ms usr + 310ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 20ms usr + 40ms krn ; 0ms fg
  u0a182:
    Wake lock *launch* realtime
    Wake lock CoreService addWakeLock realtime
    Wake lock CoreReceiver getWakeLock realtime
    Wake lock WindowManager: 3s 386ms full (1 times) realtime
    Wake lock CoreService execute realtime
    Wake lock CoreService onStart realtime
    TOTAL wake: 3s 386ms full realtime
    Foreground activities: 1m 44s 752ms realtime (12 times)
    Foreground services: 260ms realtime (2 times)
    Top for: 1m 45s 200ms 
    Fg Service for: 260ms 
    Cached for: 46ms 
    Total running: 1m 45s 506ms 
    Total cpu time: u=23s 759ms s=4s 899ms 
    Total cpu time per freq: 0 0 0 0 0 50 20 50 60 170 160 90 200 140 140 270 100 160 200 200 100 950 840 810 970 960 550 460 330 280 160 110 20 900 0 0 10 30 40 650 400 170 70 110 140 80 70 210 210 130 20 60 4360
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc com.fsck.k9.debug:
      CPU: 0ms usr + 0ms krn ; 15s 790ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 28ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 17ms uptime
        Starts: 1, launches: 1
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

