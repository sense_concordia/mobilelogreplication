Daily stats:
  Current start time: 2018-10-24-03-38-25
  Next min deadline: 2018-10-25-01-00-00
  Next max deadline: 2018-10-25-03-00-00
  Current daily steps:
    Discharge total time: 1d 3h 26m 47s 600ms  (from 26 steps)
    Discharge screen on time: 13h 52m 41s 900ms  (from 18 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 45s 988ms (99.9%) realtime, 1m 45s 988ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 46s 95ms realtime, 1m 46s 95ms uptime
  Discharge: 8.70 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.70 mAh
  Start clock time: 2018-10-24-22-32-06
  Screen on: 1m 45s 988ms (100.0%) 0x, Interactive: 1m 45s 988ms (100.0%)
  Screen brightnesses:
    dark 1m 45s 988ms (100.0%)
  Total full wakelock time: 3s 393ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 45s 988ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 45s 988ms (100.0%) 
     Cellular Sleep time:  1m 45s 186ms (99.2%)
     Cellular Idle time:   282ms (0.3%)
     Cellular Rx time:     520ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 45s 988ms (100.0%) 
     Wifi supplicant states:
       completed 1m 45s 988ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 45s 988ms (100.0%) 
     WiFi Sleep time:  1m 45s 988ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 45s 988ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 7.60, actual drain: 0
    Screen: 6.05 Excluded from smearing
    Uid 1000: 0.461 ( cpu=0.458 sensor=0.00294 ) Excluded from smearing
    Uid 0: 0.325 ( cpu=0.325 ) Excluded from smearing
    Cell standby: 0.324 ( radio=0.324 ) Excluded from smearing
    Idle: 0.289 Excluded from smearing
    Uid 2000: 0.0711 ( cpu=0.0711 ) Excluded from smearing
    Uid u0a47: 0.0253 ( cpu=0.0252 sensor=0.0000589 ) Excluded from smearing
    Uid u0a181: 0.0189 ( cpu=0.0189 ) Including smearing: 0.0225 ( proportional=0.00363 )
    Uid u0a22: 0.00976 ( cpu=0.00976 ) Excluded from smearing
    Uid u0a68: 0.00768 ( cpu=0.00768 ) Including smearing: 0.00915 ( proportional=0.00147 )
    Uid u0a16: 0.00763 ( cpu=0.00763 ) Including smearing: 0.00910 ( proportional=0.00146 )
    Uid 1036: 0.00742 ( cpu=0.00742 ) Excluded from smearing
    Uid u0a127: 0.00465 ( cpu=0.00465 ) Including smearing: 0.00554 ( proportional=0.000892 )
    Uid u0a35: 0.00217 ( cpu=0.00217 ) Including smearing: 0.00259 ( proportional=0.000417 )
    Uid u0a25: 0.00110 ( cpu=0.00110 ) Including smearing: 0.00131 ( proportional=0.000210 )
    Uid u0a182: 0.000949 ( cpu=0.000949 ) Including smearing: 0.00113 ( proportional=0.000182 )
    Uid u0a114: 0.000815 ( cpu=0.000815 ) Including smearing: 0.000972 ( proportional=0.000156 )
    Uid 1001: 0.000565 ( cpu=0.000565 ) Excluded from smearing
    Wifi: 0.000527 ( cpu=0.000527 ) Including smearing: 0.000629 ( proportional=0.000101 )
    Uid u0a40: 0.000145 ( cpu=0.000145 ) Including smearing: 0.000173 ( proportional=0.0000279 )
    Uid u0a61: 0.000145 ( cpu=0.000145 ) Including smearing: 0.000173 ( proportional=0.0000279 )
    Uid 1053: 0.000109 ( cpu=0.000109 ) Excluded from smearing
    Uid u0a8: 0.000109 ( cpu=0.000109 ) Including smearing: 0.000129 ( proportional=0.0000208 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 45s 867ms realtime (2 times)
    XO_shutdown.MPSS: 1m 44s 929ms realtime (46 times)
    XO_shutdown.ADSP: 1m 45s 883ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 16 button, 64 touch
    Wake lock *alarm* realtime
    Sensor 7: 1m 45s 970ms realtime (0 times)
    Fg Service for: 1m 45s 970ms 
    Total running: 1m 45s 970ms 
    Total cpu time: u=8s 120ms s=4s 397ms 
    Total cpu time per freq: 10 0 10 0 190 240 350 600 370 230 150 160 260 120 140 120 120 140 100 100 80 5080 20 50 90 70 30 90 20 50 10 0 40 170 0 30 40 20 10 70 260 240 60 30 20 20 60 40 20 40 20 10 1500
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 870ms usr + 880ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 2s 900ms usr + 1s 120ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 70ms usr + 150ms krn ; 0ms fg
  u0a182:
    Wake lock *launch* realtime
    Wake lock CoreService addWakeLock realtime
    Wake lock CoreReceiver getWakeLock realtime
    Wake lock WindowManager: 3s 393ms full (1 times) realtime
    Wake lock CoreService execute realtime
    Wake lock CoreService onStart realtime
    TOTAL wake: 3s 393ms full realtime
    Foreground activities: 1m 44s 742ms realtime (12 times)
    Foreground services: 302ms realtime (2 times)
    Top for: 1m 45s 193ms 
    Fg Service for: 302ms 
    Cached for: 36ms 
    Total running: 1m 45s 531ms 
    Total cpu time: u=20s 480ms s=3s 699ms 
    Total cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 20
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc com.fsck.k9.debug:
      CPU: 0ms usr + 0ms krn ; 15s 890ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 29ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 19ms uptime
        Starts: 1, launches: 1
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

