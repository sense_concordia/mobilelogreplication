Daily stats:
  Current start time: 2018-08-08-01-41-09
  Next min deadline: 2018-08-09-01-00-00
  Next max deadline: 2018-08-09-03-00-00
  Current daily steps:
    Discharge total time: 12h 37m 5s 500ms  (from 17 steps)
    Discharge screen on time: 12h 37m 5s 500ms  (from 17 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 22s 519ms (99.9%) realtime, 1m 22s 519ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 22s 596ms realtime, 1m 22s 596ms uptime
  Discharge: 9.71 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 9.71 mAh
  Start clock time: 2018-08-08-11-29-04
  Screen on: 1m 22s 519ms (100.0%) 0x, Interactive: 1m 22s 519ms (100.0%)
  Screen brightnesses:
    dim 1m 22s 519ms (100.0%)
  Total full wakelock time: 51s 35ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 22s 519ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 22s 519ms (100.0%) 
     Cellular Sleep time:  1m 8s 202ms (82.6%)
     Cellular Idle time:   13s 625ms (16.5%)
     Cellular Rx time:     693ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 22s 519ms (100.0%) 
     Wifi supplicant states:
       completed 1m 22s 519ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 22s 519ms (100.0%) 
     WiFi Sleep time:  1m 20s 155ms (97.1%)
     WiFi Idle time:   2s 327ms (2.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     38ms (0.0%)
     WiFi Battery drain: 0.00329mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 22s 520ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.5, actual drain: 0
    Screen: 7.15 Excluded from smearing
    Uid u0a148: 1.18 ( cpu=0.761 wifi=0.00153 gps=0.420 ) Including smearing: 1.44 ( proportional=0.258 )
    Uid 1041: 0.578 ( cpu=0.578 ) Excluded from smearing
    Uid 0: 0.528 ( cpu=0.528 ) Excluded from smearing
    Uid 1000: 0.385 ( cpu=0.383 sensor=0.00229 ) Excluded from smearing
    Cell standby: 0.252 ( radio=0.252 ) Excluded from smearing
    Idle: 0.225 Excluded from smearing
    Uid u0a22: 0.0480 ( cpu=0.0412 wifi=0.00108 sensor=0.00573 ) Excluded from smearing
    Uid u0a108: 0.0411 ( cpu=0.0411 ) Including smearing: 0.0501 ( proportional=0.00899 )
    Uid 2000: 0.0373 ( cpu=0.0373 ) Excluded from smearing
    Uid u0a47: 0.0296 ( cpu=0.0296 sensor=0.0000458 ) Excluded from smearing
    Uid 1036: 0.0124 ( cpu=0.0124 ) Excluded from smearing
    Uid 1021: 0.0118 ( cpu=0.0118 ) Excluded from smearing
    Uid u0a68: 0.00696 ( cpu=0.00696 ) Including smearing: 0.00848 ( proportional=0.00152 )
    Uid u0a35: 0.00693 ( cpu=0.00693 ) Including smearing: 0.00845 ( proportional=0.00151 )
    Wifi: 0.00497 ( cpu=0.00429 wifi=0.000682 ) Including smearing: 0.00606 ( proportional=0.00109 )
    Uid u0a127: 0.00434 ( cpu=0.00434 ) Including smearing: 0.00528 ( proportional=0.000948 )
    Uid u0a114: 0.00211 ( cpu=0.00211 ) Including smearing: 0.00257 ( proportional=0.000461 )
    Uid 1001: 0.00188 ( cpu=0.00188 ) Excluded from smearing
    Uid u0a25: 0.000642 ( cpu=0.000642 ) Including smearing: 0.000782 ( proportional=0.000140 )
    Uid u0a40: 0.000238 ( cpu=0.000238 ) Including smearing: 0.000290 ( proportional=0.0000520 )
    Uid 1013: 0.000119 ( cpu=0.000119 ) Excluded from smearing
    Uid 1053: 0.000119 ( cpu=0.000119 ) Excluded from smearing
    Uid u0a139: 0.000119 ( cpu=0.000119 ) Including smearing: 0.000144 ( proportional=0.0000259 )
    Uid u0a120: 0.000110 ( cpu=0.000110 ) Including smearing: 0.000134 ( proportional=0.0000240 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 20s 37ms realtime (19 times)
    XO_shutdown.MPSS: 30s 754ms realtime (23 times)
    XO_shutdown.ADSP: 7s 76ms realtime (9 times)
    wlan.Active: 2s 328ms realtime (19 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 7 other, 17 button, 37 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 291ms realtime (36 times)
    Sensor 7: 1m 22s 509ms realtime (0 times)
    Fg Service for: 1m 22s 510ms 
    Total running: 1m 22s 510ms 
    Total cpu time: u=5s 433ms s=4s 563ms 
    Total cpu time per freq: 1450 140 230 170 90 170 120 70 70 60 80 50 1810 90 90 140 40 60 50 30 70 2270 90 130 60 60 40 50 20 10 0 0 10 50 0 0 0 10 0 50 120 80 40 30 40 20 20 70 10 20 0 10 1310
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 380ms usr + 1s 940ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 30ms usr + 300ms krn ; 0ms fg
    Proc system:
      CPU: 5s 610ms usr + 3s 120ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 150ms usr + 330ms krn ; 0ms fg
  u0a148:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 1s 622ms (2.0%) 3x
    Wifi Scan (actual): 1s 622ms (2.0%) 3x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 22s 501ms (100.0%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     22ms (0.0%)
    Wake lock NlpWakeLock realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 51s 35ms full (7 times) realtime
    TOTAL wake: 51s 35ms full realtime
    Sensor GPS: 50s 401ms realtime (7 times)
    Foreground activities: 1m 21s 90ms realtime (19 times)
    Foreground services: 374ms realtime (2 times)
    Top for: 1m 21s 541ms 
    Fg Service for: 374ms 
    Cached for: 75ms 
    Total running: 1m 21s 990ms 
    Total cpu time: u=16s 613ms s=2s 766ms 
    Total cpu time per freq: 60 0 10 20 50 70 70 30 30 30 50 50 250 60 80 70 20 30 30 10 0 760 180 1250 440 440 320 220 210 60 50 60 50 330 0 30 60 40 10 330 700 620 290 280 180 150 150 270 180 60 30 20 10390
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc net.osmand:
      CPU: 2s 490ms usr + 570ms krn ; 18s 690ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

