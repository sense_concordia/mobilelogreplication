Daily stats:
  Current start time: 2018-08-08-01-41-09
  Next min deadline: 2018-08-09-01-00-00
  Next max deadline: 2018-08-09-03-00-00
  Current daily steps:
    Discharge total time: 11h 50m 9s 500ms  (from 4 steps)
    Discharge screen on time: 11h 50m 9s 500ms  (from 4 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 23s 23ms (99.9%) realtime, 1m 23s 24ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 23s 94ms realtime, 1m 23s 95ms uptime
  Discharge: 10.2 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 10.2 mAh
  Start clock time: 2018-08-08-09-49-44
  Screen on: 1m 23s 23ms (100.0%) 0x, Interactive: 1m 23s 23ms (100.0%)
  Screen brightnesses:
    dim 1m 23s 23ms (100.0%)
  Total full wakelock time: 51s 468ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 23s 23ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 23s 23ms (100.0%) 
     Cellular Sleep time:  1m 9s 403ms (83.6%)
     Cellular Idle time:   12s 950ms (15.6%)
     Cellular Rx time:     671ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 23s 23ms (100.0%) 
     Wifi supplicant states:
       completed 1m 23s 23ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 23s 23ms (100.0%) 
     WiFi Sleep time:  1m 21s 143ms (97.7%)
     WiFi Idle time:   1s 851ms (2.2%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     30ms (0.0%)
     WiFi Battery drain: 0.00260mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 23s 24ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.5, actual drain: 0
    Screen: 7.19 Excluded from smearing
    Uid u0a148: 1.19 ( cpu=0.762 wifi=0.00208 gps=0.423 ) Including smearing: 1.45 ( proportional=0.259 )
    Uid 1041: 0.585 ( cpu=0.585 ) Excluded from smearing
    Uid 0: 0.519 ( cpu=0.519 ) Excluded from smearing
    Uid 1000: 0.391 ( cpu=0.388 sensor=0.00231 ) Excluded from smearing
    Cell standby: 0.254 ( radio=0.254 ) Excluded from smearing
    Idle: 0.226 Excluded from smearing
    Uid u0a22: 0.0492 ( cpu=0.0430 wifi=0.000514 sensor=0.00576 ) Excluded from smearing
    Uid 2000: 0.0367 ( cpu=0.0367 ) Excluded from smearing
    Uid u0a108: 0.0287 ( cpu=0.0287 ) Including smearing: 0.0350 ( proportional=0.00626 )
    Uid u0a47: 0.0249 ( cpu=0.0249 sensor=0.0000461 ) Excluded from smearing
    Uid 1036: 0.0128 ( cpu=0.0128 ) Excluded from smearing
    Uid 1021: 0.0115 ( cpu=0.0115 ) Excluded from smearing
    Uid u0a68: 0.00750 ( cpu=0.00750 ) Including smearing: 0.00913 ( proportional=0.00163 )
    Uid u0a127: 0.00438 ( cpu=0.00438 ) Including smearing: 0.00533 ( proportional=0.000954 )
    Uid u0a35: 0.00307 ( cpu=0.00307 ) Including smearing: 0.00374 ( proportional=0.000670 )
    Wifi: 0.00305 ( cpu=0.00305 ) Including smearing: 0.00372 ( proportional=0.000666 )
    Uid 1001: 0.00184 ( cpu=0.00184 ) Excluded from smearing
    Uid u0a114: 0.00151 ( cpu=0.00151 ) Including smearing: 0.00184 ( proportional=0.000329 )
    Uid u0a25: 0.000531 ( cpu=0.000531 ) Including smearing: 0.000646 ( proportional=0.000116 )
    Uid 1013: 0.000277 ( cpu=0.000277 ) Excluded from smearing
    Uid 1053: 0.000158 ( cpu=0.000158 ) Excluded from smearing
    Uid u0a40: 0.000145 ( cpu=0.000145 ) Including smearing: 0.000177 ( proportional=0.0000317 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 20s 616ms realtime (16 times)
    XO_shutdown.MPSS: 31s 470ms realtime (25 times)
    XO_shutdown.ADSP: 7s 869ms realtime (13 times)
    wlan.Active: 2s 266ms realtime (16 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 7 other, 16 button, 37 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 252ms realtime (36 times)
    Sensor 7: 1m 23s 12ms realtime (0 times)
    Fg Service for: 1m 23s 12ms 
    Total running: 1m 23s 12ms 
    Total cpu time: u=5s 507ms s=4s 587ms 
    Total cpu time per freq: 1260 180 210 130 70 80 70 110 60 90 90 50 1950 140 40 140 90 70 50 70 30 2240 60 110 50 20 60 50 30 0 10 0 10 40 0 0 10 0 0 20 90 70 60 60 50 30 20 20 10 20 10 0 1480
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 690ms usr + 1s 50ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 210ms krn ; 0ms fg
    Proc system:
      CPU: 3s 70ms usr + 1s 700ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 70ms usr + 170ms krn ; 0ms fg
  u0a148:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 2s 140ms (2.6%) 4x
    Wifi Scan (actual): 2s 140ms (2.6%) 4x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 22s 997ms (100.0%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     30ms (0.0%)
    Wake lock NlpWakeLock realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 51s 468ms full (7 times) realtime
    TOTAL wake: 51s 468ms full realtime
    Sensor GPS: 50s 776ms realtime (7 times)
    Foreground activities: 1m 21s 521ms realtime (19 times)
    Foreground services: 431ms realtime (2 times)
    Top for: 1m 21s 960ms 
    Fg Service for: 431ms 
    Cached for: 90ms 
    Total running: 1m 22s 481ms 
    Total cpu time: u=16s 907ms s=2s 827ms 
    Total cpu time per freq: 50 30 50 20 50 60 60 50 60 80 80 50 310 80 30 50 20 20 50 10 10 890 170 1230 440 600 480 230 160 30 100 110 60 390 20 20 50 10 70 330 430 570 310 240 100 240 230 140 50 100 50 0 10460
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc net.osmand:
      CPU: 0ms usr + 0ms krn ; 18s 990ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

