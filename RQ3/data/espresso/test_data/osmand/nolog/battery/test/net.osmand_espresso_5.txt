Daily stats:
  Current start time: 2018-08-08-01-41-09
  Next min deadline: 2018-08-09-01-00-00
  Next max deadline: 2018-08-09-03-00-00
  Current daily steps:
    Discharge total time: 12h 24m 17s 800ms  (from 20 steps)
    Discharge screen on time: 12h 24m 17s 800ms  (from 20 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 22s 152ms (99.9%) realtime, 1m 22s 152ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 22s 223ms realtime, 1m 22s 224ms uptime
  Discharge: 8.52 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.52 mAh
  Start clock time: 2018-08-08-15-03-18
  Screen on: 1m 22s 152ms (100.0%) 0x, Interactive: 1m 22s 152ms (100.0%)
  Screen brightnesses:
    dim 1m 22s 152ms (100.0%)
  Total full wakelock time: 50s 904ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 22s 152ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 22s 152ms (100.0%) 
     Cellular Sleep time:  1m 9s 637ms (84.8%)
     Cellular Idle time:   11s 729ms (14.3%)
     Cellular Rx time:     787ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 22s 152ms (100.0%) 
     Wifi supplicant states:
       completed 1m 22s 152ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 22s 152ms (100.0%) 
     WiFi Sleep time:  1m 20s 256ms (97.7%)
     WiFi Idle time:   1s 867ms (2.3%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     30ms (0.0%)
     WiFi Battery drain: 0.00260mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 22s 153ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.4, actual drain: 0-35.2
    Screen: 7.12 Excluded from smearing
    Uid u0a148: 1.18 ( cpu=0.759 wifi=0.00208 gps=0.419 ) Including smearing: 1.44 ( proportional=0.255 )
    Uid 1041: 0.575 ( cpu=0.575 ) Excluded from smearing
    Uid 0: 0.527 ( cpu=0.527 ) Excluded from smearing
    Uid 1000: 0.359 ( cpu=0.356 sensor=0.00228 ) Excluded from smearing
    Cell standby: 0.251 ( radio=0.251 ) Excluded from smearing
    Idle: 0.224 Excluded from smearing
    Uid u0a22: 0.0450 ( cpu=0.0388 wifi=0.000519 sensor=0.00570 ) Excluded from smearing
    Uid 2000: 0.0339 ( cpu=0.0339 ) Excluded from smearing
    Uid u0a108: 0.0337 ( cpu=0.0337 ) Including smearing: 0.0409 ( proportional=0.00726 )
    Uid u0a47: 0.0326 ( cpu=0.0325 sensor=0.0000456 ) Excluded from smearing
    Uid 1021: 0.0120 ( cpu=0.0120 ) Excluded from smearing
    Uid 1036: 0.0118 ( cpu=0.0118 ) Excluded from smearing
    Uid u0a68: 0.00672 ( cpu=0.00672 ) Including smearing: 0.00817 ( proportional=0.00145 )
    Uid u0a127: 0.00489 ( cpu=0.00489 ) Including smearing: 0.00595 ( proportional=0.00106 )
    Uid u0a35: 0.00364 ( cpu=0.00364 ) Including smearing: 0.00443 ( proportional=0.000785 )
    Wifi: 0.00290 ( cpu=0.00290 ) Including smearing: 0.00353 ( proportional=0.000626 )
    Uid u0a114: 0.00209 ( cpu=0.00209 ) Including smearing: 0.00254 ( proportional=0.000450 )
    Uid 1001: 0.00188 ( cpu=0.00188 ) Excluded from smearing
    Uid u0a25: 0.00132 ( cpu=0.00132 ) Including smearing: 0.00160 ( proportional=0.000284 )
    Uid u0a40: 0.000278 ( cpu=0.000278 ) Including smearing: 0.000337 ( proportional=0.0000599 )
    Bluetooth: 0.000214 ( cpu=0.000214 ) Including smearing: 0.000260 ( proportional=0.0000461 )
    Uid 1053: 0.000158 ( cpu=0.000158 ) Excluded from smearing
    Uid 1013: 0.000118 ( cpu=0.000118 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 19s 751ms realtime (19 times)
    XO_shutdown.MPSS: 31s 23ms realtime (28 times)
    XO_shutdown.ADSP: 7s 452ms realtime (13 times)
    wlan.Active: 2s 329ms realtime (19 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 7 other, 16 button, 38 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 226ms realtime (36 times)
    Sensor 7: 1m 22s 141ms realtime (0 times)
    Fg Service for: 1m 22s 142ms 
    Total running: 1m 22s 142ms 
    Total cpu time: u=5s 410ms s=3s 900ms 
    Total cpu time per freq: 1370 170 120 130 150 50 50 80 30 50 80 20 1760 140 70 100 50 60 130 30 60 2040 50 40 30 80 20 30 30 0 0 10 20 60 40 0 10 20 10 20 90 70 70 10 10 30 10 30 30 20 10 0 1370
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 110ms usr + 1s 140ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 4s 320ms usr + 2s 460ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 120ms usr + 240ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a148:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 2s 135ms (2.6%) 4x
    Wifi Scan (actual): 2s 135ms (2.6%) 4x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 22s 126ms (100.0%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     30ms (0.0%)
    Wake lock NlpWakeLock realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 50s 904ms full (7 times) realtime
    TOTAL wake: 50s 904ms full realtime
    Sensor GPS: 50s 300ms realtime (7 times)
    Foreground activities: 1m 20s 833ms realtime (19 times)
    Foreground services: 396ms realtime (2 times)
    Top for: 1m 21s 258ms 
    Fg Service for: 396ms 
    Cached for: 44ms 
    Total running: 1m 21s 698ms 
    Total cpu time: u=16s 870ms s=2s 590ms 
    Total cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 170
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc net.osmand:
      CPU: 0ms usr + 0ms krn ; 18s 850ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

