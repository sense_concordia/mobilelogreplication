Daily stats:
  Current start time: 2018-08-08-01-41-09
  Next min deadline: 2018-08-09-01-00-00
  Next max deadline: 2018-08-09-03-00-00
  Current daily steps:
    Discharge total time: 12h 11m 34s 900ms  (from 24 steps)
    Discharge screen on time: 12h 11m 34s 900ms  (from 24 steps)
    Charge total time: 15h 23m 0s 0ms  (from 9 steps)
    Charge screen on time: 15h 23m 0s 0ms  (from 9 steps)
  Daily from 2018-08-07-07-13-31 to 2018-08-08-01-41-09:
    Discharge total time: 11h 59m 39s 900ms  (from 6 steps)
    Discharge screen on time: 11h 59m 39s 900ms  (from 6 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 22s 542ms (99.9%) realtime, 1m 22s 542ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 22s 617ms realtime, 1m 22s 617ms uptime
  Discharge: 10.1 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 10.1 mAh
  Start clock time: 2018-08-08-15-37-12
  Screen on: 1m 22s 542ms (100.0%) 0x, Interactive: 1m 22s 542ms (100.0%)
  Screen brightnesses:
    dim 1m 22s 542ms (100.0%)
  Total full wakelock time: 51s 140ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 22s 542ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 22s 542ms (100.0%) 
     Cellular Sleep time:  1m 10s 651ms (85.6%)
     Cellular Idle time:   11s 239ms (13.6%)
     Cellular Rx time:     652ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 22s 542ms (100.0%) 
     Wifi supplicant states:
       completed 1m 22s 542ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 22s 542ms (100.0%) 
     WiFi Sleep time:  1m 20s 627ms (97.7%)
     WiFi Idle time:   1s 884ms (2.3%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     31ms (0.0%)
     WiFi Battery drain: 0.00268mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 22s 543ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.4, actual drain: 0
    Screen: 7.15 Excluded from smearing
    Uid u0a148: 1.17 ( cpu=0.750 wifi=0.00215 gps=0.421 ) Including smearing: 1.43 ( proportional=0.252 )
    Uid 1041: 0.571 ( cpu=0.571 ) Excluded from smearing
    Uid 0: 0.516 ( cpu=0.516 ) Excluded from smearing
    Uid 1000: 0.364 ( cpu=0.362 sensor=0.00229 ) Excluded from smearing
    Cell standby: 0.252 ( radio=0.252 ) Excluded from smearing
    Idle: 0.225 Excluded from smearing
    Uid u0a22: 0.0454 ( cpu=0.0391 wifi=0.000523 sensor=0.00573 ) Excluded from smearing
    Uid 2000: 0.0366 ( cpu=0.0366 ) Excluded from smearing
    Uid u0a47: 0.0324 ( cpu=0.0323 sensor=0.0000458 ) Excluded from smearing
    Uid u0a108: 0.0287 ( cpu=0.0287 ) Including smearing: 0.0348 ( proportional=0.00615 )
    Uid 1036: 0.0121 ( cpu=0.0121 ) Excluded from smearing
    Uid 1021: 0.0115 ( cpu=0.0115 ) Excluded from smearing
    Uid u0a68: 0.00640 ( cpu=0.00640 ) Including smearing: 0.00778 ( proportional=0.00137 )
    Uid u0a127: 0.00457 ( cpu=0.00457 ) Including smearing: 0.00555 ( proportional=0.000981 )
    Uid u0a35: 0.00370 ( cpu=0.00370 ) Including smearing: 0.00450 ( proportional=0.000794 )
    Wifi: 0.00320 ( cpu=0.00320 ) Including smearing: 0.00389 ( proportional=0.000686 )
    Uid 1001: 0.00163 ( cpu=0.00163 ) Excluded from smearing
    Uid u0a25: 0.00141 ( cpu=0.00141 ) Including smearing: 0.00171 ( proportional=0.000303 )
    Uid u0a114: 0.000705 ( cpu=0.000705 ) Including smearing: 0.000856 ( proportional=0.000151 )
    Uid u0a40: 0.000383 ( cpu=0.000383 ) Including smearing: 0.000465 ( proportional=0.0000821 )
    Uid 1013: 0.000157 ( cpu=0.000157 ) Excluded from smearing
    Uid 1053: 0.000157 ( cpu=0.000157 ) Excluded from smearing
    Uid 1027: 0.000107 ( cpu=0.000107 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 20s 90ms realtime (16 times)
    XO_shutdown.MPSS: 31s 250ms realtime (22 times)
    XO_shutdown.ADSP: 7s 533ms realtime (13 times)
    wlan.Active: 2s 307ms realtime (16 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 7 other, 17 button, 38 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 206ms realtime (36 times)
    Sensor 7: 1m 22s 532ms realtime (0 times)
    Fg Service for: 1m 22s 532ms 
    Total running: 1m 22s 532ms 
    Total cpu time: u=5s 327ms s=4s 133ms 
    Total cpu time per freq: 1390 180 200 160 90 110 50 50 20 60 90 100 1790 110 120 80 60 50 40 50 0 2050 130 100 70 70 40 30 30 20 0 20 0 20 20 30 10 0 20 30 80 70 30 60 10 10 40 10 10 10 0 0 1180
  u0a148:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 2s 156ms (2.6%) 4x
    Wifi Scan (actual): 2s 156ms (2.6%) 4x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 22s 540ms (100.0%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     31ms (0.0%)
    Wake lock NlpWakeLock realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 51s 140ms full (7 times) realtime
    TOTAL wake: 51s 140ms full realtime
    Sensor GPS: 50s 533ms realtime (7 times)
    Foreground activities: 1m 21s 214ms realtime (19 times)
    Foreground services: 301ms realtime (2 times)
    Top for: 1m 21s 611ms 
    Fg Service for: 301ms 
    Cached for: 49ms 
    Total running: 1m 21s 961ms 
    Total cpu time: u=16s 803ms s=2s 693ms 
    Total cpu time per freq: 60 10 40 50 90 80 50 50 10 80 80 130 320 70 50 50 10 20 10 20 0 870 180 1190 500 520 360 360 220 40 50 20 10 290 0 20 30 0 10 330 820 570 260 180 210 290 110 320 30 140 0 20 10440
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc net.osmand:
      CPU: 0ms usr + 0ms krn ; 18s 940ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

