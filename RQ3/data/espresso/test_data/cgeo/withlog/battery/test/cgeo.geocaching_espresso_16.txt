Daily stats:
  Current start time: 2018-08-06-01-30-22
  Next min deadline: 2018-08-07-01-00-00
  Next max deadline: 2018-08-07-03-00-00
  Current daily steps:
    Discharge total time: 10h 48m 8s 700ms  (from 3 steps)
    Discharge screen on time: 10h 48m 8s 700ms  (from 3 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 17s 849ms (99.9%) realtime, 1m 17s 850ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 17s 891ms realtime, 1m 17s 892ms uptime
  Discharge: 8.61 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.61 mAh
  Start clock time: 2018-08-06-15-55-57
  Screen on: 1m 17s 849ms (100.0%) 0x, Interactive: 1m 17s 849ms (100.0%)
  Screen brightnesses:
    dim 1m 17s 849ms (100.0%)
  Total full wakelock time: 22s 805ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 17s 849ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 17s 849ms (100.0%) 
     Cellular Sleep time:  56s 384ms (72.4%)
     Cellular Idle time:   20s 808ms (26.7%)
     Cellular Rx time:     658ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 17s 849ms (100.0%) 
     Wifi supplicant states:
       completed 1m 17s 849ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 17s 849ms (100.0%) 
     WiFi Sleep time:  1m 14s 336ms (95.5%)
     WiFi Idle time:   3s 458ms (4.4%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     56ms (0.1%)
     WiFi Battery drain: 0.00485mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 17s 850ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.4, actual drain: 0
    Screen: 6.74 Excluded from smearing
    Uid u0a144: 1.17 ( cpu=0.716 wifi=0.00389 gps=0.444 sensor=0.00598 ) Including smearing: 1.49 ( proportional=0.322 )
    Uid 1041: 0.621 ( cpu=0.621 ) Excluded from smearing
    Uid 1000: 0.595 ( cpu=0.593 sensor=0.00216 ) Excluded from smearing
    Uid 0: 0.504 ( cpu=0.504 ) Excluded from smearing
    Cell standby: 0.238 ( radio=0.238 ) Excluded from smearing
    Idle: 0.212 Excluded from smearing
    Uid u0a22: 0.165 ( cpu=0.130 wifi=0.000961 sensor=0.0342 ) Excluded from smearing
    Uid u0a47: 0.0684 ( cpu=0.0683 sensor=0.0000432 ) Excluded from smearing
    Uid u0a70: 0.0466 ( cpu=0.0452 sensor=0.00137 ) Including smearing: 0.0594 ( proportional=0.0128 )
    Uid 2000: 0.0386 ( cpu=0.0386 ) Excluded from smearing
    Uid 1036: 0.0142 ( cpu=0.0142 ) Excluded from smearing
    Uid 1021: 0.00892 ( cpu=0.00892 ) Excluded from smearing
    Uid u0a35: 0.00549 ( cpu=0.00549 ) Including smearing: 0.00700 ( proportional=0.00151 )
    Uid u0a127: 0.00549 ( cpu=0.00549 ) Including smearing: 0.00700 ( proportional=0.00151 )
    Uid u0a68: 0.00452 ( cpu=0.00452 ) Including smearing: 0.00576 ( proportional=0.00124 )
    Wifi: 0.00360 ( cpu=0.00360 ) Including smearing: 0.00459 ( proportional=0.000991 )
    Uid 1001: 0.00293 ( cpu=0.00293 ) Excluded from smearing
    Uid u0a25: 0.00173 ( cpu=0.00173 ) Including smearing: 0.00221 ( proportional=0.000476 )
    Uid u0a40: 0.000519 ( cpu=0.000519 ) Including smearing: 0.000661 ( proportional=0.000143 )
    Uid 1027: 0.000279 ( cpu=0.000279 ) Excluded from smearing
    Uid u0a28: 0.000202 ( cpu=0.000202 ) Including smearing: 0.000258 ( proportional=0.0000556 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 13s 696ms realtime (12 times)
    XO_shutdown.MPSS: 24s 27ms realtime (14 times)
    XO_shutdown.ADSP: 3s 236ms realtime (5 times)
    wlan.Active: 3s 401ms realtime (12 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 4 other, 17 button, 44 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 650ms realtime (43 times)
    Sensor 7: 1m 17s 838ms realtime (0 times)
    Fg Service for: 1m 17s 838ms 
    Total running: 1m 17s 838ms 
    Total cpu time: u=8s 257ms s=6s 730ms 
    Total cpu time per freq: 2670 320 150 140 120 110 110 90 140 80 70 50 3390 80 130 70 110 70 30 90 40 2870 550 170 60 30 50 10 20 0 0 10 30 270 50 0 10 10 30 40 70 70 30 50 10 10 50 50 40 0 30 40 2050
    Proc servicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 590ms usr + 600ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 2s 540ms usr + 1s 240ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 80ms usr + 170ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 330ms usr + 420ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 3s 876ms (5.0%) 7x
    Wifi Scan (actual): 3s 876ms (5.0%) 7x
    Background Wifi Scan: 107ms (0.1%) 0x
       WiFi Sleep time:  1m 17s 799ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     56ms (0.1%)
    Wake lock GCoreFlp realtime
    Wake lock NlpWakeLock realtime
    Wake lock *alarm* realtime
    Wake lock fiid-sync realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 22s 805ms full (4 times) realtime
    TOTAL wake: 22s 805ms full realtime
    Sensor GPS: 53s 297ms realtime (3 times), 16ms background (0 times)
    Sensor 16: 21s 541ms realtime (4 times)
    Foreground activities: 1m 16s 652ms realtime (19 times)
    Foreground services: 372ms realtime (2 times)
    Top for: 1m 16s 932ms 
    Fg Service for: 372ms 
    Cached for: 47ms 
    Total running: 1m 17s 351ms 
    Total cpu time: u=11s 940ms s=5s 976ms 
    Total cpu time per freq: 100 20 20 10 40 50 30 30 40 40 40 60 90 40 40 40 10 0 0 0 0 1030 760 660 540 270 220 200 160 170 130 90 190 650 140 90 170 80 140 430 410 280 200 220 220 280 190 330 230 210 70 130 8430
    Proc cgeo.geocaching:
      CPU: 0ms usr + 0ms krn ; 17s 630ms fg
      1 starts
    Apk cgeo.geocaching:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 0 times

