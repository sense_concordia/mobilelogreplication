Daily stats:
  Current start time: 2018-08-06-01-30-22
  Next min deadline: 2018-08-07-01-00-00
  Next max deadline: 2018-08-07-03-00-00
  Current daily steps:
    Discharge total time: 12h 36m 20s 100ms  (from 19 steps)
    Discharge screen on time: 12h 36m 20s 100ms  (from 19 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 17s 69ms (99.9%) realtime, 1m 17s 69ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 17s 122ms realtime, 1m 17s 122ms uptime
  Discharge: 8.49 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.49 mAh
  Start clock time: 2018-08-06-20-02-58
  Screen on: 1m 17s 69ms (100.0%) 0x, Interactive: 1m 17s 69ms (100.0%)
  Screen brightnesses:
    dark 1m 17s 69ms (100.0%)
  Total full wakelock time: 22s 876ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 17s 69ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 17s 69ms (100.0%) 
     Cellular Sleep time:  56s 23ms (72.7%)
     Cellular Idle time:   20s 297ms (26.3%)
     Cellular Rx time:     749ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 17s 69ms (100.0%) 
     Wifi supplicant states:
       completed 1m 17s 69ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 17s 69ms (100.0%) 
     WiFi Sleep time:  1m 12s 879ms (94.6%)
     WiFi Idle time:   4s 137ms (5.4%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     53ms (0.1%)
     WiFi Battery drain: 0.00483mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 17s 69ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.21, actual drain: 0
    Screen: 4.40 Excluded from smearing
    Uid u0a144: 1.20 ( cpu=0.755 wifi=0.00340 gps=0.439 sensor=0.00598 ) Including smearing: 1.67 ( proportional=0.466 )
    Uid 1000: 0.620 ( cpu=0.620 ) Excluded from smearing
    Uid 1041: 0.615 ( cpu=0.615 ) Excluded from smearing
    Uid 0: 0.568 ( cpu=0.568 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.235 ( radio=0.235 ) Excluded from smearing
    Idle: 0.210 Excluded from smearing
    Uid u0a22: 0.111 ( cpu=0.0950 wifi=0.00105 sensor=0.0146 ) Excluded from smearing
    Uid u0a70: 0.0853 ( cpu=0.0831 sensor=0.00217 ) Including smearing: 0.118 ( proportional=0.0330 )
    Uid u0a47: 0.0834 ( cpu=0.0834 sensor=0.0000428 ) Excluded from smearing
    Uid 2000: 0.0351 ( cpu=0.0351 ) Excluded from smearing
    Uid 1036: 0.0149 ( cpu=0.0149 ) Excluded from smearing
    Uid u0a61: 0.00894 ( cpu=0.00894 ) Including smearing: 0.0124 ( proportional=0.00346 )
    Uid 1021: 0.00751 ( cpu=0.00751 ) Excluded from smearing
    Uid u0a130: 0.00348 ( cpu=0.00348 ) Including smearing: 0.00482 ( proportional=0.00135 )
    Wifi: 0.00343 ( cpu=0.00305 wifi=0.000379 ) Including smearing: 0.00476 ( proportional=0.00133 )
    Uid u0a25: 0.00283 ( cpu=0.00283 ) Including smearing: 0.00392 ( proportional=0.00109 )
    Uid u0a40: 0.00254 ( cpu=0.00254 ) Including smearing: 0.00352 ( proportional=0.000981 )
    Uid u0a35: 0.00238 ( cpu=0.00238 ) Including smearing: 0.00331 ( proportional=0.000923 )
    Uid 1001: 0.00230 ( cpu=0.00230 ) Excluded from smearing
    Uid u0a68: 0.00190 ( cpu=0.00190 ) Including smearing: 0.00263 ( proportional=0.000734 )
    Uid u0a127: 0.00121 ( cpu=0.00121 ) Including smearing: 0.00168 ( proportional=0.000468 )
    Uid 1027: 0.000241 ( cpu=0.000241 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 12s 678ms realtime (11 times)
    XO_shutdown.MPSS: 23s 909ms realtime (12 times)
    XO_shutdown.ADSP: 3s 119ms realtime (5 times)
    wlan.Active: 4s 37ms realtime (11 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 4 other, 17 button, 43 touch
    Wake lock LocationManagerService realtime
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 739ms realtime (43 times)
    Fg Service for: 1m 17s 57ms 
    Total running: 1m 17s 57ms 
    Total cpu time: u=8s 717ms s=7s 487ms 
    Total cpu time per freq: 3840 390 250 170 110 100 120 130 80 70 80 50 2720 60 60 90 70 70 100 50 0 2360 510 120 80 60 50 20 30 0 20 0 20 310 0 20 50 20 10 30 70 40 20 50 50 10 30 70 0 30 0 0 1710
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 420ms usr + 470ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 40ms krn ; 0ms fg
    Proc android.hardware.vibrator@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 1s 570ms usr + 1s 50ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 100ms usr + 200ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 210ms usr + 440ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 3s 123ms (4.1%) 5x
    Wifi Scan (actual): 3s 123ms (4.1%) 5x
    Background Wifi Scan: 108ms (0.1%) 0x
       WiFi Sleep time:  1m 17s 25ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     49ms (0.1%)
    Wake lock GCoreFlp realtime
    Wake lock LocationManagerService realtime
    Wake lock NlpWakeLock realtime
    Wake lock *alarm* realtime
    Wake lock fiid-sync realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 22s 876ms full (4 times) realtime
    TOTAL wake: 22s 876ms full realtime
    Sensor GPS: 52s 688ms realtime (3 times), 20ms background (0 times)
    Sensor 16: 21s 537ms realtime (4 times)
    Foreground activities: 1m 15s 982ms realtime (19 times)
    Foreground services: 297ms realtime (2 times)
    Top for: 1m 16s 271ms 
    Fg Service for: 297ms 
    Cached for: 50ms 
    Total running: 1m 16s 618ms 
    Total cpu time: u=12s 667ms s=6s 26ms 
    Total cpu time per freq: 100 20 30 10 20 70 60 60 20 20 20 40 200 20 20 10 10 10 20 10 10 1000 720 700 480 560 200 210 100 110 250 90 140 700 60 120 70 130 130 350 360 350 230 330 150 130 310 420 220 190 30 100 9040
    Proc cgeo.geocaching:
      CPU: 0ms usr + 0ms krn ; 18s 420ms fg
      1 starts
    Apk cgeo.geocaching:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 0 times

