Daily stats:
  Current start time: 2018-08-06-01-30-22
  Next min deadline: 2018-08-07-01-00-00
  Next max deadline: 2018-08-07-03-00-00
  Current daily steps:
    Discharge total time: 12h 55m 50s 900ms  (from 16 steps)
    Discharge screen on time: 12h 55m 50s 900ms  (from 16 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 17s 314ms (99.9%) realtime, 1m 17s 313ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 17s 353ms realtime, 1m 17s 353ms uptime
  Discharge: 7.84 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 7.84 mAh
  Start clock time: 2018-08-06-19-45-27
  Screen on: 1m 17s 314ms (100.0%) 0x, Interactive: 1m 17s 314ms (100.0%)
  Screen brightnesses:
    dark 1m 17s 314ms (100.0%)
  Total full wakelock time: 22s 748ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 17s 314ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 17s 314ms (100.0%) 
     Cellular Sleep time:  55s 390ms (71.6%)
     Cellular Idle time:   21s 7ms (27.2%)
     Cellular Rx time:     917ms (1.2%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 17s 314ms (100.0%) 
     Wifi supplicant states:
       completed 1m 17s 314ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 17s 314ms (100.0%) 
     WiFi Sleep time:  1m 11s 589ms (92.6%)
     WiFi Idle time:   5s 654ms (7.3%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     71ms (0.1%)
     WiFi Battery drain: 0.00650mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 17s 314ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 8.32, actual drain: 0
    Screen: 4.41 Excluded from smearing
    Uid u0a144: 1.15 ( cpu=0.702 wifi=0.00382 gps=0.441 sensor=0.00597 ) Including smearing: 1.63 ( proportional=0.476 )
    Uid 1000: 0.694 ( cpu=0.694 ) Excluded from smearing
    Uid 0: 0.584 ( cpu=0.584 ) Excluded from smearing
    Uid 1041: 0.584 ( cpu=0.584 ) Excluded from smearing
    Cell standby: 0.236 ( radio=0.236 ) Excluded from smearing
    Idle: 0.211 Excluded from smearing
    Uid u0a22: 0.178 ( cpu=0.142 wifi=0.00222 sensor=0.0339 ) Excluded from smearing
    Uid u0a47: 0.0896 ( cpu=0.0896 sensor=0.0000429 ) Excluded from smearing
    Uid u0a70: 0.0818 ( cpu=0.0804 sensor=0.00145 ) Including smearing: 0.116 ( proportional=0.0338 )
    Uid 2000: 0.0373 ( cpu=0.0373 ) Excluded from smearing
    Uid 1036: 0.0147 ( cpu=0.0147 ) Excluded from smearing
    Uid 1021: 0.0104 ( cpu=0.0104 ) Excluded from smearing
    Uid u0a61: 0.00938 ( cpu=0.00938 ) Including smearing: 0.0133 ( proportional=0.00388 )
    Wifi: 0.00553 ( cpu=0.00507 wifi=0.000457 ) Including smearing: 0.00781 ( proportional=0.00229 )
    Uid u0a68: 0.00454 ( cpu=0.00454 ) Including smearing: 0.00641 ( proportional=0.00188 )
    Uid 1001: 0.00419 ( cpu=0.00419 ) Excluded from smearing
    Uid u0a25: 0.00407 ( cpu=0.00407 ) Including smearing: 0.00575 ( proportional=0.00168 )
    Uid u0a130: 0.00334 ( cpu=0.00334 ) Including smearing: 0.00471 ( proportional=0.00138 )
    Uid u0a40: 0.00242 ( cpu=0.00242 ) Including smearing: 0.00342 ( proportional=0.000999 )
    Uid u0a127: 0.00145 ( cpu=0.00145 ) Including smearing: 0.00205 ( proportional=0.000599 )
    Uid u0a35: 0.00110 ( cpu=0.00110 ) Including smearing: 0.00155 ( proportional=0.000454 )
    Uid 1027: 0.000400 ( cpu=0.000400 ) Excluded from smearing
    Bluetooth: 0.000113 ( cpu=0.000113 ) Including smearing: 0.000159 ( proportional=0.0000466 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 11s 238ms realtime (14 times)
    XO_shutdown.MPSS: 22s 805ms realtime (13 times)
    XO_shutdown.ADSP: 3s 93ms realtime (1 times)
    wlan.Active: 5s 845ms realtime (14 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 4 other, 17 button, 43 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 608ms realtime (43 times)
    Fg Service for: 1m 17s 306ms 
    Total running: 1m 17s 306ms 
    Total cpu time: u=9s 620ms s=8s 530ms 
    Total cpu time per freq: 4210 550 200 180 130 150 130 120 60 30 110 70 3210 160 90 100 60 40 110 70 60 2960 720 100 30 80 40 10 30 0 30 20 0 370 40 10 80 20 10 80 70 90 20 20 10 50 60 30 20 30 10 0 2090
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 230ms usr + 360ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 1s 160ms usr + 690ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 50ms usr + 190ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 180ms usr + 340ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 3s 710ms (4.8%) 6x
    Wifi Scan (actual): 3s 710ms (4.8%) 6x
    Background Wifi Scan: 111ms (0.1%) 1x
       WiFi Sleep time:  1m 17s 266ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     55ms (0.1%)
    Wake lock GCoreFlp realtime
    Wake lock NlpWakeLock realtime
    Wake lock *alarm* realtime
    Wake lock fiid-sync realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 22s 748ms full (4 times) realtime
    TOTAL wake: 22s 748ms full realtime
    Sensor GPS: 52s 885ms realtime (3 times), 9ms background (0 times)
    Sensor 16: 21s 482ms realtime (4 times)
    Foreground activities: 1m 16s 118ms realtime (19 times)
    Foreground services: 306ms realtime (2 times)
    Top for: 1m 16s 433ms 
    Fg Service for: 306ms 
    Cached for: 66ms 
    Total running: 1m 16s 805ms 
    Total cpu time: u=11s 590ms s=5s 883ms 
    Total cpu time per freq: 80 20 30 0 20 40 40 60 30 10 40 20 130 30 40 30 10 10 20 10 0 1070 670 670 280 240 320 200 110 140 30 140 110 710 30 20 120 80 150 360 320 360 180 240 160 220 200 360 260 310 70 40 8690
    Proc cgeo.geocaching:
      CPU: 0ms usr + 0ms krn ; 17s 180ms fg
      1 starts
    Apk cgeo.geocaching:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 0 times

