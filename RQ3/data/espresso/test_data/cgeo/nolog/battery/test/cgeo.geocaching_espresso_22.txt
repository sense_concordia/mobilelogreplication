Daily stats:
  Current start time: 2018-08-07-07-13-31
  Next min deadline: 2018-08-08-01-00-00
  Next max deadline: 2018-08-08-03-00-00
  Current daily steps:
    Discharge total time: 11h 40m 29s 900ms  (from 4 steps)
    Discharge screen on time: 11h 40m 29s 900ms  (from 4 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 17s 534ms (99.9%) realtime, 1m 17s 533ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 17s 584ms realtime, 1m 17s 583ms uptime
  Discharge: 10.2 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 10.2 mAh
  Start clock time: 2018-08-07-11-24-08
  Screen on: 1m 17s 534ms (100.0%) 0x, Interactive: 1m 17s 534ms (100.0%)
  Screen brightnesses:
    dim 1m 17s 534ms (100.0%)
  Total full wakelock time: 22s 831ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 17s 534ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 17s 534ms (100.0%) 
     Cellular Sleep time:  55s 376ms (71.4%)
     Cellular Idle time:   21s 519ms (27.8%)
     Cellular Rx time:     639ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 17s 534ms (100.0%) 
     Wifi supplicant states:
       completed 1m 17s 534ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 17s 534ms (100.0%) 
     WiFi Sleep time:  1m 14s 15ms (95.5%)
     WiFi Idle time:   3s 464ms (4.5%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     56ms (0.1%)
     WiFi Battery drain: 0.00485mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 17s 535ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.4, actual drain: 0
    Screen: 6.72 Excluded from smearing
    Uid u0a144: 1.15 ( cpu=0.699 wifi=0.00389 gps=0.442 sensor=0.00599 ) Including smearing: 1.47 ( proportional=0.324 )
    Uid 1041: 0.639 ( cpu=0.639 ) Excluded from smearing
    Uid 1000: 0.599 ( cpu=0.597 sensor=0.00215 ) Excluded from smearing
    Uid 0: 0.512 ( cpu=0.512 ) Excluded from smearing
    Cell standby: 0.237 ( radio=0.237 ) Excluded from smearing
    Idle: 0.211 Excluded from smearing
    Uid u0a22: 0.175 ( cpu=0.140 wifi=0.000892 sensor=0.0340 ) Excluded from smearing
    Uid u0a47: 0.0621 ( cpu=0.0621 sensor=0.0000431 ) Excluded from smearing
    Uid u0a70: 0.0423 ( cpu=0.0410 sensor=0.00135 ) Including smearing: 0.0542 ( proportional=0.0119 )
    Uid 2000: 0.0366 ( cpu=0.0366 ) Excluded from smearing
    Uid 1036: 0.0144 ( cpu=0.0144 ) Excluded from smearing
    Uid 1021: 0.00955 ( cpu=0.00955 ) Excluded from smearing
    Uid u0a127: 0.00449 ( cpu=0.00449 ) Including smearing: 0.00576 ( proportional=0.00126 )
    Wifi: 0.00360 ( cpu=0.00353 wifi=0.0000703 ) Including smearing: 0.00461 ( proportional=0.00101 )
    Uid 1001: 0.00332 ( cpu=0.00332 ) Excluded from smearing
    Uid u0a35: 0.00304 ( cpu=0.00304 ) Including smearing: 0.00389 ( proportional=0.000854 )
    Uid u0a68: 0.00201 ( cpu=0.00201 ) Including smearing: 0.00257 ( proportional=0.000564 )
    Uid u0a25: 0.00120 ( cpu=0.00120 ) Including smearing: 0.00154 ( proportional=0.000338 )
    Uid u0a40: 0.000561 ( cpu=0.000561 ) Including smearing: 0.000719 ( proportional=0.000158 )
    Uid 1027: 0.000441 ( cpu=0.000441 ) Excluded from smearing
    Uid 9999: 0.000240 ( cpu=0.000240 ) Excluded from smearing
    Uid u0a139: 0.000119 ( cpu=0.000119 ) Including smearing: 0.000153 ( proportional=0.0000336 )
    Bluetooth: 0.000107 ( cpu=0.000107 ) Including smearing: 0.000137 ( proportional=0.0000300 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 13s 680ms realtime (12 times)
    XO_shutdown.MPSS: 24s 0ms realtime (13 times)
    XO_shutdown.ADSP: 3s 207ms realtime (5 times)
    wlan.Active: 3s 443ms realtime (12 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 4 other, 17 button, 43 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 657ms realtime (43 times)
    Sensor 7: 1m 17s 524ms realtime (0 times)
    Fg Service for: 1m 17s 524ms 
    Total running: 1m 17s 524ms 
    Total cpu time: u=7s 804ms s=7s 236ms 
    Total cpu time per freq: 2600 270 200 230 120 90 70 50 50 80 30 40 2710 90 90 70 20 40 110 50 30 2520 670 130 40 60 40 40 20 20 10 0 20 350 0 0 0 30 10 70 80 30 50 30 40 20 20 70 40 20 0 0 2030
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 370ms usr + 540ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 1s 750ms usr + 940ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 60ms usr + 130ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 130ms usr + 230ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 3s 597ms (4.6%) 7x
    Wifi Scan (actual): 3s 597ms (4.6%) 7x
    Background Wifi Scan: 110ms (0.1%) 0x
       WiFi Sleep time:  1m 17s 488ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     56ms (0.1%)
    Wake lock GCoreFlp realtime
    Wake lock NlpWakeLock realtime
    Wake lock *alarm* realtime
    Wake lock fiid-sync realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 22s 831ms full (4 times) realtime
    TOTAL wake: 22s 831ms full realtime
    Sensor GPS: 53s 39ms realtime (3 times), 13ms background (0 times)
    Sensor 16: 21s 576ms realtime (4 times)
    Foreground activities: 1m 16s 343ms realtime (19 times)
    Foreground services: 333ms realtime (2 times)
    Top for: 1m 16s 644ms 
    Fg Service for: 333ms 
    Cached for: 117ms 
    Total running: 1m 17s 94ms 
    Total cpu time: u=11s 440ms s=5s 963ms 
    Total cpu time per freq: 40 10 0 20 10 20 30 30 10 30 20 30 130 20 40 10 10 40 10 10 0 960 650 710 350 260 240 240 130 120 110 80 100 750 100 80 120 90 70 350 380 270 330 270 150 230 230 360 180 170 150 60 8650
    Proc cgeo.geocaching:
      CPU: 0ms usr + 0ms krn ; 17s 110ms fg
      1 starts
    Apk cgeo.geocaching:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 0 times

