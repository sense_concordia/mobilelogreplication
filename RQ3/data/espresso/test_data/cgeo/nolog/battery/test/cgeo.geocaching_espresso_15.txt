Daily stats:
  Current start time: 2018-08-07-07-13-31
  Next min deadline: 2018-08-08-01-00-00
  Next max deadline: 2018-08-08-03-00-00
  Current daily steps:
    Discharge total time: 10h 39m 44s 200ms  (from 3 steps)
    Discharge screen on time: 10h 39m 44s 200ms  (from 3 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 17s 445ms (99.9%) realtime, 1m 17s 446ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 17s 503ms realtime, 1m 17s 504ms uptime
  Discharge: 7.97 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 7.97 mAh
  Start clock time: 2018-08-07-11-13-54
  Screen on: 1m 17s 445ms (100.0%) 0x, Interactive: 1m 17s 445ms (100.0%)
  Screen brightnesses:
    dim 1m 17s 445ms (100.0%)
  Total full wakelock time: 22s 697ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 17s 445ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 17s 445ms (100.0%) 
     Cellular Sleep time:  55s 493ms (71.7%)
     Cellular Idle time:   21s 293ms (27.5%)
     Cellular Rx time:     660ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 17s 445ms (100.0%) 
     Wifi supplicant states:
       completed 1m 17s 445ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 17s 445ms (100.0%) 
     WiFi Sleep time:  1m 13s 828ms (95.3%)
     WiFi Idle time:   3s 561ms (4.6%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     57ms (0.1%)
     WiFi Battery drain: 0.00495mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 17s 446ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.4, actual drain: 0-35.2
    Screen: 6.71 Excluded from smearing
    Uid u0a144: 1.15 ( cpu=0.696 wifi=0.00278 gps=0.441 sensor=0.00597 ) Including smearing: 1.47 ( proportional=0.319 )
    Uid 1041: 0.635 ( cpu=0.635 ) Excluded from smearing
    Uid 1000: 0.599 ( cpu=0.597 sensor=0.00215 ) Excluded from smearing
    Uid 0: 0.498 ( cpu=0.498 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.237 ( radio=0.237 ) Excluded from smearing
    Idle: 0.211 Excluded from smearing
    Uid u0a22: 0.176 ( cpu=0.140 wifi=0.00141 sensor=0.0339 ) Excluded from smearing
    Uid u0a47: 0.0569 ( cpu=0.0569 sensor=0.0000430 ) Excluded from smearing
    Uid u0a70: 0.0413 ( cpu=0.0399 sensor=0.00135 ) Including smearing: 0.0527 ( proportional=0.0115 )
    Uid 2000: 0.0352 ( cpu=0.0352 ) Excluded from smearing
    Uid 1036: 0.0135 ( cpu=0.0135 ) Excluded from smearing
    Uid 1021: 0.00946 ( cpu=0.00946 ) Excluded from smearing
    Wifi: 0.00539 ( cpu=0.00463 wifi=0.000759 ) Including smearing: 0.00689 ( proportional=0.00150 )
    Uid u0a127: 0.00449 ( cpu=0.00449 ) Including smearing: 0.00574 ( proportional=0.00125 )
    Uid u0a68: 0.00418 ( cpu=0.00418 ) Including smearing: 0.00534 ( proportional=0.00116 )
    Uid u0a35: 0.00326 ( cpu=0.00326 ) Including smearing: 0.00417 ( proportional=0.000909 )
    Uid 1001: 0.00279 ( cpu=0.00279 ) Excluded from smearing
    Uid u0a25: 0.00116 ( cpu=0.00116 ) Including smearing: 0.00148 ( proportional=0.000322 )
    Uid 1027: 0.000440 ( cpu=0.000440 ) Excluded from smearing
    Uid u0a40: 0.000277 ( cpu=0.000277 ) Including smearing: 0.000354 ( proportional=0.0000771 )
    Uid u0a139: 0.000156 ( cpu=0.000156 ) Including smearing: 0.000199 ( proportional=0.0000435 )
    Bluetooth: 0.000117 ( cpu=0.000117 ) Including smearing: 0.000149 ( proportional=0.0000325 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 13s 741ms realtime (16 times)
    XO_shutdown.MPSS: 23s 336ms realtime (16 times)
    XO_shutdown.ADSP: 3s 206ms realtime (1 times)
    wlan.Active: 3s 613ms realtime (16 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 4 other, 17 button, 43 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 684ms realtime (43 times)
    Sensor 7: 1m 17s 435ms realtime (0 times)
    Fg Service for: 1m 17s 435ms 
    Total running: 1m 17s 435ms 
    Total cpu time: u=8s 237ms s=6s 790ms 
    Total cpu time per freq: 2700 350 220 180 60 50 50 90 60 110 60 20 3150 90 90 90 60 60 60 40 30 2720 430 120 30 10 50 10 20 0 30 10 10 310 0 30 0 10 20 100 100 40 30 30 20 30 80 30 40 20 30 20 2050
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 120ms usr + 1s 600ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 20ms usr + 140ms krn ; 0ms fg
    Proc system:
      CPU: 6s 990ms usr + 4s 40ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 180ms usr + 330ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 1s 130ms usr + 2s 220ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 2s 767ms (3.6%) 5x
    Wifi Scan (actual): 2s 767ms (3.6%) 5x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 17s 414ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     40ms (0.1%)
    Wake lock GCoreFlp realtime
    Wake lock NlpWakeLock realtime
    Wake lock *alarm* realtime
    Wake lock fiid-sync realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 22s 697ms full (4 times) realtime
    TOTAL wake: 22s 697ms full realtime
    Sensor GPS: 52s 909ms realtime (3 times), 16ms background (0 times)
    Sensor 16: 21s 475ms realtime (4 times)
    Foreground activities: 1m 16s 295ms realtime (19 times)
    Foreground services: 355ms realtime (2 times)
    Top for: 1m 16s 597ms 
    Fg Service for: 355ms 
    Cached for: 60ms 
    Total running: 1m 17s 12ms 
    Total cpu time: u=11s 457ms s=5s 937ms 
    Total cpu time per freq: 40 10 20 20 20 20 20 10 10 30 40 10 120 20 20 20 30 10 30 20 10 1000 670 630 270 230 260 190 120 100 100 140 100 710 60 100 120 80 50 410 300 250 220 210 300 300 240 310 280 180 150 100 8380
    Proc cgeo.geocaching:
      CPU: 0ms usr + 0ms krn ; 17s 80ms fg
      1 starts
    Apk cgeo.geocaching:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 0 times

