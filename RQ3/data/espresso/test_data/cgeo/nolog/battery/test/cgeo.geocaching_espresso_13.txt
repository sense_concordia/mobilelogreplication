Daily stats:
  Current start time: 2018-08-07-07-13-31
  Next min deadline: 2018-08-08-01-00-00
  Next max deadline: 2018-08-08-03-00-00
  Current daily steps:
    Discharge total time: 11h 28m 0s 300ms  (from 2 steps)
    Discharge screen on time: 11h 28m 0s 300ms  (from 2 steps)
  Daily from 2018-08-06-01-30-22 to 2018-08-07-07-13-31:
    Discharge total time: 13h 4m 45s 300ms  (from 23 steps)
    Discharge screen on time: 13h 4m 45s 300ms  (from 23 steps)
    Charge total time: 18h 48m 35s 800ms  (from 6 steps)
    Charge screen on time: 18h 48m 35s 800ms  (from 6 steps)
  Daily from 2018-08-05-10-43-15 to 2018-08-06-01-30-22:
    Discharge total time: 14h 50m 2s 700ms  (from 13 steps)
    Discharge screen on time: 14h 50m 2s 700ms  (from 13 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 17s 913ms (99.9%) realtime, 1m 17s 913ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 17s 970ms realtime, 1m 17s 971ms uptime
  Discharge: 8.74 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 8.74 mAh
  Start clock time: 2018-08-07-11-10-58
  Screen on: 1m 17s 913ms (100.0%) 0x, Interactive: 1m 17s 913ms (100.0%)
  Screen brightnesses:
    dim 1m 17s 913ms (100.0%)
  Total full wakelock time: 22s 807ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 17s 913ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 17s 913ms (100.0%) 
     Cellular Sleep time:  54s 425ms (69.9%)
     Cellular Idle time:   22s 805ms (29.3%)
     Cellular Rx time:     684ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 17s 913ms (100.0%) 
     Wifi supplicant states:
       completed 1m 17s 913ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 17s 913ms (100.0%) 
     WiFi Sleep time:  1m 14s 201ms (95.2%)
     WiFi Idle time:   3s 654ms (4.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     60ms (0.1%)
     WiFi Battery drain: 0.00518mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 17s 915ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.5, actual drain: 0
    Screen: 6.75 Excluded from smearing
    Uid u0a144: 1.17 ( cpu=0.716 wifi=0.00292 gps=0.444 sensor=0.00599 ) Including smearing: 1.49 ( proportional=0.324 )
    Uid 1041: 0.632 ( cpu=0.632 ) Excluded from smearing
    Uid 1000: 0.606 ( cpu=0.604 sensor=0.00216 ) Excluded from smearing
    Uid 0: 0.503 ( cpu=0.503 ) Excluded from smearing
    Cell standby: 0.238 ( radio=0.238 ) Excluded from smearing
    Idle: 0.212 Excluded from smearing
    Uid u0a22: 0.172 ( cpu=0.136 wifi=0.00141 sensor=0.0341 ) Excluded from smearing
    Uid u0a47: 0.0596 ( cpu=0.0595 sensor=0.0000433 ) Excluded from smearing
    Uid u0a70: 0.0454 ( cpu=0.0440 sensor=0.00137 ) Including smearing: 0.0580 ( proportional=0.0126 )
    Uid 2000: 0.0347 ( cpu=0.0347 ) Excluded from smearing
    Uid 1036: 0.0135 ( cpu=0.0135 ) Excluded from smearing
    Uid 1021: 0.0101 ( cpu=0.0101 ) Excluded from smearing
    Wifi: 0.00508 ( cpu=0.00423 wifi=0.000855 ) Including smearing: 0.00649 ( proportional=0.00141 )
    Uid u0a127: 0.00446 ( cpu=0.00446 ) Including smearing: 0.00570 ( proportional=0.00124 )
    Uid 1001: 0.00280 ( cpu=0.00280 ) Excluded from smearing
    Uid u0a68: 0.00189 ( cpu=0.00189 ) Including smearing: 0.00241 ( proportional=0.000523 )
    Uid u0a35: 0.00121 ( cpu=0.00121 ) Including smearing: 0.00155 ( proportional=0.000336 )
    Uid u0a25: 0.000681 ( cpu=0.000681 ) Including smearing: 0.000870 ( proportional=0.000189 )
    Uid 1027: 0.000360 ( cpu=0.000360 ) Excluded from smearing
    Uid u0a40: 0.000280 ( cpu=0.000280 ) Including smearing: 0.000357 ( proportional=0.0000776 )
    Uid u0a139: 0.000240 ( cpu=0.000240 ) Including smearing: 0.000306 ( proportional=0.0000665 )
    Uid u0a61: 0.000159 ( cpu=0.000159 ) Including smearing: 0.000204 ( proportional=0.0000442 )
    Bluetooth: 0.000107 ( cpu=0.000107 ) Including smearing: 0.000137 ( proportional=0.0000296 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 14s 230ms realtime (14 times)
    XO_shutdown.MPSS: 23s 557ms realtime (15 times)
    XO_shutdown.ADSP: 3s 322ms realtime (1 times)
    wlan.Active: 3s 590ms realtime (14 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 4 other, 17 button, 44 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock GnssLocationProvider realtime
    Audio: 5s 609ms realtime (43 times)
    Sensor 7: 1m 17s 904ms realtime (0 times)
    Fg Service for: 1m 17s 904ms 
    Total running: 1m 17s 904ms 
    Total cpu time: u=8s 400ms s=6s 800ms 
    Total cpu time per freq: 2570 250 260 200 110 40 80 140 70 100 80 50 3470 180 90 60 90 100 60 110 70 2440 500 130 30 50 20 10 0 20 10 0 0 300 0 30 10 20 0 90 110 60 10 80 10 40 10 60 20 30 10 10 1940
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 140ms usr + 170ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 870ms usr + 500ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 60ms usr + 150ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 2s 837ms (3.6%) 5x
    Wifi Scan (actual): 2s 837ms (3.6%) 5x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 17s 885ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     42ms (0.1%)
    Wake lock GCoreFlp realtime
    Wake lock NlpWakeLock realtime
    Wake lock *alarm* realtime
    Wake lock fiid-sync realtime
    Wake lock *launch* realtime
    Wake lock WindowManager: 22s 807ms full (4 times) realtime
    TOTAL wake: 22s 807ms full realtime
    Sensor GPS: 53s 285ms realtime (3 times), 14ms background (0 times)
    Sensor 16: 21s 554ms realtime (4 times)
    Foreground activities: 1m 16s 654ms realtime (19 times)
    Foreground services: 380ms realtime (2 times)
    Top for: 1m 16s 965ms 
    Fg Service for: 380ms 
    Cached for: 115ms 
    Total running: 1m 17s 460ms 
    Total cpu time: u=11s 850ms s=6s 0ms 
    Total cpu time per freq: 30 20 20 0 10 30 30 50 20 60 40 50 160 30 20 30 30 10 30 40 20 1100 630 660 320 370 210 200 170 70 230 60 120 780 50 180 110 90 170 340 340 320 170 280 150 230 210 370 240 350 80 40 8440
    Proc cgeo.geocaching:
      CPU: 0ms usr + 0ms krn ; 17s 450ms fg
      1 starts
    Apk cgeo.geocaching:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 0 times

