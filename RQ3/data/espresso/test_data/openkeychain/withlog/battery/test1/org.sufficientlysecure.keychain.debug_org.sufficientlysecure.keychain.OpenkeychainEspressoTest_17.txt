Daily stats:
  Current start time: 2018-10-31-03-05-14
  Next min deadline: 2018-11-01-01-00-00
  Next max deadline: 2018-11-01-03-00-00
  Current daily steps:
    Discharge total time: 13h 51m 37s 700ms  (from 46 steps)
    Discharge screen on time: 13h 51m 37s 700ms  (from 46 steps)
    Charge total time: 11h 11m 2s 700ms  (from 18 steps)
    Charge screen off time: 8h 20m 0s 0ms  (from 4 steps)
    Charge screen on time: 11h 53m 45s 200ms  (from 13 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 35s 390ms (99.9%) realtime, 1m 35s 391ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 35s 515ms realtime, 1m 35s 515ms uptime
  Discharge: 7.51 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 7.51 mAh
  Start clock time: 2018-10-31-15-25-56
  Screen on: 1m 35s 390ms (100.0%) 0x, Interactive: 1m 35s 390ms (100.0%)
  Screen brightnesses:
    dim 1m 35s 390ms (100.0%)
  Total full wakelock time: 1s 291ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 35s 390ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 35s 390ms (100.0%) 
     Cellular Sleep time:  1m 34s 34ms (98.6%)
     Cellular Idle time:   967ms (1.0%)
     Cellular Rx time:     390ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 35s 390ms (100.0%) 
     Wifi supplicant states:
       completed 1m 35s 390ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 35s 390ms (100.0%) 
     WiFi Sleep time:  1m 34s 734ms (99.3%)
     WiFi Idle time:   646ms (0.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     11ms (0.0%)
     WiFi Battery drain: 0.000943mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 35s 391ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.5, actual drain: 0
    Screen: 8.26 Excluded from smearing
    Uid u0a181: 0.550 ( cpu=0.186 sensor=0.0000528 camera=0.365 ) Including smearing: 0.630 ( proportional=0.0797 )
    Uid 1000: 0.493 ( cpu=0.493 sensor=0.000535 ) Excluded from smearing
    Uid 0: 0.361 ( cpu=0.361 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.291 ( radio=0.291 ) Excluded from smearing
    Idle: 0.260 Excluded from smearing
    Uid u0a22: 0.0548 ( cpu=0.0482 wifi=0.0000181 sensor=0.00662 ) Excluded from smearing
    Uid 2000: 0.0481 ( cpu=0.0481 ) Excluded from smearing
    Uid u0a70: 0.0327 ( cpu=0.0326 sensor=0.000160 ) Including smearing: 0.0375 ( proportional=0.00474 )
    Uid u0a47: 0.0326 ( cpu=0.0325 sensor=0.0000529 ) Excluded from smearing
    Uid u0a68: 0.0302 ( cpu=0.0302 ) Including smearing: 0.0346 ( proportional=0.00438 )
    Uid 1036: 0.0199 ( cpu=0.0199 ) Excluded from smearing
    Uid 1047: 0.0185 ( cpu=0.0185 ) Excluded from smearing
    Uid u0a61: 0.0153 ( cpu=0.0151 wifi=0.000139 ) Including smearing: 0.0175 ( proportional=0.00221 )
    Uid u0a35: 0.0123 ( cpu=0.0123 ) Including smearing: 0.0141 ( proportional=0.00178 )
    Uid u0a16: 0.0112 ( cpu=0.0112 ) Including smearing: 0.0129 ( proportional=0.00163 )
    Uid 1027: 0.00815 ( cpu=0.00815 ) Excluded from smearing
    Uid u0a40: 0.00258 ( cpu=0.00258 ) Including smearing: 0.00295 ( proportional=0.000373 )
    Uid u0a114: 0.00215 ( cpu=0.00215 ) Including smearing: 0.00246 ( proportional=0.000311 )
    Wifi: 0.00193 ( cpu=0.00114 wifi=0.000786 ) Including smearing: 0.00220 ( proportional=0.000279 )
    Uid u0a46: 0.00164 ( cpu=0.00164 ) Including smearing: 0.00188 ( proportional=0.000237 )
    Uid 1001: 0.00108 ( cpu=0.00108 ) Excluded from smearing
    Uid u0a25: 0.00105 ( cpu=0.00105 ) Including smearing: 0.00121 ( proportional=0.000153 )
    Uid u0a8: 0.000569 ( cpu=0.000569 ) Including smearing: 0.000652 ( proportional=0.0000825 )
    Uid 1040: 0.000420 ( cpu=0.000420 ) Excluded from smearing
    Uid 1041: 0.000391 ( cpu=0.000391 ) Excluded from smearing
    Uid 1053: 0.000293 ( cpu=0.000293 ) Excluded from smearing
    Bluetooth: 0.000277 ( cpu=0.000277 ) Including smearing: 0.000317 ( proportional=0.0000401 )
    Uid 1013: 0.000251 ( cpu=0.000251 ) Excluded from smearing
    Uid 1021: 0.000175 ( cpu=0.000175 ) Excluded from smearing
    Uid u0a75: 0.000167 ( cpu=0.000167 ) Including smearing: 0.000191 ( proportional=0.0000242 )
    Uid u0a127: 0.000116 ( cpu=0.000116 ) Including smearing: 0.000133 ( proportional=0.0000168 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 35s 145ms realtime (3 times)
    XO_shutdown.MPSS: 1m 34s 466ms realtime (40 times)
    XO_shutdown.ADSP: 1m 34s 127ms realtime (1 times)
    wlan.Active: 34ms realtime (3 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 1 button, 51 touch
    Wake lock *alarm* realtime
    Wake lock NetworkStats realtime
    Wake lock SyncLoopWakeLock realtime
    Sensor 4: 1s 353ms realtime (4 times)
    Sensor 9: 1s 316ms realtime (4 times)
    Sensor 14: 1s 315ms realtime (4 times)
    Fg Service for: 1m 35s 369ms 
    Total running: 1m 35s 369ms 
    Total cpu time: u=7s 424ms s=4s 737ms 
    Total cpu time per freq: 50 0 0 0 200 400 380 360 250 260 320 240 250 150 70 160 30 50 50 50 30 4360 170 60 70 70 20 20 20 10 20 50 20 60 0 30 40 30 20 30 80 40 30 60 20 60 40 10 10 30 20 10 2430
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc android.hardware.light@2.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 20ms usr + 1s 200ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 90ms krn ; 0ms fg
    Proc system:
      CPU: 6s 500ms usr + 2s 630ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 190ms usr + 340ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 160ms usr + 290ms krn ; 0ms fg
  u0a181:
    Wake lock *launch* realtime
    Wake lock WindowManager: 1s 291ms full (1 times) realtime
    TOTAL wake: 1s 291ms full realtime
    Camera: 1s 579ms realtime (1 times)
    Sensor 1: 1s 267ms realtime (1 times)
    Foreground activities: 1m 32s 923ms realtime (30 times)
    Foreground services: 756ms realtime (2 times)
    Top for: 1m 34s 155ms 
    Fg Service for: 756ms 
    Cached for: 38ms 
    Total running: 1m 34s 949ms 
    Total cpu time: u=24s 933ms s=2s 993ms 
    Total cpu time per freq: 0 0 0 0 230 180 90 120 110 220 220 260 400 250 180 160 80 60 80 50 50 4490 1770 2490 1670 1410 1160 630 380 370 160 180 170 1970 10 70 180 80 100 790 1490 1760 1270 580 450 480 320 230 280 150 50 80 30930
    Proc org.sufficientlysecure.keychain.debug:
      CPU: 0ms usr + 0ms krn ; 18s 350ms fg
      1 starts
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.sufficientlysecure.keychain.debug:passphrase_cache:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.sufficientlysecure.keychain.debug:sync:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Apk org.sufficientlysecure.keychain.debug:
      Service org.sufficientlysecure.keychain.service.KeyserverSyncAdapterService:
        Created for: 1m 33s 987ms uptime
        Starts: 1, launches: 1
      Service org.sufficientlysecure.keychain.service.PassphraseCacheService:
        Created for: 24s 933ms uptime
        Starts: 2, launches: 2
      Service org.sufficientlysecure.keychain.service.KeychainService:
        Created for: 1m 16s 204ms uptime
        Starts: 1, launches: 1
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

