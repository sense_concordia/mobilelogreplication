Daily stats:
  Current start time: 2018-10-31-03-05-14
  Next min deadline: 2018-11-01-01-00-00
  Next max deadline: 2018-11-01-03-00-00
  Current daily steps:
    Discharge total time: 17h 2m 35s 800ms  (from 17 steps)
    Discharge screen on time: 17h 2m 35s 800ms  (from 17 steps)
    Charge total time: 11h 11m 2s 700ms  (from 18 steps)
    Charge screen off time: 8h 20m 0s 0ms  (from 4 steps)
    Charge screen on time: 11h 53m 45s 200ms  (from 13 steps)
  Daily from 2018-10-30-03-04-08 to 2018-10-31-03-05-14:
    Discharge total time: 14h 53m 23s 800ms  (from 86 steps)
    Discharge screen on time: 14h 14m 29s 600ms  (from 85 steps)
    Charge total time: 1d 1h 56m 9s 700ms  (from 66 steps)
    Charge screen on time: 15h 21m 31s 100ms  (from 56 steps)
    Charge screen doze time: 3d 13h 10m 9s 900ms  (from 10 steps)
  Daily from 2018-10-29-08-14-30 to 2018-10-30-03-04-08:
    Discharge total time: 3d 11h 54m 25s 200ms  (from 13 steps)
    Discharge screen on time: 18h 20m 0s 400ms  (from 11 steps)
  Daily from 2018-10-28-04-13-33 to 2018-10-29-08-14-30:
    Discharge total time: 16d 2h 23m 13s 500ms  (from 5 steps)
    Discharge screen off time: 19d 17h 24m 37s 900ms  (from 1 steps)
    Discharge screen doze time: 7d 0h 45m 8s 900ms  (from 2 steps)
  Daily from 2018-10-27-07-24-08 to 2018-10-28-04-13-33:
    Discharge total time: 11d 2h 40m 41s 500ms  (from 2 steps)
    Discharge screen off time: 17d 8h 41m 26s 800ms  (from 1 steps)
    Discharge screen doze time: 4d 20h 39m 56s 300ms  (from 1 steps)
  Daily from 2018-10-26-01-29-55 to 2018-10-27-07-24-08:
  Daily from 2018-10-25-07-06-28 to 2018-10-26-01-29-55:
    Discharge total time: 18h 52m 12s 100ms  (from 23 steps)
    Discharge screen on time: 18h 52m 12s 100ms  (from 23 steps)
    Charge total time: 15h 26m 16s 300ms  (from 7 steps)
    Charge screen on time: 15h 26m 16s 300ms  (from 7 steps)
  Daily from 2018-10-24-03-38-25 to 2018-10-25-07-06-28:
    Discharge total time: 1d 2h 53m 5s 200ms  (from 28 steps)
    Discharge screen on time: 14h 26m 55s 100ms  (from 20 steps)
    Discharge screen doze time: 2d 22h 5m 48s 500ms  (from 5 steps)
    Charge total time: 1d 13h 34m 19s 200ms  (from 19 steps)
    Charge screen on time: 2d 10h 13m 49s 600ms  (from 11 steps)
    Charge screen doze time: 9h 10m 0s 0ms  (from 6 steps)
  Daily from 2018-10-23-03-13-17 to 2018-10-24-03-38-25:
    Discharge total time: 2d 5h 20m 1s 900ms  (from 22 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 2d 7h 43m 28s 500ms  (from 19 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 36s 76ms (99.9%) realtime, 1m 36s 75ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 36s 184ms realtime, 1m 36s 183ms uptime
  Discharge: 10.5 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 10.5 mAh
  Start clock time: 2018-10-31-12-00-41
  Screen on: 1m 36s 76ms (100.0%) 0x, Interactive: 1m 36s 76ms (100.0%)
  Screen brightnesses:
    dim 1m 36s 76ms (100.0%)
  Total full wakelock time: 1s 297ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 36s 76ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 36s 76ms (100.0%) 
     Cellular Sleep time:  1m 34s 924ms (98.8%)
     Cellular Idle time:   690ms (0.7%)
     Cellular Rx time:     462ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 36s 76ms (100.0%) 
     Wifi supplicant states:
       completed 1m 36s 76ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 1m 36s 76ms (100.0%) 
     WiFi Sleep time:  1m 35s 622ms (99.5%)
     WiFi Idle time:   448ms (0.5%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     6ms (0.0%)
     WiFi Battery drain: 0.000541mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 36s 76ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.5, actual drain: 0
    Screen: 8.32 Excluded from smearing
    Uid 1000: 0.481 ( cpu=0.480 sensor=0.000539 ) Excluded from smearing
    Uid u0a181: 0.366 ( cpu=0.000973 sensor=0.0000538 camera=0.365 ) Including smearing: 0.425 ( proportional=0.0593 )
    Uid 0: 0.350 ( cpu=0.350 ) Excluded from smearing
    Cell standby: 0.294 ( radio=0.294 ) Excluded from smearing
    Idle: 0.262 Excluded from smearing
    Uid 1036: 0.204 ( cpu=0.204 ) Excluded from smearing
    Uid u0a22: 0.0419 ( cpu=0.0419 wifi=0.00000833 ) Excluded from smearing
    Uid 2000: 0.0333 ( cpu=0.0333 ) Excluded from smearing
    Uid u0a47: 0.0320 ( cpu=0.0319 sensor=0.0000533 ) Excluded from smearing
    Uid u0a70: 0.0302 ( cpu=0.0301 sensor=0.000158 ) Including smearing: 0.0351 ( proportional=0.00489 )
    Uid u0a68: 0.0301 ( cpu=0.0301 ) Including smearing: 0.0349 ( proportional=0.00487 )
    Uid 1047: 0.0186 ( cpu=0.0186 ) Excluded from smearing
    Uid u0a61: 0.0128 ( cpu=0.0128 wifi=0.0000694 ) Including smearing: 0.0149 ( proportional=0.00208 )
    Uid u0a16: 0.00968 ( cpu=0.00968 ) Including smearing: 0.0113 ( proportional=0.00157 )
    Uid 1027: 0.00792 ( cpu=0.00792 ) Excluded from smearing
    Uid u0a40: 0.00245 ( cpu=0.00245 ) Including smearing: 0.00285 ( proportional=0.000397 )
    Uid u0a35: 0.00140 ( cpu=0.00140 ) Including smearing: 0.00162 ( proportional=0.000226 )
    Wifi: 0.00110 ( cpu=0.000633 wifi=0.000463 ) Including smearing: 0.00127 ( proportional=0.000177 )
    Uid 1001: 0.000854 ( cpu=0.000854 ) Excluded from smearing
    Uid 1013: 0.000295 ( cpu=0.000295 ) Excluded from smearing
    Uid 1041: 0.000295 ( cpu=0.000295 ) Excluded from smearing
    Uid u0a25: 0.000295 ( cpu=0.000295 ) Including smearing: 0.000343 ( proportional=0.0000478 )
    Uid 1019: 0.000126 ( cpu=0.000126 ) Excluded from smearing
    Uid 1040: 0.000126 ( cpu=0.000126 ) Excluded from smearing
    Uid 1053: 0.000126 ( cpu=0.000126 ) Excluded from smearing
    Uid u0a114: 0.000126 ( cpu=0.000126 ) Including smearing: 0.000146 ( proportional=0.0000204 )
    Uid u0a127: 0.000126 ( cpu=0.000126 ) Including smearing: 0.000146 ( proportional=0.0000204 )
    Bluetooth: 0.0000871 ( cpu=0.0000871 ) Including smearing: 0.000101 ( proportional=0.0000141 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 35s 903ms realtime (1 times)
    XO_shutdown.MPSS: 1m 35s 182ms realtime (38 times)
    XO_shutdown.ADSP: 1m 34s 848ms realtime (1 times)
    wlan.Active: 12ms realtime (1 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 1 other, 1 button, 51 touch
    Wake lock *alarm* realtime
    Sensor 4: 1s 366ms realtime (4 times)
    Sensor 9: 1s 328ms realtime (4 times)
    Sensor 14: 1s 324ms realtime (4 times)
    Fg Service for: 1m 36s 52ms 
    Total running: 1m 36s 52ms 
    Total cpu time: u=7s 150ms s=4s 667ms 
    Total cpu time per freq: 30 0 0 0 220 320 350 380 220 430 240 150 210 80 120 90 50 20 50 10 30 4590 60 110 50 110 60 40 0 70 0 10 0 60 20 30 40 40 30 30 100 110 40 10 50 90 30 60 50 10 0 0 2150
    Proc android.hardware.memtrack@1.0-service:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc android.hardware.light@2.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 160ms usr + 1s 250ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 40ms usr + 90ms krn ; 0ms fg
    Proc system:
      CPU: 6s 920ms usr + 2s 910ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 210ms usr + 360ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 190ms usr + 280ms krn ; 0ms fg
    Proc android.hardware.contexthub@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a181:
    Wake lock *launch* realtime
    Wake lock WindowManager: 1s 297ms full (1 times) realtime
    TOTAL wake: 1s 297ms full realtime
    Camera: 1s 579ms realtime (1 times)
    Sensor 1: 1s 291ms realtime (1 times)
    Foreground activities: 1m 33s 645ms realtime (30 times)
    Foreground services: 724ms realtime (2 times)
    Top for: 1m 34s 857ms 
    Fg Service for: 724ms 
    Cached for: 49ms 
    Total running: 1m 35s 630ms 
    Total cpu time: u=25s 323ms s=2s 940ms 
    Total cpu time per freq: 10 0 0 0 200 260 150 160 100 200 330 320 470 370 190 120 90 80 80 80 10 5230 2530 3430 2130 2260 1530 890 430 300 280 160 250 2920 80 240 120 310 120 980 1970 2300 1260 820 710 660 510 590 440 470 40 120 41830
    Proc org.sufficientlysecure.keychain.debug:
      CPU: 0ms usr + 0ms krn ; 18s 680ms fg
      1 starts
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.sufficientlysecure.keychain.debug:passphrase_cache:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.sufficientlysecure.keychain.debug:sync:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Apk org.sufficientlysecure.keychain.debug:
      Service org.sufficientlysecure.keychain.service.KeyserverSyncAdapterService:
        Created for: 1m 34s 696ms uptime
        Starts: 1, launches: 1
      Service org.sufficientlysecure.keychain.service.PassphraseCacheService:
        Created for: 24s 957ms uptime
        Starts: 2, launches: 2
      Service org.sufficientlysecure.keychain.service.KeychainService:
        Created for: 1m 16s 923ms uptime
        Starts: 1, launches: 1
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1

