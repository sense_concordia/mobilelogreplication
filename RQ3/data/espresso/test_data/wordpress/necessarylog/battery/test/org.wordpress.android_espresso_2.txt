Daily stats:
  Current start time: 2018-08-05-10-43-15
  Next min deadline: 2018-08-06-01-00-00
  Next max deadline: 2018-08-06-03-00-00
  Current daily steps:
    Discharge total time: 14h 14m 55s 600ms  (from 8 steps)
    Discharge screen on time: 14h 14m 55s 600ms  (from 8 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 57s 16ms (99.9%) realtime, 57s 15ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 57s 60ms realtime, 57s 60ms uptime
  Discharge: 5.42 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 5.42 mAh
  Start clock time: 2018-08-05-23-14-45
  Screen on: 57s 16ms (100.0%) 0x, Interactive: 57s 16ms (100.0%)
  Screen brightnesses:
    dark 57s 16ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 57s 16ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  57s 16ms (100.0%) 
     Cellular Sleep time:  56s 271ms (98.7%)
     Cellular Idle time:   178ms (0.3%)
     Cellular Rx time:     567ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 57s 16ms (100.0%) 
     Wifi supplicant states:
       completed 57s 16ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 57s 16ms (100.0%) 
     WiFi Sleep time:  57s 16ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  57s 16ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.75, actual drain: 0
    Screen: 3.25 Excluded from smearing
    Uid u0a139: 0.626 ( cpu=0.626 ) Including smearing: 0.875 ( proportional=0.249 )
    Uid 1041: 0.603 ( cpu=0.603 ) Excluded from smearing
    Uid 0: 0.408 ( cpu=0.408 ) Excluded from smearing
    Uid 1000: 0.347 ( cpu=0.345 sensor=0.00158 ) Excluded from smearing
    Cell standby: 0.174 ( radio=0.174 ) Excluded from smearing
    Idle: 0.155 Excluded from smearing
    Uid u0a70: 0.0455 ( cpu=0.0454 sensor=0.000158 ) Including smearing: 0.0636 ( proportional=0.0181 )
    Uid u0a22: 0.0397 ( cpu=0.0358 sensor=0.00396 ) Excluded from smearing
    Uid 2000: 0.0281 ( cpu=0.0281 ) Excluded from smearing
    Uid u0a47: 0.0277 ( cpu=0.0277 sensor=0.0000317 ) Excluded from smearing
    Uid u0a61: 0.0154 ( cpu=0.0154 ) Including smearing: 0.0215 ( proportional=0.00612 )
    Uid u0a68: 0.00872 ( cpu=0.00872 ) Including smearing: 0.0122 ( proportional=0.00347 )
    Uid 1036: 0.00726 ( cpu=0.00726 ) Excluded from smearing
    Uid u0a144: 0.00277 ( cpu=0.00277 ) Including smearing: 0.00388 ( proportional=0.00110 )
    Uid u0a40: 0.00263 ( cpu=0.00263 ) Including smearing: 0.00368 ( proportional=0.00105 )
    Uid u0a25: 0.00145 ( cpu=0.00145 ) Including smearing: 0.00202 ( proportional=0.000575 )
    Uid u0a35: 0.00102 ( cpu=0.00102 ) Including smearing: 0.00142 ( proportional=0.000405 )
    Uid 1001: 0.000826 ( cpu=0.000826 ) Excluded from smearing
    Uid u0a127: 0.000544 ( cpu=0.000544 ) Including smearing: 0.000760 ( proportional=0.000216 )
    Uid 1053: 0.000116 ( cpu=0.000116 ) Excluded from smearing
    Wifi: 0.0000831 ( cpu=0.0000831 ) Including smearing: 0.000116 ( proportional=0.0000331 )
    Bluetooth: 0.0000831 ( cpu=0.0000831 ) Including smearing: 0.000116 ( proportional=0.0000331 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 56s 899ms realtime (2 times)
    XO_shutdown.MPSS: 56s 200ms realtime (25 times)
    XO_shutdown.ADSP: 3s 18ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 2 button, 39 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 969ms realtime (38 times)
    Sensor 7: 57s 5ms realtime (0 times)
    Fg Service for: 57s 5ms 
    Total running: 57s 5ms 
    Total cpu time: u=5s 257ms s=4s 370ms 
    Total cpu time per freq: 2650 400 80 60 50 70 40 40 50 50 90 40 1420 70 50 10 20 20 100 50 30 1610 220 10 30 50 20 20 20 10 10 0 0 60 0 0 0 10 0 20 130 140 100 30 20 0 20 40 20 10 10 0 980
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 260ms usr + 510ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 1s 180ms usr + 750ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 90ms usr + 170ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 140ms usr + 290ms krn ; 0ms fg
  u0a139:
    Wake lock *alarm* realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService realtime
    Wake lock *launch* realtime
    Foreground activities: 55s 593ms realtime (14 times)
    Foreground services: 637ms realtime (2 times)
    Top for: 55s 848ms 
    Fg Service for: 637ms 
    Cached for: 60ms 
    Total running: 56s 545ms 
    Total cpu time: u=14s 229ms s=1s 867ms 
    Total cpu time per freq: 100 20 30 40 60 80 40 70 70 80 120 10 310 110 140 40 20 30 20 0 10 1470 640 1100 440 520 410 390 60 150 120 30 10 1370 20 50 90 20 40 420 1190 1000 570 220 220 220 190 200 160 120 0 70 8750
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 0ms usr + 0ms krn ; 9s 930ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service org.wordpress.android.push.GCMRegistrationIntentService:
        Created for: 573ms uptime
        Starts: 1, launches: 1
      Service org.wordpress.android.ui.notifications.services.NotificationsUpdateService:
        Created for: 545ms uptime
        Starts: 1, launches: 1
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 364ms uptime
        Starts: 2, launches: 2

