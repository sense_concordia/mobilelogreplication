Daily stats:
  Current start time: 2018-08-05-10-43-15
  Next min deadline: 2018-08-06-01-00-00
  Next max deadline: 2018-08-06-03-00-00
  Current daily steps:
    Discharge total time: 14h 15m 19s 700ms  (from 10 steps)
    Discharge screen on time: 14h 15m 19s 700ms  (from 10 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 56s 765ms (99.9%) realtime, 56s 765ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 56s 803ms realtime, 56s 803ms uptime
  Discharge: 4.77 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 4.77 mAh
  Start clock time: 2018-08-05-23-39-19
  Screen on: 56s 765ms (100.0%) 0x, Interactive: 56s 765ms (100.0%)
  Screen brightnesses:
    dark 56s 765ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 56s 765ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  56s 765ms (100.0%) 
     Cellular Sleep time:  56s 98ms (98.8%)
     Cellular Idle time:   200ms (0.4%)
     Cellular Rx time:     467ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 56s 765ms (100.0%) 
     Wifi supplicant states:
       completed 56s 765ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 56s 765ms (100.0%) 
     WiFi Sleep time:  56s 765ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  56s 765ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.10, actual drain: 0
    Screen: 3.24 Excluded from smearing
    Uid 1041: 0.592 ( cpu=0.592 ) Excluded from smearing
    Uid 0: 0.412 ( cpu=0.412 ) Excluded from smearing
    Uid 1000: 0.348 ( cpu=0.346 sensor=0.00158 ) Excluded from smearing
    Cell standby: 0.173 ( radio=0.173 ) Excluded from smearing
    Idle: 0.155 Excluded from smearing
    Uid u0a70: 0.0446 ( cpu=0.0444 sensor=0.000160 ) Including smearing: 0.0655 ( proportional=0.0210 )
    Uid u0a22: 0.0409 ( cpu=0.0369 sensor=0.00394 ) Excluded from smearing
    Uid 2000: 0.0283 ( cpu=0.0283 ) Excluded from smearing
    Uid u0a47: 0.0279 ( cpu=0.0278 sensor=0.0000315 ) Excluded from smearing
    Uid u0a61: 0.0108 ( cpu=0.0108 ) Including smearing: 0.0158 ( proportional=0.00505 )
    Uid u0a68: 0.00866 ( cpu=0.00866 ) Including smearing: 0.0127 ( proportional=0.00407 )
    Uid 1036: 0.00783 ( cpu=0.00783 ) Excluded from smearing
    Uid u0a35: 0.00503 ( cpu=0.00503 ) Including smearing: 0.00739 ( proportional=0.00236 )
    Uid u0a144: 0.00281 ( cpu=0.00281 ) Including smearing: 0.00413 ( proportional=0.00132 )
    Uid u0a40: 0.00266 ( cpu=0.00266 ) Including smearing: 0.00391 ( proportional=0.00125 )
    Uid u0a25: 0.00166 ( cpu=0.00166 ) Including smearing: 0.00245 ( proportional=0.000782 )
    Uid u0a139: 0.00101 ( cpu=0.00101 ) Including smearing: 0.00148 ( proportional=0.000474 )
    Uid 1001: 0.000966 ( cpu=0.000966 ) Excluded from smearing
    Wifi: 0.000698 ( cpu=0.000698 ) Including smearing: 0.00103 ( proportional=0.000328 )
    Uid u0a127: 0.000615 ( cpu=0.000615 ) Including smearing: 0.000903 ( proportional=0.000289 )
    Bluetooth: 0.000154 ( cpu=0.000154 ) Including smearing: 0.000227 ( proportional=0.0000726 )
    Uid 1053: 0.000116 ( cpu=0.000116 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 56s 652ms realtime (2 times)
    XO_shutdown.MPSS: 56s 29ms realtime (24 times)
    XO_shutdown.ADSP: 2s 670ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 2 button, 39 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Wake lock NetworkStats realtime
    Audio: 4s 958ms realtime (38 times)
    Sensor 7: 56s 755ms realtime (0 times)
    Fg Service for: 56s 755ms 
    Total running: 56s 755ms 
    Total cpu time: u=5s 280ms s=4s 356ms 
    Total cpu time per freq: 2490 350 130 70 80 50 60 50 20 70 50 40 1550 90 120 60 40 30 60 10 60 1630 230 40 20 40 30 40 0 10 30 20 0 100 20 0 0 30 0 40 130 80 40 20 40 20 20 30 20 10 0 0 940
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 100ms usr + 200ms krn ; 0ms fg
    Proc system:
      CPU: 210ms usr + 290ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 60ms usr + 110ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 110ms usr + 200ms krn ; 0ms fg
  u0a139:
    Wake lock *alarm* realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService realtime
    Wake lock *launch* realtime
    Foreground activities: 55s 331ms realtime (14 times)
    Foreground services: 656ms realtime (2 times)
    Top for: 55s 583ms 
    Fg Service for: 656ms 
    Cached for: 57ms 
    Total running: 56s 296ms 
    Total cpu time: u=13s 996ms s=1s 874ms 
    Total cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 30
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 0ms usr + 0ms krn ; 9s 810ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service org.wordpress.android.push.GCMRegistrationIntentService:
        Created for: 612ms uptime
        Starts: 1, launches: 1
      Service org.wordpress.android.ui.notifications.services.NotificationsUpdateService:
        Created for: 582ms uptime
        Starts: 1, launches: 1
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 376ms uptime
        Starts: 2, launches: 2

