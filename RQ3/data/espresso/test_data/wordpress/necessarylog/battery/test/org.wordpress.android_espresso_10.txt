Daily stats:
  Current start time: 2018-08-05-10-43-15
  Next min deadline: 2018-08-06-01-00-00
  Next max deadline: 2018-08-06-03-00-00
  Current daily steps:
    Discharge total time: 13h 55m 21s 700ms  (from 9 steps)
    Discharge screen on time: 13h 55m 21s 700ms  (from 9 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 57s 47ms (99.9%) realtime, 57s 46ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 57s 87ms realtime, 57s 87ms uptime
  Discharge: 6.06 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 6.06 mAh
  Start clock time: 2018-08-05-23-23-41
  Screen on: 57s 47ms (100.0%) 0x, Interactive: 57s 47ms (100.0%)
  Screen brightnesses:
    dark 57s 47ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 57s 47ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  57s 47ms (100.0%) 
     Cellular Sleep time:  56s 316ms (98.7%)
     Cellular Idle time:   231ms (0.4%)
     Cellular Rx time:     500ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 57s 47ms (100.0%) 
     Wifi supplicant states:
       completed 57s 47ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 57s 47ms (100.0%) 
     WiFi Sleep time:  57s 47ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  57s 47ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.75, actual drain: 0-35.2
    Screen: 3.25 Excluded from smearing
    Uid u0a139: 0.613 ( cpu=0.613 ) Including smearing: 0.860 ( proportional=0.247 )
    Uid 1041: 0.584 ( cpu=0.584 ) Excluded from smearing
    Uid 0: 0.417 ( cpu=0.417 ) Excluded from smearing
    Uid 1000: 0.368 ( cpu=0.366 sensor=0.00158 ) Excluded from smearing
    Cell standby: 0.174 ( radio=0.174 ) Excluded from smearing
    Idle: 0.155 Excluded from smearing
    Uid u0a22: 0.0431 ( cpu=0.0391 sensor=0.00396 ) Excluded from smearing
    Uid u0a70: 0.0420 ( cpu=0.0418 sensor=0.000170 ) Including smearing: 0.0589 ( proportional=0.0169 )
    Uid u0a47: 0.0290 ( cpu=0.0290 sensor=0.0000317 ) Excluded from smearing
    Uid 2000: 0.0283 ( cpu=0.0283 ) Excluded from smearing
    Uid u0a61: 0.0144 ( cpu=0.0144 ) Including smearing: 0.0202 ( proportional=0.00580 )
    Uid u0a68: 0.00935 ( cpu=0.00935 ) Including smearing: 0.0131 ( proportional=0.00377 )
    Uid 1036: 0.00769 ( cpu=0.00769 ) Excluded from smearing
    Uid u0a144: 0.00269 ( cpu=0.00269 ) Including smearing: 0.00377 ( proportional=0.00108 )
    Uid u0a40: 0.00243 ( cpu=0.00243 ) Including smearing: 0.00341 ( proportional=0.000979 )
    Uid u0a35: 0.00207 ( cpu=0.00207 ) Including smearing: 0.00290 ( proportional=0.000833 )
    Uid u0a25: 0.00178 ( cpu=0.00178 ) Including smearing: 0.00250 ( proportional=0.000720 )
    Uid 1001: 0.00120 ( cpu=0.00120 ) Excluded from smearing
    Uid u0a127: 0.000600 ( cpu=0.000600 ) Including smearing: 0.000843 ( proportional=0.000242 )
    Wifi: 0.000538 ( cpu=0.000538 ) Including smearing: 0.000755 ( proportional=0.000217 )
    Uid 1053: 0.000120 ( cpu=0.000120 ) Excluded from smearing
    Bluetooth: 0.0000827 ( cpu=0.0000827 ) Including smearing: 0.000116 ( proportional=0.0000334 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 56s 904ms realtime (5 times)
    XO_shutdown.MPSS: 56s 245ms realtime (27 times)
    XO_shutdown.ADSP: 3s 85ms realtime (0 times)
    wlan.Active: 56ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 2 button, 40 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 5s 7ms realtime (38 times)
    Sensor 7: 57s 38ms realtime (0 times)
    Fg Service for: 57s 38ms 
    Total running: 57s 38ms 
    Total cpu time: u=5s 450ms s=4s 657ms 
    Total cpu time per freq: 2820 290 70 90 40 60 50 60 20 30 50 30 1670 90 70 120 40 50 40 30 50 1630 170 10 10 20 10 10 10 0 10 20 20 70 0 20 50 20 20 40 110 40 90 40 40 20 60 20 20 20 0 10 1170
    Proc android.hardware.memtrack@1.0-service:
      CPU: 40ms usr + 40ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 1s 10ms usr + 1s 150ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 3s 600ms usr + 2s 280ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 150ms usr + 320ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 220ms usr + 320ms krn ; 0ms fg
  u0a139:
    Wake lock *alarm* realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService realtime
    Wake lock *launch* realtime
    Foreground activities: 55s 535ms realtime (14 times)
    Foreground services: 636ms realtime (2 times)
    Top for: 55s 799ms 
    Fg Service for: 636ms 
    Cached for: 106ms 
    Total running: 56s 541ms 
    Total cpu time: u=14s 102ms s=1s 993ms 
    Total cpu time per freq: 160 0 10 10 140 100 130 90 100 80 50 40 610 120 220 170 80 40 70 20 20 2290 930 1410 830 580 620 240 170 150 100 80 50 2140 60 90 120 70 10 640 1670 1480 1140 320 190 520 340 280 320 70 80 50 12400
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 860ms usr + 160ms krn ; 9s 900ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service org.wordpress.android.push.GCMRegistrationIntentService:
        Created for: 599ms uptime
        Starts: 1, launches: 1
      Service org.wordpress.android.ui.notifications.services.NotificationsUpdateService:
        Created for: 568ms uptime
        Starts: 1, launches: 1
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 377ms uptime
        Starts: 2, launches: 2

