Daily stats:
  Current start time: 2018-08-05-10-43-15
  Next min deadline: 2018-08-06-01-00-00
  Next max deadline: 2018-08-06-03-00-00
  Current daily steps:
    Discharge total time: 14h 14m 55s 600ms  (from 8 steps)
    Discharge screen on time: 14h 14m 55s 600ms  (from 8 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 57s 48ms (99.9%) realtime, 57s 48ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 57s 85ms realtime, 57s 85ms uptime
  Discharge: 5.14 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 5.14 mAh
  Start clock time: 2018-08-05-23-15-52
  Screen on: 57s 48ms (100.0%) 0x, Interactive: 57s 48ms (100.0%)
  Screen brightnesses:
    dark 57s 48ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 57s 48ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  57s 48ms (100.0%) 
     Cellular Sleep time:  56s 409ms (98.9%)
     Cellular Idle time:   210ms (0.4%)
     Cellular Rx time:     429ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 57s 48ms (100.0%) 
     Wifi supplicant states:
       completed 57s 48ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 57s 48ms (100.0%) 
     WiFi Sleep time:  57s 49ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  57s 49ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.13, actual drain: 0
    Screen: 3.25 Excluded from smearing
    Uid 1041: 0.615 ( cpu=0.615 ) Excluded from smearing
    Uid 0: 0.413 ( cpu=0.413 ) Excluded from smearing
    Uid 1000: 0.339 ( cpu=0.338 sensor=0.00158 ) Excluded from smearing
    Cell standby: 0.174 ( radio=0.174 ) Excluded from smearing
    Idle: 0.155 Excluded from smearing
    Uid u0a70: 0.0463 ( cpu=0.0461 sensor=0.000169 ) Including smearing: 0.0681 ( proportional=0.0218 )
    Uid u0a22: 0.0392 ( cpu=0.0352 sensor=0.00396 ) Excluded from smearing
    Uid 2000: 0.0290 ( cpu=0.0290 ) Excluded from smearing
    Uid u0a47: 0.0224 ( cpu=0.0223 sensor=0.0000317 ) Excluded from smearing
    Uid u0a61: 0.0118 ( cpu=0.0118 ) Including smearing: 0.0173 ( proportional=0.00553 )
    Uid u0a68: 0.00914 ( cpu=0.00914 ) Including smearing: 0.0134 ( proportional=0.00430 )
    Uid 1036: 0.00727 ( cpu=0.00727 ) Excluded from smearing
    Uid u0a144: 0.00541 ( cpu=0.00541 ) Including smearing: 0.00796 ( proportional=0.00254 )
    Uid u0a40: 0.00243 ( cpu=0.00243 ) Including smearing: 0.00357 ( proportional=0.00114 )
    Uid u0a139: 0.00101 ( cpu=0.00101 ) Including smearing: 0.00148 ( proportional=0.000475 )
    Uid u0a35: 0.000931 ( cpu=0.000931 ) Including smearing: 0.00137 ( proportional=0.000438 )
    Uid 1001: 0.000837 ( cpu=0.000837 ) Excluded from smearing
    Uid u0a25: 0.000659 ( cpu=0.000659 ) Including smearing: 0.000969 ( proportional=0.000310 )
    Wifi: 0.000296 ( cpu=0.000296 ) Including smearing: 0.000435 ( proportional=0.000139 )
    Uid u0a127: 0.000271 ( cpu=0.000271 ) Including smearing: 0.000398 ( proportional=0.000127 )
    Uid 1027: 0.000115 ( cpu=0.000115 ) Excluded from smearing
    Uid 1053: 0.000115 ( cpu=0.000115 ) Excluded from smearing
    Bluetooth: 0.000110 ( cpu=0.000110 ) Including smearing: 0.000162 ( proportional=0.0000519 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 56s 911ms realtime (2 times)
    XO_shutdown.MPSS: 56s 306ms realtime (26 times)
    XO_shutdown.ADSP: 2s 732ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 2 button, 39 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 905ms realtime (38 times)
    Sensor 7: 57s 40ms realtime (0 times)
    Fg Service for: 57s 40ms 
    Total running: 57s 40ms 
    Total cpu time: u=4s 976ms s=4s 426ms 
    Total cpu time per freq: 2610 380 100 60 70 70 60 40 30 60 20 40 1640 90 120 40 20 10 40 20 10 1340 150 10 20 20 30 20 0 10 0 10 50 60 20 0 0 0 30 20 130 110 30 20 50 30 10 30 0 10 20 0 930
    Proc android.hardware.memtrack@1.0-service:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 620ms usr + 790ms krn ; 0ms fg
    Proc android.hardware.graphics.allocator@2.0-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 2s 250ms usr + 1s 460ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 130ms usr + 240ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 150ms usr + 250ms krn ; 0ms fg
  u0a139:
    Wake lock *alarm* realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService realtime
    Wake lock *launch* realtime
    Foreground activities: 55s 564ms realtime (14 times)
    Foreground services: 655ms realtime (2 times)
    Top for: 55s 806ms 
    Fg Service for: 655ms 
    Cached for: 97ms 
    Total running: 56s 558ms 
    Total cpu time: u=13s 996ms s=2s 33ms 
    Total cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 20
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 0ms usr + 0ms krn ; 9s 760ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service org.wordpress.android.push.GCMRegistrationIntentService:
        Created for: 601ms uptime
        Starts: 1, launches: 1
      Service org.wordpress.android.ui.notifications.services.NotificationsUpdateService:
        Created for: 574ms uptime
        Starts: 1, launches: 1
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 379ms uptime
        Starts: 2, launches: 2

