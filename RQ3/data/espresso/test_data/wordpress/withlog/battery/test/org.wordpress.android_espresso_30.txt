Daily stats:
  Current start time: 2018-08-05-10-43-15
  Next min deadline: 2018-08-06-01-00-00
  Next max deadline: 2018-08-06-03-00-00
  Current daily steps:
    Discharge total time: 14h 14m 55s 600ms  (from 8 steps)
    Discharge screen on time: 14h 14m 55s 600ms  (from 8 steps)
  Daily from 2018-08-04-03-22-45 to 2018-08-05-10-43-15:
    Discharge total time: 31d 22h 39m 36s 400ms  (from 1 steps)
  Daily from 2018-08-03-04-18-55 to 2018-08-04-03-22-45:
    Discharge total time: 3d 7h 22m 43s 600ms  (from 9 steps)
    Discharge screen doze time: 1d 6h 51m 6s 400ms  (from 8 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 57s 459ms (99.9%) realtime, 57s 459ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 57s 491ms realtime, 57s 491ms uptime
  Discharge: 5.41 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 5.41 mAh
  Start clock time: 2018-08-05-17-49-05
  Screen on: 57s 459ms (100.0%) 0x, Interactive: 57s 459ms (100.0%)
  Screen brightnesses:
    dark 57s 459ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 57s 459ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  57s 459ms (100.0%) 
     Cellular Sleep time:  56s 912ms (99.0%)
     Cellular Idle time:   159ms (0.3%)
     Cellular Rx time:     390ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 57s 459ms (100.0%) 
     Wifi supplicant states:
       completed 57s 459ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         great (greater than -55dBm): 57s 459ms (100.0%) 
     WiFi Sleep time:  57s 462ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  57s 462ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.13, actual drain: 0
    Screen: 3.28 Excluded from smearing
    Uid 1041: 0.568 ( cpu=0.568 ) Excluded from smearing
    Uid 0: 0.385 ( cpu=0.385 ) Excluded from smearing
    Uid 1000: 0.357 ( cpu=0.355 sensor=0.00160 ) Excluded from smearing
    Cell standby: 0.176 ( radio=0.176 ) Excluded from smearing
    Idle: 0.157 Excluded from smearing
    Uid u0a22: 0.0524 ( cpu=0.0484 sensor=0.00399 ) Excluded from smearing
    Uid u0a70: 0.0416 ( cpu=0.0414 sensor=0.000212 ) Including smearing: 0.0607 ( proportional=0.0191 )
    Uid u0a47: 0.0364 ( cpu=0.0364 sensor=0.0000319 ) Excluded from smearing
    Uid 2000: 0.0317 ( cpu=0.0317 ) Excluded from smearing
    Uid u0a61: 0.0174 ( cpu=0.0174 ) Including smearing: 0.0254 ( proportional=0.00799 )
    Uid u0a68: 0.00888 ( cpu=0.00888 ) Including smearing: 0.0130 ( proportional=0.00408 )
    Uid 1036: 0.00805 ( cpu=0.00805 ) Excluded from smearing
    Uid u0a40: 0.00406 ( cpu=0.00406 ) Including smearing: 0.00593 ( proportional=0.00187 )
    Uid u0a25: 0.00305 ( cpu=0.00305 ) Including smearing: 0.00445 ( proportional=0.00140 )
    Uid u0a144: 0.00305 ( cpu=0.00305 ) Including smearing: 0.00445 ( proportional=0.00140 )
    Uid u0a35: 0.00149 ( cpu=0.00149 ) Including smearing: 0.00218 ( proportional=0.000685 )
    Uid u0a139: 0.00113 ( cpu=0.00113 ) Including smearing: 0.00165 ( proportional=0.000519 )
    Uid u0a127: 0.00101 ( cpu=0.00101 ) Including smearing: 0.00147 ( proportional=0.000462 )
    Uid 1001: 0.000995 ( cpu=0.000995 ) Excluded from smearing
    Wifi: 0.000414 ( cpu=0.000414 ) Including smearing: 0.000604 ( proportional=0.000190 )
    Uid 1053: 0.000272 ( cpu=0.000272 ) Excluded from smearing
    Bluetooth: 0.000116 ( cpu=0.000116 ) Including smearing: 0.000169 ( proportional=0.0000532 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 57s 347ms realtime (2 times)
    XO_shutdown.MPSS: 56s 803ms realtime (26 times)
    XO_shutdown.ADSP: 3s 21ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    User activity: 2 button, 40 touch
    Wake lock *alarm* realtime
    Wake lock AudioMix realtime
    Audio: 4s 933ms realtime (38 times)
    Sensor 7: 57s 451ms realtime (0 times)
    Fg Service for: 57s 451ms 
    Total running: 57s 451ms 
    Total cpu time: u=5s 400ms s=4s 407ms 
    Total cpu time per freq: 2330 260 60 110 110 90 70 20 60 70 30 20 1480 80 90 70 30 40 60 60 10 1560 460 40 80 60 30 50 10 30 10 10 20 100 0 0 10 10 0 20 270 150 50 50 40 20 40 30 10 20 0 0 960
  u0a139:
    Wake lock *alarm* realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService realtime
    Wake lock *launch* realtime
    Foreground activities: 55s 633ms realtime (14 times)
    Foreground services: 613ms realtime (2 times)
    Top for: 55s 863ms 
    Fg Service for: 613ms 
    Cached for: 38ms 
    Total running: 56s 514ms 
    Total cpu time: u=15s 200ms s=2s 466ms 
    Total cpu time per freq: 160 10 10 20 140 140 80 80 100 180 70 150 500 160 140 110 60 20 110 20 10 1970 920 1430 850 730 740 340 240 140 100 50 50 2090 20 100 0 150 0 660 2270 2110 730 560 490 430 300 390 60 160 30 60 13160
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 0ms usr + 0ms krn ; 9s 740ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service org.wordpress.android.push.GCMRegistrationIntentService:
        Created for: 603ms uptime
        Starts: 1, launches: 1
      Service org.wordpress.android.ui.notifications.services.NotificationsUpdateService:
        Created for: 571ms uptime
        Starts: 1, launches: 1
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 396ms uptime
        Starts: 2, launches: 2

