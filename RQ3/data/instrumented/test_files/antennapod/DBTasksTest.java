package de.test.antennapod.storage;

import android.content.Context;
import android.test.FlakyTest;
import android.test.InstrumentationTestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.danoeh.antennapod.core.feed.Feed;
import de.danoeh.antennapod.core.feed.FeedItem;
import de.danoeh.antennapod.core.feed.FeedMedia;
import de.danoeh.antennapod.core.preferences.UserPreferences;
import de.danoeh.antennapod.core.storage.DBReader;
import de.danoeh.antennapod.core.storage.DBTasks;
import de.danoeh.antennapod.core.storage.PodDBAdapter;

/**
 * Test class for DBTasks
 */
public class DBTasksTest extends InstrumentationTestCase {

    private static final String TAG = "DBTasksTest";

    private Context context;

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        assertTrue(PodDBAdapter.deleteDatabase());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        context = getInstrumentation().getTargetContext();

        // create new database
        PodDBAdapter.init(context);
        PodDBAdapter.deleteDatabase();
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.close();

        UserPreferences.init(context);
    }

    @FlakyTest(tolerance = 3)
    public void testUpdateFeedNewFeed() {
        final int NUM_ITEMS = 10;

        Feed feed = new Feed("url", null, "title");
        feed.setItems(new ArrayList<>());
        for (int i = 0; i < NUM_ITEMS; i++) {
            feed.getItems().add(new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(), FeedItem.UNPLAYED, feed));
        }
        Feed newFeed = DBTasks.updateFeed(context, feed)[0];

        assertTrue(newFeed == feed);
        assertTrue(feed.getId() != 0);
        for (FeedItem item : feed.getItems()) {
            assertFalse(item.isPlayed());
            assertTrue(item.getId() != 0);
        }

        try {
            tearDown();
        } catch (Exception e) {

        }


        for (int aa = 0; aa < 60; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int NUM_ITEMS1 = 10;

            Feed feed1 = new Feed("url", null, "title");
            feed1.setItems(new ArrayList<>());
            for (int i = 0; i < NUM_ITEMS1; i++) {
                feed1.getItems().add(new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(), FeedItem.UNPLAYED, feed1));
            }
            Feed newFeed1 = DBTasks.updateFeed(context, feed1)[0];

            assertTrue(newFeed1 == feed1);
            assertTrue(feed1.getId() != 0);
            for (FeedItem item : feed1.getItems()) {
                assertFalse(item.isPlayed());
                assertTrue(item.getId() != 0);
            }

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int NUM_ITEMS1 = 10;

        Feed feed1 = new Feed("url", null, "title");
        feed1.setItems(new ArrayList<>());
        for (int i = 0; i < NUM_ITEMS1; i++) {
            feed1.getItems().add(new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(), FeedItem.UNPLAYED, feed1));
        }
        Feed newFeed1 = DBTasks.updateFeed(context, feed1)[0];

        assertTrue(newFeed1 == feed1);
        assertTrue(feed1.getId() != 0);
        for (FeedItem item : feed1.getItems()) {
            assertFalse(item.isPlayed());
            assertTrue(item.getId() != 0);
        }
    }

    /** Two feeds with the same title, but different download URLs should be treated as different feeds. */
    public void testUpdateFeedSameTitle() {
        Feed feed1 = new Feed("url1", null, "title");
        Feed feed2 = new Feed("url2", null, "title");

        feed1.setItems(new ArrayList<>());
        feed2.setItems(new ArrayList<>());

        Feed savedFeed1 = DBTasks.updateFeed(context, feed1)[0];
        Feed savedFeed2 = DBTasks.updateFeed(context, feed2)[0];

        assertTrue(savedFeed1.getId() != savedFeed2.getId());

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 60; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            Feed feed11 = new Feed("url1", null, "title");
            Feed feed21 = new Feed("url2", null, "title");

            feed11.setItems(new ArrayList<>());
            feed21.setItems(new ArrayList<>());

            Feed savedFeed11 = DBTasks.updateFeed(context, feed11)[0];
            Feed savedFeed21 = DBTasks.updateFeed(context, feed21)[0];

            assertTrue(savedFeed11.getId() != savedFeed21.getId());

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        Feed feed11 = new Feed("url1", null, "title");
        Feed feed21 = new Feed("url2", null, "title");

        feed11.setItems(new ArrayList<>());
        feed21.setItems(new ArrayList<>());

        Feed savedFeed11 = DBTasks.updateFeed(context, feed11)[0];
        Feed savedFeed21 = DBTasks.updateFeed(context, feed21)[0];

        assertTrue(savedFeed11.getId() != savedFeed21.getId());
    }

    public void testUpdateFeedUpdatedFeed() {
        final int NUM_ITEMS_OLD = 10;
        final int NUM_ITEMS_NEW = 10;

        final Feed feed = new Feed("url", null, "title");
        feed.setItems(new ArrayList<>());
        for (int i = 0; i < NUM_ITEMS_OLD; i++) {
            feed.getItems().add(new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(i), FeedItem.PLAYED, feed));
        }
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.setCompleteFeed(feed);
        adapter.close();

        // ensure that objects have been saved in db, then reset
        assertTrue(feed.getId() != 0);
        final long feedID = feed.getId();
        feed.setId(0);
        List<Long> itemIDs = new ArrayList<Long>();
        for (FeedItem item : feed.getItems()) {
            assertTrue(item.getId() != 0);
            itemIDs.add(item.getId());
            item.setId(0);
        }

        for (int i = NUM_ITEMS_OLD; i < NUM_ITEMS_NEW + NUM_ITEMS_OLD; i++) {
            feed.getItems().add(0, new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(i), FeedItem.UNPLAYED, feed));
        }

        final Feed newFeed = DBTasks.updateFeed(context, feed)[0];
        assertTrue(feed != newFeed);

        updatedFeedTest(newFeed, feedID, itemIDs, NUM_ITEMS_OLD, NUM_ITEMS_NEW);

        final Feed feedFromDB = DBReader.getFeed(newFeed.getId());
        assertNotNull(feedFromDB);
        assertTrue(feedFromDB.getId() == newFeed.getId());
        updatedFeedTest(feedFromDB, feedID, itemIDs, NUM_ITEMS_OLD, NUM_ITEMS_NEW);

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 60; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }

            final int NUM_ITEMS_OLD1 = 10;
            final int NUM_ITEMS_NEW1 = 10;

            final Feed feed1 = new Feed("url", null, "title");
            feed1.setItems(new ArrayList<>());
            for (int i = 0; i < NUM_ITEMS_OLD1; i++) {
                feed1.getItems().add(new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(i), FeedItem.PLAYED, feed1));
            }
            PodDBAdapter adapter1 = PodDBAdapter.getInstance();
            adapter1.open();
            adapter1.setCompleteFeed(feed1);
            adapter1.close();

            // ensure that objects have been saved in db, then reset
            assertTrue(feed1.getId() != 0);
            final long feedID1 = feed1.getId();
            feed1.setId(0);
            List<Long> itemIDs1 = new ArrayList<Long>();
            for (FeedItem item : feed1.getItems()) {
                assertTrue(item.getId() != 0);
                itemIDs1.add(item.getId());
                item.setId(0);
            }

            for (int i = NUM_ITEMS_OLD1; i < NUM_ITEMS_NEW1 + NUM_ITEMS_OLD1; i++) {
                feed1.getItems().add(0, new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(i), FeedItem.UNPLAYED, feed1));
            }

            final Feed newFeed1 = DBTasks.updateFeed(context, feed1)[0];
            assertTrue(feed1 != newFeed1);

            updatedFeedTest(newFeed1, feedID1, itemIDs1, NUM_ITEMS_OLD1, NUM_ITEMS_NEW1);

            final Feed feedFromDB1 = DBReader.getFeed(newFeed1.getId());
            assertNotNull(feedFromDB1);
            assertTrue(feedFromDB1.getId() == newFeed1.getId());
            updatedFeedTest(feedFromDB1, feedID1, itemIDs1, NUM_ITEMS_OLD1, NUM_ITEMS_NEW1);

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int NUM_ITEMS_OLD1 = 10;
        final int NUM_ITEMS_NEW1 = 10;

        final Feed feed1 = new Feed("url", null, "title");
        feed1.setItems(new ArrayList<>());
        for (int i = 0; i < NUM_ITEMS_OLD1; i++) {
            feed1.getItems().add(new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(i), FeedItem.PLAYED, feed1));
        }
        PodDBAdapter adapter1 = PodDBAdapter.getInstance();
        adapter1.open();
        adapter1.setCompleteFeed(feed1);
        adapter1.close();

        // ensure that objects have been saved in db, then reset
        assertTrue(feed1.getId() != 0);
        final long feedID1 = feed1.getId();
        feed1.setId(0);
        List<Long> itemIDs1 = new ArrayList<Long>();
        for (FeedItem item : feed1.getItems()) {
            assertTrue(item.getId() != 0);
            itemIDs1.add(item.getId());
            item.setId(0);
        }

        for (int i = NUM_ITEMS_OLD1; i < NUM_ITEMS_NEW1 + NUM_ITEMS_OLD1; i++) {
            feed1.getItems().add(0, new FeedItem(0, "item " + i, "id " + i, "link " + i, new Date(i), FeedItem.UNPLAYED, feed1));
        }

        final Feed newFeed1 = DBTasks.updateFeed(context, feed1)[0];
        assertTrue(feed1 != newFeed1);

        updatedFeedTest(newFeed1, feedID1, itemIDs1, NUM_ITEMS_OLD1, NUM_ITEMS_NEW1);

        final Feed feedFromDB1 = DBReader.getFeed(newFeed1.getId());
        assertNotNull(feedFromDB1);
        assertTrue(feedFromDB1.getId() == newFeed1.getId());
        updatedFeedTest(feedFromDB1, feedID1, itemIDs1, NUM_ITEMS_OLD1, NUM_ITEMS_NEW1);

    }

    public void testUpdateFeedMediaUrlResetState() {
        final Feed feed = new Feed("url", null, "title");
        FeedItem item = new FeedItem(0, "item", "id", "link", new Date(), FeedItem.PLAYED, feed);
        feed.setItems(Arrays.asList(item));

        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.setCompleteFeed(feed);
        adapter.close();

        // ensure that objects have been saved in db, then reset
        assertTrue(feed.getId() != 0);
        assertTrue(item.getId() != 0);

        FeedMedia media = new FeedMedia(item, "url", 1024, "mime/type");
        item.setMedia(media);
        feed.setItems(Arrays.asList(item));

        final Feed newFeed = DBTasks.updateFeed(context, feed)[0];
        assertTrue(feed != newFeed);

        final Feed feedFromDB = DBReader.getFeed(newFeed.getId());
        final FeedItem feedItemFromDB = feedFromDB.getItems().get(0);
        assertTrue("state: " + feedItemFromDB.getState(), feedItemFromDB.isNew());

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 60; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }

            final Feed feed1 = new Feed("url", null, "title");
            FeedItem item1 = new FeedItem(0, "item", "id", "link", new Date(), FeedItem.PLAYED, feed1);
            feed1.setItems(Arrays.asList(item1));

            PodDBAdapter adapter1 = PodDBAdapter.getInstance();
            adapter1.open();
            adapter1.setCompleteFeed(feed1);
            adapter1.close();

            // ensure that objects have been saved in db, then reset
            assertTrue(feed1.getId() != 0);
            assertTrue(item1.getId() != 0);

            FeedMedia media1 = new FeedMedia(item1, "url", 1024, "mime/type");
            item1.setMedia(media1);
            feed1.setItems(Arrays.asList(item1));

            final Feed newFeed1 = DBTasks.updateFeed(context, feed1)[0];
            assertTrue(feed1 != newFeed1);

            final Feed feedFromDB1 = DBReader.getFeed(newFeed1.getId());
            final FeedItem feedItemFromDB1 = feedFromDB1.getItems().get(0);
            assertTrue("state: " + feedItemFromDB1.getState(), feedItemFromDB1.isNew());

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final Feed feed1 = new Feed("url", null, "title");
        FeedItem item1 = new FeedItem(0, "item", "id", "link", new Date(), FeedItem.PLAYED, feed1);
        feed1.setItems(Arrays.asList(item1));

        PodDBAdapter adapter1 = PodDBAdapter.getInstance();
        adapter1.open();
        adapter1.setCompleteFeed(feed1);
        adapter1.close();

        // ensure that objects have been saved in db, then reset
        assertTrue(feed1.getId() != 0);
        assertTrue(item1.getId() != 0);

        FeedMedia media1 = new FeedMedia(item1, "url", 1024, "mime/type");
        item1.setMedia(media1);
        feed1.setItems(Arrays.asList(item1));

        final Feed newFeed1 = DBTasks.updateFeed(context, feed1)[0];
        assertTrue(feed1 != newFeed1);

        final Feed feedFromDB1 = DBReader.getFeed(newFeed1.getId());
        final FeedItem feedItemFromDB1 = feedFromDB1.getItems().get(0);
        assertTrue("state: " + feedItemFromDB1.getState(), feedItemFromDB1.isNew());
    }

    private void updatedFeedTest(final Feed newFeed, long feedID, List<Long> itemIDs, final int NUM_ITEMS_OLD, final int NUM_ITEMS_NEW) {
        assertTrue(newFeed.getId() == feedID);
        assertTrue(newFeed.getItems().size() == NUM_ITEMS_NEW + NUM_ITEMS_OLD);
        Collections.reverse(newFeed.getItems());
        Date lastDate = new Date(0);
        for (int i = 0; i < NUM_ITEMS_OLD; i++) {
            FeedItem item = newFeed.getItems().get(i);
            assertTrue(item.getFeed() == newFeed);
            assertTrue(item.getId() == itemIDs.get(i));
            assertTrue(item.isPlayed());
            assertTrue(item.getPubDate().getTime() >= lastDate.getTime());
            lastDate = item.getPubDate();
        }
        for (int i = NUM_ITEMS_OLD; i < NUM_ITEMS_NEW + NUM_ITEMS_OLD; i++) {
            FeedItem item = newFeed.getItems().get(i);
            assertTrue(item.getFeed() == newFeed);
            assertTrue(item.getId() != 0);
            assertFalse(item.isPlayed());
            assertTrue(item.getPubDate().getTime() >= lastDate.getTime());
            lastDate = item.getPubDate();
        }
    }
}
