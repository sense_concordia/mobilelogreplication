package de.test.antennapod.storage;

import android.content.Context;
import android.test.InstrumentationTestCase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import de.danoeh.antennapod.core.feed.Feed;
import de.danoeh.antennapod.core.feed.FeedItem;
import de.danoeh.antennapod.core.feed.FeedMedia;
import de.danoeh.antennapod.core.storage.DBReader;
import de.danoeh.antennapod.core.storage.FeedItemStatistics;
import de.danoeh.antennapod.core.storage.PodDBAdapter;
import de.danoeh.antennapod.core.util.LongList;

import static de.test.antennapod.storage.DBTestUtils.saveFeedlist;

/**
 * Test class for DBReader
 */
public class DBReaderTest extends InstrumentationTestCase {

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        assertTrue(PodDBAdapter.deleteDatabase());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // create new database
        PodDBAdapter.init(getInstrumentation().getTargetContext());
        PodDBAdapter.deleteDatabase();
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.close();
    }

    public void testGetFeedList() {
        List<Feed> feeds = saveFeedlist(10, 0, false);
        List<Feed> savedFeeds = DBReader.getFeedList();
        assertNotNull(savedFeeds);
        assertEquals(feeds.size(), savedFeeds.size());
        for (int i = 0; i < feeds.size(); i++) {
            assertTrue(savedFeeds.get(i).getId() == feeds.get(i).getId());
        }

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {

            try {
                setUp();
            } catch (Exception e) {

            }
            List<Feed> feeds1 = saveFeedlist(10, 0, false);
            List<Feed> savedFeeds1 = DBReader.getFeedList();
            assertNotNull(savedFeeds1);
            assertEquals(feeds1.size(), savedFeeds1.size());
            for (int i = 0; i < feeds1.size(); i++) {
                assertTrue(savedFeeds1.get(i).getId() == feeds1.get(i).getId());
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        List<Feed> feeds1 = saveFeedlist(10, 0, false);
        List<Feed> savedFeeds1 = DBReader.getFeedList();
        assertNotNull(savedFeeds1);
        assertEquals(feeds1.size(), savedFeeds1.size());
        for (int i = 0; i < feeds1.size(); i++) {
            assertTrue(savedFeeds1.get(i).getId() == feeds1.get(i).getId());
        }
    }

    public void testGetFeedListSortOrder() {
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();

        Feed feed1 = new Feed(0, null, "A", "link", "d", null, null, null, "rss", "A", null, "", "", true);
        Feed feed2 = new Feed(0, null, "b", "link", "d", null, null, null, "rss", "b", null, "", "", true);
        Feed feed3 = new Feed(0, null, "C", "link", "d", null, null, null, "rss", "C", null, "", "", true);
        Feed feed4 = new Feed(0, null, "d", "link", "d", null, null, null, "rss", "d", null, "", "", true);
        adapter.setCompleteFeed(feed1);
        adapter.setCompleteFeed(feed2);
        adapter.setCompleteFeed(feed3);
        adapter.setCompleteFeed(feed4);
        assertTrue(feed1.getId() != 0);
        assertTrue(feed2.getId() != 0);
        assertTrue(feed3.getId() != 0);
        assertTrue(feed4.getId() != 0);

        adapter.close();

        List<Feed> saved = DBReader.getFeedList();
        assertNotNull(saved);
        assertEquals("Wrong size: ", 4, saved.size());

        assertEquals("Wrong id of feed 1: ", feed1.getId(), saved.get(0).getId());
        assertEquals("Wrong id of feed 2: ", feed2.getId(), saved.get(1).getId());
        assertEquals("Wrong id of feed 3: ", feed3.getId(), saved.get(2).getId());
        assertEquals("Wrong id of feed 4: ", feed4.getId(), saved.get(3).getId());

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            PodDBAdapter adapter1 = PodDBAdapter.getInstance();
            adapter1.open();

            Feed feed11 = new Feed(0, null, "A", "link", "d", null, null, null, "rss", "A", null, "", "", true);
            Feed feed21 = new Feed(0, null, "b", "link", "d", null, null, null, "rss", "b", null, "", "", true);
            Feed feed31 = new Feed(0, null, "C", "link", "d", null, null, null, "rss", "C", null, "", "", true);
            Feed feed41 = new Feed(0, null, "d", "link", "d", null, null, null, "rss", "d", null, "", "", true);
            adapter1.setCompleteFeed(feed11);
            adapter1.setCompleteFeed(feed21);
            adapter1.setCompleteFeed(feed31);
            adapter1.setCompleteFeed(feed41);
            assertTrue(feed11.getId() != 0);
            assertTrue(feed21.getId() != 0);
            assertTrue(feed31.getId() != 0);
            assertTrue(feed41.getId() != 0);

            adapter1.close();

            List<Feed> saved1 = DBReader.getFeedList();
            assertNotNull(saved1);
            assertEquals("Wrong size: ", 4, saved1.size());

            assertEquals("Wrong id of feed 1: ", feed11.getId(), saved1.get(0).getId());
            assertEquals("Wrong id of feed 2: ", feed21.getId(), saved1.get(1).getId());
            assertEquals("Wrong id of feed 3: ", feed31.getId(), saved1.get(2).getId());
            assertEquals("Wrong id of feed 4: ", feed41.getId(), saved1.get(3).getId());

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        PodDBAdapter adapter1 = PodDBAdapter.getInstance();
        adapter1.open();

        Feed feed11 = new Feed(0, null, "A", "link", "d", null, null, null, "rss", "A", null, "", "", true);
        Feed feed21 = new Feed(0, null, "b", "link", "d", null, null, null, "rss", "b", null, "", "", true);
        Feed feed31 = new Feed(0, null, "C", "link", "d", null, null, null, "rss", "C", null, "", "", true);
        Feed feed41 = new Feed(0, null, "d", "link", "d", null, null, null, "rss", "d", null, "", "", true);
        adapter1.setCompleteFeed(feed11);
        adapter1.setCompleteFeed(feed21);
        adapter1.setCompleteFeed(feed31);
        adapter1.setCompleteFeed(feed41);
        assertTrue(feed11.getId() != 0);
        assertTrue(feed21.getId() != 0);
        assertTrue(feed31.getId() != 0);
        assertTrue(feed41.getId() != 0);

        adapter1.close();

        List<Feed> saved1 = DBReader.getFeedList();
        assertNotNull(saved1);
        assertEquals("Wrong size: ", 4, saved1.size());

        assertEquals("Wrong id of feed 1: ", feed11.getId(), saved1.get(0).getId());
        assertEquals("Wrong id of feed 2: ", feed21.getId(), saved1.get(1).getId());
        assertEquals("Wrong id of feed 3: ", feed31.getId(), saved1.get(2).getId());
        assertEquals("Wrong id of feed 4: ", feed41.getId(), saved1.get(3).getId());


    }

    public void testFeedListDownloadUrls() {
        List<Feed> feeds = saveFeedlist(10, 0, false);
        List<String> urls = DBReader.getFeedListDownloadUrls();
        assertNotNull(urls);
        assertTrue(urls.size() == feeds.size());
        for (int i = 0; i < urls.size(); i++) {
            assertEquals(urls.get(i), feeds.get(i).getDownload_url());
        }

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            List<Feed> feeds1 = saveFeedlist(10, 0, false);
            List<String> urls1 = DBReader.getFeedListDownloadUrls();
            assertNotNull(urls1);
            assertTrue(urls1.size() == feeds1.size());
            for (int i = 0; i < urls1.size(); i++) {
                assertEquals(urls1.get(i), feeds1.get(i).getDownload_url());
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        List<Feed> feeds1 = saveFeedlist(10, 0, false);
        List<String> urls1 = DBReader.getFeedListDownloadUrls();
        assertNotNull(urls1);
        assertTrue(urls1.size() == feeds1.size());
        for (int i = 0; i < urls1.size(); i++) {
            assertEquals(urls1.get(i), feeds1.get(i).getDownload_url());
        }
    }

    public void testLoadFeedDataOfFeedItemlist() {
        final Context context = getInstrumentation().getTargetContext();
        final int numFeeds = 10;
        final int numItems = 1;
        List<Feed> feeds = saveFeedlist(numFeeds, numItems, false);
        List<FeedItem> items = new ArrayList<>();
        for (Feed f : feeds) {
            for (FeedItem item : f.getItems()) {
                item.setFeed(null);
                item.setFeedId(f.getId());
                items.add(item);
            }
        }
        DBReader.loadAdditionalFeedItemListData(items);
        for (int i = 0; i < numFeeds; i++) {
            for (int j = 0; j < numItems; j++) {
                FeedItem item = feeds.get(i).getItems().get(j);
                assertNotNull(item.getFeed());
                assertTrue(item.getFeed().getId() == feeds.get(i).getId());
                assertTrue(item.getFeedId() == item.getFeed().getId());
            }
        }

        try {
            tearDown();
        } catch (Exception e) {

        }


        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final Context context1 = getInstrumentation().getTargetContext();
            final int numFeeds1 = 10;
            final int numItems1 = 1;
            List<Feed> feeds1 = saveFeedlist(numFeeds1, numItems1, false);
            List<FeedItem> items1 = new ArrayList<>();
            for (Feed f : feeds1) {
                for (FeedItem item : f.getItems()) {
                    item.setFeed(null);
                    item.setFeedId(f.getId());
                    items1.add(item);
                }
            }
            DBReader.loadAdditionalFeedItemListData(items1);
            for (int i = 0; i < numFeeds1; i++) {
                for (int j = 0; j < numItems1; j++) {
                    FeedItem item = feeds1.get(i).getItems().get(j);
                    assertNotNull(item.getFeed());
                    assertTrue(item.getFeed().getId() == feeds1.get(i).getId());
                    assertTrue(item.getFeedId() == item.getFeed().getId());
                }
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final Context context1 = getInstrumentation().getTargetContext();
        final int numFeeds1 = 10;
        final int numItems1 = 1;
        List<Feed> feeds1 = saveFeedlist(numFeeds1, numItems1, false);
        List<FeedItem> items1 = new ArrayList<>();
        for (Feed f : feeds1) {
            for (FeedItem item : f.getItems()) {
                item.setFeed(null);
                item.setFeedId(f.getId());
                items1.add(item);
            }
        }
        DBReader.loadAdditionalFeedItemListData(items1);
        for (int i = 0; i < numFeeds1; i++) {
            for (int j = 0; j < numItems1; j++) {
                FeedItem item = feeds1.get(i).getItems().get(j);
                assertNotNull(item.getFeed());
                assertTrue(item.getFeed().getId() == feeds1.get(i).getId());
                assertTrue(item.getFeedId() == item.getFeed().getId());
            }
        }
    }

    public void testGetFeedItemList() {
        final int numFeeds = 1;
        final int numItems = 10;
        Feed feed = saveFeedlist(numFeeds, numItems, false).get(0);
        List<FeedItem> items = feed.getItems();
        feed.setItems(null);
        List<FeedItem> savedItems = DBReader.getFeedItemList(feed);
        assertNotNull(savedItems);
        assertTrue(savedItems.size() == items.size());
        for (int i = 0; i < savedItems.size(); i++) {
            assertTrue(items.get(i).getId() == savedItems.get(i).getId());
        }

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int numFeeds1 = 1;
            final int numItems1 = 10;
            Feed feed1 = saveFeedlist(numFeeds1, numItems1, false).get(0);
            List<FeedItem> items1 = feed1.getItems();
            feed1.setItems(null);
            List<FeedItem> savedItems1 = DBReader.getFeedItemList(feed1);
            assertNotNull(savedItems1);
            assertTrue(savedItems1.size() == items1.size());
            for (int i = 0; i < savedItems1.size(); i++) {
                assertTrue(items1.get(i).getId() == savedItems1.get(i).getId());
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int numFeeds1 = 1;
        final int numItems1 = 10;
        Feed feed1 = saveFeedlist(numFeeds1, numItems1, false).get(0);
        List<FeedItem> items1 = feed1.getItems();
        feed1.setItems(null);
        List<FeedItem> savedItems1 = DBReader.getFeedItemList(feed1);
        assertNotNull(savedItems1);
        assertTrue(savedItems1.size() == items1.size());
        for (int i = 0; i < savedItems1.size(); i++) {
            assertTrue(items1.get(i).getId() == savedItems1.get(i).getId());
        }
    }

    private List<FeedItem> saveQueue(int numItems) {
        if (numItems <= 0) {
            throw new IllegalArgumentException("numItems<=0");
        }
        List<Feed> feeds = saveFeedlist(numItems, numItems, false);
        List<FeedItem> allItems = new ArrayList<>();
        for (Feed f : feeds) {
            allItems.addAll(f.getItems());
        }
        // take random items from every feed
        Random random = new Random();
        List<FeedItem> queue = new ArrayList<>();
        while (queue.size() < numItems) {
            int index = random.nextInt(numItems);
            if (!queue.contains(allItems.get(index))) {
                queue.add(allItems.get(index));
            }
        }
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.setQueue(queue);
        adapter.close();
        return queue;
    }

    public void testGetQueueIDList() {
        final int numItems = 10;
        List<FeedItem> queue = saveQueue(numItems);
        LongList ids = DBReader.getQueueIDList();
        assertNotNull(ids);
        assertTrue(queue.size() == ids.size());
        for (int i = 0; i < queue.size(); i++) {
            assertTrue(ids.get(i) != 0);
            assertTrue(queue.get(i).getId() == ids.get(i));
        }

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int numItems1 = 10;
            List<FeedItem> queue1 = saveQueue(numItems1);
            LongList ids1 = DBReader.getQueueIDList();
            assertNotNull(ids1);
            assertTrue(queue1.size() == ids1.size());
            for (int i = 0; i < queue1.size(); i++) {
                assertTrue(ids1.get(i) != 0);
                assertTrue(queue1.get(i).getId() == ids1.get(i));
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int numItems1 = 10;
        List<FeedItem> queue1 = saveQueue(numItems1);
        LongList ids1 = DBReader.getQueueIDList();
        assertNotNull(ids1);
        assertTrue(queue1.size() == ids1.size());
        for (int i = 0; i < queue1.size(); i++) {
            assertTrue(ids1.get(i) != 0);
            assertTrue(queue1.get(i).getId() == ids1.get(i));
        }


    }

    public void testGetQueue() {
        final int numItems = 10;
        List<FeedItem> queue = saveQueue(numItems);
        List<FeedItem> savedQueue = DBReader.getQueue();
        assertNotNull(savedQueue);
        assertTrue(queue.size() == savedQueue.size());
        for (int i = 0; i < queue.size(); i++) {
            assertTrue(savedQueue.get(i).getId() != 0);
            assertTrue(queue.get(i).getId() == savedQueue.get(i).getId());
        }

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int numItems1 = 10;
            List<FeedItem> queue1 = saveQueue(numItems1);
            List<FeedItem> savedQueue1 = DBReader.getQueue();
            assertNotNull(savedQueue1);
            assertTrue(queue1.size() == savedQueue1.size());
            for (int i = 0; i < queue1.size(); i++) {
                assertTrue(savedQueue1.get(i).getId() != 0);
                assertTrue(queue1.get(i).getId() == savedQueue1.get(i).getId());
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        final int numItems1 = 10;
        List<FeedItem> queue1 = saveQueue(numItems1);
        List<FeedItem> savedQueue1 = DBReader.getQueue();
        assertNotNull(savedQueue1);
        assertTrue(queue1.size() == savedQueue1.size());
        for (int i = 0; i < queue1.size(); i++) {
            assertTrue(savedQueue1.get(i).getId() != 0);
            assertTrue(queue1.get(i).getId() == savedQueue1.get(i).getId());
        }
    }

    private List<FeedItem> saveDownloadedItems(int numItems) {
        if (numItems <= 0) {
            throw new IllegalArgumentException("numItems<=0");
        }
        List<Feed> feeds = saveFeedlist(numItems, numItems, true);
        List<FeedItem> items = new ArrayList<>();
        for (Feed f : feeds) {
            items.addAll(f.getItems());
        }
        List<FeedItem> downloaded = new ArrayList<>();
        Random random = new Random();

        while (downloaded.size() < numItems) {
            int i = random.nextInt(numItems);
            if (!downloaded.contains(items.get(i))) {
                FeedItem item = items.get(i);
                item.getMedia().setDownloaded(true);
                item.getMedia().setFile_url("file" + i);
                downloaded.add(item);
            }
        }
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.setFeedItemlist(downloaded);
        adapter.close();
        return downloaded;
    }

    public void testGetDownloadedItems() {
        final int numItems = 10;
        List<FeedItem> downloaded = saveDownloadedItems(numItems);
        List<FeedItem> downloaded_saved = DBReader.getDownloadedItems();
        assertNotNull(downloaded_saved);
        assertTrue(downloaded_saved.size() == downloaded.size());
        for (FeedItem item : downloaded_saved) {
            assertNotNull(item.getMedia());
            assertTrue(item.getMedia().isDownloaded());
            assertNotNull(item.getMedia().getDownload_url());
        }

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int numItems1 = 10;
            List<FeedItem> downloaded1 = saveDownloadedItems(numItems1);
            List<FeedItem> downloaded_saved1 = DBReader.getDownloadedItems();
            assertNotNull(downloaded_saved1);
            assertTrue(downloaded_saved1.size() == downloaded1.size());
            for (FeedItem item : downloaded_saved1) {
                assertNotNull(item.getMedia());
                assertTrue(item.getMedia().isDownloaded());
                assertNotNull(item.getMedia().getDownload_url());
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        final int numItems1 = 10;
        List<FeedItem> downloaded1 = saveDownloadedItems(numItems1);
        List<FeedItem> downloaded_saved1 = DBReader.getDownloadedItems();
        assertNotNull(downloaded_saved1);
        assertTrue(downloaded_saved1.size() == downloaded1.size());
        for (FeedItem item : downloaded_saved1) {
            assertNotNull(item.getMedia());
            assertTrue(item.getMedia().isDownloaded());
            assertNotNull(item.getMedia().getDownload_url());
        }
    }

    private List<FeedItem> saveNewItems(int numItems) {
        List<Feed> feeds = saveFeedlist(numItems, numItems, true);
        List<FeedItem> items = new ArrayList<>();
        for (Feed f : feeds) {
            items.addAll(f.getItems());
        }
        List<FeedItem> newItems = new ArrayList<>();
        Random random = new Random();

        while (newItems.size() < numItems) {
            int i = random.nextInt(numItems);
            if (!newItems.contains(items.get(i))) {
                FeedItem item = items.get(i);
                item.setNew();
                newItems.add(item);
            }
        }
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        adapter.setFeedItemlist(newItems);
        adapter.close();
        return newItems;
    }

    public void testGetNewItemIds() {
        final int numItems = 10;

        List<FeedItem> newItems = saveNewItems(numItems);
        long[] unreadIds = new long[newItems.size()];
        for (int i = 0; i < newItems.size(); i++) {
            unreadIds[i] = newItems.get(i).getId();
        }
        List<FeedItem> newItemsSaved = DBReader.getNewItemsList();
        assertNotNull(newItemsSaved);
        assertTrue(newItems.size() == newItemsSaved.size());
        for (int i = 0; i < newItemsSaved.size(); i++) {
            long savedId = newItemsSaved.get(i).getId();
            boolean found = false;
            for (long id : unreadIds) {
                if (id == savedId) {
                    found = true;
                    break;
                }
            }
            assertTrue(found);
        }

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int numItems1 = 10;

            List<FeedItem> newItems1 = saveNewItems(numItems1);
            long[] unreadIds1 = new long[newItems1.size()];
            for (int i = 0; i < newItems1.size(); i++) {
                unreadIds1[i] = newItems1.get(i).getId();
            }
            List<FeedItem> newItemsSaved1 = DBReader.getNewItemsList();
            assertNotNull(newItemsSaved1);
            assertTrue(newItems1.size() == newItemsSaved1.size());
            for (int i = 0; i < newItemsSaved1.size(); i++) {
                long savedId = newItemsSaved1.get(i).getId();
                boolean found = false;
                for (long id : unreadIds1) {
                    if (id == savedId) {
                        found = true;
                        break;
                    }
                }
                assertTrue(found);
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int numItems1 = 10;

        List<FeedItem> newItems1 = saveNewItems(numItems1);
        long[] unreadIds1 = new long[newItems1.size()];
        for (int i = 0; i < newItems1.size(); i++) {
            unreadIds1[i] = newItems1.get(i).getId();
        }
        List<FeedItem> newItemsSaved1 = DBReader.getNewItemsList();
        assertNotNull(newItemsSaved1);
        assertTrue(newItems1.size() == newItemsSaved1.size());
        for (int i = 0; i < newItemsSaved1.size(); i++) {
            long savedId = newItemsSaved1.get(i).getId();
            boolean found = false;
            for (long id : unreadIds1) {
                if (id == savedId) {
                    found = true;
                    break;
                }
            }
            assertTrue(found);
        }

    }

    public void testGetPlaybackHistory() {
        final int numItems = (DBReader.PLAYBACK_HISTORY_SIZE + 1) * 2;
        final int playedItems = DBReader.PLAYBACK_HISTORY_SIZE + 1;
        final int numReturnedItems = Math.min(playedItems, DBReader.PLAYBACK_HISTORY_SIZE);
        final int numFeeds = 1;

        Feed feed = DBTestUtils.saveFeedlist(numFeeds, numItems, true).get(0);
        long[] ids = new long[playedItems];

        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        for (int i = 0; i < playedItems; i++) {
            FeedMedia m = feed.getItems().get(i).getMedia();
            m.setPlaybackCompletionDate(new Date(i + 1));
            adapter.setFeedMediaPlaybackCompletionDate(m);
            ids[ids.length - 1 - i] = m.getItem().getId();
        }
        adapter.close();

        List<FeedItem> saved = DBReader.getPlaybackHistory();
        assertNotNull(saved);
        assertEquals("Wrong size: ", numReturnedItems, saved.size());
        for (int i = 0; i < numReturnedItems; i++) {
            FeedItem item = saved.get(i);
            assertNotNull(item.getMedia().getPlaybackCompletionDate());
            assertEquals("Wrong sort order: ", item.getId(), ids[i]);
        }

        try {
            tearDown();
        } catch (Exception e) {

        }


        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int numItems1 = (DBReader.PLAYBACK_HISTORY_SIZE + 1) * 2;
            final int playedItems1 = DBReader.PLAYBACK_HISTORY_SIZE + 1;
            final int numReturnedItems1 = Math.min(playedItems1, DBReader.PLAYBACK_HISTORY_SIZE);
            final int numFeeds1 = 1;

            Feed feed1 = DBTestUtils.saveFeedlist(numFeeds1, numItems1, true).get(0);
            long[] ids1 = new long[playedItems1];

            PodDBAdapter adapter1 = PodDBAdapter.getInstance();
            adapter1.open();
            for (int i = 0; i < playedItems1; i++) {
                FeedMedia m = feed1.getItems().get(i).getMedia();
                m.setPlaybackCompletionDate(new Date(i + 1));
                adapter1.setFeedMediaPlaybackCompletionDate(m);
                ids1[ids1.length - 1 - i] = m.getItem().getId();
            }
            adapter1.close();

            List<FeedItem> saved1 = DBReader.getPlaybackHistory();
            assertNotNull(saved1);
            assertEquals("Wrong size: ", numReturnedItems1, saved1.size());
            for (int i = 0; i < numReturnedItems1; i++) {
                FeedItem item = saved1.get(i);
                assertNotNull(item.getMedia().getPlaybackCompletionDate());
                assertEquals("Wrong sort order: ", item.getId(), ids1[i]);
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int numItems1 = (DBReader.PLAYBACK_HISTORY_SIZE + 1) * 2;
        final int playedItems1 = DBReader.PLAYBACK_HISTORY_SIZE + 1;
        final int numReturnedItems1 = Math.min(playedItems1, DBReader.PLAYBACK_HISTORY_SIZE);
        final int numFeeds1 = 1;

        Feed feed1 = DBTestUtils.saveFeedlist(numFeeds1, numItems1, true).get(0);
        long[] ids1 = new long[playedItems1];

        PodDBAdapter adapter1 = PodDBAdapter.getInstance();
        adapter1.open();
        for (int i = 0; i < playedItems1; i++) {
            FeedMedia m = feed1.getItems().get(i).getMedia();
            m.setPlaybackCompletionDate(new Date(i + 1));
            adapter1.setFeedMediaPlaybackCompletionDate(m);
            ids1[ids1.length - 1 - i] = m.getItem().getId();
        }
        adapter1.close();

        List<FeedItem> saved1 = DBReader.getPlaybackHistory();
        assertNotNull(saved1);
        assertEquals("Wrong size: ", numReturnedItems1, saved1.size());
        for (int i = 0; i < numReturnedItems1; i++) {
            FeedItem item = saved1.get(i);
            assertNotNull(item.getMedia().getPlaybackCompletionDate());
            assertEquals("Wrong sort order: ", item.getId(), ids1[i]);
        }
    }

    public void testGetFeedStatisticsCheckOrder() {
        final int NUM_FEEDS = 10;
        final int NUM_ITEMS = 10;
        List<Feed> feeds = DBTestUtils.saveFeedlist(NUM_FEEDS, NUM_ITEMS, false);
        List<FeedItemStatistics> statistics = DBReader.getFeedStatisticsList();
        assertNotNull(statistics);
        assertEquals(feeds.size(), statistics.size());
        for (int i = 0; i < NUM_FEEDS; i++) {
            assertEquals("Wrong entry at index " + i, feeds.get(i).getId(), statistics.get(i).getFeedID());
        }

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int NUM_FEEDS1 = 10;
            final int NUM_ITEMS1 = 10;
            List<Feed> feeds1 = DBTestUtils.saveFeedlist(NUM_FEEDS1, NUM_ITEMS1, false);
            List<FeedItemStatistics> statistics1 = DBReader.getFeedStatisticsList();
            assertNotNull(statistics1);
            assertEquals(feeds1.size(), statistics1.size());
            for (int i = 0; i < NUM_FEEDS1; i++) {
                assertEquals("Wrong entry at index " + i, feeds1.get(i).getId(), statistics1.get(i).getFeedID());
            }
            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        final int NUM_FEEDS1 = 10;
        final int NUM_ITEMS1 = 10;
        List<Feed> feeds1 = DBTestUtils.saveFeedlist(NUM_FEEDS1, NUM_ITEMS1, false);
        List<FeedItemStatistics> statistics1 = DBReader.getFeedStatisticsList();
        assertNotNull(statistics1);
        assertEquals(feeds1.size(), statistics1.size());
        for (int i = 0; i < NUM_FEEDS1; i++) {
            assertEquals("Wrong entry at index " + i, feeds1.get(i).getId(), statistics1.get(i).getFeedID());
        }
    }

    public void testGetNavDrawerDataQueueEmptyNoUnreadItems() {
        final int NUM_FEEDS = 10;
        final int NUM_ITEMS = 10;
        List<Feed> feeds = DBTestUtils.saveFeedlist(NUM_FEEDS, NUM_ITEMS, true);
        DBReader.NavDrawerData navDrawerData = DBReader.getNavDrawerData();
        assertEquals(NUM_FEEDS, navDrawerData.feeds.size());
        assertEquals(0, navDrawerData.numNewItems);
        assertEquals(0, navDrawerData.queueSize);

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int NUM_FEEDS1 = 10;
            final int NUM_ITEMS1 = 10;
            List<Feed> feeds1 = DBTestUtils.saveFeedlist(NUM_FEEDS1, NUM_ITEMS1, true);
            DBReader.NavDrawerData navDrawerData1 = DBReader.getNavDrawerData();
            assertEquals(NUM_FEEDS1, navDrawerData1.feeds.size());
            assertEquals(0, navDrawerData1.numNewItems);
            assertEquals(0, navDrawerData1.queueSize);

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        final int NUM_FEEDS1 = 10;
        final int NUM_ITEMS1 = 10;
        List<Feed> feeds1 = DBTestUtils.saveFeedlist(NUM_FEEDS1, NUM_ITEMS1, true);
        DBReader.NavDrawerData navDrawerData1 = DBReader.getNavDrawerData();
        assertEquals(NUM_FEEDS1, navDrawerData1.feeds.size());
        assertEquals(0, navDrawerData1.numNewItems);
        assertEquals(0, navDrawerData1.queueSize);
    }

    public void testGetNavDrawerDataQueueNotEmptyWithUnreadItems() {
        final int NUM_FEEDS = 10;
        final int NUM_ITEMS = 10;
        final int NUM_QUEUE = 1;
        final int NUM_NEW = 2;
        List<Feed> feeds = DBTestUtils.saveFeedlist(NUM_FEEDS, NUM_ITEMS, true);
        PodDBAdapter adapter = PodDBAdapter.getInstance();
        adapter.open();
        for (int i = 0; i < NUM_NEW; i++) {
            FeedItem item = feeds.get(0).getItems().get(i);
            item.setNew();
            adapter.setSingleFeedItem(item);
        }
        List<FeedItem> queue = new ArrayList<>();
        for (int i = 0; i < NUM_QUEUE; i++) {
            FeedItem item = feeds.get(1).getItems().get(i);
            queue.add(item);
        }
        adapter.setQueue(queue);

        adapter.close();

        DBReader.NavDrawerData navDrawerData = DBReader.getNavDrawerData();
        assertEquals(NUM_FEEDS, navDrawerData.feeds.size());
        assertEquals(NUM_NEW, navDrawerData.numNewItems);
        assertEquals(NUM_QUEUE, navDrawerData.queueSize);

        try {
            tearDown();
        } catch (Exception e) {

        }


        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int NUM_FEEDS1 = 10;
            final int NUM_ITEMS1 = 10;
            final int NUM_QUEUE1 = 1;
            final int NUM_NEW1 = 2;
            List<Feed> feeds1 = DBTestUtils.saveFeedlist(NUM_FEEDS1, NUM_ITEMS1, true);
            PodDBAdapter adapter1 = PodDBAdapter.getInstance();
            adapter1.open();
            for (int i = 0; i < NUM_NEW1; i++) {
                FeedItem item = feeds1.get(0).getItems().get(i);
                item.setNew();
                adapter1.setSingleFeedItem(item);
            }
            List<FeedItem> queue1 = new ArrayList<>();
            for (int i = 0; i < NUM_QUEUE1; i++) {
                FeedItem item = feeds1.get(1).getItems().get(i);
                queue1.add(item);
            }
            adapter1.setQueue(queue1);

            adapter1.close();

            DBReader.NavDrawerData navDrawerData1 = DBReader.getNavDrawerData();
            assertEquals(NUM_FEEDS1, navDrawerData1.feeds.size());
            assertEquals(NUM_NEW1, navDrawerData1.numNewItems);
            assertEquals(NUM_QUEUE1, navDrawerData1.queueSize);

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int NUM_FEEDS1 = 10;
        final int NUM_ITEMS1 = 10;
        final int NUM_QUEUE1 = 1;
        final int NUM_NEW1 = 2;
        List<Feed> feeds1 = DBTestUtils.saveFeedlist(NUM_FEEDS1, NUM_ITEMS1, true);
        PodDBAdapter adapter1 = PodDBAdapter.getInstance();
        adapter1.open();
        for (int i = 0; i < NUM_NEW1; i++) {
            FeedItem item = feeds1.get(0).getItems().get(i);
            item.setNew();
            adapter1.setSingleFeedItem(item);
        }
        List<FeedItem> queue1 = new ArrayList<>();
        for (int i = 0; i < NUM_QUEUE1; i++) {
            FeedItem item = feeds1.get(1).getItems().get(i);
            queue1.add(item);
        }
        adapter1.setQueue(queue1);

        adapter1.close();

        DBReader.NavDrawerData navDrawerData1 = DBReader.getNavDrawerData();
        assertEquals(NUM_FEEDS1, navDrawerData1.feeds.size());
        assertEquals(NUM_NEW1, navDrawerData1.numNewItems);
        assertEquals(NUM_QUEUE1, navDrawerData1.queueSize);

    }

    public void testGetFeedItemlistCheckChaptersFalse() throws Exception {
        Context context = getInstrumentation().getTargetContext();
        List<Feed> feeds = DBTestUtils.saveFeedlist(10, 10, false, false, 0);
        for (Feed feed : feeds) {
            for (FeedItem item : feed.getItems()) {
                assertFalse(item.hasChapters());
            }
        }

        try {
            tearDown();
        } catch (Exception e) {

        }


        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            Context context1 = getInstrumentation().getTargetContext();
            List<Feed> feeds1 = DBTestUtils.saveFeedlist(10, 10, false, false, 0);
            for (Feed feed : feeds1) {
                for (FeedItem item : feed.getItems()) {
                    assertFalse(item.hasChapters());
                }
            }

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        Context context1 = getInstrumentation().getTargetContext();
        List<Feed> feeds1 = DBTestUtils.saveFeedlist(10, 10, false, false, 0);
        for (Feed feed : feeds1) {
            for (FeedItem item : feed.getItems()) {
                assertFalse(item.hasChapters());
            }
        }
    }

    public void testGetFeedItemlistCheckChaptersTrue() throws Exception {
        List<Feed> feeds = saveFeedlist(10, 10, false, true, 10);
        for (Feed feed : feeds) {
            for (FeedItem item : feed.getItems()) {
                assertTrue(item.hasChapters());
            }
        }

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }

            List<Feed> feeds1 = saveFeedlist(10, 10, false, true, 10);
            for (Feed feed : feeds1) {
                for (FeedItem item : feed.getItems()) {
                    assertTrue(item.hasChapters());
                }
            }

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        List<Feed> feeds1 = saveFeedlist(10, 10, false, true, 10);
        for (Feed feed : feeds1) {
            for (FeedItem item : feed.getItems()) {
                assertTrue(item.hasChapters());
            }
        }
    }

    public void testLoadChaptersOfFeedItemNoChapters() throws Exception {
        List<Feed> feeds = saveFeedlist(1, 3, false, false, 0);
        saveFeedlist(1, 3, false, true, 3);
        for (Feed feed : feeds) {
            for (FeedItem item : feed.getItems()) {
                assertFalse(item.hasChapters());
                DBReader.loadChaptersOfFeedItem(item);
                assertFalse(item.hasChapters());
                assertNull(item.getChapters());
            }
        }

        try {
            tearDown();
        } catch (Exception e) {

        }
        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }

            List<Feed> feeds1 = saveFeedlist(1, 3, false, false, 0);
            saveFeedlist(1, 3, false, true, 3);
            for (Feed feed : feeds1) {
                for (FeedItem item : feed.getItems()) {
                    assertFalse(item.hasChapters());
                    DBReader.loadChaptersOfFeedItem(item);
                    assertFalse(item.hasChapters());
                    assertNull(item.getChapters());
                }
            }

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        List<Feed> feeds1 = saveFeedlist(1, 3, false, false, 0);
        saveFeedlist(1, 3, false, true, 3);
        for (Feed feed : feeds1) {
            for (FeedItem item : feed.getItems()) {
                assertFalse(item.hasChapters());
                DBReader.loadChaptersOfFeedItem(item);
                assertFalse(item.hasChapters());
                assertNull(item.getChapters());
            }
        }
    }

    public void testLoadChaptersOfFeedItemWithChapters() throws Exception {
        final int NUM_CHAPTERS = 3;
        DBTestUtils.saveFeedlist(1, 3, false, false, 0);
        List<Feed> feeds = saveFeedlist(1, 3, false, true, NUM_CHAPTERS);
        for (Feed feed : feeds) {
            for (FeedItem item : feed.getItems()) {
                assertTrue(item.hasChapters());
                DBReader.loadChaptersOfFeedItem(item);
                assertTrue(item.hasChapters());
                assertNotNull(item.getChapters());
                assertEquals(NUM_CHAPTERS, item.getChapters().size());
            }
        }

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }
            final int NUM_CHAPTERS1 = 3;
            DBTestUtils.saveFeedlist(1, 3, false, false, 0);
            List<Feed> feeds1 = saveFeedlist(1, 3, false, true, NUM_CHAPTERS1);
            for (Feed feed : feeds1) {
                for (FeedItem item : feed.getItems()) {
                    assertTrue(item.hasChapters());
                    DBReader.loadChaptersOfFeedItem(item);
                    assertTrue(item.hasChapters());
                    assertNotNull(item.getChapters());
                    assertEquals(NUM_CHAPTERS1, item.getChapters().size());
                }
            }

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }

        final int NUM_CHAPTERS1 = 3;
        DBTestUtils.saveFeedlist(1, 3, false, false, 0);
        List<Feed> feeds1 = saveFeedlist(1, 3, false, true, NUM_CHAPTERS1);
        for (Feed feed : feeds1) {
            for (FeedItem item : feed.getItems()) {
                assertTrue(item.hasChapters());
                DBReader.loadChaptersOfFeedItem(item);
                assertTrue(item.hasChapters());
                assertNotNull(item.getChapters());
                assertEquals(NUM_CHAPTERS1, item.getChapters().size());
            }
        }
    }

    public void testGetItemWithChapters() throws Exception {
        final int NUM_CHAPTERS = 3;
        List<Feed> feeds = saveFeedlist(1, 1, false, true, NUM_CHAPTERS);
        FeedItem item1 = feeds.get(0).getItems().get(0);
        FeedItem item2 = DBReader.getFeedItem(item1.getId());
        assertTrue(item2.hasChapters());
        assertEquals(item1.getChapters(), item2.getChapters());

        try {
            tearDown();
        } catch (Exception e) {

        }

        for (int aa = 0; aa < 10; aa++) {
            try {
                setUp();
            } catch (Exception e) {

            }

            final int NUM_CHAPTERS1 = 3;
            List<Feed> feeds1 = saveFeedlist(1, 1, false, true, NUM_CHAPTERS1);
            FeedItem item11 = feeds1.get(0).getItems().get(0);
            FeedItem item21 = DBReader.getFeedItem(item11.getId());
            assertTrue(item21.hasChapters());
            assertEquals(item11.getChapters(), item21.getChapters());

            try {
                tearDown();
            } catch (Exception e) {

            }
        }

        try {
            setUp();
        } catch (Exception e) {

        }
        final int NUM_CHAPTERS1 = 3;
        List<Feed> feeds1 = saveFeedlist(1, 1, false, true, NUM_CHAPTERS1);
        FeedItem item11 = feeds1.get(0).getItems().get(0);
        FeedItem item21 = DBReader.getFeedItem(item11.getId());
        assertTrue(item21.hasChapters());
        assertEquals(item11.getChapters(), item21.getChapters());


    }
}
