Daily stats:
  Current start time: 2018-10-23-03-13-17
  Next min deadline: 2018-10-24-01-00-00
  Next max deadline: 2018-10-24-03-00-00
  Current daily steps:
    Discharge total time: 2d 21h 43m 22s 700ms  (from 6 steps)
    Discharge screen on time: 13h 19m 52s 400ms  (from 1 steps)
    Discharge screen doze time: 3d 9h 0m 4s 700ms  (from 5 steps)
    Charge total time: 12h 35m 33s 300ms  (from 9 steps)
    Charge screen on time: 21h 39m 59s 800ms  (from 1 steps)
    Charge screen doze time: 11h 11m 25s 800ms  (from 7 steps)
  Daily from 2018-10-22-07-32-32 to 2018-10-23-03-13-17:
    Discharge total time: 20h 2m 47s 0ms  (from 34 steps)
    Discharge screen on time: 12h 8m 6s 800ms  (from 9 steps)
    Discharge screen doze time: 23h 43m 24s 500ms  (from 21 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 4s 313ms (99.9%) realtime, 1m 4s 313ms (100.0%) uptime
  Time on battery screen off: 1m 4s 313ms (100.0%) realtime, 1m 4s 313ms (100.0%) uptime
  Time on battery screen doze: 1m 4s 313ms (100.0%)
  Total run time: 1m 4s 363ms realtime, 1m 4s 363ms uptime
  Discharge: 1.24 mAh
  Screen off discharge: 1.24 mAh
  Screen doze discharge: 1.24 mAh
  Start clock time: 2018-10-23-19-23-45
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 4s 313ms (100.0%) 0x
  Idle mode light time: 1m 4s 313ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 236ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 4s 313ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 4s 313ms (100.0%) 
     Cellular Sleep time:  1m 3s 310ms (98.4%)
     Cellular Idle time:   192ms (0.3%)
     Cellular Rx time:     812ms (1.3%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 4s 313ms (100.0%) 
     Wifi supplicant states:
       completed 1m 4s 313ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 4s 313ms (100.0%) 
     WiFi Sleep time:  1m 4s 315ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 4s 315ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 0.658, actual drain: 0
    Cell standby: 0.197 ( radio=0.197 ) Excluded from smearing
    Idle: 0.175 Excluded from smearing
    Uid 0: 0.112 ( cpu=0.0607 wake=0.0508 ) Excluded from smearing
    Uid u0a200: 0.0996 ( cpu=0.0996 ) Including smearing: 0.237 ( proportional=0.137 )
    Uid 1000: 0.0344 ( cpu=0.0344 wake=0.0000206 ) Excluded from smearing
    Uid 2000: 0.0223 ( cpu=0.0223 ) Excluded from smearing
    Uid u0a22: 0.00613 ( cpu=0.00607 wake=0.0000587 ) Excluded from smearing
    Uid u0a47: 0.00573 ( cpu=0.00563 wake=0.000102 ) Excluded from smearing
    Uid 1036: 0.00437 ( cpu=0.00437 ) Excluded from smearing
    Uid u0a32: 0.000477 ( cpu=0.000477 ) Including smearing: 0.00114 ( proportional=0.000658 )
    Uid 1001: 0.000453 ( cpu=0.000448 wake=0.00000555 ) Excluded from smearing
    Uid u0a114: 0.000287 ( cpu=0.000287 ) Including smearing: 0.000682 ( proportional=0.000395 )
    Uid u0a35: 0.000283 ( cpu=0.000283 ) Including smearing: 0.000673 ( proportional=0.000390 )
    Wifi: 0.000271 ( cpu=0.000271 ) Including smearing: 0.000644 ( proportional=0.000373 )
    Bluetooth: 0.000157 ( cpu=0.000157 ) Including smearing: 0.000373 ( proportional=0.000216 )
    Uid u0a25: 0.0000880 ( cpu=0.0000880 ) Including smearing: 0.000209 ( proportional=0.000121 )
    Uid u0a40: 0.0000880 ( cpu=0.0000880 ) Including smearing: 0.000209 ( proportional=0.000121 )
    Uid u0a8: 0.0000834 ( cpu=0.0000834 ) Including smearing: 0.000198 ( proportional=0.000115 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 4s 182ms realtime (3 times)
    XO_shutdown.MPSS: 1m 3s 297ms realtime (27 times)
    XO_shutdown.ADSP: 1m 4s 210ms realtime (0 times)
    wlan.Active: 34ms realtime (3 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 26ms partial (1 times) max=42 actual=42 realtime
    Fg Service for: 1m 4s 305ms 
    Total running: 1m 4s 305ms 
    Total cpu time: u=597ms s=570ms 
    Total cpu time per freq: 30 0 0 0 100 120 50 30 20 20 10 30 30 20 10 20 60 30 0 30 30 220 20 50 0 0 0 10 10 10 0 0 0 10 0 10 30 30 30 50 10 0 20 0 10 60 0 0 0 10 0 0 110
    Total screen-off cpu time per freq: 30 0 0 0 100 120 50 30 20 20 10 30 30 20 10 20 60 30 0 30 30 220 20 50 0 0 0 10 10 10 0 0 0 10 0 10 30 30 30 50 10 0 20 0 10 60 0 0 0 10 0 0 110
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 380ms usr + 410ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1m 2s 877ms (97.8%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Foreground services: 1m 3s 662ms realtime (1 times)
    Fg Service for: 1m 3s 662ms 
    Cached for: 90ms 
    Total running: 1m 3s 752ms 
    Total cpu time: u=2s 794ms s=460ms 
    Total cpu time per freq: 70 30 0 0 120 160 70 50 50 120 40 70 50 50 60 50 30 90 20 30 50 470 180 180 50 30 10 0 0 0 10 10 30 30 0 50 70 20 60 70 70 50 20 40 50 50 60 10 0 60 0 10 750
    Total screen-off cpu time per freq: 70 30 0 0 120 160 70 50 50 120 40 70 50 50 60 50 30 90 20 30 50 470 180 180 50 30 10 0 0 0 10 10 30 30 0 50 70 20 60 70 70 50 20 40 50 50 60 10 0 60 0 10 750
    Proc de.danoeh.antennapod.debug:
      CPU: 710ms usr + 170ms krn ; 0ms fg
      1 starts

