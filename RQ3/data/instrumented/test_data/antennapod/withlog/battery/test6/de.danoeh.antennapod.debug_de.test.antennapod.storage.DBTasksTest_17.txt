Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 1d 11h 51m 50s 900ms  (from 22 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 1d 0h 23m 26s 400ms  (from 16 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 7s 619ms (99.9%) realtime, 1m 7s 619ms (100.0%) uptime
  Time on battery screen off: 1m 7s 619ms (100.0%) realtime, 1m 7s 619ms (100.0%) uptime
  Time on battery screen doze: 1m 7s 619ms (100.0%)
  Total run time: 1m 7s 655ms realtime, 1m 7s 655ms uptime
  Discharge: 4.28 mAh
  Screen off discharge: 4.28 mAh
  Screen doze discharge: 4.28 mAh
  Start clock time: 2018-10-16-02-39-55
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 7s 619ms (100.0%) 0x
  Idle mode full time: 1m 7s 619ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 298ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 7s 619ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 7s 619ms (100.0%) 
     Cellular Sleep time:  1m 7s 173ms (99.3%)
     Cellular Idle time:   205ms (0.3%)
     Cellular Rx time:     241ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 7s 619ms (100.0%) 
     Wifi supplicant states:
       completed 1m 7s 619ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 7s 619ms (100.0%) 
     WiFi Sleep time:  1m 7s 620ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 7s 620ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.42, actual drain: 0
    Uid u0a200: 1.37 ( cpu=1.37 ) Including smearing: 2.14 ( proportional=0.762 )
    Uid 0: 0.431 ( cpu=0.378 wake=0.0534 ) Excluded from smearing
    Cell standby: 0.207 ( radio=0.207 ) Excluded from smearing
    Idle: 0.184 Excluded from smearing
    Uid 2000: 0.129 ( cpu=0.129 ) Excluded from smearing
    Uid 1036: 0.0590 ( cpu=0.0590 ) Excluded from smearing
    Uid 1000: 0.0349 ( cpu=0.0349 ) Excluded from smearing
    Uid u0a47: 0.00446 ( cpu=0.00424 wake=0.000223 ) Excluded from smearing
    Uid u0a35: 0.000892 ( cpu=0.000892 ) Including smearing: 0.00139 ( proportional=0.000495 )
    Uid 1001: 0.000439 ( cpu=0.000432 wake=0.00000714 ) Excluded from smearing
    Wifi: 0.000391 ( cpu=0.000391 ) Including smearing: 0.000607 ( proportional=0.000217 )
    Bluetooth: 0.000222 ( cpu=0.000216 wake=0.00000634 ) Including smearing: 0.000345 ( proportional=0.000123 )
    Uid u0a25: 0.0000916 ( cpu=0.0000916 ) Including smearing: 0.000142 ( proportional=0.0000508 )
    Uid u0a40: 0.0000916 ( cpu=0.0000916 ) Including smearing: 0.000142 ( proportional=0.0000508 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 7s 474ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 124ms realtime (29 times)
    XO_shutdown.ADSP: 1m 7s 503ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Fg Service for: 1m 7s 611ms 
    Total running: 1m 7s 611ms 
    Total cpu time: u=580ms s=550ms 
    Total cpu time per freq: 0 0 0 0 10 80 100 50 20 40 20 50 40 30 60 50 30 40 90 90 40 100 80 10 0 10 10 0 0 0 20 10 10 0 10 20 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 10
    Total screen-off cpu time per freq: 0 0 0 0 10 80 100 50 20 40 20 50 40 30 60 50 30 40 90 90 40 100 80 10 0 10 10 0 0 0 20 10 10 0 10 20 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 10
    Proc servicemanager:
      CPU: 30ms usr + 80ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 400ms usr + 330ms krn ; 0ms fg
  u0a200:
    Foreground services: 1m 7s 33ms realtime (1 times)
    Fg Service for: 1m 7s 33ms 
    Cached for: 35ms 
    Total running: 1m 7s 68ms 
    Total cpu time: u=31s 657ms s=12s 580ms 
    Total cpu time per freq: 200 70 50 20 250 870 1260 860 550 930 750 500 930 940 1130 1730 1050 1550 1770 1250 1020 6730 6530 1750 1670 1360 1690 1310 930 390 470 360 840 720 350 660 480 410 330 160 20 20 0 10 20 10 10 20 0 0 20 0 110
    Total screen-off cpu time per freq: 200 70 50 20 250 870 1260 860 550 930 750 500 930 940 1130 1730 1050 1550 1770 1250 1020 6730 6530 1750 1670 1360 1690 1310 930 390 470 360 840 720 350 660 480 410 330 160 20 20 0 10 20 10 10 20 0 0 20 0 110
    Proc de.danoeh.antennapod.debug:
      CPU: 19s 370ms usr + 8s 30ms krn ; 0ms fg
      1 starts

