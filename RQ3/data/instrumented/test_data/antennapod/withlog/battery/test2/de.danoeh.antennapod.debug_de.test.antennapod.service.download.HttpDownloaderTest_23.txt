Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 2d 10h 16m 31s 200ms  (from 7 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 9h 10m 0s 200ms  (from 1 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 14s 150ms (100.0%) realtime, 1m 14s 151ms (100.0%) uptime
  Time on battery screen off: 1m 14s 150ms (100.0%) realtime, 1m 14s 151ms (100.0%) uptime
  Time on battery screen doze: 1m 14s 150ms (100.0%)
  Total run time: 1m 14s 187ms realtime, 1m 14s 188ms uptime
  Discharge: 1.28 mAh
  Screen off discharge: 1.28 mAh
  Screen doze discharge: 1.28 mAh
  Start clock time: 2018-10-15-23-26-14
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 14s 150ms (100.0%) 0x
  Idle mode light time: 1m 14s 150ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 578ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 14s 150ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 14s 150ms (100.0%) 
     Cellular Sleep time:  1m 13s 43ms (98.5%)
     Cellular Idle time:   318ms (0.4%)
     Cellular Rx time:     791ms (1.1%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 14s 150ms (100.0%) 
     Wifi supplicant states:
       completed 1m 14s 150ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 14s 150ms (100.0%) 
     WiFi Sleep time:  1m 14s 152ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 14s 153ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 1.02, actual drain: 0
    Uid u0a200: 0.257 ( cpu=0.257 ) Including smearing: 0.556 ( proportional=0.299 )
    Cell standby: 0.227 ( radio=0.227 ) Excluded from smearing
    Idle: 0.202 Excluded from smearing
    Uid 0: 0.175 ( cpu=0.117 wake=0.0583 ) Excluded from smearing
    Uid 1000: 0.0727 ( cpu=0.0675 wake=0.0000278 sensor=0.00515 ) Excluded from smearing
    Uid 2000: 0.0433 ( cpu=0.0433 ) Excluded from smearing
    Uid 1036: 0.0192 ( cpu=0.0192 ) Excluded from smearing
    Uid u0a100: 0.00744 ( cpu=0.00718 wake=0.000259 ) Including smearing: 0.0161 ( proportional=0.00866 )
    Uid u0a22: 0.00710 ( cpu=0.00703 wake=0.0000706 ) Excluded from smearing
    Uid u0a35: 0.00466 ( cpu=0.00466 ) Including smearing: 0.0101 ( proportional=0.00542 )
    Uid u0a47: 0.00354 ( cpu=0.00345 wake=0.0000928 ) Excluded from smearing
    Uid u0a38: 0.00104 ( cpu=0.00104 ) Excluded from smearing
    Uid 1001: 0.000397 ( cpu=0.000390 wake=0.00000714 ) Excluded from smearing
    Uid u0a8: 0.000343 ( cpu=0.000343 ) Including smearing: 0.000743 ( proportional=0.000399 )
    Wifi: 0.000288 ( cpu=0.000288 ) Including smearing: 0.000623 ( proportional=0.000335 )
    Uid u0a25: 0.000169 ( cpu=0.000169 ) Including smearing: 0.000366 ( proportional=0.000197 )
    Uid u0a114: 0.0000872 ( cpu=0.0000872 ) Including smearing: 0.000189 ( proportional=0.000101 )
    Uid u0a40: 0.0000843 ( cpu=0.0000843 ) Including smearing: 0.000182 ( proportional=0.0000980 )
    Uid u0a61: 0.0000830 ( cpu=0.0000830 ) Including smearing: 0.000180 ( proportional=0.0000966 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 13s 997ms realtime (2 times)
    XO_shutdown.MPSS: 1m 13s 298ms realtime (33 times)
    XO_shutdown.ADSP: 1m 14s 22ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 36ms partial (2 times) max=49 actual=53 realtime
    Sensor 17: 1m 14s 143ms realtime (0 times)
    Fg Service for: 1m 14s 143ms 
    Total running: 1m 14s 143ms 
    Total cpu time: u=1s 173ms s=1s 97ms 
    Total cpu time per freq: 60 0 0 0 140 150 60 100 30 60 50 20 30 50 20 10 60 60 90 40 40 300 110 10 30 50 20 60 0 10 30 10 100 0 0 70 30 10 30 50 20 10 30 50 0 10 0 10 10 20 0 0 150
    Total screen-off cpu time per freq: 60 0 0 0 140 150 60 100 30 60 50 20 30 50 20 10 60 60 90 40 40 300 110 10 30 50 20 60 0 10 30 10 100 0 0 70 30 10 30 50 20 10 30 50 0 10 0 10 10 20 0 0 150
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 840ms usr + 830ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:EventConditionProvider.EVALUATE: 1 times
  u0a200:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1m 12s 562ms (97.9%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Foreground services: 1m 13s 601ms realtime (1 times)
    Fg Service for: 1m 13s 601ms 
    Cached for: 36ms 
    Total running: 1m 13s 637ms 
    Total cpu time: u=6s 587ms s=1s 494ms 
    Total cpu time per freq: 160 10 0 40 450 230 220 210 230 290 130 160 60 200 150 300 210 230 280 130 90 870 1340 60 200 70 60 30 60 70 80 70 40 70 30 80 40 60 180 120 110 140 10 120 20 90 30 80 40 70 20 10 930
    Total screen-off cpu time per freq: 160 10 0 40 450 230 220 210 230 290 130 160 60 200 150 300 210 230 280 130 90 870 1340 60 200 70 60 30 60 70 80 70 40 70 30 80 40 60 180 120 110 140 10 120 20 90 30 80 40 70 20 10 930
    Proc de.danoeh.antennapod.debug:
      CPU: 1s 850ms usr + 450ms krn ; 0ms fg
      1 starts

