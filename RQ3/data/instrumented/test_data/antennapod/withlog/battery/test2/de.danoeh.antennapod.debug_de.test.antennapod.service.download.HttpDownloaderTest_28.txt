Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 2d 12h 4m 20s 500ms  (from 8 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 1d 16h 54m 33s 100ms  (from 2 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 14s 339ms (99.9%) realtime, 1m 14s 339ms (100.0%) uptime
  Time on battery screen off: 1m 14s 339ms (100.0%) realtime, 1m 14s 339ms (100.0%) uptime
  Time on battery screen doze: 1m 14s 339ms (100.0%)
  Total run time: 1m 14s 387ms realtime, 1m 14s 387ms uptime
  Discharge: 1.21 mAh
  Screen off discharge: 1.21 mAh
  Screen doze discharge: 1.21 mAh
  Start clock time: 2018-10-15-23-33-15
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 14s 339ms (100.0%) 0x
  Idle mode light time: 1m 14s 339ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 178ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 14s 339ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 14s 339ms (100.0%) 
     Cellular Sleep time:  1m 13s 692ms (99.1%)
     Cellular Idle time:   285ms (0.4%)
     Cellular Rx time:     364ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 14s 339ms (100.0%) 
     Wifi supplicant states:
       completed 1m 14s 339ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 14s 339ms (100.0%) 
     WiFi Sleep time:  1m 14s 341ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 14s 341ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 0.948, actual drain: 0
    Uid u0a200: 0.254 ( cpu=0.254 ) Including smearing: 0.524 ( proportional=0.270 )
    Cell standby: 0.227 ( radio=0.227 ) Excluded from smearing
    Idle: 0.203 Excluded from smearing
    Uid 0: 0.156 ( cpu=0.0973 wake=0.0588 ) Excluded from smearing
    Uid 1000: 0.0448 ( cpu=0.0396 wake=0.0000222 sensor=0.00516 ) Excluded from smearing
    Uid 2000: 0.0382 ( cpu=0.0382 ) Excluded from smearing
    Uid 1036: 0.0156 ( cpu=0.0156 ) Excluded from smearing
    Uid u0a47: 0.00430 ( cpu=0.00421 wake=0.0000888 ) Excluded from smearing
    Uid u0a22: 0.00216 ( cpu=0.00214 wake=0.0000222 ) Excluded from smearing
    Uid u0a35: 0.00151 ( cpu=0.00151 ) Including smearing: 0.00311 ( proportional=0.00160 )
    Uid 1001: 0.000389 ( cpu=0.000381 wake=0.00000714 ) Excluded from smearing
    Wifi: 0.000380 ( cpu=0.000380 ) Including smearing: 0.000784 ( proportional=0.000404 )
    Uid u0a25: 0.000278 ( cpu=0.000278 ) Including smearing: 0.000573 ( proportional=0.000296 )
    Uid u0a114: 0.000195 ( cpu=0.000195 ) Including smearing: 0.000403 ( proportional=0.000208 )
    Uid u0a40: 0.000193 ( cpu=0.000193 ) Including smearing: 0.000399 ( proportional=0.000206 )
    Uid u0a8: 0.000123 ( cpu=0.000123 ) Including smearing: 0.000254 ( proportional=0.000131 )
    Uid u0a127: 0.0000826 ( cpu=0.0000826 ) Including smearing: 0.000171 ( proportional=0.0000880 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 14s 229ms realtime (2 times)
    XO_shutdown.MPSS: 1m 13s 670ms realtime (33 times)
    XO_shutdown.ADSP: 1m 14s 247ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 29ms partial (1 times) max=46 actual=46 realtime
    Sensor 17: 1m 14s 328ms realtime (0 times)
    Fg Service for: 1m 14s 328ms 
    Total running: 1m 14s 328ms 
    Total cpu time: u=760ms s=580ms 
    Total cpu time per freq: 30 0 0 10 100 90 40 80 50 10 30 10 10 10 30 40 40 0 60 10 20 180 40 10 10 10 0 0 0 0 0 10 20 20 10 10 10 10 10 0 10 0 10 40 0 10 30 0 0 10 20 0 130
    Total screen-off cpu time per freq: 30 0 0 10 100 90 40 80 50 10 30 10 10 10 30 40 40 0 60 10 20 180 40 10 10 10 0 0 0 0 0 10 20 20 10 10 10 10 10 0 10 0 10 40 0 10 30 0 0 10 20 0 130
    Proc servicemanager:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 460ms usr + 390ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a200:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1m 12s 731ms (97.8%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Foreground services: 1m 13s 763ms realtime (1 times)
    Fg Service for: 1m 13s 763ms 
    Cached for: 44ms 
    Total running: 1m 13s 807ms 
    Total cpu time: u=7s 47ms s=1s 377ms 
    Total cpu time per freq: 530 40 40 30 390 320 180 220 250 250 210 160 210 80 180 250 170 170 230 120 130 1290 1180 80 170 50 60 20 10 30 0 60 70 50 110 70 60 140 60 90 130 30 40 50 50 30 100 110 50 80 40 0 690
    Total screen-off cpu time per freq: 530 40 40 30 390 320 180 220 250 250 210 160 210 80 180 250 170 170 230 120 130 1290 1180 80 170 50 60 20 10 30 0 60 70 50 110 70 60 140 60 90 130 30 40 50 50 30 100 110 50 80 40 0 690
    Proc de.danoeh.antennapod.debug:
      CPU: 840ms usr + 170ms krn ; 0ms fg
      1 starts

