Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 2d 10h 16m 31s 200ms  (from 7 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 9h 10m 0s 200ms  (from 1 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 7s 540ms (99.9%) realtime, 1m 7s 540ms (100.0%) uptime
  Time on battery screen off: 1m 7s 540ms (100.0%) realtime, 1m 7s 540ms (100.0%) uptime
  Time on battery screen doze: 1m 7s 540ms (100.0%)
  Total run time: 1m 7s 611ms realtime, 1m 7s 611ms uptime
  Discharge: 7.47 mAh
  Screen off discharge: 7.47 mAh
  Screen doze discharge: 7.47 mAh
  Start clock time: 2018-10-15-22-48-59
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 7s 540ms (100.0%) 0x
  Idle mode light time: 1m 7s 540ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 379ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 7s 540ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 7s 540ms (100.0%) 
     Cellular Sleep time:  1m 6s 938ms (99.1%)
     Cellular Idle time:   272ms (0.4%)
     Cellular Rx time:     331ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 7s 540ms (100.0%) 
     Wifi supplicant states:
       completed 1m 7s 540ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 7s 540ms (100.0%) 
     WiFi Sleep time:  1m 7s 541ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 7s 541ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.92, actual drain: 0-35.2
    Uid u0a200: 3.89 ( cpu=3.89 ) Including smearing: 5.64 ( proportional=1.75 )
    Uid 0: 0.728 ( cpu=0.674 wake=0.0533 ) Excluded from smearing
    Uid 2000: 0.422 ( cpu=0.422 ) Excluded from smearing
    Uid 1036: 0.250 ( cpu=0.250 ) Excluded from smearing
    Cell standby: 0.206 ( radio=0.206 ) Excluded from smearing
    Uid 1000: 0.191 ( cpu=0.191 wake=0.0000222 ) Excluded from smearing
    Idle: 0.184 Excluded from smearing
    Uid u0a47: 0.0280 ( cpu=0.0278 wake=0.000203 ) Excluded from smearing
    Uid u0a22: 0.00729 ( cpu=0.00726 wake=0.0000270 ) Excluded from smearing
    Uid u0a35: 0.00465 ( cpu=0.00465 ) Including smearing: 0.00673 ( proportional=0.00208 )
    Uid u0a25: 0.00342 ( cpu=0.00342 ) Including smearing: 0.00496 ( proportional=0.00153 )
    Uid u0a127: 0.000957 ( cpu=0.000957 ) Including smearing: 0.00139 ( proportional=0.000429 )
    Uid 1001: 0.000816 ( cpu=0.000768 wake=0.0000476 ) Excluded from smearing
    Uid u0a40: 0.000775 ( cpu=0.000775 ) Including smearing: 0.00112 ( proportional=0.000347 )
    Wifi: 0.000706 ( cpu=0.000706 ) Including smearing: 0.00102 ( proportional=0.000317 )
    Uid u0a114: 0.000181 ( cpu=0.000181 ) Including smearing: 0.000263 ( proportional=0.0000814 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 7s 387ms realtime (4 times)
    XO_shutdown.MPSS: 1m 6s 774ms realtime (31 times)
    XO_shutdown.ADSP: 1m 7s 429ms realtime (0 times)
    wlan.Active: 47ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 28ms partial (2 times) max=27 actual=50 realtime
    Fg Service for: 1m 7s 521ms 
    Total running: 1m 7s 521ms 
    Total cpu time: u=1s 633ms s=1s 623ms 
    Total cpu time per freq: 120 0 0 0 80 730 230 80 10 10 20 40 40 10 0 20 50 10 40 60 0 190 140 20 20 10 30 10 10 10 0 0 10 0 10 0 0 20 0 0 0 10 0 10 0 10 20 0 10 0 0 0 1230
    Total screen-off cpu time per freq: 120 0 0 0 80 730 230 80 10 10 20 40 40 10 0 20 50 10 40 60 0 190 140 20 20 10 30 10 10 10 0 0 10 0 10 0 0 20 0 0 0 10 0 10 0 10 20 0 10 0 0 0 1230
    Proc servicemanager:
      CPU: 150ms usr + 510ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 1s 190ms usr + 860ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a200:
    Foreground services: 1m 6s 957ms realtime (1 times)
    Fg Service for: 1m 6s 957ms 
    Cached for: 57ms 
    Total running: 1m 7s 14ms 
    Total cpu time: u=59s 574ms s=5s 176ms 
    Total cpu time per freq: 40 0 0 0 30 430 70 40 10 20 0 0 10 0 0 0 10 20 0 0 0 230 2930 100 120 60 40 20 20 10 10 0 10 0 0 10 10 0 0 100 70 70 50 60 70 100 170 280 180 220 160 250 57280
    Total screen-off cpu time per freq: 40 0 0 0 30 430 70 40 10 20 0 0 10 0 0 0 10 20 0 0 0 230 2930 100 120 60 40 20 20 10 10 0 10 0 0 10 10 0 0 100 70 70 50 60 70 100 170 280 180 220 160 250 57280
    Proc de.danoeh.antennapod.debug:
      CPU: 50s 420ms usr + 4s 380ms krn ; 0ms fg
      1 starts

