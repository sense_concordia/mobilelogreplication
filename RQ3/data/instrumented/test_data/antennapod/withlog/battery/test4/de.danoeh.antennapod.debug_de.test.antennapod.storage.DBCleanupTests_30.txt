Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 1d 18h 55m 15s 900ms  (from 16 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 1d 4h 47m 51s 700ms  (from 10 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 8s 398ms (99.9%) realtime, 1m 8s 398ms (100.0%) uptime
  Time on battery screen off: 1m 8s 398ms (100.0%) realtime, 1m 8s 398ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 398ms (100.0%)
  Total run time: 1m 8s 445ms realtime, 1m 8s 445ms uptime
  Discharge: 4.41 mAh
  Screen off discharge: 4.41 mAh
  Screen doze discharge: 4.41 mAh
  Start clock time: 2018-10-16-01-36-09
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 8s 398ms (100.0%) 0x
  Idle mode full time: 1m 8s 398ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 174ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 398ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 398ms (100.0%) 
     Cellular Sleep time:  1m 7s 686ms (99.0%)
     Cellular Idle time:   285ms (0.4%)
     Cellular Rx time:     428ms (0.6%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 398ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 398ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 8s 398ms (100.0%) 
     WiFi Sleep time:  1m 8s 399ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 400ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.76, actual drain: 0-35.2
    Uid u0a200: 1.36 ( cpu=1.36 ) Including smearing: 2.42 ( proportional=1.06 )
    Uid 0: 0.494 ( cpu=0.440 wake=0.0541 ) Excluded from smearing
    Uid 2000: 0.241 ( cpu=0.241 ) Excluded from smearing
    Cell standby: 0.209 ( radio=0.209 ) Excluded from smearing
    Idle: 0.186 Excluded from smearing
    Uid 1036: 0.173 ( cpu=0.173 ) Excluded from smearing
    Uid 1000: 0.0529 ( cpu=0.0529 ) Excluded from smearing
    Uid 1013: 0.0370 ( cpu=0.0370 ) Excluded from smearing
    Uid u0a47: 0.00178 ( cpu=0.00167 wake=0.000115 ) Excluded from smearing
    Uid u0a22: 0.00132 ( cpu=0.00131 wake=0.00000872 ) Excluded from smearing
    Uid u0a35: 0.00104 ( cpu=0.00104 ) Including smearing: 0.00185 ( proportional=0.000812 )
    Uid 1001: 0.000511 ( cpu=0.000497 wake=0.0000143 ) Excluded from smearing
    Uid u0a25: 0.000509 ( cpu=0.000509 ) Including smearing: 0.000907 ( proportional=0.000398 )
    Wifi: 0.000282 ( cpu=0.000282 ) Including smearing: 0.000502 ( proportional=0.000220 )
    Uid u0a127: 0.000126 ( cpu=0.000126 ) Including smearing: 0.000225 ( proportional=0.0000989 )
    Bluetooth: 0.000123 ( cpu=0.000123 ) Including smearing: 0.000219 ( proportional=0.0000963 )
    Uid 1027: 0.0000950 ( cpu=0.0000950 ) Excluded from smearing
    Uid u0a114: 0.0000950 ( cpu=0.0000950 ) Including smearing: 0.000169 ( proportional=0.0000744 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 223ms realtime (6 times)
    XO_shutdown.MPSS: 1m 7s 611ms realtime (33 times)
    XO_shutdown.ADSP: 1m 8s 309ms realtime (0 times)
    wlan.Active: 81ms realtime (6 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Fg Service for: 1m 8s 391ms 
    Total running: 1m 8s 391ms 
    Total cpu time: u=856ms s=803ms 
    Total cpu time per freq: 10 10 0 0 20 60 60 90 30 110 70 10 50 60 80 60 30 10 60 10 20 500 50 40 10 0 0 0 10 0 10 20 10 0 0 10 30 60 0 0 10 10 0 0 0 0 0 0 0 0 0 20 80
    Total screen-off cpu time per freq: 10 10 0 0 20 60 60 90 30 110 70 10 50 60 80 60 30 10 60 10 20 500 50 40 10 0 0 0 10 0 10 20 10 0 0 10 30 60 0 0 10 10 0 0 0 0 0 0 0 0 0 20 80
    Proc servicemanager:
      CPU: 60ms usr + 100ms krn ; 0ms fg
    Proc system:
      CPU: 610ms usr + 510ms krn ; 0ms fg
  u0a200:
    Foreground services: 1m 7s 725ms realtime (1 times)
    Fg Service for: 1m 7s 725ms 
    Cached for: 89ms 
    Total running: 1m 7s 814ms 
    Total cpu time: u=30s 290ms s=12s 450ms 
    Total cpu time per freq: 220 30 10 10 150 570 630 650 620 630 470 400 670 650 690 810 610 620 980 900 760 4910 1640 480 1320 1280 1540 1860 2620 2370 2970 3020 2950 1880 1200 770 640 320 160 170 70 0 20 0 40 50 10 10 0 0 0 0 80
    Total screen-off cpu time per freq: 220 30 10 10 150 570 630 650 620 630 470 400 670 650 690 810 610 620 980 900 760 4910 1640 480 1320 1280 1540 1860 2620 2370 2970 3020 2950 1880 1200 770 640 320 160 170 70 0 20 0 40 50 10 10 0 0 0 0 80
    Proc de.danoeh.antennapod.debug:
      CPU: 20s 870ms usr + 8s 640ms krn ; 0ms fg
      1 starts

