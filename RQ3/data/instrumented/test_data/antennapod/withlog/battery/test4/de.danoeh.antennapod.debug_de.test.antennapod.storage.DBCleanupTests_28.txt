Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 1d 20h 37m 28s 800ms  (from 15 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 1d 6h 4m 3s 800ms  (from 9 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 6s 145ms (99.9%) realtime, 1m 6s 145ms (100.0%) uptime
  Time on battery screen off: 1m 6s 145ms (100.0%) realtime, 1m 6s 145ms (100.0%) uptime
  Time on battery screen doze: 1m 6s 145ms (100.0%)
  Total run time: 1m 6s 198ms realtime, 1m 6s 198ms uptime
  Discharge: 2.81 mAh
  Screen off discharge: 2.81 mAh
  Screen doze discharge: 2.81 mAh
  Start clock time: 2018-10-16-01-33-37
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 6s 145ms (100.0%) 0x
  Idle mode full time: 1m 6s 145ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 159ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 6s 145ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 6s 145ms (100.0%) 
     Cellular Sleep time:  1m 5s 637ms (99.2%)
     Cellular Idle time:   216ms (0.3%)
     Cellular Rx time:     293ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 6s 145ms (100.0%) 
     Wifi supplicant states:
       completed 1m 6s 145ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 6s 145ms (100.0%) 
     WiFi Sleep time:  1m 6s 146ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 6s 146ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.69, actual drain: 0
    Uid u0a200: 1.33 ( cpu=1.33 ) Including smearing: 2.36 ( proportional=1.03 )
    Uid 0: 0.490 ( cpu=0.438 wake=0.0523 ) Excluded from smearing
    Uid 2000: 0.235 ( cpu=0.235 ) Excluded from smearing
    Cell standby: 0.202 ( radio=0.202 ) Excluded from smearing
    Idle: 0.180 Excluded from smearing
    Uid 1036: 0.172 ( cpu=0.172 ) Excluded from smearing
    Uid 1013: 0.0358 ( cpu=0.0358 ) Excluded from smearing
    Uid 1000: 0.0347 ( cpu=0.0347 ) Excluded from smearing
    Uid u0a47: 0.00238 ( cpu=0.00227 wake=0.000112 ) Excluded from smearing
    Uid u0a22: 0.000830 ( cpu=0.000822 wake=0.00000793 ) Excluded from smearing
    Uid u0a35: 0.000603 ( cpu=0.000603 ) Including smearing: 0.00107 ( proportional=0.000467 )
    Uid 1001: 0.000361 ( cpu=0.000355 wake=0.00000634 ) Excluded from smearing
    Wifi: 0.000349 ( cpu=0.000349 ) Including smearing: 0.000619 ( proportional=0.000270 )
    Uid u0a114: 0.000222 ( cpu=0.000222 ) Including smearing: 0.000395 ( proportional=0.000172 )
    Uid u0a25: 0.0000953 ( cpu=0.0000953 ) Including smearing: 0.000169 ( proportional=0.0000739 )
    Uid u0a40: 0.0000953 ( cpu=0.0000953 ) Including smearing: 0.000169 ( proportional=0.0000739 )
    Uid u0a127: 0.0000953 ( cpu=0.0000953 ) Including smearing: 0.000169 ( proportional=0.0000739 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 6s 35ms realtime (2 times)
    XO_shutdown.MPSS: 1m 5s 606ms realtime (29 times)
    XO_shutdown.ADSP: 1m 6s 48ms realtime (0 times)
    wlan.Active: 87ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Fg Service for: 1m 6s 133ms 
    Total running: 1m 6s 133ms 
    Total cpu time: u=654ms s=447ms 
    Total cpu time per freq: 20 0 0 0 20 60 50 70 50 20 40 60 70 80 40 50 30 20 10 10 70 350 60 10 0 10 0 10 10 10 30 10 20 20 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 30
    Total screen-off cpu time per freq: 20 0 0 0 20 60 50 70 50 20 40 60 70 80 40 50 30 20 10 10 70 350 60 10 0 10 0 10 10 10 30 10 20 20 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 30
    Proc servicemanager:
      CPU: 60ms usr + 60ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 400ms usr + 200ms krn ; 0ms fg
  u0a200:
    Foreground services: 1m 5s 514ms realtime (1 times)
    Fg Service for: 1m 5s 514ms 
    Cached for: 49ms 
    Total running: 1m 5s 563ms 
    Total cpu time: u=29s 357ms s=12s 420ms 
    Total cpu time per freq: 190 20 40 0 100 470 490 620 510 410 600 440 580 640 680 930 610 520 1040 890 760 4590 1260 260 820 1220 1520 1870 2480 2330 3270 3140 2850 2330 980 1080 600 350 160 110 70 20 30 20 30 0 30 0 60 0 0 0 60
    Total screen-off cpu time per freq: 190 20 40 0 100 470 490 620 510 410 600 440 580 640 680 930 610 520 1040 890 760 4590 1260 260 820 1220 1520 1870 2480 2330 3270 3140 2850 2330 980 1080 600 350 160 110 70 20 30 20 30 0 30 0 60 0 0 0 60
    Proc de.danoeh.antennapod.debug:
      CPU: 8s 970ms usr + 4s 40ms krn ; 0ms fg
      1 starts

