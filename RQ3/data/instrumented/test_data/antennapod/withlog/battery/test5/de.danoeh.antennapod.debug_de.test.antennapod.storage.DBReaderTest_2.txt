Daily stats:
  Current start time: 2018-10-15-05-10-29
  Next min deadline: 2018-10-16-01-00-00
  Next max deadline: 2018-10-16-03-00-00
  Current daily steps:
    Discharge total time: 1d 18h 55m 15s 900ms  (from 16 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 1d 4h 47m 51s 700ms  (from 10 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)
  Daily from 2018-10-05-23-25-55 to 2018-10-06-09-21-59:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 3s 589ms (100.0%) realtime, 1m 3s 589ms (100.0%) uptime
  Time on battery screen off: 1m 3s 589ms (100.0%) realtime, 1m 3s 589ms (100.0%) uptime
  Time on battery screen doze: 1m 3s 589ms (100.0%)
  Total run time: 1m 3s 617ms realtime, 1m 3s 617ms uptime
  Discharge: 5.56 mAh
  Screen off discharge: 5.56 mAh
  Screen doze discharge: 5.56 mAh
  Start clock time: 2018-10-16-01-40-49
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 3s 589ms (100.0%) 0x
  Idle mode full time: 1m 3s 589ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 165ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 3s 589ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 3s 589ms (100.0%) 
     Cellular Sleep time:  1m 3s 95ms (99.2%)
     Cellular Idle time:   198ms (0.3%)
     Cellular Rx time:     297ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 3s 589ms (100.0%) 
     Wifi supplicant states:
       completed 1m 3s 589ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 3s 589ms (100.0%) 
     WiFi Sleep time:  1m 3s 591ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 3s 591ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.21, actual drain: 0
    Uid u0a200: 1.24 ( cpu=1.24 ) Including smearing: 1.94 ( proportional=0.697 )
    Uid 0: 0.408 ( cpu=0.357 wake=0.0503 ) Excluded from smearing
    Cell standby: 0.194 ( radio=0.194 ) Excluded from smearing
    Idle: 0.173 Excluded from smearing
    Uid 2000: 0.119 ( cpu=0.119 ) Excluded from smearing
    Uid 1036: 0.0495 ( cpu=0.0495 ) Excluded from smearing
    Uid 1000: 0.0177 ( cpu=0.0177 ) Excluded from smearing
    Uid u0a47: 0.00513 ( cpu=0.00502 wake=0.000115 ) Excluded from smearing
    Uid u0a35: 0.00296 ( cpu=0.00296 ) Including smearing: 0.00462 ( proportional=0.00166 )
    Uid u0a22: 0.00171 ( cpu=0.00170 wake=0.00000952 ) Excluded from smearing
    Uid 1001: 0.000464 ( cpu=0.000457 wake=0.00000634 ) Excluded from smearing
    Uid u0a25: 0.000300 ( cpu=0.000300 ) Including smearing: 0.000469 ( proportional=0.000169 )
    Wifi: 0.000144 ( cpu=0.000144 ) Including smearing: 0.000224 ( proportional=0.0000807 )
    Uid u0a127: 0.0000885 ( cpu=0.0000885 ) Including smearing: 0.000138 ( proportional=0.0000497 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 3s 447ms realtime (2 times)
    XO_shutdown.MPSS: 1m 2s 998ms realtime (29 times)
    XO_shutdown.ADSP: 1m 3s 469ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Fg Service for: 1m 3s 581ms 
    Total running: 1m 3s 581ms 
    Total cpu time: u=223ms s=363ms 
    Total cpu time per freq: 10 0 0 0 20 50 30 40 10 40 40 10 0 20 0 30 20 30 0 20 20 170 20 0 0 10 0 0 10 0 0 20 10 0 0 20 0 10 10 10 0 0 0 0 0 0 0 0 0 0 0 0 10
    Total screen-off cpu time per freq: 10 0 0 0 20 50 30 40 10 40 40 10 0 20 0 30 20 30 0 20 20 170 20 0 0 10 0 0 10 0 0 20 10 0 0 20 0 10 10 10 0 0 0 0 0 0 0 0 0 0 0 0 10
    Proc servicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 140ms usr + 300ms krn ; 0ms fg
  u0a200:
    Foreground services: 1m 2s 965ms realtime (1 times)
    Fg Service for: 1m 2s 965ms 
    Cached for: 32ms 
    Total running: 1m 2s 997ms 
    Total cpu time: u=30s 790ms s=10s 530ms 
    Total cpu time per freq: 180 10 0 10 140 450 470 370 350 430 230 270 220 430 400 510 660 270 620 370 350 2090 3650 3370 6230 4800 3660 2480 2150 1110 860 460 820 740 580 480 490 310 320 150 80 10 0 0 0 0 40 0 0 0 0 0 110
    Total screen-off cpu time per freq: 180 10 0 10 140 450 470 370 350 430 230 270 220 430 400 510 660 270 620 370 350 2090 3650 3370 6230 4800 3660 2480 2150 1110 860 460 820 740 580 480 490 310 320 150 80 10 0 0 0 0 40 0 0 0 0 0 110
    Proc de.danoeh.antennapod.debug:
      CPU: 19s 930ms usr + 6s 490ms krn ; 0ms fg
      1 starts

