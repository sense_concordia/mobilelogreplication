Daily stats:
  Current start time: 2018-10-20-01-07-03
  Next min deadline: 2018-10-21-01-00-00
  Next max deadline: 2018-10-21-03-00-00
  Current daily steps:
    Discharge total time: 1d 23h 1m 54s 900ms  (from 3 steps)
    Discharge screen doze time: 1d 23h 1m 54s 900ms  (from 3 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Time on battery: 57s 341ms (99.9%) realtime, 57s 341ms (100.0%) uptime
  Time on battery screen off: 57s 341ms (100.0%) realtime, 57s 341ms (100.0%) uptime
  Time on battery screen doze: 57s 341ms (100.0%)
  Total run time: 57s 412ms realtime, 57s 412ms uptime
  Discharge: 0 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 0 mAh
  Start clock time: 2018-10-20-02-53-03
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 57s 341ms (100.0%) 0x
  Idle mode full time: 57s 341ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 174ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 57s 341ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  57s 341ms (100.0%) 
     Cellular Sleep time:  56s 754ms (99.0%)
     Cellular Idle time:   177ms (0.3%)
     Cellular Rx time:     411ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 57s 341ms (100.0%) 
     Wifi supplicant states:
       completed 57s 341ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 57s 341ms (100.0%) 
     WiFi Sleep time:  57s 343ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  57s 343ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 0.571, actual drain: 0
    Cell standby: 0.175 ( radio=0.175 ) Excluded from smearing
    Idle: 0.156 Excluded from smearing
    Uid 0: 0.103 ( cpu=0.0579 wake=0.0453 ) Excluded from smearing
    Uid u0a200: 0.0847 ( cpu=0.0847 ) Including smearing: 0.199 ( proportional=0.114 )
    Uid 1000: 0.0224 ( cpu=0.0224 ) Excluded from smearing
    Uid 2000: 0.0181 ( cpu=0.0181 ) Excluded from smearing
    Uid u0a47: 0.00329 ( cpu=0.00318 wake=0.000117 ) Excluded from smearing
    Uid 1036: 0.00321 ( cpu=0.00321 ) Excluded from smearing
    Uid u0a22: 0.00172 ( cpu=0.00171 wake=0.0000174 ) Excluded from smearing
    Uid u0a35: 0.00129 ( cpu=0.00129 ) Including smearing: 0.00301 ( proportional=0.00173 )
    Uid u0a90: 0.000561 ( cpu=0.000561 ) Including smearing: 0.00132 ( proportional=0.000755 )
    Uid 1001: 0.000399 ( cpu=0.000395 wake=0.00000397 ) Excluded from smearing
    Wifi: 0.000198 ( cpu=0.000198 ) Including smearing: 0.000464 ( proportional=0.000266 )
    Bluetooth: 0.000104 ( cpu=0.000104 ) Including smearing: 0.000244 ( proportional=0.000140 )
    Uid u0a61: 0.0000844 ( cpu=0.0000844 ) Including smearing: 0.000198 ( proportional=0.000114 )
    Uid u0a25: 0.0000832 ( cpu=0.0000832 ) Including smearing: 0.000195 ( proportional=0.000112 )
    Uid u0a40: 0.0000832 ( cpu=0.0000832 ) Including smearing: 0.000195 ( proportional=0.000112 )
    Uid u0a127: 0.0000832 ( cpu=0.0000832 ) Including smearing: 0.000195 ( proportional=0.000112 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 57s 200ms realtime (2 times)
    XO_shutdown.MPSS: 56s 739ms realtime (24 times)
    XO_shutdown.ADSP: 57s 250ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Fg Service for: 57s 329ms 
    Total running: 57s 329ms 
    Total cpu time: u=310ms s=477ms 
    Total cpu time per freq: 60 0 0 0 70 60 30 0 20 10 0 0 30 0 30 20 10 30 0 0 20 230 30 20 20 0 0 10 30 0 20 20 0 20 10 0 0 20 10 0 0 10 20 0 0 0 0 10 0 0 0 0 10
    Total screen-off cpu time per freq: 60 0 0 0 70 60 30 0 20 10 0 0 30 0 30 20 10 30 0 0 20 230 30 20 20 0 0 10 30 0 20 20 0 20 10 0 0 20 10 0 0 10 20 0 0 0 0 10 0 0 0 0 10
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 220ms usr + 260ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a200:
    Foreground services: 56s 693ms realtime (1 times)
    Fg Service for: 56s 693ms 
    Cached for: 83ms 
    Total running: 56s 776ms 
    Total cpu time: u=2s 433ms s=557ms 
    Total cpu time per freq: 40 0 0 0 50 100 80 120 60 100 60 20 10 50 90 60 70 20 80 10 40 160 500 80 150 90 70 60 110 190 200 30 90 130 70 0 60 80 30 10 0 0 10 0 0 30 30 0 0 0 0 0 10
    Total screen-off cpu time per freq: 40 0 0 0 50 100 80 120 60 100 60 20 10 50 90 60 70 20 80 10 40 160 500 80 150 90 70 60 110 190 200 30 90 130 70 0 60 80 30 10 0 0 10 0 0 30 30 0 0 0 0 0 10
    Proc de.danoeh.antennapod.debug:
      CPU: 340ms usr + 80ms krn ; 0ms fg
      1 starts

