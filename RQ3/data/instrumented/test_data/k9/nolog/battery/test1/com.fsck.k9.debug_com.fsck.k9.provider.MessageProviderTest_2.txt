Daily stats:
  Current start time: 2018-10-16-03-08-12
  Next min deadline: 2018-10-17-01-00-00
  Next max deadline: 2018-10-17-03-00-00
  Current daily steps:
    Discharge total time: 21h 28m 35s 400ms  (from 21 steps)
    Discharge screen on time: 9h 18m 41s 800ms  (from 8 steps)
    Discharge screen doze time: 1d 6h 38m 41s 600ms  (from 12 steps)
    Charge total time: 21h 14m 18s 800ms  (from 19 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 2h 28m 21s 900ms  (from 7 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 21s 404ms (99.8%) realtime, 21s 404ms (100.0%) uptime
  Time on battery screen off: 21s 404ms (100.0%) realtime, 21s 404ms (100.0%) uptime
  Time on battery screen doze: 21s 404ms (100.0%)
  Total run time: 21s 453ms realtime, 21s 453ms uptime
  Discharge: 2.22 mAh
  Screen off discharge: 2.22 mAh
  Screen doze discharge: 2.22 mAh
  Start clock time: 2018-10-16-12-18-32
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 21s 404ms (100.0%) 0x
  Idle mode light time: 17s 163ms (80.2%) 1x -- longest 0ms 
  Total partial wakelock time: 4s 307ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 21s 404ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  21s 404ms (100.0%) 
     Cellular Sleep time:  21s 93ms (98.5%)
     Cellular Idle time:   210ms (1.0%)
     Cellular Rx time:     102ms (0.5%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 21s 404ms (100.0%) 
     Wifi supplicant states:
       completed 21s 404ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 21s 404ms (100.0%) 
     WiFi Sleep time:  21s 406ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  21s 406ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.13, actual drain: 0-35.2
    Uid u0a182: 1.73 ( cpu=1.73 wake=0.0000206 ) Including smearing: 2.03 ( proportional=0.305 )
    Uid 0: 0.128 ( cpu=0.114 wake=0.0136 ) Excluded from smearing
    Cell standby: 0.0654 ( radio=0.0654 ) Excluded from smearing
    Idle: 0.0583 Excluded from smearing
    Uid 1000: 0.0569 ( cpu=0.0536 wake=0.00333 ) Excluded from smearing
    Uid 2000: 0.0294 ( cpu=0.0294 ) Excluded from smearing
    Uid u0a22: 0.0186 ( cpu=0.0186 wake=0.0000182 ) Excluded from smearing
    Uid u0a35: 0.0140 ( cpu=0.0140 ) Including smearing: 0.0165 ( proportional=0.00248 )
    Uid u0a100: 0.0100 ( cpu=0.0100 ) Including smearing: 0.0118 ( proportional=0.00177 )
    Uid u0a47: 0.00962 ( cpu=0.00903 sensor=0.000594 ) Excluded from smearing
    Uid 1036: 0.00958 ( cpu=0.00958 ) Excluded from smearing
    Uid u0a38: 0.00227 ( cpu=0.00227 ) Excluded from smearing
    Uid u0a25: 0.00148 ( cpu=0.00148 ) Including smearing: 0.00174 ( proportional=0.000262 )
    Uid 1001: 0.000877 ( cpu=0.000833 wake=0.0000444 ) Excluded from smearing
    Uid u0a127: 0.000611 ( cpu=0.000611 ) Including smearing: 0.000719 ( proportional=0.000108 )
    Wifi: 0.000316 ( cpu=0.000316 ) Including smearing: 0.000372 ( proportional=0.0000558 )
    Uid u0a114: 0.000244 ( cpu=0.000244 ) Including smearing: 0.000287 ( proportional=0.0000431 )
    Uid u0a40: 0.000177 ( cpu=0.000177 ) Including smearing: 0.000208 ( proportional=0.0000312 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 21s 246ms realtime (5 times)
    XO_shutdown.MPSS: 21s 63ms realtime (12 times)
    XO_shutdown.ADSP: 21s 293ms realtime (0 times)
    wlan.Active: 56ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock deviceidle_maint: 4s 182ms partial (0 times) max=4210 actual=4210 realtime
    Wake lock deviceidle_going_idle: 20ms partial (1 times) max=20 realtime
    TOTAL wake: 4s 202ms blamed partial, 4s 230ms actual partial realtime
    Fg Service for: 21s 393ms 
    Total running: 21s 393ms 
    Total cpu time: u=783ms s=487ms 
    Total cpu time per freq: 10 0 0 0 20 70 30 0 30 20 40 0 30 10 0 20 40 30 10 20 30 300 30 0 20 40 30 10 0 30 40 10 10 20 0 20 40 10 20 10 0 0 0 0 20 20 20 10 10 0 0 10 230
    Total screen-off cpu time per freq: 10 0 0 0 20 70 30 0 30 20 40 0 30 10 0 20 40 30 10 20 30 300 30 0 20 40 30 10 0 30 40 10 10 20 0 20 40 10 20 10 0 0 0 0 20 20 20 10 10 0 0 10 230
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 690ms usr + 340ms krn ; 0ms fg
  u0a182:
    Wake lock CoreService addWakeLock: 14ms partial (2 times) max=19 actual=33 realtime
    Wake lock CoreReceiver getWakeLock: 4ms partial (1 times) max=12 actual=12 realtime
    Wake lock CoreService execute: 3ms partial (1 times) max=8 actual=8 realtime
    Wake lock CoreService onStart: 5ms partial (2 times) max=9 actual=11 realtime
    TOTAL wake: 26ms blamed partial, 47ms actual partial realtime
    Foreground services: 20s 871ms realtime (1 times)
    Fg Service for: 20s 871ms 
    Cached for: 41ms 
    Total running: 20s 912ms 
    Total cpu time: u=26s 420ms s=1s 927ms 
    Total cpu time per freq: 160 20 20 50 230 210 150 40 0 30 0 40 70 0 30 70 40 10 40 0 0 10 270 10 10 0 0 0 30 80 40 20 0 10 0 10 40 0 10 0 40 40 30 10 70 90 20 40 110 150 60 80 24130
    Total screen-off cpu time per freq: 160 20 20 50 230 210 150 40 0 30 0 40 70 0 30 70 40 10 40 0 0 10 270 10 10 0 0 0 30 80 40 20 0 10 0 10 40 0 10 0 40 40 30 10 70 90 20 40 110 150 60 80 24130
    Proc com.fsck.k9.debug:
      CPU: 8s 830ms usr + 880ms krn ; 0ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 29ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 18ms uptime
        Starts: 1, launches: 1

