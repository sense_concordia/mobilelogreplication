Daily stats:
  Current start time: 2018-10-16-03-08-12
  Next min deadline: 2018-10-17-01-00-00
  Next max deadline: 2018-10-17-03-00-00
  Current daily steps:
    Discharge total time: 21h 39m 58s 200ms  (from 20 steps)
    Discharge screen on time: 9h 18m 41s 800ms  (from 8 steps)
    Discharge screen doze time: 1d 7h 49m 23s 600ms  (from 11 steps)
    Charge total time: 21h 14m 18s 800ms  (from 19 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 2h 28m 21s 900ms  (from 7 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 7s 662ms (100.0%) realtime, 1m 7s 663ms (100.0%) uptime
  Time on battery screen off: 1m 7s 662ms (100.0%) realtime, 1m 7s 663ms (100.0%) uptime
  Time on battery screen doze: 1m 7s 662ms (100.0%)
  Total run time: 1m 7s 688ms realtime, 1m 7s 688ms uptime
  Discharge: 8.99 mAh
  Screen off discharge: 8.99 mAh
  Screen doze discharge: 8.99 mAh
  Start clock time: 2018-10-16-12-08-28
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 7s 662ms (100.0%) 0x
  Idle mode light time: 1m 7s 662ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 202ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 7s 662ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 7s 662ms (100.0%) 
     Cellular Sleep time:  1m 7s 196ms (99.3%)
     Cellular Idle time:   211ms (0.3%)
     Cellular Rx time:     256ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 7s 662ms (100.0%) 
     Wifi supplicant states:
       completed 1m 7s 662ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 7s 662ms (100.0%) 
     WiFi Sleep time:  1m 7s 664ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 7s 664ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.37, actual drain: 0
    Uid u0a182: 5.23 ( cpu=5.23 wake=0.0000301 ) Including smearing: 9.03 ( proportional=3.81 )
    Uid 1036: 2.53 ( cpu=2.53 ) Excluded from smearing
    Uid 0: 0.990 ( cpu=0.937 wake=0.0535 ) Excluded from smearing
    Cell standby: 0.207 ( radio=0.207 ) Excluded from smearing
    Idle: 0.184 Excluded from smearing
    Uid 2000: 0.126 ( cpu=0.126 ) Excluded from smearing
    Uid 1000: 0.0550 ( cpu=0.0550 wake=0.0000159 ) Excluded from smearing
    Uid u0a47: 0.0312 ( cpu=0.0292 wake=0.0000983 sensor=0.00188 ) Excluded from smearing
    Uid u0a25: 0.00413 ( cpu=0.00413 ) Including smearing: 0.00714 ( proportional=0.00301 )
    Uid u0a22: 0.00233 ( cpu=0.00232 wake=0.00000714 ) Excluded from smearing
    Uid u0a35: 0.00130 ( cpu=0.00130 ) Including smearing: 0.00225 ( proportional=0.000947 )
    Uid u0a127: 0.00106 ( cpu=0.00106 ) Including smearing: 0.00184 ( proportional=0.000774 )
    Uid 1001: 0.00106 ( cpu=0.00105 wake=0.00000793 ) Excluded from smearing
    Uid u0a40: 0.000378 ( cpu=0.000378 ) Including smearing: 0.000653 ( proportional=0.000275 )
    Uid u0a8: 0.000254 ( cpu=0.000254 ) Including smearing: 0.000438 ( proportional=0.000185 )
    Uid u0a114: 0.000241 ( cpu=0.000241 ) Including smearing: 0.000416 ( proportional=0.000175 )
    Uid u0a61: 0.000190 ( cpu=0.000190 ) Including smearing: 0.000329 ( proportional=0.000139 )
    Wifi: 0.0000943 ( cpu=0.0000943 ) Including smearing: 0.000163 ( proportional=0.0000686 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 7s 540ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 64ms realtime (30 times)
    XO_shutdown.ADSP: 1m 7s 567ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 16ms partial (2 times) max=14 actual=26 realtime
    Wake lock NetworkStats: 5ms partial (1 times) max=9 actual=9 realtime
    TOTAL wake: 21ms blamed partial, 26ms actual partial realtime
    Fg Service for: 1m 7s 657ms 
    Total running: 1m 7s 657ms 
    Total cpu time: u=397ms s=530ms 
    Total cpu time per freq: 20 0 0 0 10 30 10 0 0 10 20 0 20 20 30 80 20 20 80 40 20 270 20 20 0 0 0 10 0 0 0 0 10 0 0 30 0 20 0 10 0 0 0 0 0 0 0 0 0 0 0 0 270
    Total screen-off cpu time per freq: 20 0 0 0 10 30 10 0 0 10 20 0 20 20 30 80 20 20 80 40 20 270 20 20 0 0 0 10 0 0 0 0 10 0 0 30 0 20 0 10 0 0 0 0 0 0 0 0 0 0 0 0 270
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 270ms usr + 370ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a182:
    Wake lock CoreService addWakeLock: 23ms partial (2 times) max=16 actual=30 realtime
    Wake lock CoreReceiver getWakeLock: 3ms partial (1 times) max=6 actual=6 realtime
    Wake lock CoreService execute: 5ms partial (1 times) max=9 actual=9 realtime
    Wake lock CoreService onStart: 8ms partial (2 times) max=7 actual=11 realtime
    TOTAL wake: 39ms blamed partial, 39ms actual partial realtime
    Foreground services: 1m 7s 0ms realtime (1 times)
    Fg Service for: 1m 7s 0ms 
    Cached for: 125ms 
    Total running: 1m 7s 125ms 
    Total cpu time: u=1m 13s 580ms s=10s 260ms 
    Total cpu time per freq: 10 0 0 0 0 10 120 70 40 80 30 60 150 350 180 170 120 180 100 70 70 280 730 120 260 120 30 90 50 10 100 40 50 50 40 30 160 170 150 30 40 70 140 110 70 140 180 120 100 90 50 40 77460
    Total screen-off cpu time per freq: 10 0 0 0 0 10 120 70 40 80 30 60 150 350 180 170 120 180 100 70 70 280 730 120 260 120 30 90 50 10 100 40 50 50 40 30 160 170 150 30 40 70 140 110 70 140 180 120 100 90 50 40 77460
    Proc com.fsck.k9.debug:
      CPU: 1m 5s 610ms usr + 9s 160ms krn ; 0ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 27ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 13ms uptime
        Starts: 1, launches: 1

