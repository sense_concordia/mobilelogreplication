Daily stats:
  Current start time: 2018-10-16-03-08-12
  Next min deadline: 2018-10-17-01-00-00
  Next max deadline: 2018-10-17-03-00-00
  Current daily steps:
    Discharge total time: 19h 59m 46s 400ms  (from 31 steps)
    Discharge screen on time: 10h 17m 17s 700ms  (from 12 steps)
    Discharge screen doze time: 1d 2h 50m 15s 200ms  (from 16 steps)
    Charge total time: 21h 14m 18s 800ms  (from 19 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 2h 28m 21s 900ms  (from 7 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 50s 47ms (99.9%) realtime, 50s 48ms (100.0%) uptime
  Time on battery screen off: 50s 47ms (100.0%) realtime, 50s 48ms (100.0%) uptime
  Time on battery screen doze: 50s 47ms (100.0%)
  Total run time: 50s 89ms realtime, 50s 90ms uptime
  Discharge: 6.22 mAh
  Screen off discharge: 6.22 mAh
  Screen doze discharge: 6.22 mAh
  Start clock time: 2018-10-16-14-01-10
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 50s 47ms (100.0%) 0x
  Idle mode light time: 50s 47ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 211ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 50s 47ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  50s 47ms (100.0%) 
     Cellular Sleep time:  49s 558ms (99.0%)
     Cellular Idle time:   196ms (0.4%)
     Cellular Rx time:     295ms (0.6%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 50s 47ms (100.0%) 
     Wifi supplicant states:
       completed 50s 47ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 50s 47ms (100.0%) 
     WiFi Sleep time:  50s 50ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  50s 50ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 4.60, actual drain: 0
    Uid u0a182: 3.98 ( cpu=3.98 wake=0.0000436 ) Including smearing: 4.45 ( proportional=0.471 )
    Uid 0: 0.263 ( cpu=0.223 wake=0.0395 ) Excluded from smearing
    Cell standby: 0.153 ( radio=0.153 ) Excluded from smearing
    Idle: 0.136 Excluded from smearing
    Uid 1000: 0.0308 ( cpu=0.0308 wake=0.00000872 ) Excluded from smearing
    Uid 2000: 0.0286 ( cpu=0.0286 ) Excluded from smearing
    Uid u0a47: 0.00728 ( cpu=0.00718 wake=0.000109 ) Excluded from smearing
    Uid 1036: 0.00365 ( cpu=0.00365 ) Excluded from smearing
    Uid u0a35: 0.00128 ( cpu=0.00128 ) Including smearing: 0.00143 ( proportional=0.000151 )
    Uid u0a25: 0.000773 ( cpu=0.000773 ) Including smearing: 0.000865 ( proportional=0.0000915 )
    Uid u0a22: 0.000592 ( cpu=0.000592 ) Excluded from smearing
    Uid 1001: 0.000539 ( cpu=0.000534 wake=0.00000476 ) Excluded from smearing
    Uid u0a127: 0.000342 ( cpu=0.000342 ) Including smearing: 0.000383 ( proportional=0.0000405 )
    Uid u0a40: 0.000183 ( cpu=0.000183 ) Including smearing: 0.000204 ( proportional=0.0000216 )
    Uid u0a114: 0.000183 ( cpu=0.000183 ) Including smearing: 0.000204 ( proportional=0.0000216 )
    Wifi: 0.000107 ( cpu=0.000107 ) Including smearing: 0.000119 ( proportional=0.0000126 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 49s 929ms realtime (2 times)
    XO_shutdown.MPSS: 49s 453ms realtime (22 times)
    XO_shutdown.ADSP: 49s 943ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 12ms partial (1 times) max=18 actual=18 realtime
    Fg Service for: 50s 38ms 
    Total running: 50s 38ms 
    Total cpu time: u=230ms s=357ms 
    Total cpu time per freq: 0 0 0 0 80 30 30 30 0 20 20 10 20 0 10 30 0 10 40 10 10 150 30 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 140
    Total screen-off cpu time per freq: 0 0 0 0 80 30 30 30 0 20 20 10 20 0 10 30 0 10 40 10 10 150 30 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 140
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 180ms usr + 270ms krn ; 0ms fg
  u0a182:
    Wake lock CoreService addWakeLock: 23ms partial (2 times) max=17 actual=27 realtime
    Wake lock CoreReceiver getWakeLock: 2ms partial (1 times) max=6 actual=6 realtime
    Wake lock CoreService execute: 24ms partial (1 times) max=28 actual=28 realtime
    Wake lock CoreService onStart: 7ms partial (2 times) max=4 actual=8 realtime
    TOTAL wake: 56ms blamed partial, 56ms actual partial realtime
    Foreground services: 49s 545ms realtime (1 times)
    Fg Service for: 49s 545ms 
    Cached for: 37ms 
    Total running: 49s 582ms 
    Total cpu time: u=1m 2s 410ms s=5s 714ms 
    Total cpu time per freq: 1160 460 1030 540 2260 760 740 310 250 240 310 150 150 100 100 70 10 30 10 0 20 90 640 20 40 150 80 30 50 90 90 100 140 290 390 630 500 330 430 270 280 220 120 380 160 170 240 480 350 150 20 0 52350
    Total screen-off cpu time per freq: 1160 460 1030 540 2260 760 740 310 250 240 310 150 150 100 100 70 10 30 10 0 20 90 640 20 40 150 80 30 50 90 90 100 140 290 390 630 500 330 430 270 280 220 120 380 160 170 240 480 350 150 20 0 52350
    Proc com.fsck.k9.debug:
      CPU: 45s 20ms usr + 4s 190ms krn ; 0ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 47ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 11ms uptime
        Starts: 1, launches: 1

