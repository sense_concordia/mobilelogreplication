Daily stats:
  Current start time: 2018-10-16-03-08-12
  Next min deadline: 2018-10-17-01-00-00
  Next max deadline: 2018-10-17-03-00-00
  Current daily steps:
    Discharge total time: 1d 7h 28m 19s 100ms  (from 12 steps)
    Discharge screen on time: 17h 9m 54s 100ms  (from 2 steps)
    Discharge screen doze time: 1d 10h 20m 0s 100ms  (from 10 steps)
    Charge total time: 21h 14m 18s 800ms  (from 19 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 2h 28m 21s 900ms  (from 7 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 7s 408ms (99.9%) realtime, 1m 7s 407ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 7s 447ms realtime, 1m 7s 447ms uptime
  Discharge: 11.3 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 11.3 mAh
  Start clock time: 2018-10-16-11-36-12
  Screen on: 1m 7s 408ms (100.0%) 0x, Interactive: 1m 7s 408ms (100.0%)
  Screen brightnesses:
    dim 1m 7s 408ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 7s 408ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 7s 408ms (100.0%) 
     Cellular Sleep time:  1m 6s 931ms (99.3%)
     Cellular Idle time:   227ms (0.3%)
     Cellular Rx time:     251ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 7s 408ms (100.0%) 
     Wifi supplicant states:
       completed 1m 7s 408ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 7s 408ms (100.0%) 
     WiFi Sleep time:  1m 7s 410ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 7s 410ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 15.3, actual drain: 0
    Screen: 5.84 Excluded from smearing
    Uid u0a182: 5.29 ( cpu=5.29 ) Including smearing: 7.16 ( proportional=1.87 )
    Uid 1036: 2.56 ( cpu=2.56 ) Excluded from smearing
    Uid 0: 0.981 ( cpu=0.981 ) Excluded from smearing
    Cell standby: 0.206 ( radio=0.206 ) Excluded from smearing
    Idle: 0.184 Excluded from smearing
    Uid 2000: 0.171 ( cpu=0.171 ) Excluded from smearing
    Uid 1000: 0.0545 ( cpu=0.0526 sensor=0.00187 ) Excluded from smearing
    Uid u0a47: 0.0282 ( cpu=0.0281 sensor=0.0000374 ) Excluded from smearing
    Uid u0a127: 0.00560 ( cpu=0.00560 ) Including smearing: 0.00758 ( proportional=0.00198 )
    Uid u0a25: 0.00237 ( cpu=0.00237 ) Including smearing: 0.00320 ( proportional=0.000836 )
    Uid u0a35: 0.00136 ( cpu=0.00136 ) Including smearing: 0.00183 ( proportional=0.000479 )
    Uid u0a22: 0.00115 ( cpu=0.00115 ) Excluded from smearing
    Uid 1001: 0.000944 ( cpu=0.000944 ) Excluded from smearing
    Uid u0a40: 0.000712 ( cpu=0.000712 ) Including smearing: 0.000963 ( proportional=0.000251 )
    Wifi: 0.000525 ( cpu=0.000525 ) Including smearing: 0.000710 ( proportional=0.000185 )
    Uid u0a61: 0.000249 ( cpu=0.000249 ) Including smearing: 0.000337 ( proportional=0.0000880 )
    Bluetooth: 0.000249 ( cpu=0.000249 ) Including smearing: 0.000337 ( proportional=0.0000880 )
    Uid u0a114: 0.0000877 ( cpu=0.0000877 ) Including smearing: 0.000119 ( proportional=0.0000310 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 7s 263ms realtime (2 times)
    XO_shutdown.MPSS: 1m 6s 772ms realtime (29 times)
    XO_shutdown.ADSP: 1m 7s 280ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm* realtime
    Sensor 7: 1m 7s 400ms realtime (0 times)
    Fg Service for: 1m 7s 400ms 
    Total running: 1m 7s 400ms 
    Total cpu time: u=507ms s=534ms 
    Total cpu time per freq: 10 0 0 0 10 20 0 10 0 0 20 0 20 10 30 0 0 20 20 10 10 340 20 20 10 0 0 0 0 0 20 0 0 0 0 0 0 0 0 0 0 0 0 0 10 0 10 0 0 0 0 10 370
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 210ms usr + 330ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a182:
    Wake lock CoreService addWakeLock realtime
    Wake lock CoreReceiver getWakeLock realtime
    Wake lock CoreService execute realtime
    Wake lock CoreService onStart realtime
    Foreground services: 1m 6s 789ms realtime (1 times)
    Fg Service for: 1m 6s 789ms 
    Cached for: 53ms 
    Total running: 1m 6s 842ms 
    Total cpu time: u=1m 14s 76ms s=10s 516ms 
    Total cpu time per freq: 10 0 0 0 0 10 10 20 40 70 100 150 330 580 310 270 320 290 210 60 20 420 800 130 310 80 40 30 30 20 50 30 20 20 40 70 30 10 40 80 60 120 80 110 70 110 190 480 150 70 40 160 76870
    Proc com.fsck.k9.debug:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 30ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 32ms uptime
        Starts: 1, launches: 1

