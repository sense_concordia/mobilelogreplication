Daily stats:
  Current start time: 2018-10-16-03-08-12
  Next min deadline: 2018-10-17-01-00-00
  Next max deadline: 2018-10-17-03-00-00
  Current daily steps:
    Discharge total time: 1d 0h 16m 46s 400ms  (from 17 steps)
    Discharge screen on time: 9h 55m 1s 200ms  (from 7 steps)
    Discharge screen doze time: 1d 10h 20m 0s 100ms  (from 10 steps)
    Charge total time: 21h 14m 18s 800ms  (from 19 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 2h 28m 21s 900ms  (from 7 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)
  Daily from 2018-10-10-07-38-18 to 2018-10-11-03-59-04:
    Discharge total time: 9d 14h 37m 30s 200ms  (from 2 steps)
    Discharge screen off time: 9d 14h 37m 30s 200ms  (from 2 steps)
  Daily from 2018-10-09-07-16-19 to 2018-10-10-07-38-18:
    Discharge total time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off time: 17d 15h 12m 37s 300ms  (from 1 steps)
    Discharge screen off device idle time: 17d 15h 12m 37s 300ms  (from 1 steps)
  Daily from 2018-10-08-03-42-37 to 2018-10-09-07-16-19:
    Discharge total time: 4d 21h 16m 5s 300ms  (from 17 steps)
    Discharge screen doze time: 5d 0h 50m 2s 100ms  (from 16 steps)
    Charge total time: 2h 13m 56s 700ms  (from 34 steps)
    Charge screen on time: 3h 22m 6s 200ms  (from 10 steps)
    Charge screen doze time: 1h 45m 24s 400ms  (from 23 steps)
  Daily from 2018-10-07-04-02-38 to 2018-10-08-03-42-37:
    Discharge total time: 5d 12h 20m 11s 400ms  (from 17 steps)
    Discharge screen doze time: 5d 12h 20m 11s 400ms  (from 17 steps)
  Daily from 2018-10-06-09-21-59 to 2018-10-07-04-02-38:
    Discharge total time: 6d 9h 19m 54s 700ms  (from 10 steps)
    Discharge screen doze time: 6d 9h 19m 54s 700ms  (from 10 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 6s 834ms (99.9%) realtime, 1m 6s 833ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 1m 6s 877ms realtime, 1m 6s 877ms uptime
  Discharge: 11.6 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 11.6 mAh
  Start clock time: 2018-10-16-11-55-32
  Screen on: 1m 6s 834ms (100.0%) 0x, Interactive: 1m 6s 834ms (100.0%)
  Screen brightnesses:
    dim 1m 6s 834ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 6s 834ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 6s 834ms (100.0%) 
     Cellular Sleep time:  1m 6s 329ms (99.2%)
     Cellular Idle time:   245ms (0.4%)
     Cellular Rx time:     260ms (0.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 6s 834ms (100.0%) 
     Wifi supplicant states:
       completed 1m 6s 834ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 6s 834ms (100.0%) 
     WiFi Sleep time:  1m 6s 835ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 6s 835ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 15.2, actual drain: 0-35.2
    Screen: 5.79 Excluded from smearing
    Uid u0a182: 5.22 ( cpu=5.22 ) Including smearing: 7.08 ( proportional=1.85 )
    Uid 1036: 2.57 ( cpu=2.57 ) Excluded from smearing
    Uid 0: 0.932 ( cpu=0.932 ) Excluded from smearing
    Cell standby: 0.204 ( radio=0.204 ) Excluded from smearing
    Idle: 0.182 Excluded from smearing
    Uid 2000: 0.177 ( cpu=0.177 ) Excluded from smearing
    Uid 1000: 0.0553 ( cpu=0.0535 sensor=0.00186 ) Excluded from smearing
    Uid u0a47: 0.0384 ( cpu=0.0383 sensor=0.0000371 ) Excluded from smearing
    Uid u0a127: 0.00728 ( cpu=0.00728 ) Including smearing: 0.00987 ( proportional=0.00259 )
    Uid u0a25: 0.00377 ( cpu=0.00377 ) Including smearing: 0.00510 ( proportional=0.00134 )
    Uid 1001: 0.00159 ( cpu=0.00159 ) Excluded from smearing
    Uid u0a35: 0.00140 ( cpu=0.00140 ) Including smearing: 0.00190 ( proportional=0.000498 )
    Uid u0a40: 0.000881 ( cpu=0.000881 ) Including smearing: 0.00119 ( proportional=0.000313 )
    Uid u0a22: 0.000818 ( cpu=0.000818 ) Excluded from smearing
    Wifi: 0.000563 ( cpu=0.000563 ) Including smearing: 0.000762 ( proportional=0.000200 )
    Uid u0a114: 0.000251 ( cpu=0.000251 ) Including smearing: 0.000340 ( proportional=0.0000892 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 6s 673ms realtime (4 times)
    XO_shutdown.MPSS: 1m 6s 212ms realtime (31 times)
    XO_shutdown.ADSP: 1m 6s 712ms realtime (0 times)
    wlan.Active: 46ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm* realtime
    Sensor 7: 1m 6s 825ms realtime (0 times)
    Fg Service for: 1m 6s 825ms 
    Total running: 1m 6s 825ms 
    Total cpu time: u=523ms s=487ms 
    Total cpu time per freq: 0 0 0 0 10 30 40 20 10 40 20 0 30 20 30 40 10 30 60 20 20 280 20 10 0 0 10 0 0 0 0 0 20 0 10 0 0 0 0 10 0 0 0 10 0 0 0 0 0 0 0 0 190
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 90ms krn ; 0ms fg
    Proc system:
      CPU: 520ms usr + 330ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a182:
    Wake lock CoreService addWakeLock realtime
    Wake lock CoreReceiver getWakeLock realtime
    Wake lock CoreService execute realtime
    Wake lock CoreService onStart realtime
    Foreground services: 1m 6s 191ms realtime (1 times)
    Fg Service for: 1m 6s 191ms 
    Cached for: 77ms 
    Total running: 1m 6s 268ms 
    Total cpu time: u=1m 12s 184ms s=10s 884ms 
    Total cpu time per freq: 0 0 0 0 10 10 0 10 0 30 40 80 150 360 150 60 40 300 130 70 50 450 650 60 200 30 10 20 50 30 30 30 50 20 10 60 0 10 10 110 40 10 60 110 80 30 150 240 300 160 40 10 77700
    Proc com.fsck.k9.debug:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Apk com.fsck.k9.debug:
      Service com.fsck.k9.service.MailService:
        Created for: 72ms uptime
        Starts: 1, launches: 1
      Service com.fsck.k9.service.PushService:
        Created for: 6ms uptime
        Starts: 1, launches: 1

