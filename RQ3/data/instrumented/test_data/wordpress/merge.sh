#!/bin/bash
#debug output
#set -x
mergePerf()
{
    paste -d "\n" withlog_runtime.csv nolog_runtime.csv > runtime.csv

    paste -d "\n" withlog_cpu_time.csv nolog_cpu_time.csv > cpu_time.csv
    paste -d "\n" withlog_cpu_percentage.csv nolog_cpu_percentage.csv > cpu_percentage.csv

    paste -d "\n" withlog_battery.csv nolog_battery.csv > battery.csv
}
mergePerf
