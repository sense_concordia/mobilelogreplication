Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 14h 42m 39s 800ms  (from 64 steps)
    Discharge screen doze time: 12h 42m 23s 800ms  (from 50 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 5s 535ms (100.0%) realtime, 1m 5s 535ms (100.0%) uptime
  Time on battery screen off: 1m 5s 535ms (100.0%) realtime, 1m 5s 535ms (100.0%) uptime
  Time on battery screen doze: 1m 5s 535ms (100.0%)
  Total run time: 1m 5s 566ms realtime, 1m 5s 566ms uptime
  Discharge: 9.48 mAh
  Screen off discharge: 9.48 mAh
  Screen doze discharge: 9.48 mAh
  Start clock time: 2018-07-30-22-16-49
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 5s 535ms (100.0%) 0x
  Idle mode full time: 1m 5s 535ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 652ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 5s 535ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 5s 535ms (100.0%) 
     Cellular Sleep time:  1m 3s 954ms (97.6%)
     Cellular Idle time:   416ms (0.6%)
     Cellular Rx time:     1s 165ms (1.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 5s 535ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 5s 536ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 5s 536ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.58, actual drain: 0-35.2
    Uid u0a130: 4.70 ( cpu=4.70 wake=0.000391 ) Including smearing: 9.22 ( proportional=4.52 )
    Uid 1036: 1.66 ( cpu=1.66 ) Excluded from smearing
    Uid 2000: 1.63 ( cpu=1.63 ) Excluded from smearing
    Uid 0: 0.878 ( cpu=0.826 wake=0.0515 ) Excluded from smearing
    Cell standby: 0.200 ( radio=0.200 ) Excluded from smearing
    Uid 1000: 0.182 ( cpu=0.182 ) Excluded from smearing
    Idle: 0.179 Excluded from smearing
    Uid u0a47: 0.130 ( cpu=0.130 wake=0.000117 ) Excluded from smearing
    Uid u0a22: 0.0119 ( cpu=0.0119 ) Excluded from smearing
    Uid u0a40: 0.00196 ( cpu=0.00196 ) Including smearing: 0.00384 ( proportional=0.00188 )
    Uid u0a127: 0.00171 ( cpu=0.00171 ) Including smearing: 0.00335 ( proportional=0.00164 )
    Uid 1001: 0.00145 ( cpu=0.00144 wake=0.00000872 ) Excluded from smearing
    Wifi: 0.000560 ( cpu=0.000560 ) Including smearing: 0.00110 ( proportional=0.000539 )
    Uid 1053: 0.000170 ( cpu=0.000170 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 5s 405ms realtime (4 times)
    XO_shutdown.MPSS: 1m 3s 990ms realtime (31 times)
    XO_shutdown.ADSP: 1m 5s 467ms realtime (0 times)
    wlan.Active: 53ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Fg Service for: 1m 5s 529ms 
    Total running: 1m 5s 529ms 
    Total cpu time: u=1s 420ms s=1s 800ms 
    Total cpu time per freq: 1180 150 220 140 120 110 100 100 70 80 20 60 80 10 60 30 10 20 40 40 0 100 10 20 10 0 0 0 0 0 0 0 20 0 0 0 10 0 10 0 10 0 0 10 0 10 10 10 0 0 10 0 420
    Total screen-off cpu time per freq: 1180 150 220 140 120 110 100 100 70 80 20 60 80 10 60 30 10 20 40 40 0 100 10 20 10 0 0 0 0 0 0 0 20 0 0 0 10 0 10 0 10 0 0 10 0 10 10 10 0 0 10 0 420
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 530ms usr + 650ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
  u0a130:
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 493ms partial (1 times) max=493 realtime
    Foreground services: 1m 4s 979ms realtime (1 times)
    Fg Service for: 1m 4s 979ms 
    Cached for: 74ms 
    Total running: 1m 5s 53ms 
    Total cpu time: u=1m 17s 140ms s=8s 400ms 
    Total cpu time per freq: 80 80 80 100 70 40 110 90 30 70 30 30 40 30 20 10 10 0 10 0 30 260 890 410 460 660 520 350 390 170 270 260 220 160 170 250 210 210 240 220 280 320 280 340 180 170 220 160 120 150 30 40 74970
    Total screen-off cpu time per freq: 80 80 80 100 70 40 110 90 30 70 30 30 40 30 20 10 10 0 10 0 30 260 890 410 460 660 520 350 390 170 270 260 220 160 170 250 210 210 240 220 280 320 280 340 180 170 220 160 120 150 30 40 74970
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 4s 520ms usr + 7s 90ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 498ms uptime
        Starts: 1, launches: 1

