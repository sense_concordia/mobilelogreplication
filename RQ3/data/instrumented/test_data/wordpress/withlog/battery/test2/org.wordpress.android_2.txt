Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 15h 8m 43s 200ms  (from 59 steps)
    Discharge screen doze time: 13h 3m 11s 800ms  (from 45 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 5s 329ms (100.0%) realtime, 1m 5s 328ms (100.0%) uptime
  Time on battery screen off: 1m 5s 329ms (100.0%) realtime, 1m 5s 328ms (100.0%) uptime
  Time on battery screen doze: 1m 5s 329ms (100.0%)
  Total run time: 1m 5s 346ms realtime, 1m 5s 345ms uptime
  Discharge: 9.76 mAh
  Screen off discharge: 9.76 mAh
  Screen doze discharge: 9.76 mAh
  Start clock time: 2018-07-30-21-54-01
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 5s 329ms (100.0%) 0x
  Idle mode light time: 1m 0s 271ms (92.3%) 1x -- longest 39s 35ms 
  Total partial wakelock time: 5s 810ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 5s 329ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 5s 329ms (100.0%) 
     Cellular Sleep time:  1m 4s 592ms (98.9%)
     Cellular Idle time:   205ms (0.3%)
     Cellular Rx time:     533ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 5s 329ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 5s 331ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 5s 331ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.72, actual drain: 0
    Uid u0a130: 4.73 ( cpu=4.73 wake=0.000336 ) Including smearing: 9.35 ( proportional=4.62 )
    Uid 2000: 1.69 ( cpu=1.69 ) Excluded from smearing
    Uid 1036: 1.65 ( cpu=1.65 ) Excluded from smearing
    Uid 0: 0.876 ( cpu=0.829 wake=0.0472 ) Excluded from smearing
    Uid 1000: 0.214 ( cpu=0.205 wake=0.00399 sensor=0.00454 ) Excluded from smearing
    Cell standby: 0.200 ( radio=0.200 ) Excluded from smearing
    Idle: 0.178 Excluded from smearing
    Uid u0a47: 0.143 ( cpu=0.143 wake=0.000108 ) Excluded from smearing
    Uid u0a22: 0.0329 ( cpu=0.0328 wake=0.000123 ) Excluded from smearing
    Uid u0a40: 0.00350 ( cpu=0.00350 ) Including smearing: 0.00692 ( proportional=0.00342 )
    Uid u0a127: 0.00277 ( cpu=0.00277 ) Including smearing: 0.00547 ( proportional=0.00270 )
    Uid u0a35: 0.00252 ( cpu=0.00252 ) Including smearing: 0.00498 ( proportional=0.00246 )
    Uid 1001: 0.00207 ( cpu=0.00202 wake=0.0000539 ) Excluded from smearing
    Wifi: 0.000328 ( cpu=0.000328 ) Including smearing: 0.000649 ( proportional=0.000320 )
    Uid 1053: 0.000173 ( cpu=0.000173 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 5s 217ms realtime (2 times)
    XO_shutdown.MPSS: 1m 4s 489ms realtime (27 times)
    XO_shutdown.ADSP: 1m 5s 231ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 51ms partial (4 times) max=60 actual=84 realtime
    Wake lock NetworkStats: 5ms partial (1 times) max=15 actual=15 realtime
    Wake lock deviceidle_maint: 4s 952ms partial (1 times) max=5134 actual=5134 realtime
    Wake lock deviceidle_going_idle: 17ms partial (1 times) max=17 realtime
    TOTAL wake: 5s 25ms blamed partial, 5s 211ms actual partial realtime
    Sensor 17: 1m 5s 325ms realtime (0 times)
    Fg Service for: 1m 5s 325ms 
    Total running: 1m 5s 325ms 
    Total cpu time: u=1s 686ms s=2s 244ms 
    Total cpu time per freq: 1280 150 160 170 90 110 160 50 70 80 40 50 80 30 20 20 10 90 0 30 30 110 0 10 0 0 0 10 0 10 10 10 20 0 0 0 0 10 0 0 10 0 0 0 0 10 0 0 0 0 0 0 370
    Total screen-off cpu time per freq: 1280 150 160 170 90 110 160 50 70 80 40 50 80 30 20 20 10 90 0 30 30 110 0 10 0 0 0 10 0 10 10 10 20 0 0 0 0 10 0 0 10 0 0 0 0 10 0 0 0 0 0 0 370
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 20ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 690ms usr + 710ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a130:
    Wake lock *alarm*: 3ms partial (1 times) max=8 actual=8 realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 422ms partial (2 times) max=413 actual=439 realtime
    TOTAL wake: 425ms blamed partial, 447ms actual partial realtime
    Foreground services: 1m 4s 810ms realtime (1 times)
    Fg Service for: 1m 4s 810ms 
    Cached for: 45ms 
    Total running: 1m 4s 855ms 
    Total cpu time: u=1m 17s 550ms s=8s 336ms 
    Total cpu time per freq: 150 10 60 50 30 20 30 40 40 30 50 10 10 0 10 70 20 30 0 40 10 180 740 380 390 550 610 470 280 180 310 220 170 190 200 220 280 290 320 220 270 230 240 290 70 220 100 230 190 160 40 40 74720
    Total screen-off cpu time per freq: 150 10 60 50 30 20 30 40 40 30 50 10 10 0 10 70 20 30 0 40 10 180 740 380 390 550 610 470 280 180 310 220 170 190 200 220 280 290 320 220 270 230 240 290 70 220 100 230 190 160 40 40 74720
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 9s 280ms usr + 7s 360ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 445ms uptime
        Starts: 2, launches: 2

