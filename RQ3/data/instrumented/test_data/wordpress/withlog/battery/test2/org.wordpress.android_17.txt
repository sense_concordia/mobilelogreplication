Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 14h 46m 31s 200ms  (from 63 steps)
    Discharge screen doze time: 12h 44m 54s 100ms  (from 49 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 5s 865ms (100.0%) realtime, 1m 5s 864ms (100.0%) uptime
  Time on battery screen off: 1m 5s 865ms (100.0%) realtime, 1m 5s 864ms (100.0%) uptime
  Time on battery screen doze: 1m 5s 865ms (100.0%)
  Total run time: 1m 5s 883ms realtime, 1m 5s 882ms uptime
  Discharge: 9.14 mAh
  Screen off discharge: 9.14 mAh
  Screen doze discharge: 9.14 mAh
  Start clock time: 2018-07-30-22-13-02
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 5s 865ms (100.0%) 0x
  Idle mode light time: 1m 5s 865ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 742ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 5s 865ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 5s 865ms (100.0%) 
     Cellular Sleep time:  1m 5s 151ms (98.9%)
     Cellular Idle time:   200ms (0.3%)
     Cellular Rx time:     514ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 5s 865ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 5s 866ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 5s 866ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.70, actual drain: 0
    Uid u0a130: 4.74 ( cpu=4.74 wake=0.000471 ) Including smearing: 9.33 ( proportional=4.60 )
    Uid 1036: 1.69 ( cpu=1.69 ) Excluded from smearing
    Uid 2000: 1.69 ( cpu=1.69 ) Excluded from smearing
    Uid 0: 0.841 ( cpu=0.789 wake=0.0516 ) Excluded from smearing
    Cell standby: 0.201 ( radio=0.201 ) Excluded from smearing
    Uid 1000: 0.194 ( cpu=0.190 wake=0.0000190 sensor=0.00457 ) Excluded from smearing
    Idle: 0.180 Excluded from smearing
    Uid u0a47: 0.144 ( cpu=0.144 wake=0.0000896 ) Excluded from smearing
    Uid u0a22: 0.0137 ( cpu=0.0137 ) Excluded from smearing
    Uid u0a127: 0.00232 ( cpu=0.00232 ) Including smearing: 0.00457 ( proportional=0.00225 )
    Uid u0a40: 0.00166 ( cpu=0.00166 ) Including smearing: 0.00328 ( proportional=0.00161 )
    Uid 1001: 0.00163 ( cpu=0.00162 wake=0.00000714 ) Excluded from smearing
    Uid 1053: 0.000334 ( cpu=0.000334 ) Excluded from smearing
    Wifi: 0.000269 ( cpu=0.000269 ) Including smearing: 0.000531 ( proportional=0.000261 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 5s 750ms realtime (2 times)
    XO_shutdown.MPSS: 1m 5s 94ms realtime (27 times)
    XO_shutdown.ADSP: 1m 5s 748ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 25ms partial (1 times) max=54 actual=54 realtime
    Sensor 17: 1m 5s 861ms realtime (0 times)
    Fg Service for: 1m 5s 861ms 
    Total running: 1m 5s 861ms 
    Total cpu time: u=1s 704ms s=1s 894ms 
    Total cpu time per freq: 1100 160 150 170 60 140 150 80 60 40 80 100 60 20 10 80 10 30 30 20 10 120 10 20 0 0 20 10 0 0 10 0 0 0 0 20 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 300
    Total screen-off cpu time per freq: 1100 160 150 170 60 140 150 80 60 40 80 100 60 20 10 80 10 30 30 20 10 120 10 20 0 0 20 10 0 0 10 0 0 0 0 20 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 300
    Proc servicemanager:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc android.hardware.configstore@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 610ms usr + 640ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a130:
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 595ms partial (2 times) max=585 actual=614 realtime
    Foreground services: 1m 5s 257ms realtime (1 times)
    Fg Service for: 1m 5s 257ms 
    Cached for: 75ms 
    Total running: 1m 5s 332ms 
    Total cpu time: u=1m 18s 233ms s=8s 650ms 
    Total cpu time per freq: 180 80 110 40 80 110 150 120 10 40 70 0 0 0 30 80 0 40 0 0 30 330 890 420 660 560 490 440 340 220 270 260 250 190 190 160 280 170 370 300 320 230 330 170 240 140 120 200 170 280 30 110 75550
    Total screen-off cpu time per freq: 180 80 110 40 80 110 150 120 10 40 70 0 0 0 30 80 0 40 0 0 30 330 890 420 660 560 490 440 340 220 270 260 250 190 190 160 280 170 370 300 320 230 330 170 240 140 120 200 170 280 30 110 75550
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 6s 730ms usr + 7s 330ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 618ms uptime
        Starts: 2, launches: 2

