Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 16h 8m 52s 700ms  (from 51 steps)
    Discharge screen doze time: 13h 58m 58s 600ms  (from 37 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 18s 133ms (100.0%) realtime, 1m 18s 134ms (100.0%) uptime
  Time on battery screen off: 1m 18s 133ms (100.0%) realtime, 1m 18s 134ms (100.0%) uptime
  Time on battery screen doze: 1m 18s 133ms (100.0%)
  Total run time: 1m 18s 152ms realtime, 1m 18s 152ms uptime
  Discharge: 10.4 mAh
  Screen off discharge: 10.4 mAh
  Screen doze discharge: 10.4 mAh
  Start clock time: 2018-07-30-21-09-09
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 46s 825ms (59.9%) 1x
  Idle mode light time: 46s 825ms (59.9%) 1x -- longest 0ms 
  Total partial wakelock time: 675ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 18s 133ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 18s 133ms (100.0%) 
     Cellular Sleep time:  1m 16s 959ms (98.5%)
     Cellular Idle time:   286ms (0.4%)
     Cellular Rx time:     889ms (1.1%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 18s 133ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 18s 134ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 18s 134ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 12.5, actual drain: 0
    Uid u0a130: 5.77 ( cpu=5.77 wake=0.000337 ) Including smearing: 12.0 ( proportional=6.25 )
    Uid 1036: 2.55 ( cpu=2.55 ) Excluded from smearing
    Uid 2000: 2.31 ( cpu=2.31 ) Excluded from smearing
    Uid 0: 1.05 ( cpu=0.985 wake=0.0614 ) Excluded from smearing
    Cell standby: 0.239 ( radio=0.239 ) Excluded from smearing
    Idle: 0.213 Excluded from smearing
    Uid 1000: 0.178 ( cpu=0.178 wake=0.000102 ) Excluded from smearing
    Uid u0a47: 0.151 ( cpu=0.151 wake=0.0000896 ) Excluded from smearing
    Uid u0a22: 0.00670 ( cpu=0.00670 ) Excluded from smearing
    Uid u0a35: 0.00221 ( cpu=0.00221 ) Including smearing: 0.00460 ( proportional=0.00239 )
    Uid u0a40: 0.000916 ( cpu=0.000916 ) Including smearing: 0.00191 ( proportional=0.000992 )
    Uid 1001: 0.000707 ( cpu=0.000700 wake=0.00000634 ) Excluded from smearing
    Uid u0a127: 0.000644 ( cpu=0.000644 ) Including smearing: 0.00134 ( proportional=0.000698 )
    Wifi: 0.000335 ( cpu=0.000335 ) Including smearing: 0.000699 ( proportional=0.000363 )
    Uid 1053: 0.000234 ( cpu=0.000234 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 18s 18ms realtime (2 times)
    XO_shutdown.MPSS: 1m 16s 938ms realtime (33 times)
    XO_shutdown.ADSP: 1m 18s 27ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 24ms partial (2 times) max=39 actual=42 realtime
    Wake lock deviceidle_going_idle: 105ms partial (1 times) max=120 actual=120 realtime
    TOTAL wake: 129ms blamed partial, 161ms actual partial realtime
    Fg Service for: 1m 18s 128ms 
    Total running: 1m 18s 128ms 
    Total cpu time: u=1s 490ms s=1s 827ms 
    Total cpu time per freq: 780 340 250 100 40 40 90 50 30 20 20 30 40 0 30 10 20 30 70 30 0 190 120 40 30 20 0 10 40 0 0 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 10 0 0 430
    Total screen-off cpu time per freq: 780 340 250 100 40 40 90 50 30 20 20 30 40 0 30 10 20 30 70 30 0 190 120 40 30 20 0 10 40 0 0 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 10 0 0 430
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 390ms usr + 460ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a130:
    Wake lock *alarm*: 1ms partial (1 times) max=3 actual=3 realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 424ms partial (2 times) max=411 actual=437 realtime
    TOTAL wake: 425ms blamed partial, 439ms actual partial realtime
    Foreground services: 1m 17s 554ms realtime (1 times)
    Fg Service for: 1m 17s 554ms 
    Cached for: 422ms 
    Total running: 1m 17s 976ms 
    Total cpu time: u=1m 35s 593ms s=10s 687ms 
    Total cpu time per freq: 600 410 450 330 120 160 160 110 50 80 50 30 40 20 40 50 0 30 10 30 40 1160 130 210 720 1170 1270 740 380 190 250 130 150 160 90 190 100 190 260 240 310 350 290 190 180 120 20 130 100 140 70 70 92370
    Total screen-off cpu time per freq: 600 410 450 330 120 160 160 110 50 80 50 30 40 20 40 50 0 30 10 30 40 1160 130 210 720 1170 1270 740 380 190 250 130 150 160 90 190 100 190 260 240 310 350 290 190 180 120 20 130 100 140 70 70 92370
    Proc com.android.chrome:sandboxed_process0:
      CPU: 30ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 26s 610ms usr + 9s 460ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 445ms uptime
        Starts: 2, launches: 2

