Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 15h 36m 49s 800ms  (from 55 steps)
    Discharge screen doze time: 13h 28m 39s 500ms  (from 41 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 18s 558ms (100.0%) realtime, 1m 18s 559ms (100.0%) uptime
  Time on battery screen off: 1m 18s 558ms (100.0%) realtime, 1m 18s 559ms (100.0%) uptime
  Time on battery screen doze: 1m 18s 558ms (100.0%)
  Total run time: 1m 18s 578ms realtime, 1m 18s 578ms uptime
  Discharge: 11.5 mAh
  Screen off discharge: 11.5 mAh
  Screen doze discharge: 11.5 mAh
  Start clock time: 2018-07-30-21-31-19
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 18s 558ms (100.0%) 0x
  Idle mode light time: 1m 18s 558ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 551ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 18s 558ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 18s 558ms (100.0%) 
     Cellular Sleep time:  1m 17s 859ms (99.1%)
     Cellular Idle time:   191ms (0.2%)
     Cellular Rx time:     510ms (0.6%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 18s 558ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 18s 561ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 18s 561ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 12.5, actual drain: 0
    Uid u0a130: 5.88 ( cpu=5.88 wake=0.000304 ) Including smearing: 12.1 ( proportional=6.18 )
    Uid 1036: 2.74 ( cpu=2.74 ) Excluded from smearing
    Uid 2000: 2.04 ( cpu=2.04 ) Excluded from smearing
    Uid 0: 1.03 ( cpu=0.969 wake=0.0619 ) Excluded from smearing
    Cell standby: 0.240 ( radio=0.240 ) Excluded from smearing
    Idle: 0.214 Excluded from smearing
    Uid 1000: 0.195 ( cpu=0.195 wake=0.0000254 ) Excluded from smearing
    Uid u0a47: 0.162 ( cpu=0.162 wake=0.0000999 ) Excluded from smearing
    Uid u0a22: 0.00576 ( cpu=0.00576 ) Excluded from smearing
    Uid u0a127: 0.00202 ( cpu=0.00202 ) Including smearing: 0.00415 ( proportional=0.00213 )
    Uid u0a40: 0.00178 ( cpu=0.00178 ) Including smearing: 0.00366 ( proportional=0.00188 )
    Uid 1001: 0.00128 ( cpu=0.00127 wake=0.00000634 ) Excluded from smearing
    Wifi: 0.000204 ( cpu=0.000204 ) Including smearing: 0.000418 ( proportional=0.000214 )
    Uid 1053: 0.000176 ( cpu=0.000176 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 18s 456ms realtime (2 times)
    XO_shutdown.MPSS: 1m 17s 732ms realtime (32 times)
    XO_shutdown.ADSP: 1m 18s 490ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 33ms partial (1 times) max=69 actual=69 realtime
    Fg Service for: 1m 18s 553ms 
    Total running: 1m 18s 553ms 
    Total cpu time: u=1s 504ms s=1s 966ms 
    Total cpu time per freq: 990 290 270 110 20 100 120 90 20 60 10 50 20 40 60 30 50 30 40 10 0 80 0 0 0 0 10 10 0 10 0 0 0 20 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 650
    Total screen-off cpu time per freq: 990 290 270 110 20 100 120 90 20 60 10 50 20 40 60 30 50 30 40 10 0 80 0 0 0 0 10 10 0 10 0 0 0 20 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 650
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 540ms usr + 580ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 20ms usr + 10ms krn ; 0ms fg
  u0a130:
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 383ms partial (2 times) max=377 actual=396 realtime
    Foreground services: 1m 17s 975ms realtime (1 times)
    Fg Service for: 1m 17s 975ms 
    Cached for: 43ms 
    Total running: 1m 18s 18ms 
    Total cpu time: u=1m 34s 573ms s=10s 194ms 
    Total cpu time per freq: 440 250 240 100 180 90 120 70 130 80 110 10 60 20 40 50 40 10 0 0 0 260 280 350 1090 1980 2350 1860 980 460 410 280 360 330 160 280 250 300 460 400 520 790 550 440 310 170 220 130 100 150 100 40 185840
    Total screen-off cpu time per freq: 440 250 240 100 180 90 120 70 130 80 110 10 60 20 40 50 40 10 0 0 0 260 280 350 1090 1980 2350 1860 980 460 410 280 360 330 160 280 250 300 460 400 520 790 550 440 310 170 220 130 100 150 100 40 185840
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 28s 500ms usr + 9s 570ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 407ms uptime
        Starts: 2, launches: 2

