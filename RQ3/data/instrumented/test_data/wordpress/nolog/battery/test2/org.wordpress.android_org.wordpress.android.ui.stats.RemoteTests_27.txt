Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 14h 3m 4s 900ms  (from 78 steps)
    Discharge screen doze time: 12h 14m 16s 400ms  (from 63 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 59s 741ms (100.0%) realtime, 59s 741ms (100.0%) uptime
  Time on battery screen off: 59s 741ms (100.0%) realtime, 59s 741ms (100.0%) uptime
  Time on battery screen doze: 59s 741ms (100.0%)
  Total run time: 59s 767ms realtime, 59s 767ms uptime
  Discharge: 7.73 mAh
  Screen off discharge: 7.73 mAh
  Screen doze discharge: 7.73 mAh
  Start clock time: 2018-07-30-23-52-55
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 59s 741ms (100.0%) 0x
  Idle mode light time: 59s 741ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 29s 190ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 59s 741ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  59s 741ms (100.0%) 
     Cellular Sleep time:  32s 712ms (54.8%)
     Cellular Idle time:   26s 575ms (44.5%)
     Cellular Rx time:     455ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 59s 741ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  34s 359ms (57.5%)
     WiFi Idle time:   25s 373ms (42.5%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     10ms (0.0%)
     WiFi Battery drain: 0.00774mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  59s 742ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.65, actual drain: 0
    Uid u0a139: 3.15 ( cpu=3.15 wake=0.000416 ) Including smearing: 5.36 ( proportional=2.21 )
    Uid u0a22: 1.41 ( cpu=1.23 wake=0.0186 wifi=0.00705 gps=0.160 ) Excluded from smearing
    Uid 0: 0.344 ( cpu=0.319 wake=0.0242 ) Excluded from smearing
    Uid 1000: 0.281 ( cpu=0.197 wake=0.00399 wifi=0.000696 gps=0.0772 sensor=0.00255 ) Excluded from smearing
    Cell standby: 0.183 ( radio=0.183 ) Excluded from smearing
    Idle: 0.163 Excluded from smearing
    Uid u0a47: 0.0603 ( cpu=0.0602 wake=0.000102 ) Excluded from smearing
    Uid 2000: 0.0350 ( cpu=0.0350 ) Excluded from smearing
    Uid 1036: 0.0111 ( cpu=0.0111 ) Excluded from smearing
    Uid 1021: 0.00312 ( cpu=0.00312 ) Excluded from smearing
    Uid u0a127: 0.00203 ( cpu=0.00203 ) Including smearing: 0.00346 ( proportional=0.00143 )
    Uid 1001: 0.00184 ( cpu=0.00184 wake=0.00000555 ) Excluded from smearing
    Uid u0a40: 0.00126 ( cpu=0.00126 ) Including smearing: 0.00215 ( proportional=0.000886 )
    Wifi: 0.00125 ( cpu=0.00125 wifi=0.00000028 ) Including smearing: 0.00213 ( proportional=0.000880 )
    Uid 1053: 0.000798 ( cpu=0.000798 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 33s 614ms realtime (2 times)
    XO_shutdown.MPSS: 30s 827ms realtime (14 times)
    XO_shutdown.ADSP: 59s 660ms realtime (2 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 7ms (0.0%)
    Wifi Scan (blamed): 673ms (1.1%) 1x
    Wifi Scan (actual): 673ms (1.1%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  59s 728ms (100.0%)
       WiFi Idle time:   6ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     10ms (0.0%)
    Wake lock NlpWakeLock: 7ms partial (2 times) max=16 actual=18 realtime
    Wake lock *alarm*: 24ms partial (4 times) max=26 actual=51 realtime
    Wake lock NetworkStats: 2ms partial (1 times) max=8 actual=8 realtime
    Wake lock GnssLocationProvider: 14ms partial (2 times) max=52 actual=54 realtime
    Wake lock AnyMotionDetector: 4s 986ms partial (1 times) max=10007 actual=10007 realtime
    TOTAL wake: 5s 33ms blamed partial, 10s 57ms actual partial realtime
    Sensor GPS: 9s 259ms blamed realtime, 18s 517ms realtime (1 times)
    Sensor 1: 4s 980ms realtime (2 times)
    Sensor 17: 33s 704ms realtime (0 times)
    Fg Service for: 59s 736ms 
    Total running: 59s 736ms 
    Total cpu time: u=1s 575ms s=2s 70ms 
    Total cpu time per freq: 2150 100 110 90 80 70 110 40 40 20 50 60 30 20 10 20 10 60 0 20 20 100 30 0 10 0 0 0 0 10 10 0 0 10 10 0 0 0 0 10 0 0 10 0 0 0 0 0 10 10 10 10 490
    Total screen-off cpu time per freq: 2150 100 110 90 80 70 110 40 40 20 50 60 30 20 10 20 10 60 0 20 20 100 30 0 10 0 0 0 0 10 10 0 0 10 10 0 0 0 0 10 0 0 10 0 0 0 0 0 10 10 10 10 490
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 680ms usr + 800ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 70ms usr + 80ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.deep: 1 times
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
      Wakeup alarm *walarm*:com.android.server.ACTION_TRIGGER_IDLE: 1 times
  u0a139:
    Wake lock *alarm*: 1ms partial (1 times) max=5 actual=5 realtime
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 524ms partial (2 times) max=514 actual=553 realtime
    TOTAL wake: 525ms blamed partial, 558ms actual partial realtime
    Foreground services: 59s 282ms realtime (1 times)
    Fg Service for: 59s 282ms 
    Cached for: 38ms 
    Total running: 59s 320ms 
    Total cpu time: u=54s 817ms s=5s 660ms 
    Total cpu time per freq: 3210 350 460 1480 280 440 1040 150 120 180 420 120 80 30 50 50 60 10 50 50 70 250 3440 320 350 430 420 380 340 350 500 560 360 540 410 400 490 720 960 870 800 400 380 390 300 430 310 800 270 460 190 230 127940
    Total screen-off cpu time per freq: 3210 350 460 1480 280 440 1040 150 120 180 420 120 80 30 50 50 60 10 50 50 70 250 3440 320 350 430 420 380 340 350 500 560 360 540 410 400 490 720 960 870 800 400 380 390 300 430 310 800 270 460 190 230 127940
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 2s 530ms usr + 6s 410ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 560ms uptime
        Starts: 2, launches: 2

