Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 14h 9m 38s 0ms  (from 75 steps)
    Discharge screen doze time: 12h 17m 1s 400ms  (from 60 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 59s 958ms (100.0%) realtime, 59s 957ms (100.0%) uptime
  Time on battery screen off: 59s 958ms (100.0%) realtime, 59s 957ms (100.0%) uptime
  Time on battery screen doze: 59s 958ms (100.0%)
  Total run time: 59s 977ms realtime, 59s 976ms uptime
  Discharge: 6.88 mAh
  Screen off discharge: 6.88 mAh
  Screen doze discharge: 6.88 mAh
  Start clock time: 2018-07-30-23-31-39
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 59s 958ms (100.0%) 0x
  Idle mode light time: 59s 958ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 564ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 59s 958ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  59s 958ms (100.0%) 
     Cellular Sleep time:  59s 234ms (98.8%)
     Cellular Idle time:   217ms (0.4%)
     Cellular Rx time:     509ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 59s 958ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  59s 961ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  59s 961ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.73, actual drain: 0
    Uid u0a139: 4.57 ( cpu=4.57 wake=0.000315 ) Including smearing: 5.53 ( proportional=0.963 )
    Uid 0: 0.463 ( cpu=0.416 wake=0.0471 ) Excluded from smearing
    Uid 1000: 0.210 ( cpu=0.205 wake=0.0000174 sensor=0.00416 ) Excluded from smearing
    Cell standby: 0.183 ( radio=0.183 ) Excluded from smearing
    Idle: 0.163 Excluded from smearing
    Uid u0a47: 0.0753 ( cpu=0.0752 wake=0.000105 ) Excluded from smearing
    Uid 2000: 0.0423 ( cpu=0.0423 ) Excluded from smearing
    Uid 1036: 0.0126 ( cpu=0.0126 ) Excluded from smearing
    Uid u0a22: 0.00972 ( cpu=0.00972 ) Excluded from smearing
    Uid u0a40: 0.00194 ( cpu=0.00194 ) Including smearing: 0.00235 ( proportional=0.000409 )
    Uid 1001: 0.00191 ( cpu=0.00190 wake=0.0000103 ) Excluded from smearing
    Uid u0a127: 0.00147 ( cpu=0.00147 ) Including smearing: 0.00178 ( proportional=0.000310 )
    Wifi: 0.000500 ( cpu=0.000500 ) Including smearing: 0.000606 ( proportional=0.000105 )
    Uid 1053: 0.000233 ( cpu=0.000233 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 825ms realtime (2 times)
    XO_shutdown.MPSS: 59s 249ms realtime (25 times)
    XO_shutdown.ADSP: 59s 857ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 22ms partial (1 times) max=40 actual=40 realtime
    Sensor 17: 59s 953ms realtime (0 times)
    Fg Service for: 59s 953ms 
    Total running: 59s 953ms 
    Total cpu time: u=1s 797ms s=1s 930ms 
    Total cpu time per freq: 1860 60 80 50 30 40 70 30 50 30 20 30 40 40 20 70 10 50 10 0 10 80 30 0 0 10 10 0 0 0 0 10 0 0 10 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 310
    Total screen-off cpu time per freq: 1860 60 80 50 30 40 70 30 50 30 20 30 40 40 20 70 10 50 10 0 10 80 30 0 0 10 10 0 0 0 0 10 0 0 10 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 310
    Proc servicemanager:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 70ms krn ; 0ms fg
    Proc system:
      CPU: 490ms usr + 460ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a139:
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 397ms partial (1 times) max=397 realtime
    Foreground services: 59s 309ms realtime (1 times)
    Fg Service for: 59s 309ms 
    Cached for: 36ms 
    Total running: 59s 345ms 
    Total cpu time: u=1m 11s 47ms s=7s 146ms 
    Total cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10
    Total screen-off cpu time per freq: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 2s 210ms usr + 6s 290ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 402ms uptime
        Starts: 1, launches: 1

