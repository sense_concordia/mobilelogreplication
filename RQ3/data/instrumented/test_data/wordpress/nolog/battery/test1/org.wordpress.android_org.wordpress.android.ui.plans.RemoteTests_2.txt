Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 14h 36m 30s 300ms  (from 67 steps)
    Discharge screen doze time: 12h 34m 19s 200ms  (from 52 steps)
    Charge total time: 1h 37m 24s 0ms  (from 41 steps)
    Charge screen doze time: 1h 37m 24s 0ms  (from 41 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 9s 986ms (100.0%) realtime, 1m 9s 985ms (100.0%) uptime
  Time on battery screen off: 1m 9s 986ms (100.0%) realtime, 1m 9s 985ms (100.0%) uptime
  Time on battery screen doze: 1m 9s 986ms (100.0%)
  Total run time: 1m 10s 15ms realtime, 1m 10s 14ms uptime
  Discharge: 7.96 mAh
  Screen off discharge: 7.96 mAh
  Screen doze discharge: 7.96 mAh
  Start clock time: 2018-07-30-22-41-27
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Total partial wakelock time: 735ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 9s 986ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 9s 986ms (100.0%) 
     Cellular Sleep time:  1m 9s 228ms (98.9%)
     Cellular Idle time:   195ms (0.3%)
     Cellular Rx time:     566ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 9s 986ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 9s 991ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 9s 991ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 6.45, actual drain: 0
    Uid u0a139: 5.09 ( cpu=5.09 wake=0.000442 ) Including smearing: 6.21 ( proportional=1.12 )
    Uid 0: 0.556 ( cpu=0.501 wake=0.0549 ) Excluded from smearing
    Uid 1000: 0.249 ( cpu=0.249 wake=0.0000293 ) Excluded from smearing
    Cell standby: 0.214 ( radio=0.214 ) Excluded from smearing
    Idle: 0.191 Excluded from smearing
    Uid u0a47: 0.102 ( cpu=0.102 wake=0.000106 ) Excluded from smearing
    Uid 2000: 0.0278 ( cpu=0.0278 ) Excluded from smearing
    Uid 1036: 0.00714 ( cpu=0.00714 ) Excluded from smearing
    Uid u0a22: 0.00677 ( cpu=0.00677 ) Excluded from smearing
    Uid u0a127: 0.00155 ( cpu=0.00155 ) Including smearing: 0.00189 ( proportional=0.000342 )
    Uid u0a40: 0.00150 ( cpu=0.00150 ) Including smearing: 0.00183 ( proportional=0.000331 )
    Uid 1001: 0.00104 ( cpu=0.00103 wake=0.00000476 ) Excluded from smearing
    Wifi: 0.000517 ( cpu=0.000517 ) Including smearing: 0.000631 ( proportional=0.000114 )
    Uid 1053: 0.000350 ( cpu=0.000350 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 9s 880ms realtime (2 times)
    XO_shutdown.MPSS: 1m 9s 159ms realtime (30 times)
    XO_shutdown.ADSP: 1m 9s 909ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 38ms partial (1 times) max=71 actual=71 realtime
    Fg Service for: 1m 9s 979ms 
    Total running: 1m 9s 979ms 
    Total cpu time: u=2s 303ms s=2s 360ms 
    Total cpu time per freq: 2120 80 50 90 20 50 60 20 0 20 20 40 40 90 0 0 10 40 0 10 0 130 40 10 0 0 0 10 0 0 0 10 0 0 0 10 0 10 10 10 0 0 0 0 0 0 0 0 0 0 0 0 400
    Total screen-off cpu time per freq: 2120 80 50 90 20 50 60 20 0 20 20 40 40 90 0 0 10 40 0 10 0 130 40 10 0 0 0 10 0 0 0 10 0 0 0 10 0 10 10 10 0 0 0 0 0 0 0 0 0 0 0 0 400
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 510ms usr + 460ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a139:
    Wake lock wake:org.wordpress.android/com.google.firebase.iid.FirebaseInstanceIdService: 558ms partial (2 times) max=545 actual=573 realtime
    Foreground services: 1m 9s 460ms realtime (1 times)
    Fg Service for: 1m 9s 460ms 
    Cached for: 38ms 
    Total running: 1m 9s 498ms 
    Total cpu time: u=1m 24s 280ms s=9s 0ms 
    Total cpu time per freq: 2910 160 190 660 140 220 420 70 50 140 50 0 10 40 0 20 10 0 40 20 0 100 1530 220 310 230 230 170 80 130 190 220 180 220 170 170 250 500 680 550 300 60 150 120 100 70 210 230 180 130 50 130 78300
    Total screen-off cpu time per freq: 2910 160 190 660 140 220 420 70 50 140 50 0 10 40 0 20 10 0 40 20 0 100 1530 220 310 230 230 170 80 130 190 220 180 220 170 170 250 500 680 550 300 60 150 120 100 70 210 230 180 130 50 130 78300
    Proc com.android.chrome:sandboxed_process0:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Proc org.wordpress.android:
      CPU: 1m 8s 10ms usr + 7s 170ms krn ; 0ms fg
      1 starts
    Apk com.android.chrome:
      Service org.chromium.content.app.SandboxedProcessService0:
        Created for: 0ms uptime
        Starts: 0, launches: 1
    Apk org.wordpress.android:
      Service com.google.firebase.iid.FirebaseInstanceIdService:
        Created for: 578ms uptime
        Starts: 2, launches: 2

