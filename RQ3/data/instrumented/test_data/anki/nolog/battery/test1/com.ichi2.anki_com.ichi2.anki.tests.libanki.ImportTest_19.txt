Daily stats:
  Current start time: 2018-10-22-07-32-32
  Next min deadline: 2018-10-23-01-00-00
  Next max deadline: 2018-10-23-03-00-00
  Current daily steps:
    Discharge total time: 9h 42m 50s 100ms  (from 3 steps)
    Discharge screen on time: 9h 42m 50s 100ms  (from 3 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 52s 903ms (99.9%) realtime, 52s 904ms (100.0%) uptime
  Time on battery screen off: 0ms (0.0%) realtime, 0ms (0.0%) uptime
  Time on battery screen doze: 0ms (0.0%)
  Total run time: 52s 972ms realtime, 52s 972ms uptime
  Discharge: 5.99 mAh
  Screen off discharge: 0 mAh
  Screen doze discharge: 0 mAh
  Screen on discharge: 5.99 mAh
  Start clock time: 2018-10-22-13-57-49
  Screen on: 52s 903ms (100.0%) 0x, Interactive: 52s 903ms (100.0%)
  Screen brightnesses:
    dim 52s 903ms (100.0%)

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 52s 903ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  52s 903ms (100.0%) 
     Cellular Sleep time:  52s 158ms (98.6%)
     Cellular Idle time:   245ms (0.5%)
     Cellular Rx time:     501ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 52s 903ms (100.0%) 
     Wifi supplicant states:
       completed 52s 903ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 52s 903ms (100.0%) 
     WiFi Sleep time:  52s 904ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  52s 904ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 1
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 6.35, actual drain: 0-35.2
    Screen: 4.58 Excluded from smearing
    Uid u0a222: 0.985 ( cpu=0.985 ) Including smearing: 1.09 ( proportional=0.108 )
    Uid 0: 0.338 ( cpu=0.338 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.162 ( radio=0.162 ) Excluded from smearing
    Idle: 0.144 Excluded from smearing
    Uid 2000: 0.0568 ( cpu=0.0568 ) Excluded from smearing
    Uid 1000: 0.0463 ( cpu=0.0448 sensor=0.00147 ) Excluded from smearing
    Uid 1036: 0.00995 ( cpu=0.00995 ) Excluded from smearing
    Uid u0a47: 0.00792 ( cpu=0.00789 sensor=0.0000294 ) Excluded from smearing
    Uid u0a22: 0.00506 ( cpu=0.00506 ) Excluded from smearing
    Uid 1027: 0.00193 ( cpu=0.00193 ) Excluded from smearing
    Uid u0a127: 0.00184 ( cpu=0.00184 ) Including smearing: 0.00204 ( proportional=0.000202 )
    Uid u0a25: 0.00117 ( cpu=0.00117 ) Including smearing: 0.00130 ( proportional=0.000128 )
    Uid u0a114: 0.00108 ( cpu=0.00108 ) Including smearing: 0.00120 ( proportional=0.000118 )
    Uid 1001: 0.000713 ( cpu=0.000713 ) Excluded from smearing
    Uid u0a32: 0.000690 ( cpu=0.000690 ) Including smearing: 0.000766 ( proportional=0.0000758 )
    Bluetooth: 0.000575 ( cpu=0.000575 ) Including smearing: 0.000638 ( proportional=0.0000632 )
    Uid u0a35: 0.000488 ( cpu=0.000488 ) Including smearing: 0.000542 ( proportional=0.0000536 )
    Wifi: 0.000393 ( cpu=0.000393 ) Including smearing: 0.000436 ( proportional=0.0000432 )
    Uid u0a23: 0.000374 ( cpu=0.000374 ) Including smearing: 0.000415 ( proportional=0.0000411 )
    Uid u0a40: 0.000206 ( cpu=0.000206 ) Including smearing: 0.000229 ( proportional=0.0000227 )
    Uid u0a84: 0.000114 ( cpu=0.000114 ) Including smearing: 0.000127 ( proportional=0.0000126 )
    Uid u0a68: 0.0000941 ( cpu=0.0000941 ) Including smearing: 0.000104 ( proportional=0.0000103 )
    Uid u0a181: 0.0000919 ( cpu=0.0000919 ) Including smearing: 0.000102 ( proportional=0.0000101 )
    Uid u0a70: 0.0000857 ( cpu=0.0000857 ) Including smearing: 0.0000951 ( proportional=0.00000942 )
    Uid u0a73: 0.0000857 ( cpu=0.0000857 ) Including smearing: 0.0000951 ( proportional=0.00000942 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 52s 746ms realtime (4 times)
    XO_shutdown.MPSS: 52s 163ms realtime (24 times)
    XO_shutdown.ADSP: 52s 807ms realtime (0 times)
    wlan.Active: 52ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm* realtime
    Wake lock GnssLocationProvider realtime
    Sensor 7: 52s 887ms realtime (0 times)
    Fg Service for: 52s 887ms 
    Total running: 52s 887ms 
    Total cpu time: u=913ms s=530ms 
    Total cpu time per freq: 0 0 0 0 30 110 20 50 70 30 40 10 30 10 50 10 50 20 20 10 50 190 50 90 20 30 30 20 40 10 10 20 10 10 10 20 40 10 0 10 50 0 10 0 20 10 0 20 0 0 0 0 120
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 1s 260ms usr + 670ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a222:
    Foreground services: 52s 34ms realtime (1 times)
    Fg Service for: 52s 34ms 
    Cached for: 49ms 
    Total running: 52s 83ms 
    Total cpu time: u=24s 194ms s=6s 933ms 
    Total cpu time per freq: 70 10 20 20 110 560 590 910 700 900 530 400 280 530 730 640 690 460 610 210 200 1570 2620 1820 1410 1750 1890 1200 1280 1180 1290 610 810 780 540 940 680 420 610 280 230 180 170 30 40 70 130 140 10 0 0 20 330
    Proc com.ichi2.anki:
      CPU: 0ms usr + 0ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 8ms uptime
        Starts: 1, launches: 1

