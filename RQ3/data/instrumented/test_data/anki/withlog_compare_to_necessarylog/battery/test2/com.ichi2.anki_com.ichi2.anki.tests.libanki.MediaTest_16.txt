Daily stats:
  Current start time: 2018-10-22-07-32-32
  Next min deadline: 2018-10-23-01-00-00
  Next max deadline: 2018-10-23-03-00-00
  Current daily steps:
    Discharge total time: 15h 21m 21s 300ms  (from 16 steps)
    Discharge screen on time: 11h 14m 46s 400ms  (from 6 steps)
    Discharge screen doze time: 17h 56m 54s 300ms  (from 7 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 57s 613ms (99.8%) realtime, 57s 613ms (100.0%) uptime
  Time on battery screen off: 57s 613ms (100.0%) realtime, 57s 613ms (100.0%) uptime
  Time on battery screen doze: 57s 613ms (100.0%)
  Total run time: 57s 705ms realtime, 57s 705ms uptime
  Discharge: 3.86 mAh
  Screen off discharge: 3.86 mAh
  Screen doze discharge: 3.86 mAh
  Start clock time: 2018-10-22-19-42-46
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 57s 613ms (100.0%) 0x
  Idle mode light time: 52s 511ms (91.1%) 1x -- longest 18s 836ms 
  Total partial wakelock time: 5s 354ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 57s 613ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  57s 613ms (100.0%) 
     Cellular Sleep time:  56s 933ms (98.8%)
     Cellular Idle time:   194ms (0.3%)
     Cellular Rx time:     488ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 57s 613ms (100.0%) 
     Wifi supplicant states:
       completed 57s 613ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 57s 613ms (100.0%) 
     WiFi Sleep time:  57s 616ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  57s 616ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 1.98, actual drain: 0
    Uid u0a222: 0.985 ( cpu=0.985 ) Including smearing: 1.69 ( proportional=0.705 )
    Uid 0: 0.452 ( cpu=0.411 wake=0.0414 ) Excluded from smearing
    Cell standby: 0.176 ( radio=0.176 ) Excluded from smearing
    Idle: 0.157 Excluded from smearing
    Uid 2000: 0.0679 ( cpu=0.0679 ) Excluded from smearing
    Uid 1000: 0.0520 ( cpu=0.0484 wake=0.00356 ) Excluded from smearing
    Uid u0a22: 0.0362 ( cpu=0.0357 wake=0.000445 ) Excluded from smearing
    Uid 1036: 0.0359 ( cpu=0.0359 ) Excluded from smearing
    Uid u0a100: 0.00678 ( cpu=0.00666 wake=0.000122 ) Including smearing: 0.0116 ( proportional=0.00486 )
    Uid u0a47: 0.00313 ( cpu=0.00302 wake=0.000107 ) Excluded from smearing
    Uid u0a38: 0.00198 ( cpu=0.00198 ) Excluded from smearing
    Uid u0a114: 0.00145 ( cpu=0.00145 ) Including smearing: 0.00250 ( proportional=0.00104 )
    Uid u0a35: 0.000591 ( cpu=0.000591 ) Including smearing: 0.00101 ( proportional=0.000424 )
    Uid u0a32: 0.000473 ( cpu=0.000473 ) Including smearing: 0.000812 ( proportional=0.000339 )
    Uid u0a61: 0.000420 ( cpu=0.000416 wake=0.00000397 ) Including smearing: 0.000720 ( proportional=0.000301 )
    Wifi: 0.000300 ( cpu=0.000300 ) Including smearing: 0.000514 ( proportional=0.000215 )
    Uid u0a25: 0.000269 ( cpu=0.000269 ) Including smearing: 0.000461 ( proportional=0.000192 )
    Uid 1001: 0.000214 ( cpu=0.000208 wake=0.00000555 ) Excluded from smearing
    Uid u0a127: 0.000204 ( cpu=0.000204 ) Including smearing: 0.000349 ( proportional=0.000146 )
    Uid u0a121: 0.000153 ( cpu=0.000153 ) Including smearing: 0.000263 ( proportional=0.000110 )
    Uid 1027: 0.000119 ( cpu=0.000119 ) Excluded from smearing
    Uid u0a40: 0.000115 ( cpu=0.000115 ) Including smearing: 0.000197 ( proportional=0.0000822 )
    Bluetooth: 0.0000958 ( cpu=0.0000910 wake=0.00000476 ) Including smearing: 0.000164 ( proportional=0.0000686 )
    Uid u0a68: 0.0000887 ( cpu=0.0000887 ) Including smearing: 0.000152 ( proportional=0.0000635 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 57s 469ms realtime (2 times)
    XO_shutdown.MPSS: 56s 870ms realtime (25 times)
    XO_shutdown.ADSP: 57s 493ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 13ms partial (2 times) max=23 actual=25 realtime
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Wake lock deviceidle_maint: 4s 421ms partial (1 times) max=5098 actual=5098 realtime
    Wake lock deviceidle_going_idle: 50ms partial (1 times) max=50 realtime
    TOTAL wake: 4s 485ms blamed partial, 5s 150ms actual partial realtime
    Fg Service for: 57s 596ms 
    Total running: 57s 596ms 
    Total cpu time: u=820ms s=706ms 
    Total cpu time per freq: 30 0 0 0 60 100 100 70 20 20 10 0 10 30 40 20 40 60 30 30 10 360 50 50 0 60 30 0 20 30 10 0 0 10 0 0 40 20 10 60 20 0 0 30 10 10 10 10 30 10 0 0 170
    Total screen-off cpu time per freq: 30 0 0 0 60 100 100 70 20 20 10 0 10 30 40 20 40 60 30 30 10 360 50 50 0 60 30 0 20 30 10 0 0 10 0 0 40 20 10 60 20 0 0 30 10 10 10 10 30 10 0 0 170
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 710ms usr + 520ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a222:
    Foreground services: 56s 686ms realtime (1 times)
    Fg Service for: 56s 686ms 
    Cached for: 91ms 
    Total running: 56s 777ms 
    Total cpu time: u=24s 50ms s=7s 444ms 
    Total cpu time per freq: 320 30 30 20 160 910 1020 670 390 630 470 610 620 630 450 720 470 350 450 150 210 1550 3600 1790 960 2090 2550 2030 1500 1060 780 700 490 630 310 410 440 330 670 330 240 110 160 100 60 20 90 40 20 0 0 20 400
    Total screen-off cpu time per freq: 320 30 30 20 160 910 1020 670 390 630 470 610 620 630 450 720 470 350 450 150 210 1550 3600 1790 960 2090 2550 2030 1500 1060 780 700 490 630 310 410 440 330 670 330 240 110 160 100 60 20 90 40 20 0 0 20 400
    Proc com.ichi2.anki:
      CPU: 18s 520ms usr + 6s 20ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 7ms uptime
        Starts: 1, launches: 1

