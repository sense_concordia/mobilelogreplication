Daily stats:
  Current start time: 2018-10-21-09-09-01
  Next min deadline: 2018-10-22-01-00-00
  Next max deadline: 2018-10-22-03-00-00
  Current daily steps:
    Discharge total time: 2d 7h 58m 29s 600ms  (from 16 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 23h 15m 42s 200ms  (from 8 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 9s 17ms (99.9%) realtime, 1m 9s 17ms (100.0%) uptime
  Time on battery screen off: 1m 9s 17ms (100.0%) realtime, 1m 9s 17ms (100.0%) uptime
  Time on battery screen doze: 1m 9s 17ms (100.0%)
  Total run time: 1m 9s 108ms realtime, 1m 9s 108ms uptime
  Discharge: 3.98 mAh
  Screen off discharge: 3.98 mAh
  Screen doze discharge: 3.98 mAh
  Start clock time: 2018-10-21-20-53-16
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 9s 17ms (100.0%) 0x
  Idle mode full time: 1m 9s 17ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 209ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 9s 17ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 9s 17ms (100.0%) 
     Cellular Sleep time:  1m 8s 280ms (98.9%)
     Cellular Idle time:   196ms (0.3%)
     Cellular Rx time:     542ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 9s 17ms (100.0%) 
     Wifi supplicant states:
       completed 1m 9s 17ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 9s 17ms (100.0%) 
     WiFi Sleep time:  1m 9s 18ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 9s 18ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.57, actual drain: 0
    Uid u0a222: 1.54 ( cpu=1.54 ) Including smearing: 2.29 ( proportional=0.745 )
    Uid 0: 0.432 ( cpu=0.377 wake=0.0546 ) Excluded from smearing
    Cell standby: 0.211 ( radio=0.211 ) Excluded from smearing
    Idle: 0.188 Excluded from smearing
    Uid 1000: 0.108 ( cpu=0.108 wake=0.00000159 ) Excluded from smearing
    Uid 2000: 0.0666 ( cpu=0.0666 ) Excluded from smearing
    Uid u0a22: 0.0104 ( cpu=0.00552 wake=0.0000389 sensor=0.00479 ) Excluded from smearing
    Uid 1036: 0.00698 ( cpu=0.00698 ) Excluded from smearing
    Uid u0a47: 0.00227 ( cpu=0.00215 wake=0.000117 ) Excluded from smearing
    Uid u0a114: 0.000966 ( cpu=0.000966 ) Including smearing: 0.00143 ( proportional=0.000466 )
    Uid u0a32: 0.000560 ( cpu=0.000560 ) Including smearing: 0.000830 ( proportional=0.000270 )
    Uid u0a35: 0.000524 ( cpu=0.000524 ) Including smearing: 0.000777 ( proportional=0.000253 )
    Uid 1001: 0.000347 ( cpu=0.000339 wake=0.00000793 ) Excluded from smearing
    Wifi: 0.000317 ( cpu=0.000317 ) Including smearing: 0.000471 ( proportional=0.000153 )
    Uid u0a127: 0.000225 ( cpu=0.000225 ) Including smearing: 0.000333 ( proportional=0.000108 )
    Uid u0a40: 0.000201 ( cpu=0.000201 ) Including smearing: 0.000298 ( proportional=0.0000968 )
    Uid u0a68: 0.000167 ( cpu=0.000167 ) Including smearing: 0.000248 ( proportional=0.0000806 )
    Uid u0a25: 0.000133 ( cpu=0.000133 ) Including smearing: 0.000198 ( proportional=0.0000644 )
    Uid 1027: 0.000104 ( cpu=0.000104 ) Excluded from smearing
    Bluetooth: 0.000104 ( cpu=0.000104 ) Including smearing: 0.000154 ( proportional=0.0000500 )
    Uid u0a8: 0.0000957 ( cpu=0.0000957 ) Including smearing: 0.000142 ( proportional=0.0000461 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 881ms realtime (2 times)
    XO_shutdown.MPSS: 1m 8s 239ms realtime (28 times)
    XO_shutdown.ADSP: 1m 8s 890ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    Fg Service for: 1m 8s 997ms 
    Total running: 1m 8s 997ms 
    Total cpu time: u=1s 727ms s=1s 340ms 
    Total cpu time per freq: 80 10 0 0 60 90 70 80 30 100 50 70 80 50 50 30 20 40 50 80 60 940 110 60 20 20 0 0 10 0 10 30 190 170 170 80 50 70 40 30 30 0 10 30 0 10 10 0 0 10 10 0 180
    Total screen-off cpu time per freq: 80 10 0 0 60 90 70 80 30 100 50 70 80 50 50 30 20 40 50 80 60 940 110 60 20 20 0 0 10 0 10 30 190 170 170 80 50 70 40 30 30 0 10 30 0 10 10 0 0 10 10 0 180
    Proc servicemanager:
      CPU: 160ms usr + 280ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 1s 460ms usr + 890ms krn ; 0ms fg
  u0a222:
    Foreground services: 1m 8s 389ms realtime (1 times)
    Fg Service for: 1m 8s 389ms 
    Cached for: 53ms 
    Total running: 1m 8s 442ms 
    Total cpu time: u=34s 630ms s=11s 413ms 
    Total cpu time per freq: 110 20 30 10 70 460 590 1130 1070 1400 1260 860 1130 900 750 790 540 680 940 730 680 4590 6240 1420 2050 1460 910 1150 580 1020 640 920 1040 1260 930 900 960 790 940 700 560 580 310 420 270 350 320 190 220 170 40 120 1310
    Total screen-off cpu time per freq: 110 20 30 10 70 460 590 1130 1070 1400 1260 860 1130 900 750 790 540 680 940 730 680 4590 6240 1420 2050 1460 910 1150 580 1020 640 920 1040 1260 930 900 960 790 940 700 560 580 310 420 270 350 320 190 220 170 40 120 1310
    Proc com.ichi2.anki:
      CPU: 25s 290ms usr + 9s 200ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 6ms uptime
        Starts: 1, launches: 1

