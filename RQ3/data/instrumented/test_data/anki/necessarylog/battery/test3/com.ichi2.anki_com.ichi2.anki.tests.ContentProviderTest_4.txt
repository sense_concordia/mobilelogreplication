Daily stats:
  Current start time: 2018-10-21-09-09-01
  Next min deadline: 2018-10-22-01-00-00
  Next max deadline: 2018-10-22-03-00-00
  Current daily steps:
    Discharge total time: 2d 13h 21m 7s 100ms  (from 14 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 2d 8h 54m 14s 100ms  (from 6 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:
  Daily from 2018-10-11-03-59-04 to 2018-10-12-07-33-49:
    Discharge total time: 5d 12h 14m 26s 500ms  (from 3 steps)
    Discharge screen off time: 15d 6h 36m 58s 600ms  (from 1 steps)
    Discharge screen on time: 15h 3m 10s 500ms  (from 2 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 8s 157ms (99.9%) realtime, 1m 8s 157ms (100.0%) uptime
  Time on battery screen off: 1m 8s 157ms (100.0%) realtime, 1m 8s 157ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 157ms (100.0%)
  Total run time: 1m 8s 246ms realtime, 1m 8s 246ms uptime
  Discharge: 3.61 mAh
  Screen off discharge: 3.61 mAh
  Screen doze discharge: 3.61 mAh
  Start clock time: 2018-10-21-20-28-12
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 8s 157ms (100.0%) 0x
  Idle mode light time: 1m 8s 157ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 199ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 157ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 157ms (100.0%) 
     Cellular Sleep time:  1m 7s 224ms (98.6%)
     Cellular Idle time:   262ms (0.4%)
     Cellular Rx time:     672ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 157ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 157ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 8s 157ms (100.0%) 
     WiFi Sleep time:  1m 8s 158ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 158ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.52, actual drain: 0-35.2
    Uid u0a222: 1.51 ( cpu=1.51 ) Including smearing: 2.24 ( proportional=0.730 )
    Uid 0: 0.428 ( cpu=0.374 wake=0.0539 ) Excluded from smearing
    Cell standby: 0.208 ( radio=0.208 ) Excluded from smearing
    Idle: 0.186 Excluded from smearing
    Uid 1000: 0.104 ( cpu=0.0989 wake=0.0000190 sensor=0.00473 ) Excluded from smearing
    Uid 2000: 0.0677 ( cpu=0.0677 ) Excluded from smearing
    Uid 1036: 0.00645 ( cpu=0.00645 ) Excluded from smearing
    Uid u0a22: 0.00432 ( cpu=0.00428 wake=0.0000357 ) Excluded from smearing
    Uid u0a47: 0.00320 ( cpu=0.00312 wake=0.0000880 ) Excluded from smearing
    Uid u0a114: 0.000946 ( cpu=0.000946 ) Including smearing: 0.00140 ( proportional=0.000457 )
    Uid u0a32: 0.000910 ( cpu=0.000910 ) Including smearing: 0.00135 ( proportional=0.000440 )
    Uid u0a35: 0.000706 ( cpu=0.000706 ) Including smearing: 0.00105 ( proportional=0.000341 )
    Uid 1001: 0.000618 ( cpu=0.000603 wake=0.0000151 ) Excluded from smearing
    Wifi: 0.000329 ( cpu=0.000329 ) Including smearing: 0.000489 ( proportional=0.000159 )
    Uid u0a127: 0.000319 ( cpu=0.000319 ) Including smearing: 0.000472 ( proportional=0.000154 )
    Uid u0a25: 0.000208 ( cpu=0.000208 ) Including smearing: 0.000308 ( proportional=0.000100 )
    Uid u0a61: 0.000205 ( cpu=0.000205 ) Including smearing: 0.000304 ( proportional=0.0000992 )
    Uid u0a68: 0.000164 ( cpu=0.000164 ) Including smearing: 0.000243 ( proportional=0.0000792 )
    Uid u0a121: 0.000164 ( cpu=0.000164 ) Including smearing: 0.000243 ( proportional=0.0000792 )
    Uid u0a40: 0.000134 ( cpu=0.000134 ) Including smearing: 0.000199 ( proportional=0.0000649 )
    Bluetooth: 0.000109 ( cpu=0.000109 ) Including smearing: 0.000161 ( proportional=0.0000525 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 3ms realtime (4 times)
    XO_shutdown.MPSS: 1m 7s 205ms realtime (31 times)
    XO_shutdown.ADSP: 1m 8s 51ms realtime (0 times)
    wlan.Active: 56ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 22ms partial (1 times) max=39 actual=39 realtime
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    TOTAL wake: 24ms blamed partial, 41ms actual partial realtime
    Sensor 17: 1m 8s 140ms realtime (0 times)
    Fg Service for: 1m 8s 140ms 
    Total running: 1m 8s 140ms 
    Total cpu time: u=1s 543ms s=1s 336ms 
    Total cpu time per freq: 50 0 0 0 50 70 140 70 50 70 50 40 90 30 40 50 30 80 70 10 60 850 130 20 70 10 0 0 20 10 20 40 140 150 130 120 80 80 50 30 20 0 10 30 0 0 20 10 20 0 0 0 80
    Total screen-off cpu time per freq: 50 0 0 0 50 70 140 70 50 70 50 40 90 30 40 50 30 80 70 10 60 850 130 20 70 10 0 0 20 10 20 40 140 150 130 120 80 80 50 30 20 0 10 30 0 0 20 10 20 0 0 0 80
    Proc servicemanager:
      CPU: 140ms usr + 290ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc system:
      CPU: 1s 250ms usr + 860ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a222:
    Foreground services: 1m 7s 548ms realtime (1 times)
    Fg Service for: 1m 7s 548ms 
    Cached for: 39ms 
    Total running: 1m 7s 587ms 
    Total cpu time: u=34s 384ms s=11s 83ms 
    Total cpu time per freq: 100 10 10 10 190 520 700 710 720 1170 1060 950 1180 700 950 790 650 690 1090 610 430 5450 6290 1580 2290 1620 1090 1010 760 760 740 530 1020 1290 940 1100 790 700 980 610 510 540 350 340 160 200 460 270 180 70 30 20 890
    Total screen-off cpu time per freq: 100 10 10 10 190 520 700 710 720 1170 1060 950 1180 700 950 790 650 690 1090 610 430 5450 6290 1580 2290 1620 1090 1010 760 760 740 530 1020 1290 940 1100 790 700 980 610 510 540 350 340 160 200 460 270 180 70 30 20 890
    Proc com.ichi2.anki:
      CPU: 23s 480ms usr + 8s 50ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 5ms uptime
        Starts: 1, launches: 1

