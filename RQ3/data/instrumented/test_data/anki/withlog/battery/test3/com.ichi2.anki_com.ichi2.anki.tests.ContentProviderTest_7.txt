Daily stats:
  Current start time: 2018-10-22-07-32-32
  Next min deadline: 2018-10-23-01-00-00
  Next max deadline: 2018-10-23-03-00-00
  Current daily steps:
    Discharge total time: 16h 0m 6s 200ms  (from 19 steps)
    Discharge screen on time: 11h 14m 46s 400ms  (from 6 steps)
    Discharge screen doze time: 18h 23m 51s 700ms  (from 10 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 9s 990ms (99.9%) realtime, 1m 9s 990ms (100.0%) uptime
  Time on battery screen off: 1m 9s 990ms (100.0%) realtime, 1m 9s 990ms (100.0%) uptime
  Time on battery screen doze: 1m 9s 990ms (100.0%)
  Total run time: 1m 10s 86ms realtime, 1m 10s 86ms uptime
  Discharge: 4.28 mAh
  Screen off discharge: 4.28 mAh
  Screen doze discharge: 4.28 mAh
  Start clock time: 2018-10-22-20-10-20
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 9s 990ms (100.0%) 0x
  Idle mode light time: 1m 9s 990ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 185ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 9s 990ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 9s 990ms (100.0%) 
     Cellular Sleep time:  1m 9s 200ms (98.9%)
     Cellular Idle time:   197ms (0.3%)
     Cellular Rx time:     595ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 9s 990ms (100.0%) 
     Wifi supplicant states:
       completed 1m 9s 990ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 9s 990ms (100.0%) 
     WiFi Sleep time:  1m 9s 993ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 9s 993ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.91, actual drain: 0
    Uid u0a222: 1.65 ( cpu=1.65 ) Including smearing: 2.60 ( proportional=0.948 )
    Uid 0: 0.536 ( cpu=0.480 wake=0.0554 ) Excluded from smearing
    Cell standby: 0.214 ( radio=0.214 ) Excluded from smearing
    Idle: 0.191 Excluded from smearing
    Uid 2000: 0.140 ( cpu=0.140 ) Excluded from smearing
    Uid 1000: 0.112 ( cpu=0.107 wake=0.0000159 sensor=0.00486 ) Excluded from smearing
    Uid 1036: 0.0467 ( cpu=0.0467 ) Excluded from smearing
    Uid u0a22: 0.00768 ( cpu=0.00765 wake=0.0000333 ) Excluded from smearing
    Uid u0a47: 0.00391 ( cpu=0.00382 wake=0.0000904 ) Excluded from smearing
    Uid u0a35: 0.00220 ( cpu=0.00220 ) Including smearing: 0.00346 ( proportional=0.00126 )
    Uid u0a114: 0.00150 ( cpu=0.00150 ) Including smearing: 0.00236 ( proportional=0.000861 )
    Uid 1001: 0.000576 ( cpu=0.000569 wake=0.00000634 ) Excluded from smearing
    Uid u0a32: 0.000479 ( cpu=0.000479 ) Including smearing: 0.000754 ( proportional=0.000275 )
    Uid u0a68: 0.000322 ( cpu=0.000322 ) Including smearing: 0.000506 ( proportional=0.000185 )
    Uid u0a127: 0.000254 ( cpu=0.000254 ) Including smearing: 0.000400 ( proportional=0.000146 )
    Wifi: 0.000229 ( cpu=0.000229 ) Including smearing: 0.000361 ( proportional=0.000132 )
    Uid u0a25: 0.000139 ( cpu=0.000139 ) Including smearing: 0.000220 ( proportional=0.0000800 )
    Uid u0a23: 0.000117 ( cpu=0.000117 ) Including smearing: 0.000184 ( proportional=0.0000672 )
    Uid u0a181: 0.000117 ( cpu=0.000117 ) Including smearing: 0.000184 ( proportional=0.0000672 )
    Uid u0a8: 0.000112 ( cpu=0.000112 ) Including smearing: 0.000177 ( proportional=0.0000645 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 9s 858ms realtime (2 times)
    XO_shutdown.MPSS: 1m 9s 134ms realtime (29 times)
    XO_shutdown.ADSP: 1m 9s 881ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 20ms partial (1 times) max=34 actual=34 realtime
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    TOTAL wake: 21ms blamed partial, 35ms actual partial realtime
    Sensor 17: 1m 9s 967ms realtime (0 times)
    Fg Service for: 1m 9s 967ms 
    Total running: 1m 9s 967ms 
    Total cpu time: u=1s 610ms s=1s 257ms 
    Total cpu time per freq: 30 0 0 0 80 130 110 40 30 70 20 30 30 60 10 20 30 20 70 50 40 720 60 40 50 0 0 10 10 10 30 20 50 20 30 30 110 80 120 60 130 90 70 80 40 40 50 50 30 20 0 10 180
    Total screen-off cpu time per freq: 30 0 0 0 80 130 110 40 30 70 20 30 30 60 10 20 30 20 70 50 40 720 60 40 50 0 0 10 10 10 30 20 50 20 30 30 110 80 120 60 130 90 70 80 40 40 50 50 30 20 0 10 180
    Proc servicemanager:
      CPU: 130ms usr + 350ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 1s 400ms usr + 750ms krn ; 0ms fg
  u0a222:
    Foreground services: 1m 9s 435ms realtime (1 times)
    Fg Service for: 1m 9s 435ms 
    Cached for: 41ms 
    Total running: 1m 9s 476ms 
    Total cpu time: u=34s 633ms s=11s 167ms 
    Total cpu time per freq: 290 50 40 10 300 610 550 560 260 740 460 390 490 540 500 600 550 310 690 540 440 5640 2870 1910 1500 1340 1440 1210 1200 1100 990 860 880 990 680 760 1150 810 1350 830 930 960 840 1000 540 440 470 720 300 340 170 160 3580
    Total screen-off cpu time per freq: 290 50 40 10 300 610 550 560 260 740 460 390 490 540 500 600 550 310 690 540 440 5640 2870 1910 1500 1340 1440 1210 1200 1100 990 860 880 990 680 760 1150 810 1350 830 930 960 840 1000 540 440 470 720 300 340 170 160 3580
    Proc com.ichi2.anki:
      CPU: 26s 60ms usr + 9s 460ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 12ms uptime
        Starts: 1, launches: 1

