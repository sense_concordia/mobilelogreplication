Daily stats:
  Current start time: 2018-10-22-07-32-32
  Next min deadline: 2018-10-23-01-00-00
  Next max deadline: 2018-10-23-03-00-00
  Current daily steps:
    Discharge total time: 15h 56m 27s 0ms  (from 22 steps)
    Discharge screen on time: 11h 14m 46s 400ms  (from 6 steps)
    Discharge screen doze time: 17h 44m 30s 300ms  (from 13 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 10s 776ms (99.9%) realtime, 1m 10s 776ms (100.0%) uptime
  Time on battery screen off: 1m 10s 776ms (100.0%) realtime, 1m 10s 776ms (100.0%) uptime
  Time on battery screen doze: 1m 10s 776ms (100.0%)
  Total run time: 1m 10s 874ms realtime, 1m 10s 874ms uptime
  Discharge: 4.84 mAh
  Screen off discharge: 4.84 mAh
  Screen doze discharge: 4.84 mAh
  Start clock time: 2018-10-22-20-38-18
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 10s 776ms (100.0%) 0x
  Idle mode full time: 1m 10s 776ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 217ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 10s 776ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 10s 776ms (100.0%) 
     Cellular Sleep time:  1m 9s 798ms (98.6%)
     Cellular Idle time:   216ms (0.3%)
     Cellular Rx time:     763ms (1.1%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 10s 776ms (100.0%) 
     Wifi supplicant states:
       completed 1m 10s 776ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 10s 776ms (100.0%) 
     WiFi Sleep time:  1m 10s 777ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 10s 777ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.90, actual drain: 0
    Uid u0a222: 1.66 ( cpu=1.66 ) Including smearing: 2.60 ( proportional=0.933 )
    Uid 0: 0.514 ( cpu=0.458 wake=0.0560 ) Excluded from smearing
    Cell standby: 0.216 ( radio=0.216 ) Excluded from smearing
    Idle: 0.193 Excluded from smearing
    Uid 2000: 0.145 ( cpu=0.145 ) Excluded from smearing
    Uid 1000: 0.112 ( cpu=0.112 wake=0.00000159 ) Excluded from smearing
    Uid 1036: 0.0480 ( cpu=0.0480 ) Excluded from smearing
    Uid u0a47: 0.00450 ( cpu=0.00439 wake=0.000105 ) Excluded from smearing
    Uid u0a22: 0.00272 ( cpu=0.00271 wake=0.0000151 ) Excluded from smearing
    Uid u0a114: 0.00129 ( cpu=0.00129 ) Including smearing: 0.00202 ( proportional=0.000724 )
    Uid u0a32: 0.00103 ( cpu=0.00103 ) Including smearing: 0.00161 ( proportional=0.000578 )
    Uid 1001: 0.000580 ( cpu=0.000530 wake=0.0000500 ) Excluded from smearing
    Uid u0a127: 0.000408 ( cpu=0.000408 ) Including smearing: 0.000637 ( proportional=0.000229 )
    Uid u0a181: 0.000329 ( cpu=0.000329 ) Including smearing: 0.000514 ( proportional=0.000185 )
    Uid u0a23: 0.000280 ( cpu=0.000280 ) Including smearing: 0.000437 ( proportional=0.000157 )
    Wifi: 0.000269 ( cpu=0.000269 ) Including smearing: 0.000420 ( proportional=0.000151 )
    Uid u0a100: 0.000188 ( cpu=0.000188 ) Including smearing: 0.000293 ( proportional=0.000105 )
    Uid u0a68: 0.000141 ( cpu=0.000141 ) Including smearing: 0.000220 ( proportional=0.0000790 )
    Uid u0a25: 0.000118 ( cpu=0.000118 ) Including smearing: 0.000185 ( proportional=0.0000665 )
    Uid u0a61: 0.000102 ( cpu=0.000102 ) Including smearing: 0.000159 ( proportional=0.0000572 )
    Uid u0a70: 0.000102 ( cpu=0.000102 ) Including smearing: 0.000159 ( proportional=0.0000572 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 10s 649ms realtime (2 times)
    XO_shutdown.MPSS: 1m 9s 740ms realtime (29 times)
    XO_shutdown.ADSP: 1m 10s 676ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    Fg Service for: 1m 10s 754ms 
    Total running: 1m 10s 754ms 
    Total cpu time: u=1s 607ms s=1s 363ms 
    Total cpu time per freq: 20 0 0 10 80 120 90 90 40 50 90 50 70 40 30 60 40 30 60 20 0 680 30 50 20 20 40 0 10 20 30 0 30 30 10 60 60 70 60 100 100 90 70 60 80 40 40 60 30 20 10 10 160
    Total screen-off cpu time per freq: 20 0 0 10 80 120 90 90 40 50 90 50 70 40 30 60 40 30 60 20 0 680 30 50 20 20 40 0 10 20 30 0 30 30 10 60 60 70 60 100 100 90 70 60 80 40 40 60 30 20 10 10 160
    Proc servicemanager:
      CPU: 150ms usr + 320ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 1s 290ms usr + 830ms krn ; 0ms fg
  u0a222:
    Foreground services: 1m 10s 195ms realtime (1 times)
    Fg Service for: 1m 10s 195ms 
    Cached for: 37ms 
    Total running: 1m 10s 232ms 
    Total cpu time: u=34s 350ms s=11s 387ms 
    Total cpu time per freq: 170 40 40 10 150 470 780 580 510 700 600 550 770 660 520 630 670 580 680 680 540 4400 2020 2060 1420 1230 1420 1140 920 1220 990 840 870 830 810 980 940 1400 1120 990 1090 930 890 720 580 590 680 700 410 360 140 100 4070
    Total screen-off cpu time per freq: 170 40 40 10 150 470 780 580 510 700 600 550 770 660 520 630 670 580 680 680 540 4400 2020 2060 1420 1230 1420 1140 920 1220 990 840 870 830 810 980 940 1400 1120 990 1090 930 890 720 580 590 680 700 410 360 140 100 4070
    Proc com.ichi2.anki:
      CPU: 23s 560ms usr + 8s 590ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 5ms uptime
        Starts: 1, launches: 1

