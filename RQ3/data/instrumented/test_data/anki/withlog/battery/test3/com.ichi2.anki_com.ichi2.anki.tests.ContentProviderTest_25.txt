Daily stats:
  Current start time: 2018-10-22-07-32-32
  Next min deadline: 2018-10-23-01-00-00
  Next max deadline: 2018-10-23-03-00-00
  Current daily steps:
    Discharge total time: 15h 56m 27s 0ms  (from 22 steps)
    Discharge screen on time: 11h 14m 46s 400ms  (from 6 steps)
    Discharge screen doze time: 17h 44m 30s 300ms  (from 13 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 1m 8s 541ms (99.9%) realtime, 1m 8s 541ms (100.0%) uptime
  Time on battery screen off: 1m 8s 541ms (100.0%) realtime, 1m 8s 541ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 541ms (100.0%)
  Total run time: 1m 8s 610ms realtime, 1m 8s 610ms uptime
  Discharge: 4.33 mAh
  Screen off discharge: 4.33 mAh
  Screen doze discharge: 4.33 mAh
  Start clock time: 2018-10-22-20-34-18
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 53s 970ms (78.7%) 0x
  Idle mode light time: 53s 970ms (78.7%) 0x -- longest 54s 39ms 
  Device full idling: 14s 571ms (21.3%) 1x
  Idle mode full time: 14s 571ms (21.3%) 1x -- longest 0ms 
  Total partial wakelock time: 12s 630ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 541ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 541ms (100.0%) 
     Cellular Sleep time:  43s 336ms (63.2%)
     Cellular Idle time:   24s 637ms (35.9%)
     Cellular Rx time:     569ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 541ms (100.0%) 
     Wifi supplicant states:
       completed 1m 8s 541ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 1m 8s 541ms (100.0%) 
     WiFi Sleep time:  46s 729ms (68.2%)
     WiFi Idle time:   21s 769ms (31.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     44ms (0.1%)
     WiFi Battery drain: 0.00910mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 543ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.31, actual drain: 0
    Uid u0a222: 1.63 ( cpu=1.63 ) Including smearing: 2.92 ( proportional=1.29 )
    Uid 0: 0.507 ( cpu=0.462 wake=0.0443 ) Excluded from smearing
    Uid 1000: 0.492 ( cpu=0.209 wake=0.00947 wifi=0.00306 gps=0.269 sensor=0.00119 ) Excluded from smearing
    Cell standby: 0.209 ( radio=0.209 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid 2000: 0.147 ( cpu=0.147 ) Excluded from smearing
    Uid u0a22: 0.0496 ( cpu=0.0420 wake=0.000209 wifi=0.00605 sensor=0.00131 ) Excluded from smearing
    Uid 1036: 0.0439 ( cpu=0.0439 ) Excluded from smearing
    Uid u0a84: 0.0243 ( cpu=0.0241 wake=0.000237 ) Including smearing: 0.0436 ( proportional=0.0192 )
    Uid u0a47: 0.00613 ( cpu=0.00603 wake=0.0000944 ) Excluded from smearing
    Uid 1021: 0.00468 ( cpu=0.00468 ) Excluded from smearing
    Wifi: 0.00243 ( cpu=0.00243 ) Including smearing: 0.00435 ( proportional=0.00192 )
    Uid 1001: 0.00155 ( cpu=0.00154 wake=0.00000793 ) Excluded from smearing
    Uid u0a114: 0.00106 ( cpu=0.00106 ) Including smearing: 0.00190 ( proportional=0.000837 )
    Uid u0a35: 0.000486 ( cpu=0.000486 ) Including smearing: 0.000870 ( proportional=0.000384 )
    Uid u0a32: 0.000474 ( cpu=0.000474 ) Including smearing: 0.000849 ( proportional=0.000375 )
    Uid u0a40: 0.000277 ( cpu=0.000277 ) Including smearing: 0.000496 ( proportional=0.000219 )
    Uid u0a127: 0.000268 ( cpu=0.000268 ) Including smearing: 0.000480 ( proportional=0.000212 )
    Uid u0a23: 0.000143 ( cpu=0.000143 ) Including smearing: 0.000257 ( proportional=0.000113 )
    Uid u0a61: 0.000143 ( cpu=0.000143 ) Including smearing: 0.000257 ( proportional=0.000113 )
    Uid u0a68: 0.000143 ( cpu=0.000143 ) Including smearing: 0.000257 ( proportional=0.000113 )
    Uid u0a121: 0.000143 ( cpu=0.000143 ) Including smearing: 0.000257 ( proportional=0.000113 )
    Uid u0a8: 0.000111 ( cpu=0.000111 ) Including smearing: 0.000199 ( proportional=0.0000880 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 45s 931ms realtime (6 times)
    XO_shutdown.MPSS: 35s 0ms realtime (17 times)
    XO_shutdown.ADSP: 1m 8s 447ms realtime (8 times)
    wlan.Active: 22s 524ms realtime (6 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 0ms (0.0%)
    Wifi Scan (blamed): 2s 832ms (4.1%) 4x
    Wifi Scan (actual): 2s 832ms (4.1%) 4x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 8s 513ms (99.9%)
       WiFi Idle time:   0ms (0.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     44ms (0.1%)
    Wake lock GCoreFlp: 12ms partial (2 times) max=20 actual=26 realtime
    Wake lock NlpWakeLock: 2s 161ms partial (9 times) max=755 actual=2239 realtime
    Wake lock *alarm*: 108ms partial (10 times) max=71 actual=213 realtime
    Wake lock NetworkStats: 24ms partial (2 times) max=45 actual=50 realtime
    Wake lock GnssLocationProvider: 5ms partial (6 times) max=3 actual=10 realtime
    Wake lock AnyMotionDetector: 9s 626ms partial (1 times) max=9995 actual=9995 realtime
    Wake lock deviceidle_going_idle: 1ms partial (1 times) max=3 actual=3 realtime
    TOTAL wake: 11s 937ms blamed partial, 12s 339ms actual partial realtime
    Sensor GPS: 32s 291ms realtime (1 times)
    Sensor 1: 4s 989ms realtime (2 times)
    Sensor 17: 14s 164ms realtime (0 times)
    Fg Service for: 1m 8s 517ms 
    Total running: 1m 8s 517ms 
    Total cpu time: u=2s 760ms s=2s 853ms 
    Total cpu time per freq: 80 0 0 0 70 360 350 220 100 180 100 90 90 100 50 50 60 40 110 60 20 850 160 120 40 30 40 20 30 20 20 30 30 40 50 100 70 40 140 110 130 90 100 110 100 50 50 40 40 10 0 10 630
    Total screen-off cpu time per freq: 80 0 0 0 70 360 350 220 100 180 100 90 90 100 50 50 60 40 110 60 20 850 160 120 40 30 40 20 30 20 20 30 30 40 50 100 70 40 140 110 130 90 100 110 100 50 50 40 40 10 0 10 630
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 170ms usr + 300ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 2s 110ms usr + 1s 710ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 340ms usr + 680ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 6 times
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_ACTIVITY_DETECTION: 1 times
      Wakeup alarm *walarm*:DeviceIdleController.deep: 2 times
      Wakeup alarm *walarm*:com.android.server.ACTION_TRIGGER_IDLE: 1 times
  u0a222:
    Foreground services: 1m 7s 978ms realtime (1 times)
    Fg Service for: 1m 7s 978ms 
    Cached for: 62ms 
    Total running: 1m 8s 40ms 
    Total cpu time: u=33s 310ms s=10s 753ms 
    Total cpu time per freq: 40 10 20 30 170 460 570 490 440 560 600 560 760 500 580 680 460 570 910 530 510 4010 1820 1840 1120 1120 1630 1020 1010 1150 1230 870 830 810 710 1170 890 860 1030 1350 1190 1140 690 770 560 780 590 680 400 140 100 90 4400
    Total screen-off cpu time per freq: 40 10 20 30 170 460 570 490 440 560 600 560 760 500 580 680 460 570 910 530 510 4010 1820 1840 1120 1120 1630 1020 1010 1150 1230 870 830 810 710 1170 890 860 1030 1350 1190 1140 690 770 560 780 590 680 400 140 100 90 4400
    Proc com.ichi2.anki:
      CPU: 24s 170ms usr + 8s 620ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 5ms uptime
        Starts: 1, launches: 1

