Daily stats:
  Current start time: 2018-10-22-07-32-32
  Next min deadline: 2018-10-23-01-00-00
  Next max deadline: 2018-10-23-03-00-00
  Current daily steps:
    Discharge total time: 15h 48m 40s 500ms  (from 17 steps)
    Discharge screen on time: 11h 14m 46s 400ms  (from 6 steps)
    Discharge screen doze time: 18h 35m 31s 0ms  (from 8 steps)
  Daily from 2018-10-21-09-09-01 to 2018-10-22-07-32-32:
    Discharge total time: 2d 5h 29m 49s 0ms  (from 17 steps)
    Discharge screen on time: 15h 50m 24s 700ms  (from 5 steps)
    Discharge screen doze time: 1d 19h 32m 57s 500ms  (from 9 steps)
    Charge total time: 14h 9m 59s 700ms  (from 2 steps)
    Charge screen on time: 14h 9m 59s 700ms  (from 2 steps)
  Daily from 2018-10-20-01-07-03 to 2018-10-21-09-09-01:
    Discharge total time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Discharge screen doze time: 1d 3h 47m 57s 100ms  (from 26 steps)
    Charge total time: 3h 10m 11s 500ms  (from 20 steps)
    Charge screen on time: 4h 20m 0s 0ms  (from 10 steps)
    Charge screen doze time: 2h 4m 34s 400ms  (from 9 steps)
  Daily from 2018-10-19-03-02-11 to 2018-10-20-01-07-03:
    Discharge total time: 1d 10h 47m 39s 700ms  (from 12 steps)
    Discharge screen on time: 10h 41m 56s 300ms  (from 4 steps)
    Discharge screen doze time: 1d 22h 50m 31s 400ms  (from 8 steps)
    Charge total time: 9h 50m 14s 800ms  (from 16 steps)
    Charge screen doze time: 2h 5m 9s 200ms  (from 15 steps)
  Daily from 2018-10-18-03-43-00 to 2018-10-19-03-02-11:
    Discharge total time: 21h 37m 24s 300ms  (from 54 steps)
    Discharge screen on time: 9h 44m 34s 900ms  (from 11 steps)
    Discharge screen doze time: 1d 0h 33m 31s 500ms  (from 40 steps)
    Charge total time: 15h 26m 30s 200ms  (from 74 steps)
    Charge screen on time: 1d 15h 9m 9s 700ms  (from 11 steps)
    Charge screen doze time: 11h 34m 12s 200ms  (from 60 steps)
  Daily from 2018-10-17-03-01-13 to 2018-10-18-03-43-00:
    Discharge total time: 18h 54m 57s 200ms  (from 73 steps)
    Discharge screen on time: 10h 45m 13s 200ms  (from 15 steps)
    Discharge screen doze time: 22h 11m 42s 100ms  (from 50 steps)
    Charge total time: 2d 21h 58m 11s 200ms  (from 16 steps)
    Charge screen on time: 39d 23h 30m 18s 400ms  (from 1 steps)
    Charge screen doze time: 2h 4m 15s 700ms  (from 14 steps)
  Daily from 2018-10-16-03-08-12 to 2018-10-17-03-01-13:
    Discharge total time: 17h 45m 51s 600ms  (from 61 steps)
    Discharge screen on time: 10h 14m 13s 400ms  (from 16 steps)
    Discharge screen doze time: 20h 33m 48s 500ms  (from 40 steps)
    Charge total time: 19h 24m 32s 900ms  (from 64 steps)
    Charge screen on time: 1d 5h 16m 53s 900ms  (from 8 steps)
    Charge screen doze time: 1h 44m 40s 600ms  (from 51 steps)
  Daily from 2018-10-15-05-10-29 to 2018-10-16-03-08-12:
    Discharge total time: 1d 10h 7m 33s 600ms  (from 24 steps)
    Discharge screen off time: 14d 13h 59m 41s 300ms  (from 1 steps)
    Discharge screen on time: 9h 6m 58s 400ms  (from 4 steps)
    Discharge screen doze time: 23h 20m 52s 700ms  (from 18 steps)
  Daily from 2018-10-14-03-00-39 to 2018-10-15-05-10-29:
    Discharge total time: 21h 39m 49s 700ms  (from 35 steps)
    Discharge screen on time: 15h 55m 32s 600ms  (from 9 steps)
    Discharge screen doze time: 21h 18m 34s 900ms  (from 22 steps)
    Charge total time: 2d 1h 37m 23s 100ms  (from 33 steps)
    Charge screen on time: 1d 6h 32m 47s 800ms  (from 7 steps)
    Charge screen doze time: 3h 34m 45s 700ms  (from 25 steps)
  Daily from 2018-10-13-03-08-35 to 2018-10-14-03-00-39:
    Discharge total time: 1d 17h 37m 38s 500ms  (from 39 steps)
    Discharge screen off time: 20d 1h 53m 24s 0ms  (from 2 steps)
    Discharge screen off device idle time: 34d 0h 41m 8s 500ms  (from 1 steps)
    Discharge screen on time: 13h 28m 3s 500ms  (from 6 steps)
    Discharge screen doze time: 17h 42m 19s 300ms  (from 29 steps)
    Charge total time: 10h 12m 30s 0ms  (from 24 steps)
    Charge screen on time: 9h 2m 10s 800ms  (from 3 steps)
    Charge screen doze time: 10h 32m 50s 200ms  (from 20 steps)
  Daily from 2018-10-12-07-33-49 to 2018-10-13-03-08-35:

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3742 mAh
  Max learned battery capacity: 3742 mAh
  Time on battery: 59s 184ms (99.9%) realtime, 59s 184ms (100.0%) uptime
  Time on battery screen off: 59s 184ms (100.0%) realtime, 59s 184ms (100.0%) uptime
  Time on battery screen doze: 59s 184ms (100.0%)
  Total run time: 59s 261ms realtime, 59s 260ms uptime
  Discharge: 4.10 mAh
  Screen off discharge: 4.10 mAh
  Screen doze discharge: 4.10 mAh
  Start clock time: 2018-10-22-19-49-44
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 59s 184ms (100.0%) 0x
  Idle mode light time: 59s 184ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 196ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 59s 184ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  59s 184ms (100.0%) 
     Cellular Sleep time:  58s 439ms (98.7%)
     Cellular Idle time:   191ms (0.3%)
     Cellular Rx time:     555ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 59s 183ms (100.0%) 
     Wifi supplicant states:
       completed 59s 183ms (100.0%) 
     Wifi Rx signal strength (RSSI):
         good (-66.25dBm to -55dBm): 59s 183ms (100.0%) 
     WiFi Sleep time:  59s 185ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  59s 185ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 1.93, actual drain: 0
    Uid u0a222: 1.01 ( cpu=1.01 ) Including smearing: 1.65 ( proportional=0.647 )
    Uid 0: 0.455 ( cpu=0.408 wake=0.0468 ) Excluded from smearing
    Cell standby: 0.181 ( radio=0.181 ) Excluded from smearing
    Idle: 0.161 Excluded from smearing
    Uid 2000: 0.0644 ( cpu=0.0644 ) Excluded from smearing
    Uid 1000: 0.0319 ( cpu=0.0319 wake=0.0000103 ) Excluded from smearing
    Uid 1036: 0.0139 ( cpu=0.0139 ) Excluded from smearing
    Uid u0a22: 0.00329 ( cpu=0.00325 wake=0.0000397 ) Excluded from smearing
    Uid u0a47: 0.00320 ( cpu=0.00310 wake=0.0000952 ) Excluded from smearing
    Uid u0a35: 0.00123 ( cpu=0.00123 ) Including smearing: 0.00201 ( proportional=0.000786 )
    Uid u0a114: 0.00114 ( cpu=0.00114 ) Including smearing: 0.00187 ( proportional=0.000732 )
    Uid u0a32: 0.000475 ( cpu=0.000475 ) Including smearing: 0.000780 ( proportional=0.000305 )
    Uid 1001: 0.000407 ( cpu=0.000397 wake=0.00000952 ) Excluded from smearing
    Wifi: 0.000314 ( cpu=0.000314 ) Including smearing: 0.000515 ( proportional=0.000201 )
    Uid u0a40: 0.000199 ( cpu=0.000199 ) Including smearing: 0.000327 ( proportional=0.000128 )
    Uid u0a25: 0.000147 ( cpu=0.000147 ) Including smearing: 0.000241 ( proportional=0.0000942 )
    Uid u0a68: 0.000147 ( cpu=0.000147 ) Including smearing: 0.000241 ( proportional=0.0000942 )
    Uid u0a127: 0.000120 ( cpu=0.000120 ) Including smearing: 0.000196 ( proportional=0.0000767 )
    Uid u0a8: 0.000110 ( cpu=0.000110 ) Including smearing: 0.000180 ( proportional=0.0000704 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 45ms realtime (2 times)
    XO_shutdown.MPSS: 58s 403ms realtime (25 times)
    XO_shutdown.ADSP: 59s 65ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 13ms partial (1 times) max=20 actual=20 realtime
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    TOTAL wake: 14ms blamed partial, 21ms actual partial realtime
    Fg Service for: 59s 162ms 
    Total running: 59s 162ms 
    Total cpu time: u=514ms s=470ms 
    Total cpu time per freq: 10 0 0 0 20 60 120 40 10 40 40 10 50 70 20 50 20 30 10 20 0 220 40 30 10 40 10 0 0 10 0 0 0 0 0 10 0 0 10 20 0 20 0 0 0 0 0 10 0 20 0 0 230
    Total screen-off cpu time per freq: 10 0 0 0 20 60 120 40 10 40 40 10 50 70 20 50 20 30 10 20 0 220 40 30 10 40 10 0 0 10 0 0 0 0 0 10 0 0 10 20 0 20 0 0 0 0 0 10 0 20 0 0 230
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 400ms usr + 290ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a222:
    Foreground services: 58s 342ms realtime (1 times)
    Fg Service for: 58s 342ms 
    Cached for: 57ms 
    Total running: 58s 399ms 
    Total cpu time: u=24s 917ms s=7s 754ms 
    Total cpu time per freq: 350 60 70 50 380 970 880 870 610 740 480 590 530 580 710 890 490 290 280 380 230 1820 4460 2060 920 1940 2200 1880 1190 740 950 530 460 610 220 440 690 350 540 360 290 80 90 40 80 60 10 40 30 0 0 0 270
    Total screen-off cpu time per freq: 350 60 70 50 380 970 880 870 610 740 480 590 530 580 710 890 490 290 280 380 230 1820 4460 2060 920 1940 2200 1880 1190 740 950 530 460 610 220 440 690 350 540 360 290 80 90 40 80 60 10 40 30 0 0 0 270
    Proc com.ichi2.anki:
      CPU: 12s 900ms usr + 4s 190ms krn ; 0ms fg
      1 starts
    Apk com.ichi2.anki:
      Service com.ichi2.anki.services.BootService:
        Created for: 6ms uptime
        Starts: 1, launches: 1

