Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 8h 54m 5s 400ms  (from 59 steps)
    Discharge screen doze time: 8h 49m 24s 100ms  (from 58 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 58s 174ms (99.9%) realtime, 58s 174ms (100.0%) uptime
  Time on battery screen off: 58s 174ms (100.0%) realtime, 58s 174ms (100.0%) uptime
  Time on battery screen doze: 58s 174ms (100.0%)
  Total run time: 58s 204ms realtime, 58s 204ms uptime
  Discharge: 4.52 mAh
  Screen off discharge: 4.52 mAh
  Screen doze discharge: 4.52 mAh
  Start clock time: 2018-07-31-11-14-27
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 58s 174ms (100.0%) 0x
  Idle mode full time: 58s 174ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 229ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 58s 174ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  58s 174ms (100.0%) 
     Cellular Sleep time:  57s 311ms (98.5%)
     Cellular Idle time:   271ms (0.5%)
     Cellular Rx time:     593ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 58s 174ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  58s 175ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  58s 175ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.52, actual drain: 0
    Uid u0a140: 1.35 ( cpu=1.35 wake=0.0000190 ) Including smearing: 2.26 ( proportional=0.905 )
    Uid 0: 0.457 ( cpu=0.411 wake=0.0460 ) Excluded from smearing
    Cell standby: 0.178 ( radio=0.178 ) Excluded from smearing
    Idle: 0.159 Excluded from smearing
    Uid 1036: 0.153 ( cpu=0.153 ) Excluded from smearing
    Uid 2000: 0.119 ( cpu=0.119 ) Excluded from smearing
    Uid 1000: 0.0663 ( cpu=0.0663 ) Excluded from smearing
    Uid u0a47: 0.0383 ( cpu=0.0382 wake=0.000125 ) Excluded from smearing
    Uid u0a35: 0.00136 ( cpu=0.00136 ) Including smearing: 0.00227 ( proportional=0.000913 )
    Uid u0a22: 0.000931 ( cpu=0.000905 wake=0.0000254 ) Excluded from smearing
    Uid 1001: 0.000449 ( cpu=0.000438 wake=0.0000111 ) Excluded from smearing
    Wifi: 0.000265 ( cpu=0.000265 ) Including smearing: 0.000443 ( proportional=0.000178 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 58s 56ms realtime (2 times)
    XO_shutdown.MPSS: 57s 371ms realtime (25 times)
    XO_shutdown.ADSP: 58s 73ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Fg Service for: 58s 167ms 
    Total running: 58s 167ms 
    Total cpu time: u=890ms s=1s 244ms 
    Total cpu time per freq: 970 20 70 40 20 40 10 0 10 20 50 20 40 30 20 10 20 50 40 0 20 140 10 30 10 40 0 10 0 0 0 0 0 10 0 0 0 0 0 0 0 10 10 0 0 0 0 0 0 0 0 0 60
    Total screen-off cpu time per freq: 970 20 70 40 20 40 10 0 10 20 50 20 40 30 20 10 20 50 40 0 20 140 10 30 10 40 0 10 0 0 0 0 0 10 0 0 0 0 0 0 0 10 10 0 0 0 0 0 0 0 0 0 60
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 220ms usr + 240ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 13ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 20ms partial (2 times) max=14 actual=24 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 24ms blamed partial, 28ms actual partial realtime
    Foreground services: 57s 404ms realtime (1 times)
    Fg Service for: 57s 404ms 
    Cached for: 128ms 
    Total running: 57s 532ms 
    Total cpu time: u=34s 586ms s=8s 574ms 
    Total cpu time per freq: 2180 290 220 370 140 90 250 270 380 450 390 210 690 580 580 740 700 770 1070 510 380 890 2510 2700 4990 3950 3050 2390 1720 1400 1000 1030 1220 1080 530 520 570 360 330 90 240 160 130 70 90 80 210 80 40 0 0 30 1610
    Total screen-off cpu time per freq: 2180 290 220 370 140 90 250 270 380 450 390 210 690 580 580 740 700 770 1070 510 380 890 2510 2700 4990 3950 3050 2390 1720 1400 1000 1030 1220 1080 530 520 570 360 330 90 240 160 130 70 90 80 210 80 40 0 0 30 1610
    Proc cgeo.geocaching:
      CPU: 29s 760ms usr + 7s 800ms krn ; 0ms fg
      1 starts

