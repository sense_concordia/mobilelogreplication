Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 8h 54m 5s 400ms  (from 59 steps)
    Discharge screen doze time: 8h 49m 24s 100ms  (from 58 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 815ms (99.9%) realtime, 1m 0s 815ms (100.0%) uptime
  Time on battery screen off: 1m 0s 815ms (100.0%) realtime, 1m 0s 815ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 815ms (100.0%)
  Total run time: 1m 0s 849ms realtime, 1m 0s 848ms uptime
  Discharge: 4.04 mAh
  Screen off discharge: 4.04 mAh
  Screen doze discharge: 4.04 mAh
  Start clock time: 2018-07-31-11-19-04
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 0s 815ms (100.0%) 0x
  Idle mode full time: 1m 0s 815ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 183ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 815ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 815ms (100.0%) 
     Cellular Sleep time:  59s 990ms (98.6%)
     Cellular Idle time:   202ms (0.3%)
     Cellular Rx time:     623ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 815ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 0s 816ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 816ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.54, actual drain: 0
    Uid u0a140: 1.41 ( cpu=1.41 wake=0.0000111 ) Including smearing: 2.27 ( proportional=0.860 )
    Uid 0: 0.476 ( cpu=0.428 wake=0.0481 ) Excluded from smearing
    Cell standby: 0.186 ( radio=0.186 ) Excluded from smearing
    Idle: 0.166 Excluded from smearing
    Uid 2000: 0.117 ( cpu=0.117 ) Excluded from smearing
    Uid 1000: 0.0704 ( cpu=0.0704 wake=0.00000079 ) Excluded from smearing
    Uid 1036: 0.0698 ( cpu=0.0698 ) Excluded from smearing
    Uid u0a47: 0.0423 ( cpu=0.0422 wake=0.000117 ) Excluded from smearing
    Uid u0a35: 0.00112 ( cpu=0.00112 ) Including smearing: 0.00181 ( proportional=0.000686 )
    Uid u0a22: 0.000447 ( cpu=0.000436 wake=0.0000103 ) Excluded from smearing
    Wifi: 0.000398 ( cpu=0.000398 ) Including smearing: 0.000641 ( proportional=0.000243 )
    Uid 1001: 0.000255 ( cpu=0.000250 wake=0.00000555 ) Excluded from smearing
    Uid u0a127: 0.000100 ( cpu=0.000100 ) Including smearing: 0.000161 ( proportional=0.0000610 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 0s 712ms realtime (2 times)
    XO_shutdown.MPSS: 59s 990ms realtime (26 times)
    XO_shutdown.ADSP: 1m 0s 735ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Fg Service for: 1m 0s 807ms 
    Total running: 1m 0s 807ms 
    Total cpu time: u=957ms s=1s 363ms 
    Total cpu time per freq: 1470 40 40 40 0 30 30 20 20 20 10 30 40 60 20 30 0 10 30 20 10 70 10 0 0 0 0 0 0 0 0 10 0 10 0 0 0 30 10 0 0 0 0 0 0 0 0 0 0 0 0 0 60
    Total screen-off cpu time per freq: 1470 40 40 40 0 30 30 20 20 20 10 30 40 60 20 30 0 10 30 20 10 70 10 0 0 0 0 0 0 0 0 10 0 10 0 0 0 30 10 0 0 0 0 0 0 0 0 0 0 0 0 0 60
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 160ms usr + 230ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 4ms partial (2 times) max=2 realtime
    Wake lock fiid-sync: 11ms partial (1 times) max=11 realtime
    TOTAL wake: 15ms blamed partial, 15ms actual partial realtime
    Foreground services: 1m 0s 230ms realtime (1 times)
    Fg Service for: 1m 0s 230ms 
    Cached for: 41ms 
    Total running: 1m 0s 271ms 
    Total cpu time: u=36s 996ms s=8s 850ms 
    Total cpu time per freq: 2930 480 250 390 330 140 250 280 540 430 410 360 570 850 550 680 800 810 1130 660 450 770 3540 2990 5670 3970 2770 2290 1960 1060 1440 670 1090 790 380 460 320 410 250 150 160 150 170 110 100 60 80 170 20 50 0 20 1620
    Total screen-off cpu time per freq: 2930 480 250 390 330 140 250 280 540 430 410 360 570 850 550 680 800 810 1130 660 450 770 3540 2990 5670 3970 2770 2290 1960 1060 1440 670 1090 790 380 460 320 410 250 150 160 150 170 110 100 60 80 170 20 50 0 20 1620
    Proc cgeo.geocaching:
      CPU: 32s 570ms usr + 8s 230ms krn ; 0ms fg
      1 starts

