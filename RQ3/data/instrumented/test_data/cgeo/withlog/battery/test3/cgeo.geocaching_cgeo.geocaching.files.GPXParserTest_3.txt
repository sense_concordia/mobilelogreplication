Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 8h 30m 27s 900ms  (from 57 steps)
    Discharge screen doze time: 8h 25m 11s 300ms  (from 56 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 505ms (100.0%) realtime, 1m 0s 504ms (100.0%) uptime
  Time on battery screen off: 1m 0s 505ms (100.0%) realtime, 1m 0s 504ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 505ms (100.0%)
  Total run time: 1m 0s 530ms realtime, 1m 0s 529ms uptime
  Discharge: 3.98 mAh
  Screen off discharge: 3.98 mAh
  Screen doze discharge: 3.98 mAh
  Start clock time: 2018-07-31-11-00-26
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 0s 505ms (100.0%) 0x
  Idle mode full time: 1m 0s 505ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 182ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 505ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 505ms (100.0%) 
     Cellular Sleep time:  59s 758ms (98.8%)
     Cellular Idle time:   189ms (0.3%)
     Cellular Rx time:     561ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 505ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 0s 510ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 510ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.57, actual drain: 0
    Uid u0a140: 1.39 ( cpu=1.39 wake=0.00000872 ) Including smearing: 2.30 ( proportional=0.911 )
    Uid 0: 0.481 ( cpu=0.433 wake=0.0478 ) Excluded from smearing
    Cell standby: 0.185 ( radio=0.185 ) Excluded from smearing
    Idle: 0.165 Excluded from smearing
    Uid 1036: 0.123 ( cpu=0.123 ) Excluded from smearing
    Uid 2000: 0.116 ( cpu=0.116 ) Excluded from smearing
    Uid 1000: 0.0729 ( cpu=0.0729 wake=0.00000079 ) Excluded from smearing
    Uid u0a47: 0.0411 ( cpu=0.0410 wake=0.000106 ) Excluded from smearing
    Uid u0a22: 0.000875 ( cpu=0.000860 wake=0.0000151 ) Excluded from smearing
    Wifi: 0.000409 ( cpu=0.000409 ) Including smearing: 0.000678 ( proportional=0.000269 )
    Uid 1001: 0.000193 ( cpu=0.000180 wake=0.0000135 ) Excluded from smearing
    Uid u0a40: 0.000120 ( cpu=0.000120 ) Including smearing: 0.000198 ( proportional=0.0000785 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 0s 397ms realtime (2 times)
    XO_shutdown.MPSS: 59s 733ms realtime (26 times)
    XO_shutdown.ADSP: 1m 0s 420ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Fg Service for: 1m 0s 500ms 
    Total running: 1m 0s 500ms 
    Total cpu time: u=1s 17ms s=1s 323ms 
    Total cpu time per freq: 1190 60 40 50 30 0 10 50 20 30 0 10 20 60 30 20 20 30 60 10 0 120 10 0 10 0 0 0 0 0 20 0 0 0 0 0 30 20 10 20 0 0 0 0 10 0 0 0 0 0 0 0 30
    Total screen-off cpu time per freq: 1190 60 40 50 30 0 10 50 20 30 0 10 20 60 30 20 20 30 60 10 0 120 10 0 10 0 0 0 0 0 20 0 0 0 0 0 30 20 10 20 0 0 0 0 10 0 0 0 0 0 0 0 30
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 230ms usr + 210ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 3ms partial (2 times) max=2 actual=4 realtime
    Wake lock fiid-sync: 8ms partial (1 times) max=8 realtime
    TOTAL wake: 11ms blamed partial, 12ms actual partial realtime
    Foreground services: 59s 876ms realtime (1 times)
    Fg Service for: 59s 876ms 
    Cached for: 38ms 
    Total running: 59s 914ms 
    Total cpu time: u=36s 63ms s=8s 626ms 
    Total cpu time per freq: 2780 400 140 310 220 120 170 370 360 360 520 280 360 570 760 720 670 640 1060 450 460 760 3530 2970 5510 4120 3010 2580 1720 1140 1010 820 1030 840 400 460 480 370 140 300 170 100 190 110 160 120 50 100 90 30 40 20 1760
    Total screen-off cpu time per freq: 2780 400 140 310 220 120 170 370 360 360 520 280 360 570 760 720 670 640 1060 450 460 760 3530 2970 5510 4120 3010 2580 1720 1140 1010 820 1030 840 400 460 480 370 140 300 170 100 190 110 160 120 50 100 90 30 40 20 1760
    Proc cgeo.geocaching:
      CPU: 20s 170ms usr + 4s 780ms krn ; 0ms fg
      1 starts

