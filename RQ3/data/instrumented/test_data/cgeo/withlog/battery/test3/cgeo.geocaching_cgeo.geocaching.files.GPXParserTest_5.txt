Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 8h 42m 9s 500ms  (from 58 steps)
    Discharge screen doze time: 8h 37m 10s 700ms  (from 57 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 1s 889ms (100.0%) realtime, 1m 1s 889ms (100.0%) uptime
  Time on battery screen off: 1m 1s 889ms (100.0%) realtime, 1m 1s 889ms (100.0%) uptime
  Time on battery screen doze: 1m 1s 889ms (100.0%)
  Total run time: 1m 1s 915ms realtime, 1m 1s 915ms uptime
  Discharge: 4.07 mAh
  Screen off discharge: 4.07 mAh
  Screen doze discharge: 4.07 mAh
  Start clock time: 2018-07-31-11-02-48
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 1s 889ms (100.0%) 0x
  Idle mode full time: 1m 1s 889ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 190ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 1s 889ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 1s 889ms (100.0%) 
     Cellular Sleep time:  1m 0s 774ms (98.2%)
     Cellular Idle time:   258ms (0.4%)
     Cellular Rx time:     860ms (1.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 1s 889ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 1s 893ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 1s 894ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.58, actual drain: 0
    Uid u0a140: 1.40 ( cpu=1.40 wake=0.00000714 ) Including smearing: 2.31 ( proportional=0.905 )
    Uid 0: 0.511 ( cpu=0.462 wake=0.0489 ) Excluded from smearing
    Cell standby: 0.189 ( radio=0.189 ) Excluded from smearing
    Idle: 0.169 Excluded from smearing
    Uid 2000: 0.123 ( cpu=0.123 ) Excluded from smearing
    Uid 1000: 0.0760 ( cpu=0.0760 wake=0.00000159 ) Excluded from smearing
    Uid 1036: 0.0707 ( cpu=0.0707 ) Excluded from smearing
    Uid u0a47: 0.0437 ( cpu=0.0435 wake=0.000122 ) Excluded from smearing
    Uid u0a22: 0.00110 ( cpu=0.00108 wake=0.0000119 ) Excluded from smearing
    Wifi: 0.000296 ( cpu=0.000296 ) Including smearing: 0.000487 ( proportional=0.000191 )
    Uid 1001: 0.000214 ( cpu=0.000206 wake=0.00000793 ) Excluded from smearing
    Uid u0a40: 0.0000942 ( cpu=0.0000942 ) Including smearing: 0.000155 ( proportional=0.0000609 )
    Uid u0a127: 0.0000942 ( cpu=0.0000942 ) Including smearing: 0.000155 ( proportional=0.0000609 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 1s 772ms realtime (2 times)
    XO_shutdown.MPSS: 1m 0s 783ms realtime (27 times)
    XO_shutdown.ADSP: 1m 1s 797ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    Fg Service for: 1m 1s 883ms 
    Total running: 1m 1s 883ms 
    Total cpu time: u=1s 134ms s=1s 360ms 
    Total cpu time per freq: 1380 70 30 40 60 20 30 10 40 30 10 30 40 10 20 40 10 10 50 30 20 70 10 10 0 0 0 10 0 0 0 0 0 10 0 0 20 10 0 0 0 20 0 0 0 0 0 10 0 0 0 0 80
    Total screen-off cpu time per freq: 1380 70 30 40 60 20 30 10 40 30 10 30 40 10 20 40 10 10 50 30 20 70 10 10 0 0 0 10 0 0 0 0 0 10 0 0 20 10 0 0 0 20 0 0 0 0 0 10 0 0 0 0 80
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 170ms usr + 230ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 4ms partial (2 times) max=2 realtime
    Wake lock fiid-sync: 5ms partial (1 times) max=5 realtime
    TOTAL wake: 9ms blamed partial, 9ms actual partial realtime
    Foreground services: 1m 1s 337ms realtime (1 times)
    Fg Service for: 1m 1s 337ms 
    Cached for: 42ms 
    Total running: 1m 1s 379ms 
    Total cpu time: u=36s 446ms s=9s 13ms 
    Total cpu time per freq: 2420 280 320 440 160 100 210 200 370 460 280 460 430 620 620 820 600 800 1210 630 530 780 4140 3370 5710 3860 3100 2190 1540 1050 1150 890 1050 890 370 460 470 390 200 190 180 260 110 130 60 180 130 90 110 10 0 0 1600
    Total screen-off cpu time per freq: 2420 280 320 440 160 100 210 200 370 460 280 460 430 620 620 820 600 800 1210 630 530 780 4140 3370 5710 3860 3100 2190 1540 1050 1150 890 1050 890 370 460 470 390 200 190 180 260 110 130 60 180 130 90 110 10 0 0 1600
    Proc cgeo.geocaching:
      CPU: 9s 260ms usr + 1s 960ms krn ; 0ms fg
      1 starts

