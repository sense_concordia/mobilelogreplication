Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 6h 59m 4s 100ms  (from 44 steps)
    Discharge screen doze time: 7h 2m 43s 700ms  (from 43 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 6s 895ms (100.0%) realtime, 1m 6s 895ms (100.0%) uptime
  Time on battery screen off: 1m 6s 895ms (100.0%) realtime, 1m 6s 895ms (100.0%) uptime
  Time on battery screen doze: 1m 6s 895ms (100.0%)
  Total run time: 1m 6s 916ms realtime, 1m 6s 916ms uptime
  Discharge: 14.0 mAh
  Screen off discharge: 14.0 mAh
  Screen doze discharge: 14.0 mAh
  Start clock time: 2018-08-01-12-24-22
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 6s 895ms (100.0%) 0x
  Idle mode full time: 1m 6s 895ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 180ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 6s 895ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 6s 895ms (100.0%) 
     Cellular Sleep time:  1m 4s 794ms (96.9%)
     Cellular Idle time:   1s 443ms (2.2%)
     Cellular Rx time:     658ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 6s 895ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 6s 896ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 6s 896ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.7, actual drain: 0
    Uid u0a142: 5.22 ( cpu=5.22 wake=0.00000714 ) Including smearing: 10.3 ( proportional=5.11 )
    Uid 0: 4.18 ( cpu=4.13 wake=0.0529 ) Excluded from smearing
    Uid 1036: 0.343 ( cpu=0.343 ) Excluded from smearing
    Uid 2000: 0.327 ( cpu=0.327 ) Excluded from smearing
    Cell standby: 0.204 ( radio=0.204 ) Excluded from smearing
    Idle: 0.182 Excluded from smearing
    Uid 1000: 0.109 ( cpu=0.109 wake=0.00000079 ) Excluded from smearing
    Uid 1001: 0.0760 ( cpu=0.0760 wake=0.00000317 ) Excluded from smearing
    Uid u0a47: 0.0558 ( cpu=0.0539 wake=0.000121 sensor=0.00186 ) Excluded from smearing
    Uid 1021: 0.00266 ( cpu=0.00266 ) Excluded from smearing
    Uid u0a127: 0.00224 ( cpu=0.00224 ) Including smearing: 0.00443 ( proportional=0.00219 )
    Uid u0a40: 0.00181 ( cpu=0.00181 ) Including smearing: 0.00359 ( proportional=0.00178 )
    Uid u0a22: 0.00140 ( cpu=0.00139 wake=0.0000103 ) Excluded from smearing
    Wifi: 0.00110 ( cpu=0.00110 ) Including smearing: 0.00218 ( proportional=0.00108 )
    Uid u0a139: 0.000420 ( cpu=0.000420 ) Including smearing: 0.000832 ( proportional=0.000412 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 6s 810ms realtime (2 times)
    XO_shutdown.MPSS: 1m 4s 539ms realtime (113 times)
    XO_shutdown.ADSP: 1m 6s 808ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Fg Service for: 1m 6s 888ms 
    Total running: 1m 6s 888ms 
    Total cpu time: u=807ms s=1s 70ms 
    Total cpu time per freq: 720 20 80 200 40 110 150 30 0 10 40 50 20 0 0 0 0 10 20 0 0 100 20 0 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 360
    Total screen-off cpu time per freq: 720 20 80 200 40 110 150 30 0 10 40 50 20 0 0 0 0 10 20 0 0 100 20 0 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 360
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 110ms krn ; 0ms fg
    Proc system:
      CPU: 490ms usr + 690ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 30ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 4ms partial (4 times) max=2 actual=5, 3ms background partial (2 times) max=2 realtime
    Wake lock fiid-sync: 6ms partial (1 times) max=6 realtime
    TOTAL wake: 10ms blamed partial, 11ms actual partial, 3ms actual background partial realtime
    Foreground services: 1m 6s 371ms realtime (1 times)
    Fg Service for: 1m 6s 371ms 
    Cached for: 38ms 
    Total running: 1m 6s 409ms 
    Total cpu time: u=1m 20s 437ms s=5s 594ms 
    Total cpu time per freq: 9900 790 900 3860 470 390 550 100 90 100 70 30 50 20 10 0 10 10 40 0 10 30 600 10 50 10 40 40 20 50 50 20 40 40 60 60 70 30 40 30 10 90 20 40 0 70 60 60 30 40 30 40 66700
    Total screen-off cpu time per freq: 9900 790 900 3860 470 390 550 100 90 100 70 30 50 20 10 0 10 10 40 0 10 30 600 10 50 10 40 40 20 50 50 20 40 40 60 60 70 30 40 30 10 90 20 40 0 70 60 60 30 40 30 40 66700
    Proc cgeo.geocaching:
      CPU: 1m 12s 480ms usr + 4s 110ms krn ; 0ms fg
      1 starts

