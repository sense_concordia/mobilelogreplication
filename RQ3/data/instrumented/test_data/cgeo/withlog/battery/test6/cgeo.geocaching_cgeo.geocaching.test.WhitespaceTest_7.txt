Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 7h 12m 44s 100ms  (from 39 steps)
    Discharge screen doze time: 7h 17m 14s 200ms  (from 38 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 7s 52ms (100.0%) realtime, 1m 7s 52ms (100.0%) uptime
  Time on battery screen off: 1m 7s 52ms (100.0%) realtime, 1m 7s 52ms (100.0%) uptime
  Time on battery screen doze: 1m 7s 52ms (100.0%)
  Total run time: 1m 7s 70ms realtime, 1m 7s 70ms uptime
  Discharge: 14.9 mAh
  Screen off discharge: 14.9 mAh
  Screen doze discharge: 14.9 mAh
  Start clock time: 2018-08-01-12-07-42
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 7s 52ms (100.0%) 0x
  Idle mode light time: 1m 7s 52ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 10s 273ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 7s 52ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 7s 52ms (100.0%) 
     Cellular Sleep time:  41s 154ms (61.4%)
     Cellular Idle time:   25s 335ms (37.8%)
     Cellular Rx time:     563ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 7s 52ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 5s 991ms (98.4%)
     WiFi Idle time:   1s 44ms (1.6%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     17ms (0.0%)
     WiFi Battery drain: 0.00147mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 7s 53ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 11.5, actual drain: 0-35.2
    Uid u0a142: 5.87 ( cpu=5.39 wake=0.0000238 wifi=0.00104 gps=0.485 ) Including smearing: 11.2 ( proportional=5.31 )
    Uid 0: 4.21 ( cpu=4.16 wake=0.0450 ) Excluded from smearing
    Uid 1036: 0.356 ( cpu=0.356 ) Excluded from smearing
    Uid 2000: 0.309 ( cpu=0.309 ) Excluded from smearing
    Uid 1000: 0.244 ( cpu=0.164 wake=0.00797 wifi=0.000359 gps=0.0689 sensor=0.00320 ) Excluded from smearing
    Cell standby: 0.205 ( radio=0.205 ) Excluded from smearing
    Idle: 0.183 Excluded from smearing
    Uid 1001: 0.0674 ( cpu=0.0674 wake=0.0000286 ) Excluded from smearing
    Uid u0a47: 0.0649 ( cpu=0.0629 wake=0.000110 sensor=0.00186 ) Excluded from smearing
    Uid u0a22: 0.0154 ( cpu=0.0154 wake=0.0000174 ) Excluded from smearing
    Uid 1021: 0.0114 ( cpu=0.0114 ) Excluded from smearing
    Uid u0a127: 0.00310 ( cpu=0.00310 ) Including smearing: 0.00589 ( proportional=0.00280 )
    Wifi: 0.00223 ( cpu=0.00216 wifi=0.0000694 ) Including smearing: 0.00424 ( proportional=0.00201 )
    Uid u0a40: 0.00219 ( cpu=0.00219 ) Including smearing: 0.00416 ( proportional=0.00198 )
    Uid u0a139: 0.00104 ( cpu=0.00104 ) Including smearing: 0.00197 ( proportional=0.000936 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 5s 730ms realtime (8 times)
    XO_shutdown.MPSS: 455ms realtime (2 times)
    XO_shutdown.ADSP: 1m 6s 955ms realtime (4 times)
    wlan.Active: 1s 252ms realtime (8 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 19ms (0.0%)
    Wifi Scan (blamed): 283ms (0.4%) 1x
    Wifi Scan (actual): 566ms (0.8%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 6s 760ms (99.6%)
       WiFi Idle time:   291ms (0.4%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     4ms (0.0%)
    Wake lock NlpWakeLock: 17ms partial (4 times) max=14 actual=28 realtime
    Wake lock *alarm*: 22ms partial (2 times) max=19 actual=28 realtime
    Wake lock GnssLocationProvider: 5ms partial (5 times) max=1 realtime
    Wake lock AnyMotionDetector: 10s 1ms partial (1 times) max=10011 actual=10011 realtime
    TOTAL wake: 10s 45ms blamed partial, 10s 68ms actual partial realtime
    Sensor GPS: 8s 268ms blamed realtime, 16s 421ms realtime (1 times)
    Sensor 1: 4s 968ms realtime (2 times)
    Sensor 17: 43s 100ms realtime (0 times)
    Fg Service for: 1m 7s 48ms 
    Total running: 1m 7s 48ms 
    Total cpu time: u=1s 193ms s=1s 590ms 
    Total cpu time per freq: 850 100 80 260 70 120 230 70 30 30 10 70 70 30 0 10 20 0 10 20 20 90 10 0 0 0 0 0 10 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 430
    Total screen-off cpu time per freq: 850 100 80 260 70 120 230 70 30 30 10 70 70 30 0 10 20 0 10 20 20 90 10 0 0 0 0 0 10 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 430
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 40ms usr + 120ms krn ; 0ms fg
    Proc system:
      CPU: 860ms usr + 1s 40ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 30ms usr + 80ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.deep: 1 times
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 32ms (0.0%)
    Wifi Scan (blamed): 845ms (1.3%) 2x
    Wifi Scan (actual): 1s 128ms (1.7%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 6s 291ms (98.9%)
       WiFi Idle time:   752ms (1.1%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     12ms (0.0%)
    Wake lock NlpWakeLock: 25ms partial (5 times) max=15 actual=46 realtime
    Wake lock *alarm*: 1ms partial (1 times) max=3 actual=3 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 30ms blamed partial, 51ms actual partial realtime
    Sensor GPS: 58s 201ms blamed realtime, 1m 6s 354ms realtime (1 times), 2ms background (0 times)
    Foreground services: 1m 6s 553ms realtime (1 times)
    Fg Service for: 1m 6s 553ms 
    Cached for: 46ms 
    Total running: 1m 6s 599ms 
    Total cpu time: u=1m 22s 930ms s=5s 540ms 
    Total cpu time per freq: 11850 1180 1120 4780 660 510 600 160 120 70 50 30 30 30 0 0 10 0 10 0 0 80 560 20 30 30 20 20 10 30 20 10 10 20 20 20 10 20 30 30 10 20 20 10 10 10 30 70 20 70 30 10 66050
    Total screen-off cpu time per freq: 11850 1180 1120 4780 660 510 600 160 120 70 50 30 30 30 0 0 10 0 10 0 0 80 560 20 30 30 20 20 10 30 20 10 10 20 20 20 10 20 30 30 10 20 20 10 10 10 30 70 20 70 30 10 66050
    Proc cgeo.geocaching:
      CPU: 1m 12s 740ms usr + 3s 950ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 2 times

