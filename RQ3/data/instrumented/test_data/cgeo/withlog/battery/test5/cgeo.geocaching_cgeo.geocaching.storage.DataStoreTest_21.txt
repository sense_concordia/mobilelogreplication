Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 43m 10s 0ms  (from 69 steps)
    Discharge screen doze time: 9h 39m 53s 400ms  (from 68 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 21s 713ms (100.0%) realtime, 1m 21s 714ms (100.0%) uptime
  Time on battery screen off: 1m 21s 713ms (100.0%) realtime, 1m 21s 714ms (100.0%) uptime
  Time on battery screen doze: 1m 21s 713ms (100.0%)
  Total run time: 1m 21s 747ms realtime, 1m 21s 748ms uptime
  Discharge: 8.27 mAh
  Screen off discharge: 8.27 mAh
  Screen doze discharge: 8.27 mAh
  Start clock time: 2018-07-31-12-44-29
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 21s 713ms (100.0%) 0x
  Idle mode full time: 1m 21s 713ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 489ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 21s 713ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 21s 713ms (100.0%) 
     Cellular Sleep time:  1m 20s 849ms (98.9%)
     Cellular Idle time:   187ms (0.2%)
     Cellular Rx time:     684ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 21s 713ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 21s 721ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 21s 721ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.89, actual drain: 0
    Uid u0a140: 2.50 ( cpu=2.50 wake=0.0000412 ) Including smearing: 3.57 ( proportional=1.07 )
    Uid 0: 0.571 ( cpu=0.507 wake=0.0644 ) Excluded from smearing
    Cell standby: 0.250 ( radio=0.250 ) Excluded from smearing
    Idle: 0.223 Excluded from smearing
    Uid 1000: 0.137 ( cpu=0.137 wake=0.000110 ) Excluded from smearing
    Uid 2000: 0.0853 ( cpu=0.0853 ) Excluded from smearing
    Uid u0a47: 0.0722 ( cpu=0.0721 wake=0.000117 ) Excluded from smearing
    Uid 1036: 0.0389 ( cpu=0.0389 ) Excluded from smearing
    Uid u0a22: 0.0117 ( cpu=0.0115 wake=0.000113 ) Excluded from smearing
    Uid u0a35: 0.00250 ( cpu=0.00250 ) Including smearing: 0.00358 ( proportional=0.00107 )
    Uid u0a127: 0.000453 ( cpu=0.000453 ) Including smearing: 0.000647 ( proportional=0.000194 )
    Wifi: 0.000280 ( cpu=0.000280 ) Including smearing: 0.000400 ( proportional=0.000120 )
    Uid 1001: 0.000177 ( cpu=0.000171 wake=0.00000555 ) Excluded from smearing
    Uid u0a40: 0.000164 ( cpu=0.000164 ) Including smearing: 0.000235 ( proportional=0.0000704 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 21s 596ms realtime (2 times)
    XO_shutdown.MPSS: 1m 20s 840ms realtime (34 times)
    XO_shutdown.ADSP: 1m 21s 616ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 140ms partial (15 times) max=128 actual=148 realtime
    Fg Service for: 1m 21s 706ms 
    Total running: 1m 21s 706ms 
    Total cpu time: u=1s 627ms s=1s 820ms 
    Total cpu time per freq: 1330 50 60 100 70 40 160 60 40 60 70 40 120 90 80 30 10 10 40 20 30 120 30 40 10 20 10 20 0 0 0 20 0 30 0 40 10 0 20 20 0 0 0 10 10 0 0 0 0 0 0 0 130
    Total screen-off cpu time per freq: 1330 50 60 100 70 40 160 60 40 60 70 40 120 90 80 30 10 10 40 20 30 120 30 40 10 20 10 20 0 0 0 20 0 30 0 40 10 0 20 20 0 0 0 10 10 0 0 0 0 0 0 0 130
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 560ms usr + 590ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 20ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 46ms partial (23 times) max=7 actual=57 realtime
    Wake lock fiid-sync: 6ms partial (1 times) max=6 realtime
    TOTAL wake: 52ms blamed partial, 63ms actual partial realtime
    Foreground services: 1m 21s 184ms realtime (1 times)
    Fg Service for: 1m 21s 184ms 
    Cached for: 35ms 
    Total running: 1m 21s 219ms 
    Total cpu time: u=42s 336ms s=17s 496ms 
    Total cpu time per freq: 2310 490 440 670 670 1110 1300 2290 3020 2070 1740 1330 1670 1160 940 790 790 460 530 230 270 450 2800 1420 2070 1400 1010 670 370 200 170 120 200 200 60 80 40 50 10 10 40 10 0 10 20 30 30 40 0 0 0 0 25280
    Total screen-off cpu time per freq: 2310 490 440 670 670 1110 1300 2290 3020 2070 1740 1330 1670 1160 940 790 790 460 530 230 270 450 2800 1420 2070 1400 1010 670 370 200 170 120 200 200 60 80 40 50 10 10 40 10 0 10 20 30 30 40 0 0 0 0 25280
    Proc cgeo.geocaching:
      CPU: 28s 180ms usr + 10s 800ms krn ; 0ms fg
      1 starts

