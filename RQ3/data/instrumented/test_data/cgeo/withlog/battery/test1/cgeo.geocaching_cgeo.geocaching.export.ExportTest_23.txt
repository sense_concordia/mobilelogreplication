Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 7h 52m 7s 800ms  (from 52 steps)
    Discharge screen doze time: 7h 45m 35s 0ms  (from 51 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 570ms (100.0%) realtime, 1m 0s 571ms (100.0%) uptime
  Time on battery screen off: 1m 0s 570ms (100.0%) realtime, 1m 0s 571ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 570ms (100.0%)
  Total run time: 1m 0s 599ms realtime, 1m 0s 599ms uptime
  Discharge: 4.03 mAh
  Screen off discharge: 4.03 mAh
  Screen doze discharge: 4.03 mAh
  Start clock time: 2018-07-31-10-04-02
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 0s 570ms (100.0%) 0x
  Idle mode light time: 1m 0s 570ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 445ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 570ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 570ms (100.0%) 
     Cellular Sleep time:  57s 292ms (94.6%)
     Cellular Idle time:   2s 699ms (4.5%)
     Cellular Rx time:     580ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 570ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  59s 547ms (98.3%)
     WiFi Idle time:   1s 8ms (1.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     16ms (0.0%)
     WiFi Battery drain: 0.00139mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 571ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.38, actual drain: 0-35.2
    Uid u0a140: 1.30 ( cpu=1.26 wake=0.0000666 wifi=0.000176 gps=0.0431 ) Including smearing: 2.11 ( proportional=0.813 )
    Uid 0: 0.380 ( cpu=0.332 wake=0.0477 ) Excluded from smearing
    Cell standby: 0.185 ( radio=0.185 ) Excluded from smearing
    Idle: 0.165 Excluded from smearing
    Uid 1000: 0.124 ( cpu=0.124 wake=0.0000214 ) Excluded from smearing
    Uid 1036: 0.105 ( cpu=0.105 ) Excluded from smearing
    Uid 2000: 0.0627 ( cpu=0.0627 ) Excluded from smearing
    Uid u0a47: 0.0513 ( cpu=0.0511 wake=0.000121 ) Excluded from smearing
    Uid u0a22: 0.00746 ( cpu=0.00616 wake=0.0000880 wifi=0.00122 ) Excluded from smearing
    Uid 1001: 0.00142 ( cpu=0.00137 wake=0.0000539 ) Excluded from smearing
    Wifi: 0.00133 ( cpu=0.00133 wifi=0.00000056 ) Including smearing: 0.00217 ( proportional=0.000835 )
    Uid 1021: 0.00116 ( cpu=0.00116 ) Excluded from smearing
    Uid u0a35: 0.000689 ( cpu=0.000689 ) Including smearing: 0.00112 ( proportional=0.000432 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 206ms realtime (13 times)
    XO_shutdown.MPSS: 53s 334ms realtime (31 times)
    XO_shutdown.ADSP: 1m 0s 472ms realtime (4 times)
    wlan.Active: 1s 283ms realtime (13 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 11ms partial (2 times) max=8 actual=14 realtime
    Wake lock GnssLocationProvider: 17ms partial (9 times) max=10 actual=23 realtime
    TOTAL wake: 28ms blamed partial, 37ms actual partial realtime
    Fg Service for: 1m 0s 564ms 
    Total running: 1m 0s 564ms 
    Total cpu time: u=2s 240ms s=2s 27ms 
    Total cpu time per freq: 1700 110 120 90 90 70 50 80 160 80 90 80 80 110 50 120 80 30 150 70 20 290 60 10 20 10 0 0 0 10 30 0 0 0 20 0 10 0 30 20 0 0 0 0 20 0 0 10 0 0 20 0 90
    Total screen-off cpu time per freq: 1700 110 120 90 90 70 50 80 160 80 90 80 80 110 50 120 80 30 150 70 20 290 60 10 20 10 0 0 0 10 30 0 0 0 20 0 10 0 30 20 0 0 0 0 20 0 0 10 0 0 20 0 90
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 150ms usr + 240ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 1s 300ms usr + 800ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 77ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  59s 944ms (99.0%)
       WiFi Idle time:   632ms (1.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     0ms (0.0%)
    Wake lock NlpWakeLock: 79ms partial (13 times) max=27 actual=104, 22ms background partial (2 times) max=18 realtime
    Wake lock fiid-sync: 6ms partial (1 times) max=6 realtime
    TOTAL wake: 85ms blamed partial, 110ms actual partial, 22ms actual background partial realtime
    Sensor GPS: 5s 176ms realtime (3 times), 29ms background (0 times)
    Foreground services: 59s 807ms realtime (1 times)
    Fg Service for: 59s 807ms 
    Cached for: 43ms 
    Total running: 59s 850ms 
    Total cpu time: u=32s 213ms s=10s 947ms 
    Total cpu time per freq: 5080 1480 1350 2410 1040 1180 1340 1460 1880 2000 1400 1200 1260 1260 1360 1180 1020 840 670 450 350 840 6770 2380 1890 530 330 270 100 150 160 100 120 40 10 10 70 50 10 20 20 20 20 0 0 0 60 30 0 10 0 30 250
    Total screen-off cpu time per freq: 5080 1480 1350 2410 1040 1180 1340 1460 1880 2000 1400 1200 1260 1260 1360 1180 1020 840 670 450 350 840 6770 2380 1890 530 330 270 100 150 160 100 120 40 10 10 70 50 10 20 20 20 20 0 0 0 60 30 0 10 0 30 250
    Proc cgeo.geocaching:
      CPU: 28s 500ms usr + 10s 120ms krn ; 0ms fg
      1 starts

