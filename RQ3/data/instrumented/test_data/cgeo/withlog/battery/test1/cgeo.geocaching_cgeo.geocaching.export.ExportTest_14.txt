Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 7h 36m 47s 800ms  (from 51 steps)
    Discharge screen doze time: 7h 29m 48s 800ms  (from 50 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 929ms (100.0%) realtime, 1m 0s 928ms (100.0%) uptime
  Time on battery screen off: 1m 0s 929ms (100.0%) realtime, 1m 0s 928ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 929ms (100.0%)
  Total run time: 1m 0s 956ms realtime, 1m 0s 956ms uptime
  Discharge: 5.33 mAh
  Screen off discharge: 5.33 mAh
  Screen doze discharge: 5.33 mAh
  Start clock time: 2018-07-31-09-53-41
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 0s 929ms (100.0%) 0x
  Idle mode light time: 1m 0s 929ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 462ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 929ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 929ms (100.0%) 
     Cellular Sleep time:  57s 537ms (94.4%)
     Cellular Idle time:   2s 774ms (4.6%)
     Cellular Rx time:     618ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 929ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  59s 842ms (98.2%)
     WiFi Idle time:   1s 71ms (1.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     17ms (0.0%)
     WiFi Battery drain: 0.00148mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 930ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.33, actual drain: 0
    Uid u0a140: 1.30 ( cpu=1.25 wake=0.0000523 wifi=0.000173 gps=0.0432 ) Including smearing: 2.06 ( proportional=0.766 )
    Uid 0: 0.393 ( cpu=0.345 wake=0.0480 ) Excluded from smearing
    Cell standby: 0.186 ( radio=0.186 ) Excluded from smearing
    Idle: 0.166 Excluded from smearing
    Uid 1000: 0.127 ( cpu=0.127 wake=0.0000523 ) Excluded from smearing
    Uid 2000: 0.0625 ( cpu=0.0625 ) Excluded from smearing
    Uid u0a47: 0.0589 ( cpu=0.0588 wake=0.000103 ) Excluded from smearing
    Uid 1036: 0.0288 ( cpu=0.0288 ) Excluded from smearing
    Uid u0a22: 0.00667 ( cpu=0.00525 wake=0.000112 wifi=0.00131 ) Excluded from smearing
    Wifi: 0.00131 ( cpu=0.00131 ) Including smearing: 0.00209 ( proportional=0.000775 )
    Uid 1021: 0.00109 ( cpu=0.00109 ) Excluded from smearing
    Uid 1001: 0.00105 ( cpu=0.00100 wake=0.0000452 ) Excluded from smearing
    Uid u0a40: 0.000171 ( cpu=0.000171 ) Including smearing: 0.000272 ( proportional=0.000101 )
    Uid u0a127: 0.000114 ( cpu=0.000114 ) Including smearing: 0.000181 ( proportional=0.0000672 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 527ms realtime (10 times)
    XO_shutdown.MPSS: 53s 663ms realtime (28 times)
    XO_shutdown.ADSP: 1m 0s 824ms realtime (4 times)
    wlan.Active: 1s 308ms realtime (10 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 43ms partial (3 times) max=38 actual=63 realtime
    Wake lock GnssLocationProvider: 24ms partial (10 times) max=12 actual=25 realtime
    TOTAL wake: 67ms blamed partial, 88ms actual partial realtime
    Fg Service for: 1m 0s 923ms 
    Total running: 1m 0s 923ms 
    Total cpu time: u=2s 264ms s=2s 127ms 
    Total cpu time per freq: 1840 110 30 80 60 50 100 90 90 150 90 90 120 100 90 70 50 60 80 60 50 230 70 20 40 50 10 0 10 0 10 0 20 0 20 20 0 0 10 0 10 10 0 0 10 20 0 0 0 0 0 10 30
    Total screen-off cpu time per freq: 1840 110 30 80 60 50 100 90 90 150 90 90 120 100 90 70 50 60 80 60 50 230 70 20 40 50 10 0 10 0 10 0 20 0 20 20 0 0 10 0 10 10 0 0 10 20 0 0 0 0 0 10 30
    Proc servicemanager:
      CPU: 140ms usr + 240ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc system:
      CPU: 1s 220ms usr + 720ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 65ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 0s 315ms (99.0%)
       WiFi Idle time:   621ms (1.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     0ms (0.0%)
    Wake lock NlpWakeLock: 63ms partial (15 times) max=26 actual=89, 11ms background partial (2 times) max=6 realtime
    Wake lock fiid-sync: 3ms partial (1 times) max=3 realtime
    TOTAL wake: 66ms blamed partial, 92ms actual partial, 11ms actual background partial realtime
    Sensor GPS: 5s 189ms realtime (3 times), 23ms background (0 times)
    Foreground services: 1m 0s 376ms realtime (1 times)
    Fg Service for: 1m 0s 376ms 
    Cached for: 48ms 
    Total running: 1m 0s 424ms 
    Total cpu time: u=32s 220ms s=11s 260ms 
    Total cpu time per freq: 5140 1600 1220 2280 1480 1160 1680 1700 1640 2250 1670 1200 1700 1340 1620 1200 830 750 1060 570 240 820 6800 2000 1810 290 160 70 60 0 30 50 60 40 30 0 20 20 40 40 10 0 30 40 0 20 0 10 0 0 0 10 270
    Total screen-off cpu time per freq: 5140 1600 1220 2280 1480 1160 1680 1700 1640 2250 1670 1200 1700 1340 1620 1200 830 750 1060 570 240 820 6800 2000 1810 290 160 70 60 0 30 50 60 40 30 0 20 20 40 40 10 0 30 40 0 20 0 10 0 0 0 10 270
    Proc cgeo.geocaching:
      CPU: 28s 20ms usr + 10s 300ms krn ; 0ms fg
      1 starts

