Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 7h 27m 13s 600ms  (from 50 steps)
    Discharge screen doze time: 7h 19m 54s 300ms  (from 49 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 1s 142ms (100.0%) realtime, 1m 1s 142ms (100.0%) uptime
  Time on battery screen off: 1m 1s 142ms (100.0%) realtime, 1m 1s 142ms (100.0%) uptime
  Time on battery screen doze: 1m 1s 142ms (100.0%)
  Total run time: 1m 1s 168ms realtime, 1m 1s 167ms uptime
  Discharge: 4.08 mAh
  Screen off discharge: 4.08 mAh
  Screen doze discharge: 4.08 mAh
  Start clock time: 2018-07-31-09-48-57
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 1s 142ms (100.0%) 0x
  Idle mode light time: 1m 1s 142ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 396ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 1s 142ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 1s 142ms (100.0%) 
     Cellular Sleep time:  58s 57ms (95.0%)
     Cellular Idle time:   2s 490ms (4.1%)
     Cellular Rx time:     596ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 1s 142ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 0s 219ms (98.5%)
     WiFi Idle time:   909ms (1.5%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     15ms (0.0%)
     WiFi Battery drain: 0.00129mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 1s 143ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.36, actual drain: 0
    Uid u0a140: 1.31 ( cpu=1.26 wake=0.0000674 wifi=0.000192 gps=0.0436 ) Including smearing: 2.09 ( proportional=0.783 )
    Uid 0: 0.401 ( cpu=0.352 wake=0.0482 ) Excluded from smearing
    Cell standby: 0.187 ( radio=0.187 ) Excluded from smearing
    Idle: 0.167 Excluded from smearing
    Uid 1000: 0.133 ( cpu=0.133 wake=0.0000230 ) Excluded from smearing
    Uid 2000: 0.0609 ( cpu=0.0609 ) Excluded from smearing
    Uid u0a47: 0.0569 ( cpu=0.0568 wake=0.000105 ) Excluded from smearing
    Uid 1036: 0.0361 ( cpu=0.0361 ) Excluded from smearing
    Uid u0a22: 0.00701 ( cpu=0.00580 wake=0.000103 wifi=0.00110 ) Excluded from smearing
    Uid 1021: 0.00124 ( cpu=0.00124 ) Excluded from smearing
    Uid 1001: 0.00107 ( cpu=0.00105 wake=0.0000143 ) Excluded from smearing
    Wifi: 0.00101 ( cpu=0.00101 ) Including smearing: 0.00162 ( proportional=0.000608 )
    Uid u0a35: 0.000396 ( cpu=0.000396 ) Including smearing: 0.000634 ( proportional=0.000237 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 908ms realtime (10 times)
    XO_shutdown.MPSS: 54s 21ms realtime (29 times)
    XO_shutdown.ADSP: 1m 1s 24ms realtime (4 times)
    wlan.Active: 1s 139ms realtime (10 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 11ms partial (2 times) max=8 actual=15 realtime
    Wake lock GnssLocationProvider: 19ms partial (11 times) max=4 realtime
    TOTAL wake: 30ms blamed partial, 34ms actual partial realtime
    Fg Service for: 1m 1s 137ms 
    Total running: 1m 1s 137ms 
    Total cpu time: u=2s 417ms s=2s 153ms 
    Total cpu time per freq: 1690 140 100 70 40 30 60 90 90 120 90 50 150 60 140 100 70 40 60 40 40 280 60 10 20 0 10 0 0 0 0 0 10 20 0 0 10 20 20 0 0 0 20 0 10 0 10 0 0 0 20 0 140
    Total screen-off cpu time per freq: 1690 140 100 70 40 30 60 90 90 120 90 50 150 60 140 100 70 40 60 40 40 280 60 10 20 0 10 0 0 0 0 0 10 20 0 0 10 20 20 0 0 0 20 0 10 0 10 0 0 0 20 0 140
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 130ms usr + 260ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 1s 240ms usr + 930ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.light: 1 times
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 79ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 0s 459ms (98.9%)
       WiFi Idle time:   690ms (1.1%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     0ms (0.0%)
    Wake lock NlpWakeLock: 76ms partial (15 times) max=26 actual=101, 21ms background partial (1 times) max=16 realtime
    Wake lock fiid-sync: 10ms partial (1 times) max=10 realtime
    TOTAL wake: 86ms blamed partial, 111ms actual partial, 21ms actual background partial realtime
    Sensor GPS: 5s 234ms realtime (3 times), 26ms background (0 times)
    Foreground services: 1m 0s 622ms realtime (1 times)
    Fg Service for: 1m 0s 622ms 
    Cached for: 37ms 
    Total running: 1m 0s 659ms 
    Total cpu time: u=32s 393ms s=11s 283ms 
    Total cpu time per freq: 5400 1550 1510 2510 1250 1170 1560 1330 1710 1720 1460 1090 1640 1410 1330 1290 700 720 820 480 450 950 7480 2590 1940 230 140 10 10 40 30 40 30 50 50 30 30 10 10 0 40 0 10 0 30 10 40 0 0 10 0 10 330
    Total screen-off cpu time per freq: 5400 1550 1510 2510 1250 1170 1560 1330 1710 1720 1460 1090 1640 1410 1330 1290 700 720 820 480 450 950 7480 2590 1940 230 140 10 10 40 30 40 30 50 50 30 30 10 10 0 40 0 10 0 30 10 40 0 0 10 0 10 330
    Proc cgeo.geocaching:
      CPU: 28s 690ms usr + 10s 490ms krn ; 0ms fg
      1 starts

