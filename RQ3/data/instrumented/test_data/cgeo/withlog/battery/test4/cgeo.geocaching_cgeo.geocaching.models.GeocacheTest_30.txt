Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 7h 29m 39s 900ms  (from 35 steps)
    Discharge screen doze time: 7h 35m 11s 600ms  (from 34 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 43s 485ms (99.9%) realtime, 43s 485ms (100.0%) uptime
  Time on battery screen off: 43s 485ms (100.0%) realtime, 43s 485ms (100.0%) uptime
  Time on battery screen doze: 43s 485ms (100.0%)
  Total run time: 43s 550ms realtime, 43s 550ms uptime
  Discharge: 7.63 mAh
  Screen off discharge: 7.63 mAh
  Screen doze discharge: 7.63 mAh
  Start clock time: 2018-08-01-11-56-57
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 43s 485ms (100.0%) 0x
  Idle mode light time: 43s 485ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 623ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 43s 485ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  43s 485ms (100.0%) 
     Cellular Sleep time:  25s 931ms (59.6%)
     Cellular Idle time:   17s 189ms (39.5%)
     Cellular Rx time:     365ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 43s 485ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  42s 513ms (97.8%)
     WiFi Idle time:   957ms (2.2%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     15ms (0.0%)
     WiFi Battery drain: 0.00131mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  43s 485ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 4.82, actual drain: 0
    Uid 0: 2.49 ( cpu=2.46 wake=0.0340 ) Excluded from smearing
    Uid u0a142: 1.65 ( cpu=1.29 wake=0.000172 wifi=0.00131 gps=0.357 ) Including smearing: 4.48 ( proportional=2.83 )
    Cell standby: 0.133 ( radio=0.133 ) Excluded from smearing
    Uid 2000: 0.126 ( cpu=0.126 ) Excluded from smearing
    Uid 1000: 0.120 ( cpu=0.116 wake=0.0000452 sensor=0.00302 ) Excluded from smearing
    Idle: 0.119 Excluded from smearing
    Uid 1036: 0.0852 ( cpu=0.0852 ) Excluded from smearing
    Uid 1001: 0.0374 ( cpu=0.0373 wake=0.0000531 ) Excluded from smearing
    Uid u0a22: 0.0218 ( cpu=0.0216 wake=0.000128 ) Excluded from smearing
    Uid u0a47: 0.0205 ( cpu=0.0205 wake=0.0000936 ) Excluded from smearing
    Uid 1021: 0.0118 ( cpu=0.0118 ) Excluded from smearing
    Wifi: 0.00338 ( cpu=0.00338 ) Including smearing: 0.00918 ( proportional=0.00580 )
    Uid u0a127: 0.000810 ( cpu=0.000810 ) Including smearing: 0.00220 ( proportional=0.00139 )
    Uid u0a139: 0.000758 ( cpu=0.000758 ) Including smearing: 0.00206 ( proportional=0.00130 )
    Uid u0a40: 0.000710 ( cpu=0.000710 ) Including smearing: 0.00193 ( proportional=0.00122 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 42s 274ms realtime (5 times)
    XO_shutdown.MPSS: 507ms realtime (1 times)
    XO_shutdown.ADSP: 43s 392ms realtime (4 times)
    wlan.Active: 1s 127ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 20ms partial (1 times) max=37 actual=37 realtime
    Wake lock GnssLocationProvider: 38ms partial (37 times) max=3 actual=40 realtime
    TOTAL wake: 58ms blamed partial, 77ms actual partial realtime
    Sensor 17: 43s 458ms realtime (0 times)
    Fg Service for: 43s 458ms 
    Total running: 43s 458ms 
    Total cpu time: u=1s 257ms s=980ms 
    Total cpu time per freq: 550 110 30 40 80 50 40 60 20 50 20 30 20 40 30 40 20 40 20 0 10 250 10 20 0 0 10 0 10 0 0 10 0 10 0 0 20 0 10 20 0 10 10 0 30 10 10 10 10 0 0 0 850
    Total screen-off cpu time per freq: 550 110 30 40 80 50 40 60 20 50 20 30 20 40 30 40 20 40 20 0 10 250 10 20 0 0 10 0 10 0 0 10 0 10 0 0 20 0 10 20 0 10 10 0 30 10 10 10 10 0 0 0 850
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 1s 20ms usr + 670ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 190ms (0.4%)
    Wifi Scan (blamed): 1s 43ms (2.4%) 2x
    Wifi Scan (actual): 1s 43ms (2.4%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  42s 516ms (97.8%)
       WiFi Idle time:   957ms (2.2%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     15ms (0.0%)
    Wake lock NlpWakeLock: 206ms partial (59 times) max=17 actual=264, 10ms background partial (2 times) max=5 realtime
    Wake lock fiid-sync: 12ms partial (1 times) max=12 realtime
    TOTAL wake: 218ms blamed partial, 276ms actual partial, 10ms actual background partial realtime
    Sensor GPS: 42s 866ms realtime (1 times), 69ms background (0 times)
    Foreground services: 43s 18ms realtime (1 times)
    Fg Service for: 43s 18ms 
    Cached for: 40ms 
    Total running: 43s 58ms 
    Total cpu time: u=19s 43ms s=6s 26ms 
    Total cpu time per freq: 790 150 110 190 130 140 70 170 180 490 260 180 470 360 280 410 280 300 240 170 180 700 320 70 130 70 310 190 190 150 240 130 200 220 170 130 290 260 190 240 250 350 250 300 250 230 300 400 140 100 40 30 12820
    Total screen-off cpu time per freq: 790 150 110 190 130 140 70 170 180 490 260 180 470 360 280 410 280 300 240 170 180 700 320 70 130 70 310 190 190 150 240 130 200 220 170 130 290 260 190 240 250 350 250 300 250 230 300 400 140 100 40 30 12820
    Proc cgeo.geocaching:
      CPU: 13s 890ms usr + 4s 850ms krn ; 0ms fg
      1 starts

