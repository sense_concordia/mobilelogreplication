Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 7h 34m 8s 200ms  (from 32 steps)
    Discharge screen doze time: 7h 40m 20s 600ms  (from 31 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 42s 236ms (99.9%) realtime, 42s 236ms (100.0%) uptime
  Time on battery screen off: 42s 236ms (100.0%) realtime, 42s 236ms (100.0%) uptime
  Time on battery screen doze: 42s 236ms (100.0%)
  Total run time: 42s 298ms realtime, 42s 298ms uptime
  Discharge: 7.30 mAh
  Screen off discharge: 7.30 mAh
  Screen doze discharge: 7.30 mAh
  Start clock time: 2018-08-01-11-47-09
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 42s 236ms (100.0%) 0x
  Idle mode light time: 42s 236ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 469ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 42s 236ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  42s 236ms (100.0%) 
     Cellular Sleep time:  25s 567ms (60.5%)
     Cellular Idle time:   16s 334ms (38.7%)
     Cellular Rx time:     336ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 42s 236ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  41s 91ms (97.3%)
     WiFi Idle time:   1s 126ms (2.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     20ms (0.0%)
     WiFi Battery drain: 0.00170mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  42s 237ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.07, actual drain: 0
    Uid 0: 2.76 ( cpu=2.72 wake=0.0331 ) Excluded from smearing
    Uid u0a142: 1.59 ( cpu=1.24 wake=0.000183 wifi=0.00170 gps=0.347 ) Including smearing: 4.71 ( proportional=3.12 )
    Uid 2000: 0.146 ( cpu=0.146 ) Excluded from smearing
    Cell standby: 0.129 ( radio=0.129 ) Excluded from smearing
    Uid 1000: 0.129 ( cpu=0.126 wake=0.0000444 sensor=0.00293 ) Excluded from smearing
    Idle: 0.115 Excluded from smearing
    Uid 1036: 0.0907 ( cpu=0.0907 ) Excluded from smearing
    Uid 1001: 0.0428 ( cpu=0.0428 wake=0.00000793 ) Excluded from smearing
    Uid u0a47: 0.0325 ( cpu=0.0325 ) Excluded from smearing
    Uid u0a22: 0.0241 ( cpu=0.0239 wake=0.000136 ) Excluded from smearing
    Uid 1021: 0.0122 ( cpu=0.0122 ) Excluded from smearing
    Wifi: 0.00286 ( cpu=0.00286 ) Including smearing: 0.00847 ( proportional=0.00561 )
    Uid u0a127: 0.00165 ( cpu=0.00165 ) Including smearing: 0.00489 ( proportional=0.00324 )
    Uid u0a40: 0.00105 ( cpu=0.00105 ) Including smearing: 0.00312 ( proportional=0.00207 )
    Uid u0a139: 0.000768 ( cpu=0.000768 ) Including smearing: 0.00228 ( proportional=0.00151 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 40s 852ms realtime (3 times)
    XO_shutdown.MPSS: 459ms realtime (2 times)
    XO_shutdown.ADSP: 42s 178ms realtime (4 times)
    wlan.Active: 1s 195ms realtime (3 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 57ms partial (22 times) max=34 actual=58 realtime
    Sensor 17: 42s 212ms realtime (0 times)
    Fg Service for: 42s 213ms 
    Total running: 42s 213ms 
    Total cpu time: u=1s 297ms s=1s 134ms 
    Total cpu time per freq: 470 50 120 70 60 60 90 60 30 70 80 20 10 30 30 20 20 50 0 20 0 210 20 20 0 10 20 10 0 30 0 0 0 0 0 10 0 0 10 0 10 0 10 10 10 10 10 10 0 0 10 0 970
    Total screen-off cpu time per freq: 470 50 120 70 60 60 90 60 30 70 80 20 10 30 30 20 20 50 0 20 0 210 20 20 0 10 20 10 0 30 0 0 0 0 0 10 0 0 10 0 10 0 10 10 10 10 10 10 0 0 10 0 970
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 1s 30ms usr + 790ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 207ms (0.5%)
    Wifi Scan (blamed): 1s 274ms (3.0%) 3x
    Wifi Scan (actual): 1s 274ms (3.0%) 3x
    Background Wifi Scan: 122ms (0.3%) 1x
       WiFi Sleep time:  41s 95ms (97.3%)
       WiFi Idle time:   1s 126ms (2.7%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     20ms (0.0%)
    Wake lock NlpWakeLock: 227ms partial (73 times) max=21 actual=285 (running for 1ms), 54ms background partial (12 times) max=16 (running for 1ms) realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 231ms blamed partial, 289ms actual partial, 54ms actual background partial realtime
    Sensor GPS: 41s 638ms realtime (1 times), 50ms background (0 times)
    Foreground services: 41s 765ms realtime (1 times)
    Fg Service for: 41s 765ms 
    Cached for: 34ms 
    Total running: 41s 799ms 
    Total cpu time: u=17s 870ms s=5s 550ms 
    Total cpu time per freq: 720 50 130 150 40 80 150 260 180 220 320 200 280 320 310 200 170 190 290 150 60 530 170 120 150 100 110 160 170 160 180 140 210 170 90 170 310 180 220 230 240 180 220 230 210 280 220 210 130 260 70 20 13230
    Total screen-off cpu time per freq: 720 50 130 150 40 80 150 260 180 220 320 200 280 320 310 200 170 190 290 150 60 530 170 120 150 100 110 160 170 160 180 140 210 170 90 170 310 180 220 230 240 180 220 230 210 280 220 210 130 260 70 20 13230
    Proc cgeo.geocaching:
      CPU: 12s 710ms usr + 4s 460ms krn ; 0ms fg
      1 starts

