Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 7h 38m 38s 300ms  (from 31 steps)
    Discharge screen doze time: 7h 45m 12s 200ms  (from 30 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 43s 219ms (99.9%) realtime, 43s 219ms (100.0%) uptime
  Time on battery screen off: 43s 219ms (100.0%) realtime, 43s 219ms (100.0%) uptime
  Time on battery screen doze: 43s 219ms (100.0%)
  Total run time: 43s 242ms realtime, 43s 242ms uptime
  Discharge: 7.64 mAh
  Screen off discharge: 7.64 mAh
  Screen doze discharge: 7.64 mAh
  Start clock time: 2018-08-01-11-40-54
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 43s 219ms (100.0%) 0x
  Idle mode light time: 43s 219ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 662ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 43s 219ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  43s 219ms (100.0%) 
     Cellular Sleep time:  12s 907ms (29.9%)
     Cellular Idle time:   29s 690ms (68.7%)
     Cellular Rx time:     622ms (1.4%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 43s 219ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  42s 267ms (97.8%)
     WiFi Idle time:   937ms (2.2%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     15ms (0.0%)
     WiFi Battery drain: 0.00130mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  43s 219ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 4.98, actual drain: 0-35.2
    Uid 0: 2.59 ( cpu=2.56 wake=0.0338 ) Excluded from smearing
    Uid u0a142: 1.68 ( cpu=1.32 wake=0.000212 wifi=0.00130 gps=0.355 ) Including smearing: 4.63 ( proportional=2.96 )
    Uid 2000: 0.134 ( cpu=0.134 ) Excluded from smearing
    Cell standby: 0.132 ( radio=0.132 ) Excluded from smearing
    Uid 1000: 0.125 ( cpu=0.122 wake=0.0000785 sensor=0.00300 ) Excluded from smearing
    Idle: 0.118 Excluded from smearing
    Uid 1036: 0.0875 ( cpu=0.0875 ) Excluded from smearing
    Uid 1001: 0.0418 ( cpu=0.0418 wake=0.00000555 ) Excluded from smearing
    Uid u0a47: 0.0277 ( cpu=0.0276 wake=0.0000968 ) Excluded from smearing
    Uid u0a22: 0.0228 ( cpu=0.0227 wake=0.000130 ) Excluded from smearing
    Uid 1021: 0.0135 ( cpu=0.0135 ) Excluded from smearing
    Uid u0a40: 0.00256 ( cpu=0.00256 ) Including smearing: 0.00707 ( proportional=0.00451 )
    Wifi: 0.00244 ( cpu=0.00244 ) Including smearing: 0.00674 ( proportional=0.00430 )
    Uid u0a127: 0.00122 ( cpu=0.00122 ) Including smearing: 0.00336 ( proportional=0.00214 )
    Uid u0a139: 0.000214 ( cpu=0.000214 ) Including smearing: 0.000590 ( proportional=0.000377 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 42s 32ms realtime (7 times)
    XO_shutdown.MPSS: 512ms realtime (2 times)
    XO_shutdown.ADSP: 43s 171ms realtime (4 times)
    wlan.Active: 1s 134ms realtime (7 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 16ms partial (1 times) max=26 actual=26 realtime
    Wake lock GnssLocationProvider: 84ms partial (24 times) max=63 actual=88 (running for 63ms) realtime
    TOTAL wake: 100ms blamed partial, 114ms actual partial realtime
    Sensor 17: 43s 211ms realtime (0 times)
    Fg Service for: 43s 211ms 
    Total running: 43s 211ms 
    Total cpu time: u=1s 234ms s=1s 83ms 
    Total cpu time per freq: 530 50 10 50 30 60 60 40 30 30 30 10 30 20 80 60 10 20 60 10 20 310 10 0 0 0 0 20 10 20 10 10 10 10 10 0 0 0 0 10 0 10 10 0 10 10 10 10 20 0 0 0 900
    Total screen-off cpu time per freq: 530 50 10 50 30 60 60 40 30 30 30 10 30 20 80 60 10 20 60 10 20 310 10 0 0 0 0 20 10 20 10 10 10 10 10 0 0 0 0 10 0 10 10 0 10 10 10 10 20 0 0 0 900
    Proc servicemanager:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 70ms krn ; 0ms fg
    Proc system:
      CPU: 990ms usr + 750ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 231ms (0.5%)
    Wifi Scan (blamed): 1s 106ms (2.6%) 2x
    Wifi Scan (actual): 1s 106ms (2.6%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  42s 270ms (97.8%)
       WiFi Idle time:   937ms (2.2%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     15ms (0.0%)
    Wake lock NlpWakeLock: 261ms partial (58 times) max=44 actual=314, 11ms background partial (1 times) max=7 realtime
    Wake lock fiid-sync: 6ms partial (1 times) max=6 realtime
    TOTAL wake: 267ms blamed partial, 320ms actual partial, 11ms actual background partial realtime
    Sensor GPS: 42s 627ms realtime (1 times), 86ms background (0 times)
    Foreground services: 42s 797ms realtime (1 times)
    Fg Service for: 42s 797ms 
    Cached for: 34ms 
    Total running: 42s 831ms 
    Total cpu time: u=19s 153ms s=6s 30ms 
    Total cpu time per freq: 1170 210 120 300 150 190 230 50 130 160 150 170 310 300 220 300 340 310 310 130 180 620 410 120 70 60 150 320 240 310 210 220 260 270 160 260 200 220 240 330 280 340 360 290 180 190 280 380 230 150 60 50 12900
    Total screen-off cpu time per freq: 1170 210 120 300 150 190 230 50 130 160 150 170 310 300 220 300 340 310 310 130 180 620 410 120 70 60 150 320 240 310 210 220 260 270 160 260 200 220 240 330 280 340 360 290 180 190 280 380 230 150 60 50 12900
    Proc cgeo.geocaching:
      CPU: 13s 520ms usr + 4s 790ms krn ; 0ms fg
      1 starts

