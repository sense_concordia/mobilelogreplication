Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 7h 38m 38s 300ms  (from 31 steps)
    Discharge screen doze time: 7h 45m 12s 200ms  (from 30 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 43s 438ms (99.9%) realtime, 43s 438ms (100.0%) uptime
  Time on battery screen off: 43s 438ms (100.0%) realtime, 43s 438ms (100.0%) uptime
  Time on battery screen doze: 43s 438ms (100.0%)
  Total run time: 43s 501ms realtime, 43s 501ms uptime
  Discharge: 7.82 mAh
  Screen off discharge: 7.82 mAh
  Screen doze discharge: 7.82 mAh
  Start clock time: 2018-08-01-11-42-42
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 43s 438ms (100.0%) 0x
  Idle mode light time: 43s 438ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 681ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 43s 438ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  43s 438ms (100.0%) 
     Cellular Sleep time:  26s 103ms (60.1%)
     Cellular Idle time:   16s 948ms (39.0%)
     Cellular Rx time:     387ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 43s 438ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  41s 941ms (96.6%)
     WiFi Idle time:   1s 474ms (3.4%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     24ms (0.1%)
     WiFi Battery drain: 0.00208mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  43s 439ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 4.88, actual drain: 0
    Uid 0: 2.54 ( cpu=2.51 wake=0.0339 ) Excluded from smearing
    Uid u0a142: 1.63 ( cpu=1.27 wake=0.000220 wifi=0.00139 gps=0.357 ) Including smearing: 4.53 ( proportional=2.91 )
    Uid 2000: 0.136 ( cpu=0.136 ) Excluded from smearing
    Cell standby: 0.133 ( radio=0.133 ) Excluded from smearing
    Uid 1000: 0.122 ( cpu=0.119 wake=0.0000579 sensor=0.00301 ) Excluded from smearing
    Idle: 0.118 Excluded from smearing
    Uid 1036: 0.0853 ( cpu=0.0853 ) Excluded from smearing
    Uid 1001: 0.0404 ( cpu=0.0404 wake=0.00000634 ) Excluded from smearing
    Uid u0a47: 0.0292 ( cpu=0.0291 wake=0.000108 ) Excluded from smearing
    Uid u0a22: 0.0255 ( cpu=0.0253 wake=0.000147 ) Excluded from smearing
    Uid 1021: 0.0125 ( cpu=0.0125 ) Excluded from smearing
    Wifi: 0.00232 ( cpu=0.00164 wifi=0.000683 ) Including smearing: 0.00647 ( proportional=0.00415 )
    Uid u0a127: 0.00148 ( cpu=0.00148 ) Including smearing: 0.00412 ( proportional=0.00264 )
    Uid u0a40: 0.000806 ( cpu=0.000806 ) Including smearing: 0.00225 ( proportional=0.00144 )
    Uid u0a139: 0.000305 ( cpu=0.000305 ) Including smearing: 0.000849 ( proportional=0.000545 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 42s 209ms realtime (5 times)
    XO_shutdown.MPSS: 491ms realtime (3 times)
    XO_shutdown.ADSP: 43s 389ms realtime (4 times)
    wlan.Active: 1s 191ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 34ms partial (1 times) max=52 actual=52 realtime
    Wake lock GnssLocationProvider: 40ms partial (36 times) max=4 actual=49 realtime
    TOTAL wake: 74ms blamed partial, 101ms actual partial realtime
    Sensor 17: 43s 413ms realtime (0 times)
    Fg Service for: 43s 414ms 
    Total running: 43s 414ms 
    Total cpu time: u=1s 350ms s=1s 40ms 
    Total cpu time per freq: 590 110 50 50 50 50 40 10 40 40 10 40 10 10 40 50 10 30 40 10 10 270 0 0 10 0 10 10 10 10 10 0 10 10 0 20 10 0 10 0 20 20 0 10 30 0 0 10 10 0 10 0 970
    Total screen-off cpu time per freq: 590 110 50 50 50 50 40 10 40 40 10 40 10 10 40 50 10 30 40 10 10 270 0 0 10 0 10 10 10 10 10 0 10 10 0 20 10 0 10 0 20 20 0 10 30 0 0 10 10 0 10 0 970
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 1s 20ms usr + 710ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 239ms (0.6%)
    Wifi Scan (blamed): 1s 123ms (2.6%) 2x
    Wifi Scan (actual): 1s 123ms (2.6%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  42s 409ms (97.6%)
       WiFi Idle time:   1s 16ms (2.3%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     16ms (0.0%)
    Wake lock NlpWakeLock: 257ms partial (66 times) max=19 actual=316 (running for 19ms), 38ms background partial (6 times) max=19 (running for 19ms) realtime
    Wake lock fiid-sync: 21ms partial (1 times) max=21 realtime
    TOTAL wake: 278ms blamed partial, 337ms actual partial, 38ms actual background partial realtime
    Sensor GPS: 42s 819ms realtime (1 times), 44ms background (0 times)
    Foreground services: 42s 973ms realtime (1 times)
    Fg Service for: 42s 973ms 
    Cached for: 48ms 
    Total running: 43s 21ms 
    Total cpu time: u=19s 13ms s=5s 920ms 
    Total cpu time per freq: 710 50 120 150 50 30 120 140 180 270 280 270 470 410 390 390 210 240 360 160 210 700 360 130 170 120 180 270 230 220 290 130 350 260 170 270 200 170 220 250 280 330 250 310 210 240 230 310 260 130 60 80 12500
    Total screen-off cpu time per freq: 710 50 120 150 50 30 120 140 180 270 280 270 470 410 390 390 210 240 360 160 210 700 360 130 170 120 180 270 230 220 290 130 350 260 170 270 200 170 220 250 280 330 250 310 210 240 230 310 260 130 60 80 12500
    Proc cgeo.geocaching:
      CPU: 12s 600ms usr + 4s 430ms krn ; 0ms fg
      1 starts

