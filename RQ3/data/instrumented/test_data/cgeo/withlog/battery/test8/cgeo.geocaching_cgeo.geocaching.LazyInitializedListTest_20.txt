Daily stats:
  Current start time: 2018-08-02-03-08-54
  Next min deadline: 2018-08-03-01-00-00
  Next max deadline: 2018-08-03-03-00-00
  Current daily steps:
    Discharge total time: 1d 1h 12m 28s 700ms  (from 4 steps)
    Discharge screen doze time: 1d 1h 12m 28s 700ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 2s 404ms (100.0%) realtime, 1m 2s 404ms (100.0%) uptime
  Time on battery screen off: 1m 2s 404ms (100.0%) realtime, 1m 2s 404ms (100.0%) uptime
  Time on battery screen doze: 1m 2s 404ms (100.0%)
  Total run time: 1m 2s 428ms realtime, 1m 2s 428ms uptime
  Discharge: 8.39 mAh
  Screen off discharge: 8.39 mAh
  Screen doze discharge: 8.39 mAh
  Start clock time: 2018-08-02-10-58-06
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 2s 404ms (100.0%) 0x
  Idle mode light time: 1m 2s 404ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 289ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 2s 404ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 2s 404ms (100.0%) 
     Cellular Sleep time:  40s 228ms (64.5%)
     Cellular Idle time:   21s 648ms (34.7%)
     Cellular Rx time:     529ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 2s 404ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 1s 408ms (98.4%)
     WiFi Idle time:   979ms (1.6%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     18ms (0.0%)
     WiFi Battery drain: 0.00152mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 2s 406ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.30, actual drain: 0
    Uid u0a144: 5.69 ( cpu=5.17 wake=0.0000547 wifi=0.00152 gps=0.513 ) Including smearing: 9.02 ( proportional=3.33 )
    Uid 1036: 2.47 ( cpu=2.47 ) Excluded from smearing
    Uid 0: 0.632 ( cpu=0.583 wake=0.0493 ) Excluded from smearing
    Cell standby: 0.191 ( radio=0.191 ) Excluded from smearing
    Idle: 0.170 Excluded from smearing
    Uid 1000: 0.0731 ( cpu=0.0731 wake=0.00000872 ) Excluded from smearing
    Uid 2000: 0.0243 ( cpu=0.0243 ) Excluded from smearing
    Uid u0a47: 0.0222 ( cpu=0.0221 wake=0.0000983 ) Excluded from smearing
    Uid 1021: 0.0122 ( cpu=0.0122 ) Excluded from smearing
    Uid u0a22: 0.0102 ( cpu=0.0102 wake=0.0000151 ) Excluded from smearing
    Uid 1001: 0.00405 ( cpu=0.00400 wake=0.0000515 ) Excluded from smearing
    Wifi: 0.00399 ( cpu=0.00399 ) Including smearing: 0.00633 ( proportional=0.00234 )
    Uid u0a127: 0.000958 ( cpu=0.000958 ) Including smearing: 0.00152 ( proportional=0.000561 )
    Uid u0a40: 0.000592 ( cpu=0.000592 ) Including smearing: 0.000939 ( proportional=0.000347 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 1s 152ms realtime (6 times)
    XO_shutdown.MPSS: 709ms realtime (3 times)
    XO_shutdown.ADSP: 1m 2s 298ms realtime (3 times)
    wlan.Active: 1s 162ms realtime (6 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 8ms partial (1 times) max=14 actual=14 realtime
    Wake lock GnssLocationProvider: 3ms partial (2 times) max=2 realtime
    TOTAL wake: 11ms blamed partial, 17ms actual partial realtime
    Fg Service for: 1m 2s 399ms 
    Total running: 1m 2s 399ms 
    Total cpu time: u=590ms s=690ms 
    Total cpu time per freq: 280 40 80 90 50 100 150 30 10 0 50 90 20 40 10 40 0 10 30 0 10 110 30 10 10 10 0 0 10 0 0 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 260
    Total screen-off cpu time per freq: 280 40 80 90 50 100 150 30 10 0 50 90 20 40 10 40 0 10 30 0 10 110 30 10 10 10 0 0 10 0 0 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 260
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 450ms usr + 480ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 45ms (0.1%)
    Wifi Scan (blamed): 1s 71ms (1.7%) 2x
    Wifi Scan (actual): 1s 71ms (1.7%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 1s 419ms (98.4%)
       WiFi Idle time:   979ms (1.6%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     18ms (0.0%)
    Wake lock NlpWakeLock: 39ms partial (7 times) max=14 actual=55, 8ms background partial (2 times) max=6 realtime
    Wake lock *alarm*: 15ms partial (3 times) max=11 actual=20 realtime
    Wake lock fiid-sync: 16ms partial (1 times) max=16 realtime
    TOTAL wake: 70ms blamed partial, 80ms actual partial, 8ms actual background partial realtime
    Sensor GPS: 1m 1s 531ms realtime (1 times), 14ms background (0 times)
    Foreground services: 1m 1s 827ms realtime (1 times)
    Fg Service for: 1m 1s 827ms 
    Cached for: 80ms 
    Total running: 1m 1s 907ms 
    Total cpu time: u=1m 14s 133ms s=11s 547ms 
    Total cpu time per freq: 90 0 10 90 360 190 80 110 110 30 20 10 10 0 40 10 10 50 20 10 0 170 1620 60 140 80 50 30 30 10 80 130 200 410 330 490 290 140 60 50 70 40 30 50 30 30 10 40 10 100 70 60 79310
    Total screen-off cpu time per freq: 90 0 10 90 360 190 80 110 110 30 20 10 10 0 40 10 10 50 20 10 0 170 1620 60 140 80 50 30 30 10 80 130 200 410 330 490 290 140 60 50 70 40 30 50 30 30 10 40 10 100 70 60 79310
    Proc cgeo.geocaching:
      CPU: 1m 3s 320ms usr + 11s 0ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 3 times

