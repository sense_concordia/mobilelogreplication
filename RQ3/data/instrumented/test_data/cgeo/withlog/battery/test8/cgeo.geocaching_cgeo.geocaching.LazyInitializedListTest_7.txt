Daily stats:
  Current start time: 2018-08-02-03-08-54
  Next min deadline: 2018-08-03-01-00-00
  Next max deadline: 2018-08-03-03-00-00
  Current daily steps:
    Discharge total time: 1d 17h 7m 32s 200ms  (from 2 steps)
    Discharge screen doze time: 1d 17h 7m 32s 200ms  (from 2 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 1s 708ms (100.0%) realtime, 1m 1s 707ms (100.0%) uptime
  Time on battery screen off: 1m 1s 708ms (100.0%) realtime, 1m 1s 707ms (100.0%) uptime
  Time on battery screen doze: 1m 1s 708ms (100.0%)
  Total run time: 1m 1s 735ms realtime, 1m 1s 734ms uptime
  Discharge: 8.67 mAh
  Screen off discharge: 8.67 mAh
  Screen doze discharge: 8.67 mAh
  Start clock time: 2018-08-02-10-42-26
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 1s 708ms (100.0%) 0x
  Idle mode light time: 1m 1s 708ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 294ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 1s 708ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 1s 708ms (100.0%) 
     Cellular Sleep time:  38s 888ms (63.0%)
     Cellular Idle time:   22s 323ms (36.2%)
     Cellular Rx time:     500ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 1s 708ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 0s 699ms (98.4%)
     WiFi Idle time:   998ms (1.6%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     16ms (0.0%)
     WiFi Battery drain: 0.00139mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 1s 714ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.34, actual drain: 0
    Uid u0a144: 5.62 ( cpu=5.11 wake=0.0000634 wifi=0.00139 gps=0.507 ) Including smearing: 9.06 ( proportional=3.44 )
    Uid 1036: 2.42 ( cpu=2.42 ) Excluded from smearing
    Uid 0: 0.835 ( cpu=0.786 wake=0.0487 ) Excluded from smearing
    Cell standby: 0.189 ( radio=0.189 ) Excluded from smearing
    Idle: 0.168 Excluded from smearing
    Uid 1000: 0.0465 ( cpu=0.0465 wake=0.00000952 ) Excluded from smearing
    Uid 2000: 0.0233 ( cpu=0.0233 ) Excluded from smearing
    Uid u0a47: 0.0137 ( cpu=0.0136 wake=0.0000975 ) Excluded from smearing
    Uid u0a22: 0.0114 ( cpu=0.0114 wake=0.0000167 ) Excluded from smearing
    Uid 1021: 0.00908 ( cpu=0.00908 ) Excluded from smearing
    Uid u0a35: 0.00404 ( cpu=0.00404 ) Including smearing: 0.00651 ( proportional=0.00247 )
    Wifi: 0.00197 ( cpu=0.00197 ) Including smearing: 0.00317 ( proportional=0.00121 )
    Uid 1001: 0.00113 ( cpu=0.00108 wake=0.0000452 ) Excluded from smearing
    Uid u0a127: 0.000427 ( cpu=0.000427 ) Including smearing: 0.000689 ( proportional=0.000262 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 0s 441ms realtime (6 times)
    XO_shutdown.MPSS: 665ms realtime (3 times)
    XO_shutdown.ADSP: 1m 1s 595ms realtime (4 times)
    wlan.Active: 1s 181ms realtime (6 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 9ms partial (1 times) max=15 actual=15 realtime
    Wake lock GnssLocationProvider: 4ms partial (3 times) max=2 realtime
    TOTAL wake: 13ms blamed partial, 19ms actual partial realtime
    Fg Service for: 1m 1s 703ms 
    Total running: 1m 1s 703ms 
    Total cpu time: u=346ms s=503ms 
    Total cpu time per freq: 110 0 0 50 30 30 60 20 10 20 30 30 0 0 0 20 40 0 20 0 0 70 20 0 10 10 10 0 0 0 0 0 0 10 0 20 0 10 0 0 10 0 0 0 0 0 0 0 0 0 10 0 220
    Total screen-off cpu time per freq: 110 0 0 50 30 30 60 20 10 20 30 30 0 0 0 20 40 0 20 0 0 70 20 0 10 10 10 0 0 0 0 0 0 10 0 20 0 10 0 0 10 0 0 0 0 0 0 0 0 0 10 0 220
    Proc servicemanager:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 230ms usr + 350ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 54ms (0.1%)
    Wifi Scan (blamed): 1s 78ms (1.7%) 2x
    Wifi Scan (actual): 1s 78ms (1.7%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 0s 729ms (98.4%)
       WiFi Idle time:   998ms (1.6%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     16ms (0.0%)
    Wake lock NlpWakeLock: 53ms partial (7 times) max=20 actual=70, 8ms background partial (2 times) max=7 realtime
    Wake lock *alarm*: 15ms partial (3 times) max=14 actual=24 realtime
    Wake lock fiid-sync: 13ms partial (1 times) max=13 realtime
    TOTAL wake: 81ms blamed partial, 88ms actual partial, 8ms actual background partial realtime
    Sensor GPS: 1m 0s 868ms realtime (1 times), 15ms background (0 times)
    Foreground services: 1m 1s 131ms realtime (1 times)
    Fg Service for: 1m 1s 131ms 
    Cached for: 93ms 
    Total running: 1m 1s 224ms 
    Total cpu time: u=1m 12s 954ms s=11s 470ms 
    Total cpu time per freq: 230 10 20 80 30 40 90 10 20 20 30 30 10 0 20 0 30 20 40 0 10 230 1230 40 90 120 60 50 60 20 100 80 150 290 360 650 270 100 60 30 50 30 20 20 40 40 90 50 30 80 10 40 78820
    Total screen-off cpu time per freq: 230 10 20 80 30 40 90 10 20 20 30 30 10 0 20 0 30 20 40 0 10 230 1230 40 90 120 60 50 60 20 100 80 150 290 360 650 270 100 60 30 50 30 20 20 40 40 90 50 30 80 10 40 78820
    Proc cgeo.geocaching:
      CPU: 1m 1s 130ms usr + 10s 800ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 3 times

