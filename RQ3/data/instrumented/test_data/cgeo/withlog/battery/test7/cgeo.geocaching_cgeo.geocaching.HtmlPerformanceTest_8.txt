Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 42m 33s 200ms  (from 81 steps)
    Discharge screen doze time: 9h 39m 45s 600ms  (from 80 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 445ms (100.0%) realtime, 1m 8s 446ms (100.0%) uptime
  Time on battery screen off: 1m 8s 445ms (100.0%) realtime, 1m 8s 446ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 445ms (100.0%)
  Total run time: 1m 8s 472ms realtime, 1m 8s 473ms uptime
  Discharge: 10.6 mAh
  Screen off discharge: 10.6 mAh
  Screen doze discharge: 10.6 mAh
  Start clock time: 2018-07-31-13-51-58
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 8s 445ms (100.0%) 0x
  Idle mode full time: 1m 8s 445ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 287ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 445ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 445ms (100.0%) 
     Cellular Sleep time:  1m 7s 687ms (98.9%)
     Cellular Idle time:   194ms (0.3%)
     Cellular Rx time:     565ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 445ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 8s 446ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 446ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.79, actual drain: 0
    Uid u0a140: 5.83 ( cpu=5.83 wake=0.00000317 ) Including smearing: 9.48 ( proportional=3.65 )
    Uid 2000: 1.29 ( cpu=1.29 ) Excluded from smearing
    Uid 0: 1.02 ( cpu=0.964 wake=0.0541 ) Excluded from smearing
    Uid 1036: 0.923 ( cpu=0.923 ) Excluded from smearing
    Uid 1000: 0.223 ( cpu=0.223 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.209 ( radio=0.209 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid u0a47: 0.110 ( cpu=0.109 wake=0.000209 ) Excluded from smearing
    Uid 1001: 0.00169 ( cpu=0.00168 wake=0.00000555 ) Excluded from smearing
    Uid u0a40: 0.00158 ( cpu=0.00158 ) Including smearing: 0.00257 ( proportional=0.000992 )
    Uid u0a127: 0.00131 ( cpu=0.00131 ) Including smearing: 0.00214 ( proportional=0.000823 )
    Uid u0a139: 0.000945 ( cpu=0.000945 ) Including smearing: 0.00154 ( proportional=0.000592 )
    Wifi: 0.000690 ( cpu=0.000690 ) Including smearing: 0.00112 ( proportional=0.000432 )
    Uid u0a22: 0.000540 ( cpu=0.000532 wake=0.00000793 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 337ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 643ms realtime (28 times)
    XO_shutdown.ADSP: 1m 8s 357ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    Fg Service for: 1m 8s 439ms 
    Total running: 1m 8s 439ms 
    Total cpu time: u=1s 666ms s=2s 443ms 
    Total cpu time per freq: 1890 110 90 70 70 90 110 70 40 30 50 10 30 40 20 20 30 0 20 10 10 80 10 0 0 0 0 0 0 0 0 0 0 10 10 0 10 0 0 10 0 0 0 0 0 0 0 10 0 0 0 0 190
    Total screen-off cpu time per freq: 1890 110 90 70 70 90 110 70 40 30 50 10 30 40 20 20 30 0 20 10 10 80 10 0 0 0 0 0 0 0 0 0 0 10 10 0 10 0 0 10 0 0 0 0 0 0 0 10 0 0 0 0 190
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 490ms usr + 550ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wake lock NlpWakeLock: 2ms partial (2 times) max=1 realtime
    Wake lock fiid-sync: 3ms partial (1 times) max=3 realtime
    TOTAL wake: 5ms blamed partial, 5ms actual partial realtime
    Foreground services: 1m 7s 810ms realtime (1 times)
    Fg Service for: 1m 7s 810ms 
    Cached for: 37ms 
    Total running: 1m 7s 847ms 
    Total cpu time: u=1m 22s 330ms s=16s 547ms 
    Total cpu time per freq: 260 0 0 10 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10 10 1320 420 200 70 20 20 20 20 10 0 30 50 120 490 940 800 690 470 380 240 160 110 110 110 90 120 150 150 110 60 88010
    Total screen-off cpu time per freq: 260 0 0 10 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10 10 1320 420 200 70 20 20 20 20 10 0 30 50 120 490 940 800 690 470 380 240 160 110 110 110 90 120 150 150 110 60 88010
    Proc cgeo.geocaching:
      CPU: 1m 11s 840ms usr + 14s 570ms krn ; 0ms fg
      1 starts

