Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 35m 49s 500ms  (from 84 steps)
    Discharge screen doze time: 9h 33m 3s 100ms  (from 83 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 804ms (99.9%) realtime, 1m 8s 803ms (100.0%) uptime
  Time on battery screen off: 1m 8s 804ms (100.0%) realtime, 1m 8s 803ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 804ms (100.0%)
  Total run time: 1m 8s 840ms realtime, 1m 8s 839ms uptime
  Discharge: 9.73 mAh
  Screen off discharge: 9.73 mAh
  Screen doze discharge: 9.73 mAh
  Start clock time: 2018-07-31-14-02-30
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 8s 804ms (100.0%) 0x
  Idle mode full time: 1m 8s 804ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 209ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 804ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 804ms (100.0%) 
     Cellular Sleep time:  1m 7s 964ms (98.8%)
     Cellular Idle time:   249ms (0.4%)
     Cellular Rx time:     591ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 804ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 8s 805ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 805ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.79, actual drain: 0-35.2
    Uid u0a140: 5.84 ( cpu=5.84 wake=0.00000397 ) Including smearing: 9.47 ( proportional=3.64 )
    Uid 2000: 1.31 ( cpu=1.31 ) Excluded from smearing
    Uid 0: 0.998 ( cpu=0.944 wake=0.0544 ) Excluded from smearing
    Uid 1036: 0.916 ( cpu=0.916 ) Excluded from smearing
    Uid 1000: 0.215 ( cpu=0.215 ) Excluded from smearing
    Cell standby: 0.210 ( radio=0.210 ) Excluded from smearing
    Idle: 0.188 Excluded from smearing
    Uid u0a47: 0.106 ( cpu=0.106 wake=0.000142 ) Excluded from smearing
    Uid u0a127: 0.00143 ( cpu=0.00143 ) Including smearing: 0.00233 ( proportional=0.000893 )
    Uid u0a40: 0.00100 ( cpu=0.00100 ) Including smearing: 0.00162 ( proportional=0.000623 )
    Uid u0a22: 0.000945 ( cpu=0.000936 wake=0.00000952 ) Excluded from smearing
    Uid 1001: 0.000926 ( cpu=0.000916 wake=0.0000103 ) Excluded from smearing
    Uid u0a139: 0.000823 ( cpu=0.000823 ) Including smearing: 0.00134 ( proportional=0.000513 )
    Wifi: 0.000661 ( cpu=0.000661 ) Including smearing: 0.00107 ( proportional=0.000412 )
    Uid u0a61: 0.000176 ( cpu=0.000176 ) Including smearing: 0.000285 ( proportional=0.000109 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 650ms realtime (5 times)
    XO_shutdown.MPSS: 1m 7s 861ms realtime (28 times)
    XO_shutdown.ADSP: 1m 8s 700ms realtime (0 times)
    wlan.Active: 55ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider realtime
    Fg Service for: 1m 8s 796ms 
    Total running: 1m 8s 796ms 
    Total cpu time: u=1s 603ms s=2s 343ms 
    Total cpu time per freq: 1610 50 50 40 30 170 40 110 20 40 10 30 40 40 30 0 10 0 50 20 0 120 10 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 20 0 0 0 0 0 250
    Total screen-off cpu time per freq: 1610 50 50 40 30 170 40 110 20 40 10 30 40 40 30 0 10 0 50 20 0 120 10 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 20 0 0 0 0 0 250
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 420ms usr + 570ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 2ms partial (2 times) max=1 realtime
    Wake lock fiid-sync: 3ms partial (1 times) max=3 realtime
    TOTAL wake: 5ms blamed partial, 5ms actual partial realtime
    Foreground services: 1m 8s 137ms realtime (1 times)
    Fg Service for: 1m 8s 137ms 
    Cached for: 53ms 
    Total running: 1m 8s 190ms 
    Total cpu time: u=1m 22s 636ms s=16s 587ms 
    Total cpu time per freq: 290 20 0 0 0 10 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 20 1380 390 240 50 40 10 30 0 0 10 30 170 260 700 950 660 570 450 360 170 220 230 180 120 120 140 230 130 270 180 87290
    Total screen-off cpu time per freq: 290 20 0 0 0 10 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 20 1380 390 240 50 40 10 30 0 0 10 30 170 260 700 950 660 570 450 360 170 220 230 180 120 120 140 230 130 270 180 87290
    Proc cgeo.geocaching:
      CPU: 1m 15s 30ms usr + 15s 190ms krn ; 0ms fg
      1 starts

