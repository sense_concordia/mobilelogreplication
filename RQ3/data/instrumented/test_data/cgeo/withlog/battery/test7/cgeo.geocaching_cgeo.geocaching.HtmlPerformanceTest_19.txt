Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 34m 17s 0ms  (from 85 steps)
    Discharge screen doze time: 9h 31m 31s 500ms  (from 84 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 658ms (99.9%) realtime, 1m 8s 658ms (100.0%) uptime
  Time on battery screen off: 1m 8s 658ms (100.0%) realtime, 1m 8s 658ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 658ms (100.0%)
  Total run time: 1m 8s 697ms realtime, 1m 8s 697ms uptime
  Discharge: 10.6 mAh
  Screen off discharge: 10.6 mAh
  Screen doze discharge: 10.6 mAh
  Start clock time: 2018-07-31-14-06-25
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 8s 658ms (100.0%) 0x
  Idle mode full time: 1m 8s 658ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 193ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 658ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 658ms (100.0%) 
     Cellular Sleep time:  1m 7s 709ms (98.6%)
     Cellular Idle time:   251ms (0.4%)
     Cellular Rx time:     701ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 658ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 8s 662ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 663ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.82, actual drain: 0-35.2
    Uid u0a140: 5.88 ( cpu=5.88 wake=0.00000872 ) Including smearing: 9.51 ( proportional=3.63 )
    Uid 2000: 1.29 ( cpu=1.29 ) Excluded from smearing
    Uid 0: 0.979 ( cpu=0.925 wake=0.0543 ) Excluded from smearing
    Uid 1036: 0.911 ( cpu=0.911 ) Excluded from smearing
    Uid 1000: 0.246 ( cpu=0.246 wake=0.00000159 ) Excluded from smearing
    Cell standby: 0.210 ( radio=0.210 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid u0a47: 0.115 ( cpu=0.114 wake=0.000122 ) Excluded from smearing
    Uid u0a22: 0.00178 ( cpu=0.00176 wake=0.0000127 ) Excluded from smearing
    Uid u0a40: 0.00134 ( cpu=0.00134 ) Including smearing: 0.00217 ( proportional=0.000828 )
    Uid u0a127: 0.00122 ( cpu=0.00122 ) Including smearing: 0.00197 ( proportional=0.000751 )
    Uid u0a139: 0.000826 ( cpu=0.000826 ) Including smearing: 0.00134 ( proportional=0.000510 )
    Uid 1001: 0.000764 ( cpu=0.000757 wake=0.00000714 ) Excluded from smearing
    Wifi: 0.000589 ( cpu=0.000589 ) Including smearing: 0.000953 ( proportional=0.000364 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 509ms realtime (4 times)
    XO_shutdown.MPSS: 1m 7s 647ms realtime (30 times)
    XO_shutdown.ADSP: 1m 8s 568ms realtime (0 times)
    wlan.Active: 57ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    Fg Service for: 1m 8s 650ms 
    Total running: 1m 8s 650ms 
    Total cpu time: u=1s 903ms s=2s 553ms 
    Total cpu time per freq: 1940 110 70 140 50 130 110 170 20 40 0 40 100 30 10 30 20 10 10 10 20 120 10 20 10 0 10 20 10 0 0 0 10 10 10 0 0 0 30 0 0 10 20 0 0 0 20 0 30 0 0 0 190
    Total screen-off cpu time per freq: 1940 110 70 140 50 130 110 170 20 40 0 40 100 30 10 30 20 10 10 10 20 120 10 20 10 0 10 20 10 0 0 0 10 10 10 0 0 0 30 0 0 10 20 0 0 0 20 0 30 0 0 0 190
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 70ms krn ; 0ms fg
    Proc system:
      CPU: 630ms usr + 770ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 30ms krn ; 0ms fg
  u0a140:
    Wake lock NlpWakeLock: 3ms partial (2 times) max=2 realtime
    Wake lock fiid-sync: 9ms partial (1 times) max=9 realtime
    TOTAL wake: 12ms blamed partial, 12ms actual partial realtime
    Foreground services: 1m 8s 127ms realtime (1 times)
    Fg Service for: 1m 8s 127ms 
    Cached for: 59ms 
    Total running: 1m 8s 186ms 
    Total cpu time: u=1m 22s 597ms s=17s 60ms 
    Total cpu time per freq: 310 10 10 10 0 10 0 40 0 0 10 0 0 10 0 0 0 0 0 30 0 0 1560 350 120 60 30 20 0 0 10 0 40 100 170 550 800 700 590 480 390 250 230 190 180 100 60 180 140 330 90 110 88170
    Total screen-off cpu time per freq: 310 10 10 10 0 10 0 40 0 0 10 0 0 10 0 0 0 0 0 30 0 0 1560 350 120 60 30 20 0 0 10 0 40 100 170 550 800 700 590 480 390 250 230 190 180 100 60 180 140 330 90 110 88170
    Proc cgeo.geocaching:
      CPU: 1m 13s 340ms usr + 15s 260ms krn ; 0ms fg
      1 starts

