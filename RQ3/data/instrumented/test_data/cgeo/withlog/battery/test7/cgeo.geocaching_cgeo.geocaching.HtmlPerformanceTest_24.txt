Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 31m 18s 100ms  (from 86 steps)
    Discharge screen doze time: 9h 28m 32s 400ms  (from 85 steps)
    Charge total time: 3h 15m 4s 0ms  (from 76 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 91ms (99.9%) realtime, 1m 8s 91ms (100.0%) uptime
  Time on battery screen off: 1m 8s 91ms (100.0%) realtime, 1m 8s 91ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 91ms (100.0%)
  Total run time: 1m 8s 129ms realtime, 1m 8s 129ms uptime
  Discharge: 10.5 mAh
  Screen off discharge: 10.5 mAh
  Screen doze discharge: 10.5 mAh
  Start clock time: 2018-07-31-14-12-58
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 8s 91ms (100.0%) 0x
  Idle mode full time: 1m 8s 91ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 330ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 91ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 91ms (100.0%) 
     Cellular Sleep time:  1m 7s 174ms (98.6%)
     Cellular Idle time:   227ms (0.3%)
     Cellular Rx time:     693ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 91ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 8s 95ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 96ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.74, actual drain: 0
    Uid u0a140: 5.82 ( cpu=5.82 wake=0.00000714 ) Including smearing: 9.43 ( proportional=3.61 )
    Uid 2000: 1.28 ( cpu=1.28 ) Excluded from smearing
    Uid 0: 0.997 ( cpu=0.943 wake=0.0537 ) Excluded from smearing
    Uid 1036: 0.921 ( cpu=0.921 ) Excluded from smearing
    Uid 1000: 0.209 ( cpu=0.209 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.208 ( radio=0.208 ) Excluded from smearing
    Idle: 0.186 Excluded from smearing
    Uid u0a47: 0.113 ( cpu=0.113 wake=0.000233 ) Excluded from smearing
    Uid u0a116: 0.00318 ( cpu=0.00318 ) Including smearing: 0.00516 ( proportional=0.00198 )
    Uid u0a22: 0.00255 ( cpu=0.00253 wake=0.0000151 ) Excluded from smearing
    Uid u0a40: 0.00150 ( cpu=0.00150 ) Including smearing: 0.00243 ( proportional=0.000930 )
    Uid u0a139: 0.00118 ( cpu=0.00118 ) Including smearing: 0.00191 ( proportional=0.000732 )
    Uid 1001: 0.000968 ( cpu=0.000962 wake=0.00000555 ) Excluded from smearing
    Uid u0a127: 0.000847 ( cpu=0.000847 ) Including smearing: 0.00137 ( proportional=0.000527 )
    Wifi: 0.000730 ( cpu=0.000730 ) Including smearing: 0.00118 ( proportional=0.000454 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 7s 981ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 139ms realtime (29 times)
    XO_shutdown.ADSP: 1m 8s 5ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Fg Service for: 1m 8s 83ms 
    Total running: 1m 8s 83ms 
    Total cpu time: u=1s 580ms s=2s 274ms 
    Total cpu time per freq: 1870 100 20 50 30 100 80 30 20 10 10 0 100 0 30 30 10 0 10 20 10 50 10 0 0 0 0 0 0 0 0 0 0 0 10 10 0 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 250
    Total screen-off cpu time per freq: 1870 100 20 50 30 100 80 30 20 10 10 0 100 0 30 30 10 0 10 20 10 50 10 0 0 0 0 0 0 0 0 0 0 0 10 10 0 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 250
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 380ms usr + 470ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 20ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 5ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 5ms partial (2 times) max=7 actual=9 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 9ms blamed partial, 13ms actual partial realtime
    Foreground services: 1m 7s 608ms realtime (1 times)
    Fg Service for: 1m 7s 608ms 
    Cached for: 32ms 
    Total running: 1m 7s 640ms 
    Total cpu time: u=1m 21s 927ms s=16s 650ms 
    Total cpu time per freq: 470 10 0 0 0 30 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10 1470 460 180 20 20 20 10 20 10 0 20 110 160 490 910 730 590 510 430 170 110 210 160 100 70 40 70 270 160 160 87390
    Total screen-off cpu time per freq: 470 10 0 0 0 30 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 10 1470 460 180 20 20 20 10 20 10 0 20 110 160 490 910 730 590 510 430 170 110 210 160 100 70 40 70 270 160 160 87390
    Proc cgeo.geocaching:
      CPU: 1m 11s 560ms usr + 14s 790ms krn ; 0ms fg
      1 starts

