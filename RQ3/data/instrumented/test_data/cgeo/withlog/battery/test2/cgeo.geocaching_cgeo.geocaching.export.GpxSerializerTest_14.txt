Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 8h 2m 49s 600ms  (from 23 steps)
    Discharge screen doze time: 8h 12m 52s 700ms  (from 22 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 42s 89ms (100.0%) realtime, 42s 89ms (100.0%) uptime
  Time on battery screen off: 42s 89ms (100.0%) realtime, 42s 89ms (100.0%) uptime
  Time on battery screen doze: 42s 89ms (100.0%)
  Total run time: 42s 105ms realtime, 42s 105ms uptime
  Discharge: 7.88 mAh
  Screen off discharge: 7.88 mAh
  Screen doze discharge: 7.88 mAh
  Start clock time: 2018-08-01-11-14-14
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 42s 89ms (100.0%) 0x
  Idle mode light time: 42s 89ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 92ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 42s 89ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  42s 89ms (100.0%) 
     Cellular Sleep time:  25s 806ms (61.3%)
     Cellular Idle time:   15s 899ms (37.8%)
     Cellular Rx time:     384ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 42s 89ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  41s 87ms (97.6%)
     WiFi Idle time:   986ms (2.3%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     17ms (0.0%)
     WiFi Battery drain: 0.00145mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  42s 90ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.28, actual drain: 0
    Uid 0: 2.76 ( cpu=2.73 wake=0.0333 ) Excluded from smearing
    Uid u0a142: 1.84 ( cpu=1.50 wake=0.0000365 wifi=0.000838 gps=0.345 ) Including smearing: 4.95 ( proportional=3.11 )
    Uid 2000: 0.158 ( cpu=0.158 ) Excluded from smearing
    Cell standby: 0.129 ( radio=0.129 ) Excluded from smearing
    Idle: 0.115 Excluded from smearing
    Uid 1036: 0.107 ( cpu=0.107 ) Excluded from smearing
    Uid 1000: 0.0787 ( cpu=0.0787 wake=0.00000238 ) Excluded from smearing
    Uid 1001: 0.0433 ( cpu=0.0432 wake=0.00000634 ) Excluded from smearing
    Uid u0a47: 0.0197 ( cpu=0.0197 ) Excluded from smearing
    Uid 1021: 0.0102 ( cpu=0.0102 ) Excluded from smearing
    Uid u0a22: 0.00705 ( cpu=0.00702 wake=0.0000278 ) Excluded from smearing
    Wifi: 0.00322 ( cpu=0.00260 wifi=0.000617 ) Including smearing: 0.00865 ( proportional=0.00543 )
    Uid u0a40: 0.00313 ( cpu=0.00313 ) Including smearing: 0.00840 ( proportional=0.00528 )
    Uid u0a127: 0.000605 ( cpu=0.000605 ) Including smearing: 0.00163 ( proportional=0.00102 )
    Uid u0a139: 0.000545 ( cpu=0.000545 ) Including smearing: 0.00147 ( proportional=0.000920 )
    Uid 1027: 0.000163 ( cpu=0.000163 ) Excluded from smearing
    Uid u0a35: 0.000141 ( cpu=0.000141 ) Including smearing: 0.000378 ( proportional=0.000237 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 41s 376ms realtime (3 times)
    XO_shutdown.MPSS: 511ms realtime (1 times)
    XO_shutdown.ADSP: 41s 997ms realtime (2 times)
    wlan.Active: 575ms realtime (3 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 3ms partial (2 times) max=2 realtime
    Fg Service for: 42s 85ms 
    Total running: 42s 85ms 
    Total cpu time: u=660ms s=800ms 
    Total cpu time per freq: 570 70 50 100 40 10 100 40 0 40 20 30 20 20 10 0 0 0 10 0 20 140 10 0 0 0 0 0 0 0 0 0 0 20 0 0 0 0 0 10 0 0 10 0 0 10 0 10 0 10 0 0 170
    Total screen-off cpu time per freq: 570 70 50 100 40 10 100 40 0 40 20 30 20 20 10 0 0 0 10 0 20 140 10 0 0 0 0 0 0 0 0 0 0 20 0 0 0 0 0 10 0 0 10 0 0 10 0 10 0 10 0 0 170
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 440ms usr + 540ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 45ms (0.1%)
    Wifi Scan (blamed): 617ms (1.5%) 2x
    Wifi Scan (actual): 617ms (1.5%) 2x
    Background Wifi Scan: 63ms (0.1%) 1x
       WiFi Sleep time:  41s 567ms (98.8%)
       WiFi Idle time:   515ms (1.2%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     10ms (0.0%)
    Wake lock NlpWakeLock: 36ms partial (6 times) max=19 actual=53, 7ms background partial (2 times) max=6 realtime
    Wake lock *alarm*: 7ms partial (2 times) max=8 actual=9 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 47ms blamed partial, 61ms actual partial, 7ms actual background partial realtime
    Sensor GPS: 41s 430ms realtime (1 times), 19ms background (0 times)
    Foreground services: 41s 595ms realtime (1 times)
    Fg Service for: 41s 595ms 
    Cached for: 35ms 
    Total running: 41s 630ms 
    Total cpu time: u=22s 10ms s=4s 800ms 
    Total cpu time per freq: 800 80 100 30 20 30 30 10 10 10 20 10 30 20 0 40 0 10 80 10 40 100 250 30 80 130 120 170 210 110 310 260 390 270 180 180 180 200 350 340 200 200 300 450 300 270 340 620 170 320 70 80 18490
    Total screen-off cpu time per freq: 800 80 100 30 20 30 30 10 10 10 20 10 30 20 0 40 0 10 80 10 40 100 250 30 80 130 120 170 210 110 310 260 390 270 180 180 180 200 350 340 200 200 300 450 300 270 340 620 170 320 70 80 18490
    Proc cgeo.geocaching:
      CPU: 14s 990ms usr + 3s 230ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 2 times

