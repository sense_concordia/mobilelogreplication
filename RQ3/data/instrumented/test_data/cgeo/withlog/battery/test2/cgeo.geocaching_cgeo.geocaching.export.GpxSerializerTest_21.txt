Daily stats:
  Current start time: 2018-08-01-07-27-20
  Next min deadline: 2018-08-02-01-00-00
  Next max deadline: 2018-08-02-03-00-00
  Current daily steps:
    Discharge total time: 8h 2m 22s 300ms  (from 25 steps)
    Discharge screen doze time: 8h 11m 34s 0ms  (from 24 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 44s 408ms (100.0%) realtime, 44s 408ms (100.0%) uptime
  Time on battery screen off: 44s 408ms (100.0%) realtime, 44s 408ms (100.0%) uptime
  Time on battery screen doze: 44s 408ms (100.0%)
  Total run time: 44s 425ms realtime, 44s 425ms uptime
  Discharge: 7.69 mAh
  Screen off discharge: 7.69 mAh
  Screen doze discharge: 7.69 mAh
  Start clock time: 2018-08-01-11-20-33
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 44s 408ms (100.0%) 0x
  Idle mode light time: 44s 408ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 300ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 44s 408ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  44s 407ms (100.0%) 
     Cellular Sleep time:  27s 22ms (60.8%)
     Cellular Idle time:   16s 993ms (38.3%)
     Cellular Rx time:     393ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 44s 407ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  43s 331ms (97.6%)
     WiFi Idle time:   1s 60ms (2.4%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     17ms (0.0%)
     WiFi Battery drain: 0.00147mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  44s 408ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.14, actual drain: 0
    Uid 0: 2.50 ( cpu=2.47 wake=0.0350 ) Excluded from smearing
    Uid u0a142: 1.94 ( cpu=1.58 wake=0.0000404 wifi=0.000781 gps=0.365 ) Including smearing: 4.82 ( proportional=2.88 )
    Uid 2000: 0.154 ( cpu=0.154 ) Excluded from smearing
    Cell standby: 0.136 ( radio=0.136 ) Excluded from smearing
    Idle: 0.121 Excluded from smearing
    Uid 1036: 0.0997 ( cpu=0.0997 ) Excluded from smearing
    Uid 1000: 0.0697 ( cpu=0.0696 wake=0.0000785 ) Excluded from smearing
    Uid 1001: 0.0540 ( cpu=0.0540 wake=0.00000793 ) Excluded from smearing
    Uid u0a47: 0.0255 ( cpu=0.0254 wake=0.000102 ) Excluded from smearing
    Uid 1021: 0.0136 ( cpu=0.0136 ) Excluded from smearing
    Uid u0a22: 0.00820 ( cpu=0.00819 wake=0.00000872 ) Excluded from smearing
    Wifi: 0.00230 ( cpu=0.00160 wifi=0.000693 ) Including smearing: 0.00570 ( proportional=0.00340 )
    Uid u0a127: 0.000883 ( cpu=0.000883 ) Including smearing: 0.00219 ( proportional=0.00131 )
    Uid u0a40: 0.000673 ( cpu=0.000673 ) Including smearing: 0.00167 ( proportional=0.000997 )
    Uid u0a139: 0.000540 ( cpu=0.000540 ) Including smearing: 0.00134 ( proportional=0.000799 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 43s 665ms realtime (4 times)
    XO_shutdown.MPSS: 382ms realtime (4 times)
    XO_shutdown.ADSP: 44s 318ms realtime (2 times)
    wlan.Active: 668ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 26ms partial (1 times) max=37 actual=37 realtime
    Wake lock GnssLocationProvider: 73ms partial (2 times) max=76 actual=77 realtime
    TOTAL wake: 99ms blamed partial, 114ms actual partial realtime
    Fg Service for: 44s 403ms 
    Total running: 44s 403ms 
    Total cpu time: u=593ms s=753ms 
    Total cpu time per freq: 570 70 90 50 70 40 0 20 20 10 0 20 50 0 50 40 20 50 10 20 10 50 10 10 0 0 10 20 0 10 10 0 0 0 20 0 0 0 0 10 0 0 10 0 0 0 0 0 20 0 0 0 190
    Total screen-off cpu time per freq: 570 70 90 50 70 40 0 20 20 10 0 20 50 0 50 40 20 50 10 20 10 50 10 10 0 0 10 20 0 10 10 0 0 0 20 0 0 0 0 10 0 0 10 0 0 0 0 0 20 0 0 0 190
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 70ms krn ; 0ms fg
    Proc system:
      CPU: 370ms usr + 440ms krn ; 0ms fg
    Proc hwservicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 44ms (0.1%)
    Wifi Scan (blamed): 599ms (1.3%) 1x
    Wifi Scan (actual): 599ms (1.3%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  43s 839ms (98.7%)
       WiFi Idle time:   563ms (1.3%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     9ms (0.0%)
    Wake lock NlpWakeLock: 38ms partial (4 times) max=34 actual=49 realtime
    Wake lock *alarm*: 10ms partial (2 times) max=11 actual=14 realtime
    Wake lock fiid-sync: 3ms partial (1 times) max=3 realtime
    TOTAL wake: 51ms blamed partial, 58ms actual partial realtime
    Sensor GPS: 43s 819ms realtime (1 times), 90ms background (0 times)
    Foreground services: 43s 909ms realtime (1 times)
    Fg Service for: 43s 909ms 
    Cached for: 37ms 
    Total running: 43s 946ms 
    Total cpu time: u=24s 313ms s=5s 270ms 
    Total cpu time per freq: 1110 150 40 120 50 20 70 20 30 100 40 50 30 20 50 40 40 100 80 40 120 280 200 100 130 150 200 240 230 290 330 390 540 390 440 610 410 380 350 360 520 410 350 450 390 310 470 510 350 290 130 160 17260
    Total screen-off cpu time per freq: 1110 150 40 120 50 20 70 20 30 100 40 50 30 20 50 40 40 100 80 40 120 280 200 100 130 150 200 240 230 290 330 390 540 390 440 610 410 380 350 360 520 410 350 450 390 310 470 510 350 290 130 160 17260
    Proc cgeo.geocaching:
      CPU: 17s 260ms usr + 3s 860ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 2 times

