Daily stats:
  Current start time: 2018-08-02-03-08-54
  Next min deadline: 2018-08-03-01-00-00
  Next max deadline: 2018-08-03-03-00-00
  Current daily steps:
    Discharge total time: 3d 2h 59m 34s 200ms  (from 1 steps)
    Discharge screen doze time: 3d 2h 59m 34s 200ms  (from 1 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 950ms (100.0%) realtime, 1m 0s 950ms (100.0%) uptime
  Time on battery screen off: 1m 0s 950ms (100.0%) realtime, 1m 0s 950ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 950ms (100.0%)
  Total run time: 1m 0s 977ms realtime, 1m 0s 977ms uptime
  Discharge: 8.28 mAh
  Screen off discharge: 8.28 mAh
  Screen doze discharge: 8.28 mAh
  Start clock time: 2018-08-02-10-35-13
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Total partial wakelock time: 265ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 950ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 950ms (100.0%) 
     Cellular Sleep time:  -7s -290ms (-12.0%)
     Cellular Idle time:   1m 6s 845ms (109.7%)
     Cellular Rx time:     1s 395ms (2.3%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 950ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  57s 960ms (95.1%)
     WiFi Idle time:   2s 941ms (4.8%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     49ms (0.1%)
     WiFi Battery drain: 0.00422mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 951ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 10.3, actual drain: 0
    Uid u0a144: 6.32 ( cpu=5.81 wake=0.0000444 wifi=0.00343 gps=0.503 ) Including smearing: 10.0 ( proportional=3.73 )
    Uid 1036: 2.43 ( cpu=2.43 ) Excluded from smearing
    Uid 0: 1.10 ( cpu=1.05 wake=0.0481 ) Excluded from smearing
    Cell standby: 0.186 ( radio=0.186 ) Excluded from smearing
    Idle: 0.166 Excluded from smearing
    Uid 1000: 0.0580 ( cpu=0.0580 wake=0.00000793 ) Excluded from smearing
    Uid 2000: 0.0238 ( cpu=0.0238 ) Excluded from smearing
    Uid 1021: 0.0111 ( cpu=0.0111 ) Excluded from smearing
    Uid u0a22: 0.0105 ( cpu=0.0105 wake=0.0000293 ) Excluded from smearing
    Uid u0a47: 0.00333 ( cpu=0.00323 wake=0.0000968 ) Excluded from smearing
    Wifi: 0.00311 ( cpu=0.00233 wifi=0.000786 ) Including smearing: 0.00495 ( proportional=0.00184 )
    Uid 1001: 0.00106 ( cpu=0.00102 wake=0.0000309 ) Excluded from smearing
    Uid u0a127: 0.000239 ( cpu=0.000239 ) Including smearing: 0.000380 ( proportional=0.000141 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 642ms realtime (3 times)
    XO_shutdown.MPSS: 199ms realtime (1 times)
    XO_shutdown.ADSP: 1m 0s 902ms realtime (4 times)
    wlan.Active: 1s 169ms realtime (3 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 9ms partial (1 times) max=14 actual=14 realtime
    Wake lock GnssLocationProvider: 2ms partial (2 times) max=1 realtime
    TOTAL wake: 11ms blamed partial, 16ms actual partial realtime
    Fg Service for: 1m 0s 946ms 
    Total running: 1m 0s 946ms 
    Total cpu time: u=420ms s=547ms 
    Total cpu time per freq: 170 0 0 40 30 40 100 70 20 30 10 30 20 0 40 20 40 100 10 0 0 410 30 10 0 10 0 0 40 20 0 10 0 0 10 30 10 20 0 0 0 20 10 0 0 0 10 10 0 0 0 0 860
    Total screen-off cpu time per freq: 170 0 0 40 30 40 100 70 20 30 10 30 20 0 40 20 40 100 10 0 0 410 30 10 0 10 0 0 40 20 0 10 0 0 10 30 10 20 0 0 0 20 10 0 0 0 10 10 0 0 0 0 860
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 290ms usr + 430ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 43ms (0.1%)
    Wifi Scan (blamed): 1s 219ms (2.0%) 2x
    Wifi Scan (actual): 1s 219ms (2.0%) 2x
    Background Wifi Scan: 403ms (0.7%) 0x
       WiFi Sleep time:  59s 805ms (98.1%)
       WiFi Idle time:   1s 112ms (1.8%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     45ms (0.1%)
    Wake lock NlpWakeLock: 38ms partial (7 times) max=14 actual=47, 6ms background partial (2 times) max=5 realtime
    Wake lock *alarm*: 10ms partial (3 times) max=6 actual=13 realtime
    Wake lock fiid-sync: 9ms partial (1 times) max=9 realtime
    TOTAL wake: 57ms blamed partial, 62ms actual partial, 6ms actual background partial realtime
    Sensor GPS: 1m 0s 349ms realtime (1 times), 28ms background (0 times)
    Foreground services: 1m 0s 500ms realtime (1 times)
    Fg Service for: 1m 0s 500ms 
    Cached for: 31ms 
    Total running: 1m 0s 531ms 
    Total cpu time: u=1m 24s 927ms s=11s 477ms 
    Total cpu time per freq: 170 70 80 60 50 100 90 210 210 140 10 0 20 0 30 20 80 80 100 20 10 170 2910 40 240 210 30 90 110 120 200 360 640 1150 1140 2670 1340 420 240 80 90 40 70 100 70 110 80 120 80 70 30 40 196830
    Total screen-off cpu time per freq: 170 70 80 60 50 100 90 210 210 140 10 0 20 0 30 20 80 80 100 20 10 170 2910 40 240 210 30 90 110 120 200 360 640 1150 1140 2670 1340 420 240 80 90 40 70 100 70 110 80 120 80 70 30 40 196830
    Proc cgeo.geocaching:
      CPU: 54s 60ms usr + 9s 410ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 3 times

