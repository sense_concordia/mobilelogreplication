Daily stats:
  Current start time: 2018-08-02-03-08-54
  Next min deadline: 2018-08-03-01-00-00
  Next max deadline: 2018-08-03-03-00-00
  Current daily steps:
    Discharge total time: 22h 31m 21s 500ms  (from 5 steps)
    Discharge screen doze time: 22h 31m 21s 500ms  (from 5 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 2s 210ms (100.0%) realtime, 1m 2s 210ms (100.0%) uptime
  Time on battery screen off: 1m 2s 210ms (100.0%) realtime, 1m 2s 210ms (100.0%) uptime
  Time on battery screen doze: 1m 2s 210ms (100.0%)
  Total run time: 1m 2s 233ms realtime, 1m 2s 233ms uptime
  Discharge: 9.04 mAh
  Screen off discharge: 9.04 mAh
  Screen doze discharge: 9.04 mAh
  Start clock time: 2018-08-02-11-04-08
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 2s 210ms (100.0%) 0x
  Idle mode light time: 1m 2s 210ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 302ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 2s 210ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 2s 210ms (100.0%) 
     Cellular Sleep time:  39s 341ms (63.2%)
     Cellular Idle time:   22s 325ms (35.9%)
     Cellular Rx time:     545ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 2s 210ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 1s 145ms (98.3%)
     WiFi Idle time:   1s 50ms (1.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     17ms (0.0%)
     WiFi Battery drain: 0.00147mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 2s 212ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.26, actual drain: 0
    Uid u0a144: 5.69 ( cpu=5.18 wake=0.0000349 wifi=0.00147 gps=0.511 ) Including smearing: 8.99 ( proportional=3.29 )
    Uid 1036: 2.33 ( cpu=2.33 ) Excluded from smearing
    Uid 0: 0.723 ( cpu=0.674 wake=0.0491 ) Excluded from smearing
    Cell standby: 0.190 ( radio=0.190 ) Excluded from smearing
    Idle: 0.170 Excluded from smearing
    Uid 1000: 0.0768 ( cpu=0.0753 wake=0.0000151 sensor=0.00149 ) Excluded from smearing
    Uid 2000: 0.0245 ( cpu=0.0245 ) Excluded from smearing
    Uid u0a47: 0.0216 ( cpu=0.0215 wake=0.0000975 ) Excluded from smearing
    Uid u0a22: 0.0108 ( cpu=0.0107 wake=0.0000262 ) Excluded from smearing
    Uid 1021: 0.00945 ( cpu=0.00945 ) Excluded from smearing
    Uid 1001: 0.00220 ( cpu=0.00214 wake=0.0000650 ) Excluded from smearing
    Wifi: 0.00179 ( cpu=0.00179 ) Including smearing: 0.00282 ( proportional=0.00103 )
    Uid u0a127: 0.00136 ( cpu=0.00136 ) Including smearing: 0.00215 ( proportional=0.000788 )
    Uid u0a40: 0.00124 ( cpu=0.00124 ) Including smearing: 0.00196 ( proportional=0.000719 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 0s 870ms realtime (6 times)
    XO_shutdown.MPSS: 644ms realtime (4 times)
    XO_shutdown.ADSP: 1m 2s 84ms realtime (4 times)
    wlan.Active: 1s 235ms realtime (6 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 14ms partial (2 times) max=15 actual=21 realtime
    Wake lock GnssLocationProvider: 5ms partial (3 times) max=2 realtime
    TOTAL wake: 19ms blamed partial, 26ms actual partial realtime
    Sensor 17: 21s 454ms realtime (1 times)
    Fg Service for: 1m 2s 206ms 
    Total running: 1m 2s 206ms 
    Total cpu time: u=614ms s=720ms 
    Total cpu time per freq: 400 30 40 110 50 20 80 50 30 10 90 30 10 30 50 0 0 0 20 20 0 80 20 20 30 20 0 0 10 10 20 0 10 10 0 10 0 0 0 0 0 0 0 0 0 10 0 0 10 0 0 0 400
    Total screen-off cpu time per freq: 400 30 40 110 50 20 80 50 30 10 90 30 10 30 50 0 0 0 20 20 0 80 20 20 30 20 0 0 10 10 20 0 10 10 0 10 0 0 0 0 0 0 0 0 0 10 0 0 10 0 0 0 400
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 440ms usr + 490ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.deep: 1 times
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 41ms (0.1%)
    Wifi Scan (blamed): 1s 131ms (1.8%) 2x
    Wifi Scan (actual): 1s 131ms (1.8%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 1s 153ms (98.3%)
       WiFi Idle time:   1s 50ms (1.7%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     17ms (0.0%)
    Wake lock NlpWakeLock: 33ms partial (7 times) max=11 actual=48, 9ms background partial (2 times) max=7 realtime
    Wake lock *alarm*: 5ms partial (3 times) max=4 actual=7 realtime
    Wake lock fiid-sync: 6ms partial (1 times) max=6 realtime
    TOTAL wake: 44ms blamed partial, 57ms actual partial, 9ms actual background partial realtime
    Sensor GPS: 1m 1s 344ms realtime (1 times), 24ms background (0 times)
    Foreground services: 1m 1s 566ms realtime (1 times)
    Fg Service for: 1m 1s 566ms 
    Cached for: 127ms 
    Total running: 1m 1s 693ms 
    Total cpu time: u=1m 14s 470ms s=11s 387ms 
    Total cpu time per freq: 20 0 0 60 400 160 130 310 450 260 20 20 10 0 20 0 20 10 10 20 0 50 1450 90 240 60 50 20 70 50 100 140 180 510 390 440 300 150 50 70 40 20 10 30 60 50 20 40 30 60 10 20 78930
    Total screen-off cpu time per freq: 20 0 0 60 400 160 130 310 450 260 20 20 10 0 20 0 20 10 10 20 0 50 1450 90 240 60 50 20 70 50 100 140 180 510 390 440 300 150 50 70 40 20 10 30 60 50 20 40 30 60 10 20 78930
    Proc cgeo.geocaching:
      CPU: 59s 650ms usr + 10s 730ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 3 times

