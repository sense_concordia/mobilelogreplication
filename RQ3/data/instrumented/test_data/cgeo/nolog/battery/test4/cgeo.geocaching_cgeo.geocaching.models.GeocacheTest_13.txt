Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 12h 43m 6s 800ms  (from 103 steps)
    Discharge screen doze time: 11h 11m 35s 100ms  (from 88 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 59s 893ms (99.9%) realtime, 59s 893ms (100.0%) uptime
  Time on battery screen off: 59s 893ms (100.0%) realtime, 59s 893ms (100.0%) uptime
  Time on battery screen doze: 59s 893ms (100.0%)
  Total run time: 59s 925ms realtime, 59s 925ms uptime
  Discharge: 9.84 mAh
  Screen off discharge: 9.84 mAh
  Screen doze discharge: 9.84 mAh
  Start clock time: 2018-07-31-02-41-13
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 59s 893ms (100.0%) 0x
  Idle mode full time: 59s 893ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 869ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 59s 893ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  59s 893ms (100.0%) 
     Cellular Sleep time:  59s 286ms (99.0%)
     Cellular Idle time:   180ms (0.3%)
     Cellular Rx time:     428ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 59s 893ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  59s 894ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  59s 894ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.51, actual drain: 0
    Uid u0a140: 1.36 ( cpu=1.36 wake=0.000189 ) Including smearing: 2.24 ( proportional=0.871 )
    Uid 0: 0.434 ( cpu=0.387 wake=0.0468 ) Excluded from smearing
    Cell standby: 0.183 ( radio=0.183 ) Excluded from smearing
    Idle: 0.163 Excluded from smearing
    Uid 1000: 0.163 ( cpu=0.163 wake=0.0000428 ) Excluded from smearing
    Uid u0a47: 0.0808 ( cpu=0.0807 wake=0.000123 ) Excluded from smearing
    Uid 1027: 0.0546 ( cpu=0.0546 ) Excluded from smearing
    Uid 2000: 0.0294 ( cpu=0.0294 ) Excluded from smearing
    Uid u0a22: 0.0245 ( cpu=0.0242 wake=0.000327 ) Excluded from smearing
    Uid 1036: 0.00615 ( cpu=0.00615 ) Excluded from smearing
    Uid 1001: 0.00106 ( cpu=0.00105 wake=0.00000634 ) Excluded from smearing
    Uid u0a40: 0.000845 ( cpu=0.000845 ) Including smearing: 0.00138 ( proportional=0.000539 )
    Uid u0a127: 0.000759 ( cpu=0.000759 ) Including smearing: 0.00124 ( proportional=0.000484 )
    Wifi: 0.000324 ( cpu=0.000324 ) Including smearing: 0.000531 ( proportional=0.000207 )
    Uid u0a139: 0.0000913 ( cpu=0.0000913 ) Including smearing: 0.000150 ( proportional=0.0000583 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 786ms realtime (2 times)
    XO_shutdown.MPSS: 59s 302ms realtime (25 times)
    XO_shutdown.ADSP: 59s 800ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 54ms partial (41 times) max=6 actual=56 realtime
    Fg Service for: 59s 885ms 
    Total running: 59s 885ms 
    Total cpu time: u=2s 836ms s=2s 230ms 
    Total cpu time per freq: 1240 70 70 90 60 90 110 80 50 60 160 60 140 130 180 70 100 60 120 70 20 370 140 80 90 20 70 70 90 50 70 50 60 40 30 50 10 30 10 20 0 0 30 20 0 40 20 10 10 0 0 0 160
    Total screen-off cpu time per freq: 1240 70 70 90 60 90 110 80 50 60 160 60 140 130 180 70 100 60 120 70 20 370 140 80 90 20 70 70 90 50 70 50 60 40 30 50 10 30 10 20 0 0 30 20 0 40 20 10 10 0 0 0 160
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 1s 790ms usr + 1s 210ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 20ms usr + 20ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 80ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 235ms partial (114 times) max=14 actual=287, 7ms background partial (3 times) max=5 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 239ms blamed partial, 291ms actual partial, 7ms actual background partial realtime
    Foreground services: 59s 299ms realtime (1 times)
    Fg Service for: 59s 299ms 
    Cached for: 105ms 
    Total running: 59s 404ms 
    Total cpu time: u=33s 893ms s=9s 737ms 
    Total cpu time per freq: 2380 210 210 350 260 240 390 720 1180 1880 2770 1360 1540 1260 1010 1010 580 740 740 710 330 870 1610 710 1040 1200 2050 3040 3240 2150 1250 810 1470 1170 380 220 280 160 130 90 100 80 50 110 10 10 110 70 30 30 40 0 1830
    Total screen-off cpu time per freq: 2380 210 210 350 260 240 390 720 1180 1880 2770 1360 1540 1260 1010 1010 580 740 740 710 330 870 1610 710 1040 1200 2050 3040 3240 2150 1250 810 1470 1170 380 220 280 160 130 90 100 80 50 110 10 10 110 70 30 30 40 0 1830
    Proc cgeo.geocaching:
      CPU: 27s 210ms usr + 8s 430ms krn ; 0ms fg
      1 starts

