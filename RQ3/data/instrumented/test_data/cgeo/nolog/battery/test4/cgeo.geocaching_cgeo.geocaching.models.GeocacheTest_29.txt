Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 3s 154ms (100.0%) realtime, 1m 3s 154ms (100.0%) uptime
  Time on battery screen off: 1m 3s 154ms (100.0%) realtime, 1m 3s 154ms (100.0%) uptime
  Time on battery screen doze: 1m 3s 154ms (100.0%)
  Total run time: 1m 3s 185ms realtime, 1m 3s 185ms uptime
  Discharge: 9.63 mAh
  Screen off discharge: 9.63 mAh
  Screen doze discharge: 9.63 mAh
  Start clock time: 2018-07-31-03-00-12
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 3s 154ms (100.0%) 0x
  Idle mode full time: 1m 3s 154ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 936ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 3s 154ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 3s 154ms (100.0%) 
     Cellular Sleep time:  1m 2s 577ms (99.1%)
     Cellular Idle time:   189ms (0.3%)
     Cellular Rx time:     389ms (0.6%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 3s 154ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 3s 155ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 3s 155ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.58, actual drain: 0
    Uid u0a140: 1.41 ( cpu=1.41 wake=0.000167 ) Including smearing: 2.30 ( proportional=0.888 )
    Uid 0: 0.454 ( cpu=0.404 wake=0.0493 ) Excluded from smearing
    Cell standby: 0.193 ( radio=0.193 ) Excluded from smearing
    Idle: 0.172 Excluded from smearing
    Uid 1000: 0.157 ( cpu=0.157 wake=0.0000642 ) Excluded from smearing
    Uid u0a47: 0.0764 ( cpu=0.0762 wake=0.000113 ) Excluded from smearing
    Uid 1027: 0.0541 ( cpu=0.0541 ) Excluded from smearing
    Uid 2000: 0.0297 ( cpu=0.0297 ) Excluded from smearing
    Uid u0a22: 0.0258 ( cpu=0.0254 wake=0.000333 ) Excluded from smearing
    Uid 1036: 0.00643 ( cpu=0.00643 ) Excluded from smearing
    Uid u0a127: 0.000843 ( cpu=0.000843 ) Including smearing: 0.00137 ( proportional=0.000530 )
    Uid 1001: 0.000836 ( cpu=0.000771 wake=0.0000650 ) Excluded from smearing
    Uid u0a40: 0.000574 ( cpu=0.000574 ) Including smearing: 0.000934 ( proportional=0.000360 )
    Wifi: 0.000301 ( cpu=0.000301 ) Including smearing: 0.000490 ( proportional=0.000189 )
    Uid u0a139: 0.000297 ( cpu=0.000297 ) Including smearing: 0.000483 ( proportional=0.000186 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 3s 56ms realtime (2 times)
    XO_shutdown.MPSS: 1m 2s 611ms realtime (27 times)
    XO_shutdown.ADSP: 1m 3s 100ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 81ms partial (43 times) max=29 actual=101 realtime
    Fg Service for: 1m 3s 146ms 
    Total running: 1m 3s 146ms 
    Total cpu time: u=2s 673ms s=2s 213ms 
    Total cpu time per freq: 1310 130 40 140 70 130 50 130 100 70 130 90 160 100 100 60 100 90 110 60 40 310 110 50 50 130 60 60 70 60 70 60 40 20 30 40 20 30 40 20 10 10 30 10 0 0 10 0 10 0 0 10 280
    Total screen-off cpu time per freq: 1310 130 40 140 70 130 50 130 100 70 130 90 160 100 100 60 100 90 110 60 40 310 110 50 50 130 60 60 70 60 70 60 40 20 30 40 20 30 40 20 10 10 30 10 0 0 10 0 10 0 0 10 280
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 1s 690ms usr + 1s 130ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 30ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 85ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 205ms partial (111 times) max=18 actual=262, 18ms background partial (0 times) max=18 realtime
    Wake lock fiid-sync: 5ms partial (1 times) max=5 realtime
    TOTAL wake: 210ms blamed partial, 267ms actual partial, 18ms actual background partial realtime
    Foreground services: 1m 2s 504ms realtime (1 times)
    Fg Service for: 1m 2s 504ms 
    Cached for: 103ms 
    Total running: 1m 2s 607ms 
    Total cpu time: u=35s 233ms s=10s 546ms 
    Total cpu time per freq: 2700 220 220 500 290 250 470 710 1300 1960 1920 1270 1320 1060 730 780 580 690 800 540 460 910 3270 1720 1640 1940 2480 2380 2460 2430 1460 740 1340 1180 370 360 220 150 180 170 100 80 20 20 0 50 30 40 0 10 10 10 1940
    Total screen-off cpu time per freq: 2700 220 220 500 290 250 470 710 1300 1960 1920 1270 1320 1060 730 780 580 690 800 540 460 910 3270 1720 1640 1940 2480 2380 2460 2430 1460 740 1340 1180 370 360 220 150 180 170 100 80 20 20 0 50 30 40 0 10 10 10 1940
    Proc cgeo.geocaching:
      CPU: 27s 470ms usr + 8s 890ms krn ; 0ms fg
      1 starts

