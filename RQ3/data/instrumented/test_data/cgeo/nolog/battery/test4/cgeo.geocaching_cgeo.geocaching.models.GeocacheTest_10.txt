Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 12h 43m 6s 800ms  (from 103 steps)
    Discharge screen doze time: 11h 11m 35s 100ms  (from 88 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 705ms (100.0%) realtime, 1m 0s 705ms (100.0%) uptime
  Time on battery screen off: 1m 0s 705ms (100.0%) realtime, 1m 0s 705ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 705ms (100.0%)
  Total run time: 1m 0s 728ms realtime, 1m 0s 728ms uptime
  Discharge: 9.60 mAh
  Screen off discharge: 9.60 mAh
  Screen doze discharge: 9.60 mAh
  Start clock time: 2018-07-31-02-37-39
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 0s 705ms (100.0%) 0x
  Idle mode full time: 1m 0s 705ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 917ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 705ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 705ms (100.0%) 
     Cellular Sleep time:  1m 0s 75ms (99.0%)
     Cellular Idle time:   226ms (0.4%)
     Cellular Rx time:     406ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 705ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 0s 708ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 708ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.51, actual drain: 0-35.2
    Uid u0a140: 1.37 ( cpu=1.37 wake=0.000182 ) Including smearing: 2.24 ( proportional=0.867 )
    Uid 0: 0.430 ( cpu=0.382 wake=0.0474 ) Excluded from smearing
    Cell standby: 0.185 ( radio=0.185 ) Excluded from smearing
    Idle: 0.165 Excluded from smearing
    Uid 1000: 0.158 ( cpu=0.158 wake=0.0000452 ) Excluded from smearing
    Uid u0a47: 0.0804 ( cpu=0.0803 wake=0.000119 ) Excluded from smearing
    Uid 1027: 0.0530 ( cpu=0.0530 ) Excluded from smearing
    Uid 2000: 0.0308 ( cpu=0.0308 ) Excluded from smearing
    Uid u0a22: 0.0276 ( cpu=0.0273 wake=0.000337 ) Excluded from smearing
    Uid 1036: 0.00690 ( cpu=0.00690 ) Excluded from smearing
    Uid 1001: 0.00105 ( cpu=0.00101 wake=0.0000428 ) Excluded from smearing
    Uid u0a127: 0.000874 ( cpu=0.000874 ) Including smearing: 0.00143 ( proportional=0.000553 )
    Uid u0a40: 0.000812 ( cpu=0.000812 ) Including smearing: 0.00132 ( proportional=0.000513 )
    Wifi: 0.000334 ( cpu=0.000334 ) Including smearing: 0.000545 ( proportional=0.000211 )
    Uid u0a139: 0.000174 ( cpu=0.000174 ) Including smearing: 0.000283 ( proportional=0.000110 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 0s 572ms realtime (4 times)
    XO_shutdown.MPSS: 1m 0s 69ms realtime (28 times)
    XO_shutdown.ADSP: 1m 0s 625ms realtime (0 times)
    wlan.Active: 54ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 58ms partial (39 times) max=7 actual=64 realtime
    Fg Service for: 1m 0s 699ms 
    Total running: 1m 0s 699ms 
    Total cpu time: u=2s 674ms s=2s 213ms 
    Total cpu time per freq: 1300 110 60 120 60 110 90 120 110 150 90 60 90 110 130 100 50 150 40 120 50 440 130 60 70 60 50 100 70 60 40 30 80 20 10 90 10 20 10 20 0 0 0 0 20 10 0 10 30 0 0 0 230
    Total screen-off cpu time per freq: 1300 110 60 120 60 110 90 120 110 150 90 60 90 110 130 100 50 150 40 120 50 440 130 60 70 60 50 100 70 60 40 30 80 20 10 90 10 20 10 20 0 0 0 0 20 10 0 10 30 0 0 0 230
    Proc servicemanager:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 70ms krn ; 0ms fg
    Proc system:
      CPU: 1s 880ms usr + 1s 110ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 20ms usr + 20ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 83ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 219ms partial (111 times) max=10 actual=277, 10ms background partial (2 times) max=8 realtime
    Wake lock fiid-sync: 10ms partial (1 times) max=10 realtime
    TOTAL wake: 229ms blamed partial, 287ms actual partial, 10ms actual background partial realtime
    Foreground services: 1m 0s 102ms realtime (1 times)
    Fg Service for: 1m 0s 102ms 
    Cached for: 103ms 
    Total running: 1m 0s 205ms 
    Total cpu time: u=33s 844ms s=10s 173ms 
    Total cpu time per freq: 2780 360 210 390 270 340 340 680 1300 2260 2020 1140 1170 1020 930 840 640 1010 790 660 370 1210 2340 1070 1260 1550 1960 2560 2730 1960 1290 760 1350 1070 520 650 270 110 170 60 100 0 20 40 120 10 30 20 20 40 20 20 2070
    Total screen-off cpu time per freq: 2780 360 210 390 270 340 340 680 1300 2260 2020 1140 1170 1020 930 840 640 1010 790 660 370 1210 2340 1070 1260 1550 1960 2560 2730 1960 1290 760 1350 1070 520 650 270 110 170 60 100 0 20 40 120 10 30 20 20 40 20 20 2070
    Proc cgeo.geocaching:
      CPU: 29s 740ms usr + 9s 290ms krn ; 0ms fg
      1 starts

