Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 12h 34m 7s 500ms  (from 106 steps)
    Discharge screen doze time: 11h 4m 8s 0ms  (from 91 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 0s 3ms (100.0%) realtime, 1m 0s 3ms (100.0%) uptime
  Time on battery screen off: 1m 0s 3ms (100.0%) realtime, 1m 0s 3ms (100.0%) uptime
  Time on battery screen doze: 1m 0s 3ms (100.0%)
  Total run time: 1m 0s 33ms realtime, 1m 0s 33ms uptime
  Discharge: 9.25 mAh
  Screen off discharge: 9.25 mAh
  Screen doze discharge: 9.25 mAh
  Start clock time: 2018-07-31-02-53-07
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 0s 3ms (100.0%) 0x
  Idle mode full time: 1m 0s 3ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 1s 21ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 0s 3ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 0s 3ms (100.0%) 
     Cellular Sleep time:  59s 366ms (98.9%)
     Cellular Idle time:   232ms (0.4%)
     Cellular Rx time:     406ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 0s 3ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 0s 4ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 0s 4ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.46, actual drain: 0
    Uid u0a140: 1.36 ( cpu=1.36 wake=0.000201 ) Including smearing: 2.19 ( proportional=0.834 )
    Uid 0: 0.413 ( cpu=0.366 wake=0.0468 ) Excluded from smearing
    Cell standby: 0.183 ( radio=0.183 ) Excluded from smearing
    Idle: 0.164 Excluded from smearing
    Uid 1000: 0.152 ( cpu=0.152 wake=0.0000492 ) Excluded from smearing
    Uid u0a47: 0.0741 ( cpu=0.0739 wake=0.000121 ) Excluded from smearing
    Uid 1027: 0.0468 ( cpu=0.0468 ) Excluded from smearing
    Uid 2000: 0.0305 ( cpu=0.0305 ) Excluded from smearing
    Uid u0a22: 0.0274 ( cpu=0.0271 wake=0.000333 ) Excluded from smearing
    Uid 1036: 0.00760 ( cpu=0.00760 ) Excluded from smearing
    Uid 1001: 0.00111 ( cpu=0.00100 wake=0.000103 ) Excluded from smearing
    Uid u0a127: 0.000930 ( cpu=0.000930 ) Including smearing: 0.00150 ( proportional=0.000570 )
    Uid u0a40: 0.000892 ( cpu=0.000892 ) Including smearing: 0.00144 ( proportional=0.000547 )
    Uid u0a116: 0.000336 ( cpu=0.000336 ) Including smearing: 0.000542 ( proportional=0.000206 )
    Wifi: 0.000269 ( cpu=0.000269 ) Including smearing: 0.000433 ( proportional=0.000165 )
    Uid u0a139: 0.000182 ( cpu=0.000182 ) Including smearing: 0.000293 ( proportional=0.000111 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 59s 873ms realtime (4 times)
    XO_shutdown.MPSS: 59s 341ms realtime (28 times)
    XO_shutdown.ADSP: 59s 912ms realtime (0 times)
    wlan.Active: 45ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 3ms partial (1 times) max=7 actual=7 realtime
    Wake lock NetworkStats: 4ms partial (1 times) max=10 actual=10 realtime
    Wake lock GnssLocationProvider: 56ms partial (41 times) max=8 actual=61 realtime
    TOTAL wake: 63ms blamed partial, 73ms actual partial realtime
    Fg Service for: 59s 996ms 
    Total running: 59s 996ms 
    Total cpu time: u=2s 606ms s=2s 64ms 
    Total cpu time per freq: 1230 110 50 90 50 100 60 170 120 140 100 120 90 140 120 110 50 50 120 190 20 250 100 70 60 100 60 100 110 60 30 40 30 40 20 50 10 40 0 10 10 20 20 20 0 0 20 0 0 20 10 0 240
    Total screen-off cpu time per freq: 1230 110 50 90 50 100 60 170 120 140 100 120 90 140 120 110 50 50 120 190 20 250 100 70 60 100 60 100 110 60 30 40 30 40 20 50 10 40 0 10 10 20 20 20 0 0 20 0 0 20 10 0 240
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 1s 780ms usr + 1s 130ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 119ms (0.2%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 250ms partial (109 times) max=24 actual=305, 28ms background partial (2 times) max=21 realtime
    Wake lock fiid-sync: 5ms partial (1 times) max=5 realtime
    TOTAL wake: 255ms blamed partial, 310ms actual partial, 28ms actual background partial realtime
    Foreground services: 59s 381ms realtime (1 times)
    Fg Service for: 59s 381ms 
    Cached for: 121ms 
    Total running: 59s 502ms 
    Total cpu time: u=33s 557ms s=10s 110ms 
    Total cpu time per freq: 2560 220 160 480 180 310 470 710 1410 2370 2170 1180 1620 1060 880 960 490 630 1030 730 410 870 1540 710 970 1390 1880 2910 3030 1830 1690 980 1000 1320 620 450 260 120 190 60 70 70 60 50 50 30 70 40 10 80 10 0 1880
    Total screen-off cpu time per freq: 2560 220 160 480 180 310 470 710 1410 2370 2170 1180 1620 1060 880 960 490 630 1030 730 410 870 1540 710 970 1390 1880 2910 3030 1830 1690 980 1000 1320 620 450 260 120 190 60 70 70 60 50 50 30 70 40 10 80 10 0 1880
    Proc cgeo.geocaching:
      CPU: 25s 740ms usr + 8s 490ms krn ; 0ms fg
      1 starts

