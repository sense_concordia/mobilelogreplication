Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 12h 53m 58s 200ms  (from 100 steps)
    Discharge screen doze time: 11h 21m 7s 700ms  (from 85 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 1s 995ms (100.0%) realtime, 1m 1s 995ms (100.0%) uptime
  Time on battery screen off: 1m 1s 995ms (100.0%) realtime, 1m 1s 995ms (100.0%) uptime
  Time on battery screen doze: 1m 1s 995ms (100.0%)
  Total run time: 1m 2s 25ms realtime, 1m 2s 25ms uptime
  Discharge: 9.01 mAh
  Screen off discharge: 9.01 mAh
  Screen doze discharge: 9.01 mAh
  Start clock time: 2018-07-31-02-28-07
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 1s 995ms (100.0%) 0x
  Idle mode full time: 1m 1s 995ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 972ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 1s 995ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 1s 995ms (100.0%) 
     Cellular Sleep time:  1m 1s 373ms (99.0%)
     Cellular Idle time:   182ms (0.3%)
     Cellular Rx time:     443ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 1s 995ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 2s 0ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 2s 0ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.55, actual drain: 0
    Uid u0a140: 1.39 ( cpu=1.39 wake=0.000149 ) Including smearing: 2.27 ( proportional=0.884 )
    Uid 0: 0.445 ( cpu=0.396 wake=0.0484 ) Excluded from smearing
    Cell standby: 0.189 ( radio=0.189 ) Excluded from smearing
    Idle: 0.169 Excluded from smearing
    Uid 1000: 0.161 ( cpu=0.161 wake=0.000152 ) Excluded from smearing
    Uid u0a47: 0.0763 ( cpu=0.0762 wake=0.000125 ) Excluded from smearing
    Uid 1027: 0.0555 ( cpu=0.0555 ) Excluded from smearing
    Uid 2000: 0.0317 ( cpu=0.0317 ) Excluded from smearing
    Uid u0a22: 0.0269 ( cpu=0.0265 wake=0.000328 ) Excluded from smearing
    Uid 1036: 0.00634 ( cpu=0.00634 ) Excluded from smearing
    Uid u0a40: 0.000670 ( cpu=0.000670 ) Including smearing: 0.00110 ( proportional=0.000427 )
    Uid u0a127: 0.000669 ( cpu=0.000669 ) Including smearing: 0.00110 ( proportional=0.000426 )
    Uid 1001: 0.000629 ( cpu=0.000609 wake=0.0000198 ) Excluded from smearing
    Wifi: 0.000498 ( cpu=0.000498 ) Including smearing: 0.000816 ( proportional=0.000317 )
    Uid u0a139: 0.000450 ( cpu=0.000450 ) Including smearing: 0.000736 ( proportional=0.000286 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 1s 847ms realtime (2 times)
    XO_shutdown.MPSS: 1m 1s 377ms realtime (26 times)
    XO_shutdown.ADSP: 1m 1s 866ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 187ms partial (42 times) max=98 actual=232 (running for 98ms) realtime
    Fg Service for: 1m 1s 988ms 
    Total running: 1m 1s 988ms 
    Total cpu time: u=2s 746ms s=2s 310ms 
    Total cpu time per freq: 1270 80 80 170 100 50 100 120 140 70 80 100 130 160 100 180 80 50 110 60 40 380 130 40 80 50 80 70 80 50 60 10 30 60 10 50 20 10 0 20 20 20 0 0 10 0 0 0 20 10 0 0 260
    Total screen-off cpu time per freq: 1270 80 80 170 100 50 100 120 140 70 80 100 130 160 100 180 80 50 110 60 40 380 130 40 80 50 80 70 80 50 60 10 30 60 10 50 20 10 0 20 20 20 0 0 10 0 0 0 20 10 0 0 260
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 1s 830ms usr + 1s 200ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 64ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 185ms partial (110 times) max=8 actual=248 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 189ms blamed partial, 252ms actual partial realtime
    Foreground services: 1m 1s 415ms realtime (1 times)
    Fg Service for: 1m 1s 415ms 
    Cached for: 47ms 
    Total running: 1m 1s 462ms 
    Total cpu time: u=34s 867ms s=10s 76ms 
    Total cpu time per freq: 2700 350 110 350 370 150 320 580 1410 2060 1890 1120 1260 1180 890 740 570 530 920 720 410 930 3030 1250 1780 1620 2370 2860 2500 2050 1480 890 1240 1160 360 410 300 120 120 90 70 20 80 30 20 10 0 40 20 30 0 0 2070
    Total screen-off cpu time per freq: 2700 350 110 350 370 150 320 580 1410 2060 1890 1120 1260 1180 890 740 570 530 920 720 410 930 3030 1250 1780 1620 2370 2860 2500 2050 1480 890 1240 1160 360 410 300 120 120 90 70 20 80 30 20 10 0 40 20 30 0 0 2070
    Proc cgeo.geocaching:
      CPU: 29s 240ms usr + 8s 800ms krn ; 0ms fg
      1 starts

