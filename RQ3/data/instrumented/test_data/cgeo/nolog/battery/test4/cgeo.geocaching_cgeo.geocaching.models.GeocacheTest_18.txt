Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 12h 40m 43s 500ms  (from 104 steps)
    Discharge screen doze time: 11h 9m 49s 400ms  (from 89 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 1s 512ms (99.9%) realtime, 1m 1s 513ms (100.0%) uptime
  Time on battery screen off: 1m 1s 512ms (100.0%) realtime, 1m 1s 513ms (100.0%) uptime
  Time on battery screen doze: 1m 1s 512ms (100.0%)
  Total run time: 1m 1s 543ms realtime, 1m 1s 544ms uptime
  Discharge: 9.17 mAh
  Screen off discharge: 9.17 mAh
  Screen doze discharge: 9.17 mAh
  Start clock time: 2018-07-31-02-47-09
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 1s 512ms (100.0%) 0x
  Idle mode full time: 1m 1s 512ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 931ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 1s 512ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 1s 512ms (100.0%) 
     Cellular Sleep time:  1m 0s 928ms (99.0%)
     Cellular Idle time:   172ms (0.3%)
     Cellular Rx time:     415ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 1s 512ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 1s 516ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 1s 517ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.51, actual drain: 0
    Uid u0a140: 1.39 ( cpu=1.39 wake=0.000189 ) Including smearing: 2.24 ( proportional=0.848 )
    Uid 0: 0.431 ( cpu=0.383 wake=0.0480 ) Excluded from smearing
    Cell standby: 0.188 ( radio=0.188 ) Excluded from smearing
    Idle: 0.168 Excluded from smearing
    Uid 1000: 0.151 ( cpu=0.150 wake=0.0000912 ) Excluded from smearing
    Uid u0a47: 0.0694 ( cpu=0.0693 wake=0.000123 ) Excluded from smearing
    Uid 1027: 0.0492 ( cpu=0.0492 ) Excluded from smearing
    Uid 2000: 0.0298 ( cpu=0.0298 ) Excluded from smearing
    Uid u0a22: 0.0257 ( cpu=0.0253 wake=0.000331 ) Excluded from smearing
    Uid 1036: 0.00683 ( cpu=0.00683 ) Excluded from smearing
    Uid 1001: 0.000734 ( cpu=0.000730 wake=0.00000397 ) Excluded from smearing
    Uid u0a127: 0.000681 ( cpu=0.000681 ) Including smearing: 0.00110 ( proportional=0.000415 )
    Uid u0a139: 0.000665 ( cpu=0.000665 ) Including smearing: 0.00107 ( proportional=0.000404 )
    Uid u0a40: 0.000620 ( cpu=0.000620 ) Including smearing: 0.000997 ( proportional=0.000377 )
    Wifi: 0.000298 ( cpu=0.000298 ) Including smearing: 0.000479 ( proportional=0.000181 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 1s 404ms realtime (2 times)
    XO_shutdown.MPSS: 1m 0s 919ms realtime (26 times)
    XO_shutdown.ADSP: 1m 1s 421ms realtime (0 times)
    wlan.Active: 23ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 115ms partial (49 times) max=59 actual=122 realtime
    Fg Service for: 1m 1s 503ms 
    Total running: 1m 1s 503ms 
    Total cpu time: u=2s 634ms s=1s 990ms 
    Total cpu time per freq: 1350 90 80 90 50 60 120 150 90 150 100 70 150 100 100 80 120 120 70 40 60 300 80 70 50 50 70 60 70 40 60 60 70 30 30 40 10 30 0 50 10 0 0 20 20 10 0 30 30 0 0 0 250
    Total screen-off cpu time per freq: 1350 90 80 90 50 60 120 150 90 150 100 70 150 100 100 80 120 120 70 40 60 300 80 70 50 50 70 60 70 40 60 60 70 30 30 40 10 30 0 50 10 0 0 20 20 10 0 30 30 0 0 0 250
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 1s 740ms usr + 1s 70ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 80ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 227ms partial (111 times) max=15 actual=288 realtime
    Wake lock fiid-sync: 11ms partial (1 times) max=11 realtime
    TOTAL wake: 238ms blamed partial, 299ms actual partial realtime
    Foreground services: 1m 0s 978ms realtime (1 times)
    Fg Service for: 1m 0s 978ms 
    Cached for: 52ms 
    Total running: 1m 1s 30ms 
    Total cpu time: u=34s 983ms s=9s 996ms 
    Total cpu time per freq: 3150 260 210 450 290 260 410 750 1210 2540 2080 1330 1050 1030 870 860 850 770 820 410 530 890 2610 1280 1340 1350 2110 2510 2660 2020 1590 920 1490 1170 600 360 170 150 130 80 20 20 20 30 90 40 20 90 80 10 0 0 1990
    Total screen-off cpu time per freq: 3150 260 210 450 290 260 410 750 1210 2540 2080 1330 1050 1030 870 860 850 770 820 410 530 890 2610 1280 1340 1350 2110 2510 2660 2020 1590 920 1490 1170 600 360 170 150 130 80 20 20 20 30 90 40 20 90 80 10 0 0 1990
    Proc cgeo.geocaching:
      CPU: 27s 580ms usr + 8s 420ms krn ; 0ms fg
      1 starts

