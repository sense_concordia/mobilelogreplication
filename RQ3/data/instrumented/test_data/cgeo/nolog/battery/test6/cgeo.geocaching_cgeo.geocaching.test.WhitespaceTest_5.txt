Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 55m 16s 0ms  (from 117 steps)
    Discharge screen doze time: 9h 52m 53s 600ms  (from 115 steps)
    Charge total time: 7h 24m 43s 200ms  (from 121 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 13m 5s 500ms  (from 77 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 5s 343ms (100.0%) realtime, 1m 5s 343ms (100.0%) uptime
  Time on battery screen off: 1m 5s 343ms (100.0%) realtime, 1m 5s 343ms (100.0%) uptime
  Time on battery screen doze: 1m 5s 343ms (100.0%)
  Total run time: 1m 5s 366ms realtime, 1m 5s 366ms uptime
  Discharge: 7.88 mAh
  Screen off discharge: 7.88 mAh
  Screen doze discharge: 7.88 mAh
  Start clock time: 2018-07-31-22-30-52
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 5s 343ms (100.0%) 0x
  Idle mode full time: 35s 275ms (54.0%) 1x -- longest 34s 850ms 
  Total partial wakelock time: 30s 350ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 5s 343ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 5s 342ms (100.0%) 
     Cellular Sleep time:  53s 238ms (81.5%)
     Cellular Idle time:   11s 593ms (17.7%)
     Cellular Rx time:     514ms (0.8%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 5s 342ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 4s 659ms (99.0%)
     WiFi Idle time:   675ms (1.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     11ms (0.0%)
     WiFi Battery drain: 0.000951mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 5s 345ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 5.99, actual drain: 0
    Uid u0a142: 5.04 ( cpu=4.79 wake=0.0000230 wifi=0.000951 gps=0.251 ) Including smearing: 5.78 ( proportional=0.734 )
    Uid 0: 0.358 ( cpu=0.330 wake=0.0278 ) Excluded from smearing
    Cell standby: 0.200 ( radio=0.200 ) Excluded from smearing
    Idle: 0.178 Excluded from smearing
    Uid 1000: 0.111 ( cpu=0.0870 wake=0.0239 ) Excluded from smearing
    Uid u0a47: 0.0367 ( cpu=0.0366 wake=0.000109 ) Excluded from smearing
    Uid 2000: 0.0315 ( cpu=0.0315 ) Excluded from smearing
    Uid u0a22: 0.0108 ( cpu=0.0108 wake=0.0000151 ) Excluded from smearing
    Uid 1036: 0.00700 ( cpu=0.00700 ) Excluded from smearing
    Uid 1021: 0.00559 ( cpu=0.00559 ) Excluded from smearing
    Uid u0a127: 0.00211 ( cpu=0.00211 ) Including smearing: 0.00242 ( proportional=0.000307 )
    Uid 1001: 0.00169 ( cpu=0.00168 wake=0.00000555 ) Excluded from smearing
    Wifi: 0.00160 ( cpu=0.00160 ) Including smearing: 0.00183 ( proportional=0.000233 )
    Uid u0a139: 0.00136 ( cpu=0.00136 ) Including smearing: 0.00156 ( proportional=0.000199 )
    Uid u0a40: 0.00120 ( cpu=0.00120 ) Including smearing: 0.00137 ( proportional=0.000175 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 4s 478ms realtime (5 times)
    XO_shutdown.MPSS: 34s 857ms realtime (17 times)
    XO_shutdown.ADSP: 1m 5s 284ms realtime (2 times)
    wlan.Active: 797ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 19ms partial (1 times) max=41 actual=41 realtime
    Wake lock *job*/android/com.android.server.PruneInstantAppsJobService: 1ms partial (1 times) max=1 realtime
    Wake lock GnssLocationProvider: 40ms partial (1 times) max=43 actual=43 realtime
    Wake lock deviceidle_maint: 30s 75ms partial (1 times) max=30122 actual=30122 realtime
    Wake lock deviceidle_going_idle: 22ms partial (1 times) max=22 realtime
    TOTAL wake: 30s 157ms blamed partial, 30s 190ms actual partial realtime
    Job android/com.android.server.PruneInstantAppsJobService: 2ms realtime (1 times)
    Job Completions android/com.android.server.PruneInstantAppsJobService: canceled(1x)
    Fg Service for: 1m 5s 337ms 
    Total running: 1m 5s 337ms 
    Total cpu time: u=703ms s=900ms 
    Total cpu time per freq: 500 40 60 70 50 50 70 20 0 50 10 60 0 0 0 60 0 10 0 20 0 70 30 20 10 10 0 0 10 0 0 10 10 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 390
    Total screen-off cpu time per freq: 500 40 60 70 50 50 70 20 0 50 10 60 0 0 0 60 0 10 0 20 0 70 30 20 10 10 0 0 10 0 0 10 10 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 390
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 110ms krn ; 0ms fg
    Proc system:
      CPU: 450ms usr + 490ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 20ms usr + 0ms krn ; 0ms fg
    Apk android:
      Wakeup alarm *walarm*:DeviceIdleController.deep: 1 times
      Service com.android.server.PruneInstantAppsJobService:
        Created for: 0ms uptime
        Starts: 0, launches: 1
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 29ms (0.0%)
    Wifi Scan (blamed): 725ms (1.1%) 1x
    Wifi Scan (actual): 725ms (1.1%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 4s 675ms (99.0%)
       WiFi Idle time:   675ms (1.0%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     11ms (0.0%)
    Wake lock NlpWakeLock: 16ms partial (4 times) max=30 actual=34 realtime
    Wake lock *alarm*: 8ms partial (2 times) max=12 actual=18 realtime
    Wake lock fiid-sync: 6ms partial (1 times) max=6 realtime
    TOTAL wake: 30ms blamed partial, 52ms actual partial realtime
    Sensor GPS: 30s 68ms realtime (1 times)
    Foreground services: 1m 4s 777ms realtime (1 times)
    Fg Service for: 1m 4s 777ms 
    Cached for: 53ms 
    Total running: 1m 4s 830ms 
    Total cpu time: u=1m 14s 480ms s=5s 276ms 
    Total cpu time per freq: 3700 470 290 990 320 220 380 10 10 40 30 10 10 0 0 0 50 0 0 0 0 160 1550 90 40 90 40 40 60 40 80 60 120 90 90 120 70 50 30 50 30 50 30 40 10 90 70 80 40 60 30 0 69420
    Total screen-off cpu time per freq: 3700 470 290 990 320 220 380 10 10 40 30 10 10 0 0 0 50 0 0 0 0 160 1550 90 40 90 40 40 60 40 80 60 120 90 90 120 70 50 30 50 30 50 30 40 10 90 70 80 40 60 30 0 69420
    Proc cgeo.geocaching:
      CPU: 1m 5s 880ms usr + 3s 740ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 3 times

