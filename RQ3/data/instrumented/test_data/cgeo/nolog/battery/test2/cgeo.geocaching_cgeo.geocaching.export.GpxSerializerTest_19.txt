Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 13h 34m 3s 700ms  (from 88 steps)
    Discharge screen doze time: 11h 54m 11s 700ms  (from 73 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 793ms (100.0%) realtime, 1m 8s 793ms (100.0%) uptime
  Time on battery screen off: 1m 8s 793ms (100.0%) realtime, 1m 8s 793ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 793ms (100.0%)
  Total run time: 1m 8s 823ms realtime, 1m 8s 823ms uptime
  Discharge: 9.98 mAh
  Screen off discharge: 9.98 mAh
  Screen doze discharge: 9.98 mAh
  Start clock time: 2018-07-31-01-32-48
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 8s 793ms (100.0%) 0x
  Idle mode light time: 1m 8s 793ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 274ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 793ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 793ms (100.0%) 
     Cellular Sleep time:  1m 6s 956ms (97.3%)
     Cellular Idle time:   1s 375ms (2.0%)
     Cellular Rx time:     463ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 793ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 8s 108ms (99.0%)
     WiFi Idle time:   675ms (1.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     11ms (0.0%)
     WiFi Battery drain: 0.000951mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 794ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.02, actual drain: 0
    Uid u0a140: 1.81 ( cpu=1.79 wake=0.0000127 wifi=0.000854 gps=0.0209 ) Including smearing: 2.73 ( proportional=0.923 )
    Uid 0: 0.530 ( cpu=0.475 wake=0.0543 ) Excluded from smearing
    Cell standby: 0.210 ( radio=0.210 ) Excluded from smearing
    Idle: 0.188 Excluded from smearing
    Uid 1000: 0.119 ( cpu=0.114 wake=0.0000357 sensor=0.00478 ) Excluded from smearing
    Uid u0a47: 0.0663 ( cpu=0.0662 wake=0.000106 ) Excluded from smearing
    Uid 1027: 0.0648 ( cpu=0.0648 ) Excluded from smearing
    Uid 2000: 0.0233 ( cpu=0.0233 ) Excluded from smearing
    Uid 1036: 0.00364 ( cpu=0.00364 ) Excluded from smearing
    Uid u0a22: 0.00152 ( cpu=0.00141 wake=0.0000159 wifi=0.0000975 ) Excluded from smearing
    Wifi: 0.00131 ( cpu=0.00131 ) Including smearing: 0.00198 ( proportional=0.000669 )
    Uid 1001: 0.00100 ( cpu=0.000957 wake=0.0000444 ) Excluded from smearing
    Uid u0a127: 0.000910 ( cpu=0.000910 ) Including smearing: 0.00137 ( proportional=0.000464 )
    Uid 1021: 0.000892 ( cpu=0.000892 ) Excluded from smearing
    Uid u0a40: 0.000765 ( cpu=0.000765 ) Including smearing: 0.00115 ( proportional=0.000390 )
    Uid u0a139: 0.000317 ( cpu=0.000317 ) Including smearing: 0.000478 ( proportional=0.000161 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 7s 930ms realtime (4 times)
    XO_shutdown.MPSS: 1m 5s 599ms realtime (30 times)
    XO_shutdown.ADSP: 1m 8s 699ms realtime (2 times)
    wlan.Active: 785ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 21ms partial (1 times) max=29 actual=29 realtime
    Wake lock GnssLocationProvider: 24ms partial (2 times) max=27 actual=35 realtime
    TOTAL wake: 45ms blamed partial, 64ms actual partial realtime
    Sensor 17: 1m 8s 786ms realtime (0 times)
    Fg Service for: 1m 8s 786ms 
    Total running: 1m 8s 786ms 
    Total cpu time: u=1s 630ms s=2s 47ms 
    Total cpu time per freq: 1540 150 80 100 70 120 60 80 120 80 60 60 50 90 70 60 70 30 60 20 20 160 10 20 10 10 0 10 10 0 0 20 50 0 10 20 20 10 10 0 20 0 0 0 10 20 0 0 0 0 0 0 40
    Total screen-off cpu time per freq: 1540 150 80 100 70 120 60 80 120 80 60 60 50 90 70 60 70 30 60 20 20 160 10 20 10 10 0 10 10 0 0 20 50 0 10 20 20 10 10 0 20 0 0 0 10 20 0 0 0 0 0 0 40
    Proc android.hardware.memtrack@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 630ms usr + 710ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 20ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 12ms (0.0%)
    Wifi Scan (blamed): 752ms (1.1%) 1x
    Wifi Scan (actual): 752ms (1.1%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 8s 466ms (99.5%)
       WiFi Idle time:   324ms (0.5%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     11ms (0.0%)
    Wake lock NlpWakeLock: 6ms partial (2 times) max=13 actual=14 realtime
    Wake lock fiid-sync: 11ms partial (1 times) max=11 realtime
    TOTAL wake: 17ms blamed partial, 25ms actual partial realtime
    Sensor GPS: 2s 507ms realtime (1 times)
    Foreground services: 1m 8s 168ms realtime (1 times)
    Fg Service for: 1m 8s 168ms 
    Cached for: 42ms 
    Total running: 1m 8s 210ms 
    Total cpu time: u=49s 47ms s=8s 200ms 
    Total cpu time per freq: 4570 650 560 1140 460 280 520 540 450 420 400 480 420 550 560 350 500 470 600 570 220 830 2280 1560 3160 3010 3050 3250 2900 2900 3610 2630 2510 2530 1830 1620 1140 870 1100 370 490 370 260 250 110 30 100 130 50 120 0 0 290
    Total screen-off cpu time per freq: 4570 650 560 1140 460 280 520 540 450 420 400 480 420 550 560 350 500 470 600 570 220 830 2280 1560 3160 3010 3050 3250 2900 2900 3610 2630 2510 2530 1830 1620 1140 870 1100 370 490 370 260 250 110 30 100 130 50 120 0 0 290
    Proc cgeo.geocaching:
      CPU: 43s 260ms usr + 7s 300ms krn ; 0ms fg
      1 starts

