Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 13h 41m 6s 900ms  (from 86 steps)
    Discharge screen doze time: 11h 59m 55s 600ms  (from 71 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 10s 193ms (100.0%) realtime, 1m 10s 194ms (100.0%) uptime
  Time on battery screen off: 1m 10s 193ms (100.0%) realtime, 1m 10s 194ms (100.0%) uptime
  Time on battery screen doze: 1m 10s 193ms (100.0%)
  Total run time: 1m 10s 219ms realtime, 1m 10s 220ms uptime
  Discharge: 9.98 mAh
  Screen off discharge: 9.98 mAh
  Screen doze discharge: 9.98 mAh
  Start clock time: 2018-07-31-01-20-35
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 10s 193ms (100.0%) 0x
  Idle mode light time: 1m 10s 193ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 254ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 10s 193ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 10s 193ms (100.0%) 
     Cellular Sleep time:  1m 8s 398ms (97.4%)
     Cellular Idle time:   1s 326ms (1.9%)
     Cellular Rx time:     470ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 10s 193ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 9s 568ms (99.1%)
     WiFi Idle time:   616ms (0.9%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     11ms (0.0%)
     WiFi Battery drain: 0.000935mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 10s 195ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.08, actual drain: 0
    Uid u0a140: 1.83 ( cpu=1.81 wake=0.0000103 wifi=0.000813 gps=0.0210 ) Including smearing: 2.78 ( proportional=0.949 )
    Uid 0: 0.539 ( cpu=0.483 wake=0.0555 ) Excluded from smearing
    Cell standby: 0.214 ( radio=0.214 ) Excluded from smearing
    Idle: 0.191 Excluded from smearing
    Uid 1000: 0.124 ( cpu=0.119 wake=0.00000952 sensor=0.00487 ) Excluded from smearing
    Uid u0a47: 0.0728 ( cpu=0.0727 wake=0.000105 ) Excluded from smearing
    Uid 1027: 0.0683 ( cpu=0.0683 ) Excluded from smearing
    Uid 2000: 0.0240 ( cpu=0.0240 ) Excluded from smearing
    Uid 1036: 0.00419 ( cpu=0.00419 ) Excluded from smearing
    Uid u0a22: 0.00177 ( cpu=0.00162 wake=0.0000270 wifi=0.000121 ) Excluded from smearing
    Wifi: 0.00157 ( cpu=0.00157 wifi=0.00000028 ) Including smearing: 0.00238 ( proportional=0.000812 )
    Uid 1001: 0.00132 ( cpu=0.00127 wake=0.0000492 ) Excluded from smearing
    Uid u0a127: 0.00116 ( cpu=0.00116 ) Including smearing: 0.00176 ( proportional=0.000600 )
    Uid u0a40: 0.000739 ( cpu=0.000739 ) Including smearing: 0.00112 ( proportional=0.000383 )
    Uid 1021: 0.000469 ( cpu=0.000469 ) Excluded from smearing
    Uid u0a139: 0.000424 ( cpu=0.000424 ) Including smearing: 0.000644 ( proportional=0.000220 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 9s 370ms realtime (4 times)
    XO_shutdown.MPSS: 1m 6s 967ms realtime (30 times)
    XO_shutdown.ADSP: 1m 10s 86ms realtime (2 times)
    wlan.Active: 731ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 9ms partial (1 times) max=12 actual=12 realtime
    Wake lock GnssLocationProvider: 4ms partial (2 times) max=4 actual=5 realtime
    TOTAL wake: 13ms blamed partial, 17ms actual partial realtime
    Sensor 17: 1m 10s 186ms realtime (0 times)
    Fg Service for: 1m 10s 186ms 
    Total running: 1m 10s 186ms 
    Total cpu time: u=1s 603ms s=2s 284ms 
    Total cpu time per freq: 1800 70 70 170 30 70 140 120 20 70 80 60 120 30 80 20 10 60 40 10 40 180 10 10 10 0 0 0 0 10 10 10 10 0 0 20 0 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 10
    Total screen-off cpu time per freq: 1800 70 70 170 30 70 140 120 20 70 80 60 120 30 80 20 10 60 40 10 40 180 10 10 10 0 0 0 0 10 10 10 10 0 0 20 0 0 0 10 10 0 0 0 0 0 0 0 0 0 0 0 10
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 40ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 560ms usr + 860ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 30ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 9ms (0.0%)
    Wifi Scan (blamed): 695ms (1.0%) 1x
    Wifi Scan (actual): 695ms (1.0%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 10s 21ms (99.7%)
       WiFi Idle time:   178ms (0.3%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     11ms (0.0%)
    Wake lock NlpWakeLock: 9ms partial (2 times) max=10 actual=12 realtime
    Wake lock fiid-sync: 5ms partial (1 times) max=5 realtime
    TOTAL wake: 14ms blamed partial, 17ms actual partial realtime
    Sensor GPS: 2s 521ms realtime (1 times)
    Foreground services: 1m 9s 543ms realtime (1 times)
    Fg Service for: 1m 9s 543ms 
    Cached for: 66ms 
    Total running: 1m 9s 609ms 
    Total cpu time: u=49s 747ms s=8s 537ms 
    Total cpu time per freq: 4730 520 550 1180 420 370 640 590 610 370 490 390 660 500 550 410 300 490 550 570 520 1090 2040 1680 3470 3210 3130 3150 3000 2860 3580 2400 2840 2310 1580 1920 1390 910 1020 420 350 440 150 190 90 40 100 60 20 30 0 60 230
    Total screen-off cpu time per freq: 4730 520 550 1180 420 370 640 590 610 370 490 390 660 500 550 410 300 490 550 570 520 1090 2040 1680 3470 3210 3130 3150 3000 2860 3580 2400 2840 2310 1580 1920 1390 910 1020 420 350 440 150 190 90 40 100 60 20 30 0 60 230
    Proc cgeo.geocaching:
      CPU: 43s 40ms usr + 7s 570ms krn ; 0ms fg
      1 starts

