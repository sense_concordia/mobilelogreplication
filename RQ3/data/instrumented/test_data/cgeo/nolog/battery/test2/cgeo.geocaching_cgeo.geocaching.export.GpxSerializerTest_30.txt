Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 13h 19m 51s 500ms  (from 92 steps)
    Discharge screen doze time: 11h 42m 24s 800ms  (from 77 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 14s 265ms (100.0%) realtime, 1m 14s 266ms (100.0%) uptime
  Time on battery screen off: 1m 14s 265ms (100.0%) realtime, 1m 14s 266ms (100.0%) uptime
  Time on battery screen doze: 1m 14s 265ms (100.0%)
  Total run time: 1m 14s 288ms realtime, 1m 14s 289ms uptime
  Discharge: 10.5 mAh
  Screen off discharge: 10.5 mAh
  Screen doze discharge: 10.5 mAh
  Start clock time: 2018-07-31-01-47-39
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 14s 265ms (100.0%) 0x
  Idle mode full time: 1m 14s 265ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 225ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 14s 265ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 14s 265ms (100.0%) 
     Cellular Sleep time:  1m 13s 563ms (99.1%)
     Cellular Idle time:   255ms (0.3%)
     Cellular Rx time:     448ms (0.6%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 14s 265ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 14s 266ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 14s 267ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.24, actual drain: 0-35.2
    Uid u0a140: 1.93 ( cpu=1.93 wake=0.00000714 ) Including smearing: 2.93 ( proportional=1.00 )
    Uid 0: 0.566 ( cpu=0.507 wake=0.0587 ) Excluded from smearing
    Cell standby: 0.227 ( radio=0.227 ) Excluded from smearing
    Idle: 0.202 Excluded from smearing
    Uid 1000: 0.129 ( cpu=0.129 wake=0.00000634 ) Excluded from smearing
    Uid u0a47: 0.0814 ( cpu=0.0812 wake=0.000113 ) Excluded from smearing
    Uid 1027: 0.0778 ( cpu=0.0778 ) Excluded from smearing
    Uid 2000: 0.0220 ( cpu=0.0220 ) Excluded from smearing
    Uid 1036: 0.00409 ( cpu=0.00409 ) Excluded from smearing
    Uid 1001: 0.00107 ( cpu=0.00103 wake=0.0000460 ) Excluded from smearing
    Uid u0a40: 0.00103 ( cpu=0.00103 ) Including smearing: 0.00156 ( proportional=0.000533 )
    Uid u0a127: 0.000991 ( cpu=0.000991 ) Including smearing: 0.00151 ( proportional=0.000515 )
    Uid u0a139: 0.000592 ( cpu=0.000592 ) Including smearing: 0.000899 ( proportional=0.000307 )
    Wifi: 0.000548 ( cpu=0.000548 ) Including smearing: 0.000833 ( proportional=0.000285 )
    Uid u0a22: 0.000394 ( cpu=0.000390 wake=0.00000476 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 14s 118ms realtime (5 times)
    XO_shutdown.MPSS: 1m 13s 537ms realtime (33 times)
    XO_shutdown.ADSP: 1m 14s 178ms realtime (0 times)
    wlan.Active: 56ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 9ms partial (1 times) max=11 actual=11 realtime
    Fg Service for: 1m 14s 259ms 
    Total running: 1m 14s 259ms 
    Total cpu time: u=1s 807ms s=2s 476ms 
    Total cpu time per freq: 1610 90 160 120 60 90 130 60 50 90 60 70 50 40 70 90 30 40 60 10 10 200 30 10 10 10 10 10 0 0 0 0 40 10 10 0 30 20 0 20 0 0 10 0 0 0 0 0 0 0 0 0 10
    Total screen-off cpu time per freq: 1610 90 160 120 60 90 130 60 50 90 60 70 50 40 70 90 30 40 60 10 10 200 30 10 10 10 10 10 0 0 0 0 40 10 10 0 30 20 0 20 0 0 10 0 0 0 0 0 0 0 0 0 10
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 610ms usr + 710ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
  u0a140:
    Wake lock NlpWakeLock realtime
    Wake lock fiid-sync: 9ms partial (1 times) max=9 realtime
    TOTAL wake: 9ms blamed partial, 10ms actual partial realtime
    Foreground services: 1m 13s 719ms realtime (1 times)
    Fg Service for: 1m 13s 719ms 
    Cached for: 35ms 
    Total running: 1m 13s 754ms 
    Total cpu time: u=53s 693ms s=9s 210ms 
    Total cpu time per freq: 5870 760 790 1480 520 480 900 540 640 570 580 390 570 540 610 590 460 650 620 370 380 810 3200 2150 3880 4150 4380 3570 3250 2810 3050 2690 2750 2150 1340 1400 1250 770 570 520 290 200 230 100 80 70 20 50 20 50 0 20 270
    Total screen-off cpu time per freq: 5870 760 790 1480 520 480 900 540 640 570 580 390 570 540 610 590 460 650 620 370 380 810 3200 2150 3880 4150 4380 3570 3250 2810 3050 2690 2750 2150 1340 1400 1250 770 570 520 290 200 230 100 80 70 20 50 20 50 0 20 270
    Proc cgeo.geocaching:
      CPU: 44s 160ms usr + 7s 570ms krn ; 0ms fg
      1 starts

