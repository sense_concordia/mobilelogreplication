Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 13h 45m 47s 700ms  (from 85 steps)
    Discharge screen doze time: 12h 4m 9s 800ms  (from 70 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 10s 143ms (100.0%) realtime, 1m 10s 144ms (100.0%) uptime
  Time on battery screen off: 1m 10s 143ms (100.0%) realtime, 1m 10s 144ms (100.0%) uptime
  Time on battery screen doze: 1m 10s 143ms (100.0%)
  Total run time: 1m 10s 167ms realtime, 1m 10s 168ms uptime
  Discharge: 9.95 mAh
  Screen off discharge: 9.95 mAh
  Screen doze discharge: 9.95 mAh
  Start clock time: 2018-07-31-01-16-33
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 10s 143ms (100.0%) 0x
  Idle mode light time: 1m 10s 143ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 189ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 10s 143ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 10s 143ms (100.0%) 
     Cellular Sleep time:  1m 8s 257ms (97.3%)
     Cellular Idle time:   1s 425ms (2.0%)
     Cellular Rx time:     465ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 10s 143ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 9s 494ms (99.1%)
     WiFi Idle time:   643ms (0.9%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     11ms (0.0%)
     WiFi Battery drain: 0.000942mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 10s 149ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.09, actual drain: 0
    Uid u0a140: 1.85 ( cpu=1.83 wake=0.00000714 wifi=0.000813 gps=0.0209 ) Including smearing: 2.79 ( proportional=0.947 )
    Uid 0: 0.538 ( cpu=0.482 wake=0.0555 ) Excluded from smearing
    Cell standby: 0.214 ( radio=0.214 ) Excluded from smearing
    Idle: 0.191 Excluded from smearing
    Uid 1000: 0.123 ( cpu=0.118 wake=0.0000127 sensor=0.00487 ) Excluded from smearing
    Uid u0a47: 0.0715 ( cpu=0.0714 wake=0.0000983 ) Excluded from smearing
    Uid 1027: 0.0711 ( cpu=0.0711 ) Excluded from smearing
    Uid 2000: 0.0216 ( cpu=0.0216 ) Excluded from smearing
    Uid 1036: 0.00349 ( cpu=0.00349 ) Excluded from smearing
    Uid u0a22: 0.00187 ( cpu=0.00172 wake=0.0000254 wifi=0.000129 ) Excluded from smearing
    Wifi: 0.00129 ( cpu=0.00129 ) Including smearing: 0.00195 ( proportional=0.000660 )
    Uid u0a127: 0.00119 ( cpu=0.00119 ) Including smearing: 0.00179 ( proportional=0.000608 )
    Uid 1001: 0.00104 ( cpu=0.00103 wake=0.00000555 ) Excluded from smearing
    Uid 9999: 0.00102 ( cpu=0.00102 ) Excluded from smearing
    Uid u0a40: 0.000751 ( cpu=0.000751 ) Including smearing: 0.00114 ( proportional=0.000385 )
    Uid 1021: 0.000474 ( cpu=0.000474 ) Excluded from smearing
    Uid u0a139: 0.000303 ( cpu=0.000303 ) Including smearing: 0.000458 ( proportional=0.000155 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 9s 310ms realtime (4 times)
    XO_shutdown.MPSS: 1m 6s 849ms realtime (32 times)
    XO_shutdown.ADSP: 1m 10s 81ms realtime (2 times)
    wlan.Active: 756ms realtime (4 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 13ms partial (2 times) max=16 actual=21 realtime
    Wake lock GnssLocationProvider: 3ms partial (2 times) max=2 realtime
    TOTAL wake: 16ms blamed partial, 24ms actual partial realtime
    Sensor 17: 1m 10s 138ms realtime (0 times)
    Fg Service for: 1m 10s 138ms 
    Total running: 1m 10s 138ms 
    Total cpu time: u=1s 616ms s=2s 207ms 
    Total cpu time per freq: 1690 80 40 140 50 90 60 110 50 90 30 30 70 80 50 20 20 70 30 10 70 210 20 0 0 0 10 0 10 0 10 20 20 10 20 10 10 10 0 30 0 20 0 0 0 20 0 10 0 0 0 0 40
    Total screen-off cpu time per freq: 1690 80 40 140 50 90 60 110 50 90 30 30 70 80 50 20 20 70 30 10 70 210 20 0 0 0 10 0 10 0 10 20 20 10 20 10 10 10 0 30 0 20 0 0 0 20 0 10 0 0 0 0 40
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 650ms usr + 680ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 5ms (0.0%)
    Wifi Scan (blamed): 720ms (1.0%) 1x
    Wifi Scan (actual): 720ms (1.0%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 9s 995ms (99.7%)
       WiFi Idle time:   178ms (0.3%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     11ms (0.0%)
    Wake lock NlpWakeLock: 6ms partial (2 times) max=7 actual=8 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 10ms blamed partial, 12ms actual partial realtime
    Sensor GPS: 2s 509ms realtime (1 times)
    Foreground services: 1m 9s 512ms realtime (1 times)
    Fg Service for: 1m 9s 512ms 
    Cached for: 100ms 
    Total running: 1m 9s 612ms 
    Total cpu time: u=50s 14ms s=8s 700ms 
    Total cpu time per freq: 5150 630 440 1100 320 240 790 470 420 410 310 310 530 540 380 480 320 540 660 420 360 740 2980 1990 3410 3670 3850 3310 3180 2690 3700 2140 2250 2300 1540 2010 1210 950 710 490 340 260 310 250 150 140 70 160 20 20 0 10 330
    Total screen-off cpu time per freq: 5150 630 440 1100 320 240 790 470 420 410 310 310 530 540 380 480 320 540 660 420 360 740 2980 1990 3410 3670 3850 3310 3180 2690 3700 2140 2250 2300 1540 2010 1210 950 710 490 340 260 310 250 150 140 70 160 20 20 0 10 330
    Proc cgeo.geocaching:
      CPU: 39s 570ms usr + 6s 830ms krn ; 0ms fg
      1 starts

