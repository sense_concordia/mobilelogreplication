Daily stats:
  Current start time: 2018-07-30-03-01-01
  Next min deadline: 2018-07-31-01-00-00
  Next max deadline: 2018-07-31-03-00-00
  Current daily steps:
    Discharge total time: 13h 54m 49s 200ms  (from 81 steps)
    Discharge screen doze time: 12h 9m 4s 800ms  (from 66 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 54s 47ms (100.0%) realtime, 54s 47ms (100.0%) uptime
  Time on battery screen off: 54s 47ms (100.0%) realtime, 54s 47ms (100.0%) uptime
  Time on battery screen doze: 54s 47ms (100.0%)
  Total run time: 54s 73ms realtime, 54s 73ms uptime
  Discharge: 6.98 mAh
  Screen off discharge: 6.98 mAh
  Screen doze discharge: 6.98 mAh
  Start clock time: 2018-07-31-00-51-55
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 54s 47ms (100.0%) 0x
  Idle mode light time: 54s 47ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 387ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 54s 47ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  54s 47ms (100.0%) 
     Cellular Sleep time:  50s 789ms (94.0%)
     Cellular Idle time:   2s 891ms (5.3%)
     Cellular Rx time:     367ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 54s 47ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  52s 705ms (97.5%)
     WiFi Idle time:   1s 321ms (2.4%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     21ms (0.0%)
     WiFi Battery drain: 0.00183mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  54s 48ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.08, actual drain: 0
    Uid u0a140: 1.19 ( cpu=1.14 wake=0.0000523 wifi=0.000244 gps=0.0432 ) Including smearing: 1.85 ( proportional=0.662 )
    Uid 0: 0.313 ( cpu=0.271 wake=0.0426 ) Excluded from smearing
    Cell standby: 0.165 ( radio=0.165 ) Excluded from smearing
    Idle: 0.147 Excluded from smearing
    Uid 1000: 0.122 ( cpu=0.122 wake=0.0000293 ) Excluded from smearing
    Uid u0a47: 0.0731 ( cpu=0.0730 wake=0.0000968 ) Excluded from smearing
    Uid 1027: 0.0429 ( cpu=0.0429 ) Excluded from smearing
    Uid 2000: 0.0167 ( cpu=0.0167 ) Excluded from smearing
    Uid u0a22: 0.00598 ( cpu=0.00431 wake=0.0000888 wifi=0.00158 ) Excluded from smearing
    Uid 1036: 0.00253 ( cpu=0.00253 ) Excluded from smearing
    Uid 1001: 0.00240 ( cpu=0.00236 wake=0.0000381 ) Excluded from smearing
    Wifi: 0.00161 ( cpu=0.00161 wifi=0.00000028 ) Including smearing: 0.00251 ( proportional=0.000898 )
    Uid 1021: 0.00146 ( cpu=0.00146 ) Excluded from smearing
    Uid u0a127: 0.000647 ( cpu=0.000647 ) Including smearing: 0.00101 ( proportional=0.000361 )
    Uid u0a40: 0.000460 ( cpu=0.000460 ) Including smearing: 0.000716 ( proportional=0.000256 )
    Uid u0a139: 0.000408 ( cpu=0.000408 ) Including smearing: 0.000636 ( proportional=0.000227 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 52s 380ms realtime (10 times)
    XO_shutdown.MPSS: 46s 741ms realtime (26 times)
    XO_shutdown.ADSP: 53s 921ms realtime (4 times)
    wlan.Active: 1s 567ms realtime (10 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 20ms partial (3 times) max=17 actual=29 realtime
    Wake lock GnssLocationProvider: 18ms partial (11 times) max=6 actual=22 realtime
    TOTAL wake: 38ms blamed partial, 51ms actual partial realtime
    Fg Service for: 54s 40ms 
    Total running: 54s 40ms 
    Total cpu time: u=2s 96ms s=2s 97ms 
    Total cpu time per freq: 1240 100 100 150 60 70 110 110 170 120 140 90 210 110 120 70 120 130 70 110 40 220 60 50 40 10 10 30 10 0 0 20 10 0 0 10 0 0 0 10 0 10 0 10 0 0 10 0 0 0 0 0 70
    Total screen-off cpu time per freq: 1240 100 100 150 60 70 110 110 170 120 140 90 210 110 120 70 120 130 70 110 40 220 60 50 40 10 10 30 10 0 0 20 10 0 0 10 0 0 0 10 0 10 0 10 0 0 10 0 0 0 0 0 70
    Proc servicemanager:
      CPU: 110ms usr + 170ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 20ms krn ; 0ms fg
    Proc system:
      CPU: 940ms usr + 870ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 74ms (0.1%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  53s 174ms (98.4%)
       WiFi Idle time:   880ms (1.6%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     0ms (0.0%)
    Wake lock NlpWakeLock: 63ms partial (13 times) max=31 actual=99, 29ms background partial (1 times) max=23 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 67ms blamed partial, 103ms actual partial, 29ms actual background partial realtime
    Sensor GPS: 5s 184ms realtime (3 times), 33ms background (0 times)
    Foreground services: 53s 514ms realtime (1 times)
    Fg Service for: 53s 514ms 
    Cached for: 56ms 
    Total running: 53s 570ms 
    Total cpu time: u=29s 797ms s=9s 380ms 
    Total cpu time per freq: 3950 920 1070 1870 830 1070 1400 1500 1560 1880 1710 1310 2120 1920 1630 1330 1300 910 1080 570 460 1060 5070 1110 1270 280 150 30 40 10 10 70 80 60 30 10 40 0 40 20 0 20 0 20 0 20 0 20 30 0 0 0 280
    Total screen-off cpu time per freq: 3950 920 1070 1870 830 1070 1400 1500 1560 1880 1710 1310 2120 1920 1630 1330 1300 910 1080 570 460 1060 5070 1110 1270 280 150 30 40 10 10 70 80 60 30 10 40 0 40 20 0 20 0 20 0 20 0 20 30 0 0 0 280
    Proc cgeo.geocaching:
      CPU: 21s 280ms usr + 6s 880ms krn ; 0ms fg
      1 starts

