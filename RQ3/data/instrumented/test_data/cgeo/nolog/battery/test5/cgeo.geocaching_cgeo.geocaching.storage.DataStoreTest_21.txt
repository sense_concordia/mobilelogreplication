Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 6h 35m 26s 600ms  (from 7 steps)
    Discharge screen doze time: 6h 35m 26s 600ms  (from 7 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 8s 613ms (100.0%) realtime, 1m 8s 613ms (100.0%) uptime
  Time on battery screen off: 1m 8s 613ms (100.0%) realtime, 1m 8s 613ms (100.0%) uptime
  Time on battery screen doze: 1m 8s 613ms (100.0%)
  Total run time: 1m 8s 639ms realtime, 1m 8s 639ms uptime
  Discharge: 10.9 mAh
  Screen off discharge: 10.9 mAh
  Screen doze discharge: 10.9 mAh
  Start clock time: 2018-07-31-03-32-02
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 8s 613ms (100.0%) 0x
  Idle mode full time: 1m 8s 613ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 364ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 8s 613ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 8s 613ms (100.0%) 
     Cellular Sleep time:  1m 7s 885ms (98.9%)
     Cellular Idle time:   227ms (0.3%)
     Cellular Rx time:     501ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 8s 613ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 8s 614ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 8s 614ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 2.99, actual drain: 0
    Uid u0a140: 1.82 ( cpu=1.82 wake=0.0000428 ) Including smearing: 2.71 ( proportional=0.886 )
    Uid 0: 0.453 ( cpu=0.399 wake=0.0541 ) Excluded from smearing
    Cell standby: 0.210 ( radio=0.210 ) Excluded from smearing
    Idle: 0.187 Excluded from smearing
    Uid 1000: 0.116 ( cpu=0.116 wake=0.0000325 ) Excluded from smearing
    Uid u0a47: 0.0971 ( cpu=0.0970 wake=0.000113 ) Excluded from smearing
    Uid 1027: 0.0595 ( cpu=0.0595 ) Excluded from smearing
    Uid 2000: 0.0286 ( cpu=0.0286 ) Excluded from smearing
    Uid u0a22: 0.00783 ( cpu=0.00774 wake=0.0000888 ) Excluded from smearing
    Uid 1036: 0.00630 ( cpu=0.00630 ) Excluded from smearing
    Uid u0a127: 0.00145 ( cpu=0.00145 ) Including smearing: 0.00215 ( proportional=0.000702 )
    Uid u0a40: 0.00105 ( cpu=0.00105 ) Including smearing: 0.00156 ( proportional=0.000510 )
    Uid 1001: 0.000794 ( cpu=0.000784 wake=0.0000103 ) Excluded from smearing
    Wifi: 0.000337 ( cpu=0.000337 ) Including smearing: 0.000500 ( proportional=0.000164 )
    Uid u0a139: 0.000335 ( cpu=0.000335 ) Including smearing: 0.000498 ( proportional=0.000163 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 8s 495ms realtime (2 times)
    XO_shutdown.MPSS: 1m 7s 935ms realtime (29 times)
    XO_shutdown.ADSP: 1m 8s 520ms realtime (0 times)
    wlan.Active: 24ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 41ms partial (11 times) max=26 actual=46 realtime
    Fg Service for: 1m 8s 607ms 
    Total running: 1m 8s 607ms 
    Total cpu time: u=1s 584ms s=1s 570ms 
    Total cpu time per freq: 1060 40 30 90 20 70 80 130 130 120 100 60 60 80 70 80 60 20 80 30 20 110 70 50 70 40 40 10 30 20 20 0 30 30 0 0 0 0 0 0 0 0 0 0 20 0 0 0 0 20 0 0 190
    Total screen-off cpu time per freq: 1060 40 30 90 20 70 80 130 130 120 100 60 60 80 70 80 60 20 80 30 20 110 70 50 70 40 40 10 30 20 20 0 30 30 0 0 0 0 0 0 0 0 0 0 20 0 0 0 0 20 0 0 190
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 800ms usr + 600ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 21ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 50ms partial (22 times) max=8 actual=64 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 54ms blamed partial, 68ms actual partial realtime
    Foreground services: 1m 8s 10ms realtime (1 times)
    Fg Service for: 1m 8s 10ms 
    Cached for: 38ms 
    Total running: 1m 8s 48ms 
    Total cpu time: u=33s 823ms s=14s 57ms 
    Total cpu time per freq: 2080 310 270 700 670 1370 1750 2760 2790 2690 1980 1510 1410 1220 960 770 550 590 740 380 260 490 1690 1100 1090 590 630 190 390 200 160 220 260 200 70 70 40 60 60 0 20 30 30 0 0 20 20 10 0 20 0 0 14980
    Total screen-off cpu time per freq: 2080 310 270 700 670 1370 1750 2760 2790 2690 1980 1510 1410 1220 960 770 550 590 740 380 260 490 1690 1100 1090 590 630 190 390 200 160 220 260 200 70 70 40 60 60 0 20 30 30 0 0 20 20 10 0 20 0 0 14980
    Proc cgeo.geocaching:
      CPU: 26s 970ms usr + 11s 780ms krn ; 0ms fg
      1 starts

