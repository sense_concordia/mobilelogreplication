Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 9s 495ms (100.0%) realtime, 1m 9s 495ms (100.0%) uptime
  Time on battery screen off: 1m 9s 495ms (100.0%) realtime, 1m 9s 495ms (100.0%) uptime
  Time on battery screen doze: 1m 9s 495ms (100.0%)
  Total run time: 1m 9s 516ms realtime, 1m 9s 516ms uptime
  Discharge: 10.7 mAh
  Screen off discharge: 10.7 mAh
  Screen doze discharge: 10.7 mAh
  Start clock time: 2018-07-31-03-04-58
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 1m 9s 495ms (100.0%) 0x
  Idle mode full time: 1m 9s 495ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 510ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 9s 495ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 9s 495ms (100.0%) 
     Cellular Sleep time:  1m 8s 692ms (98.8%)
     Cellular Idle time:   176ms (0.3%)
     Cellular Rx time:     628ms (0.9%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 9s 495ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 9s 497ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 9s 497ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.03, actual drain: 0
    Uid u0a140: 1.85 ( cpu=1.85 wake=0.0000500 ) Including smearing: 2.75 ( proportional=0.899 )
    Uid 0: 0.466 ( cpu=0.411 wake=0.0547 ) Excluded from smearing
    Cell standby: 0.212 ( radio=0.212 ) Excluded from smearing
    Idle: 0.189 Excluded from smearing
    Uid 1000: 0.120 ( cpu=0.120 wake=0.0000333 ) Excluded from smearing
    Uid u0a47: 0.0901 ( cpu=0.0899 wake=0.000217 ) Excluded from smearing
    Uid 1027: 0.0555 ( cpu=0.0555 ) Excluded from smearing
    Uid 2000: 0.0276 ( cpu=0.0276 ) Excluded from smearing
    Uid u0a22: 0.00940 ( cpu=0.00930 wake=0.0000999 ) Excluded from smearing
    Uid 1036: 0.00665 ( cpu=0.00665 ) Excluded from smearing
    Uid 1001: 0.00445 ( cpu=0.00445 wake=0.00000397 ) Excluded from smearing
    Uid u0a127: 0.000976 ( cpu=0.000976 ) Including smearing: 0.00145 ( proportional=0.000475 )
    Uid u0a40: 0.000625 ( cpu=0.000625 ) Including smearing: 0.000930 ( proportional=0.000304 )
    Wifi: 0.000295 ( cpu=0.000295 ) Including smearing: 0.000438 ( proportional=0.000143 )
    Uid u0a139: 0.000112 ( cpu=0.000112 ) Including smearing: 0.000167 ( proportional=0.0000546 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 9s 384ms realtime (2 times)
    XO_shutdown.MPSS: 1m 8s 875ms realtime (30 times)
    XO_shutdown.ADSP: 1m 9s 420ms realtime (0 times)
    wlan.Active: 22ms realtime (2 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 42ms partial (9 times) max=34 actual=44 realtime
    Fg Service for: 1m 9s 490ms 
    Total running: 1m 9s 490ms 
    Total cpu time: u=1s 563ms s=1s 650ms 
    Total cpu time per freq: 1030 60 70 150 100 130 120 190 230 180 200 100 170 110 110 170 50 80 70 30 50 220 90 60 40 50 40 70 10 20 0 0 10 10 30 20 10 10 30 0 10 20 10 10 0 10 0 0 0 0 0 0 230
    Total screen-off cpu time per freq: 1030 60 70 150 100 130 120 190 230 180 200 100 170 110 110 170 50 80 70 30 50 220 90 60 40 50 40 70 10 20 0 0 10 10 30 20 10 10 30 0 10 20 10 10 0 10 0 0 0 0 0 0 230
    Proc android.hardware.memtrack@1.0-service:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 30ms usr + 60ms krn ; 0ms fg
    Proc system:
      CPU: 770ms usr + 700ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 20ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 24ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 58ms partial (23 times) max=17 actual=78 realtime
    Wake lock fiid-sync: 5ms partial (1 times) max=5 realtime
    TOTAL wake: 63ms blamed partial, 83ms actual partial realtime
    Foreground services: 1m 8s 963ms realtime (1 times)
    Fg Service for: 1m 8s 963ms 
    Cached for: 60ms 
    Total running: 1m 9s 23ms 
    Total cpu time: u=33s 824ms s=14s 723ms 
    Total cpu time per freq: 3350 450 820 1460 1420 2700 2520 4380 5030 4650 3310 2460 2850 1910 1630 1560 1080 760 1090 480 420 590 4900 3220 2370 790 550 440 290 240 170 80 380 320 90 10 50 50 120 20 0 0 40 20 70 0 0 0 30 0 0 10 19900
    Total screen-off cpu time per freq: 3350 450 820 1460 1420 2700 2520 4380 5030 4650 3310 2460 2850 1910 1630 1560 1080 760 1090 480 420 590 4900 3220 2370 790 550 440 290 240 170 80 380 320 90 10 50 50 120 20 0 0 40 20 70 0 0 0 30 0 0 10 19900
    Proc cgeo.geocaching:
      CPU: 28s 590ms usr + 13s 340ms krn ; 0ms fg
      1 starts

