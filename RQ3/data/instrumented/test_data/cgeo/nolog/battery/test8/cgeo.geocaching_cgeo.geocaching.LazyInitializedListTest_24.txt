Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 6h 33m 23s 400ms  (from 39 steps)
    Discharge screen doze time: 6h 33m 23s 400ms  (from 39 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 36s 440ms (99.9%) realtime, 36s 440ms (100.0%) uptime
  Time on battery screen off: 36s 440ms (100.0%) realtime, 36s 440ms (100.0%) uptime
  Time on battery screen doze: 36s 440ms (100.0%)
  Total run time: 36s 471ms realtime, 36s 471ms uptime
  Discharge: 5.52 mAh
  Screen off discharge: 5.52 mAh
  Screen doze discharge: 5.52 mAh
  Start clock time: 2018-07-31-05-37-05
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 36s 440ms (100.0%) 0x
  Idle mode full time: 36s 440ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 77ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 36s 440ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  36s 440ms (100.0%) 
     Cellular Sleep time:  35s 969ms (98.7%)
     Cellular Idle time:   223ms (0.6%)
     Cellular Rx time:     251ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 36s 440ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  36s 445ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  36s 445ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.74, actual drain: 0-35.2
    Uid u0a140: 3.00 ( cpu=3.00 wake=0.00000793 ) Including smearing: 3.62 ( proportional=0.626 )
    Uid 0: 0.259 ( cpu=0.230 wake=0.0288 ) Excluded from smearing
    Uid 1000: 0.126 ( cpu=0.126 ) Excluded from smearing
    Cell standby: 0.111 ( radio=0.111 ) Excluded from smearing
    Idle: 0.0993 Excluded from smearing
    Uid 1027: 0.0568 ( cpu=0.0568 ) Excluded from smearing
    Uid u0a47: 0.0472 ( cpu=0.0472 ) Excluded from smearing
    Uid 2000: 0.0364 ( cpu=0.0364 ) Excluded from smearing
    Uid 1036: 0.00871 ( cpu=0.00871 ) Excluded from smearing
    Uid 1001: 0.00125 ( cpu=0.00120 wake=0.0000460 ) Excluded from smearing
    Uid u0a40: 0.00111 ( cpu=0.00111 ) Including smearing: 0.00135 ( proportional=0.000233 )
    Uid u0a127: 0.00111 ( cpu=0.00111 ) Including smearing: 0.00134 ( proportional=0.000232 )
    Wifi: 0.000701 ( cpu=0.000701 ) Including smearing: 0.000847 ( proportional=0.000146 )
    Uid u0a22: 0.000541 ( cpu=0.000534 wake=0.00000714 ) Excluded from smearing

  Resource Power Manager Stats
    wlan.Deep-Sleep: 36s 280ms realtime (5 times)
    XO_shutdown.MPSS: 35s 941ms realtime (18 times)
    XO_shutdown.ADSP: 36s 356ms realtime (0 times)
    wlan.Active: 58ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider realtime
    Fg Service for: 36s 433ms 
    Total running: 36s 433ms 
    Total cpu time: u=897ms s=1s 317ms 
    Total cpu time per freq: 1260 20 20 80 30 50 40 60 0 20 10 30 50 20 10 40 30 20 0 0 0 60 10 0 0 0 10 10 0 0 0 0 10 0 0 0 0 0 0 0 0 0 10 0 0 10 0 0 10 0 0 0 300
    Total screen-off cpu time per freq: 1260 20 20 80 30 50 40 60 0 20 10 30 50 20 10 40 30 20 0 0 0 60 10 0 0 0 10 10 0 0 0 0 10 0 0 0 0 0 0 0 0 0 10 0 0 10 0 0 10 0 0 0 300
    Proc servicemanager:
      CPU: 0ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 270ms usr + 350ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 2ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 3ms partial (2 times) max=2 actual=4 realtime
    Wake lock fiid-sync: 7ms partial (1 times) max=7 realtime
    TOTAL wake: 10ms blamed partial, 11ms actual partial realtime
    Foreground services: 35s 818ms realtime (1 times)
    Fg Service for: 35s 818ms 
    Cached for: 65ms 
    Total running: 35s 883ms 
    Total cpu time: u=47s 800ms s=1s 910ms 
    Total cpu time per freq: 220 10 50 20 20 0 0 40 0 0 0 10 0 0 0 10 0 0 0 0 10 50 850 20 50 30 20 40 20 20 40 10 50 40 10 10 70 30 20 40 20 20 20 20 40 60 120 30 20 40 10 50 47420
    Total screen-off cpu time per freq: 220 10 50 20 20 0 0 40 0 0 0 10 0 0 0 10 0 0 0 0 10 50 850 20 50 30 20 40 20 20 40 10 50 40 10 10 70 30 20 40 20 20 20 20 40 60 120 30 20 40 10 50 47420
    Proc cgeo.geocaching:
      CPU: 39s 50ms usr + 1s 480ms krn ; 0ms fg
      1 starts

