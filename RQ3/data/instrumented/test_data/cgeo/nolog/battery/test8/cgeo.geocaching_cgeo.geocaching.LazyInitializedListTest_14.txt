Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 6h 34m 31s 700ms  (from 37 steps)
    Discharge screen doze time: 6h 34m 31s 700ms  (from 37 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 36s 336ms (99.9%) realtime, 36s 336ms (100.0%) uptime
  Time on battery screen off: 36s 336ms (100.0%) realtime, 36s 336ms (100.0%) uptime
  Time on battery screen doze: 36s 335ms (100.0%)
  Total run time: 36s 358ms realtime, 36s 357ms uptime
  Discharge: 5.25 mAh
  Screen off discharge: 5.25 mAh
  Screen doze discharge: 5.25 mAh
  Start clock time: 2018-07-31-05-29-44
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device full idling: 36s 335ms (100.0%) 0x
  Idle mode full time: 36s 335ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 178ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 36s 336ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  36s 335ms (100.0%) 
     Cellular Sleep time:  35s 854ms (98.7%)
     Cellular Idle time:   211ms (0.6%)
     Cellular Rx time:     271ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 36s 335ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  36s 337ms (100.0%)
     WiFi Idle time:   0ms (0.0%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     0ms (0.0%)
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  36s 337ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 1
    Amount discharged while screen on: 0
    Amount discharged while screen off: 1
    Amount discharged while screen doze: 1

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 3.77, actual drain: 0-35.2
    Uid u0a140: 3.00 ( cpu=3.00 wake=0.0000135 ) Including smearing: 3.64 ( proportional=0.635 )
    Uid 0: 0.253 ( cpu=0.225 wake=0.0287 ) Excluded from smearing
    Uid 1000: 0.130 ( cpu=0.130 wake=0.00000079 ) Excluded from smearing
    Cell standby: 0.111 ( radio=0.111 ) Excluded from smearing
    Idle: 0.0990 Excluded from smearing
    Uid 1027: 0.0581 ( cpu=0.0581 ) Excluded from smearing
    Uid u0a47: 0.0527 ( cpu=0.0526 wake=0.000102 ) Excluded from smearing
    Uid 2000: 0.0405 ( cpu=0.0405 ) Excluded from smearing
    Uid 1036: 0.00925 ( cpu=0.00925 ) Excluded from smearing
    Uid u0a139: 0.00260 ( cpu=0.00260 ) Including smearing: 0.00315 ( proportional=0.000550 )
    Uid u0a35: 0.00241 ( cpu=0.00241 ) Including smearing: 0.00293 ( proportional=0.000511 )
    Uid u0a22: 0.00202 ( cpu=0.00200 wake=0.0000143 ) Excluded from smearing
    Uid 1001: 0.00137 ( cpu=0.00136 wake=0.0000103 ) Excluded from smearing
    Uid u0a127: 0.00132 ( cpu=0.00132 ) Including smearing: 0.00160 ( proportional=0.000280 )
    Wifi: 0.000991 ( cpu=0.000991 ) Including smearing: 0.00120 ( proportional=0.000210 )
    Uid u0a40: 0.000687 ( cpu=0.000687 ) Including smearing: 0.000833 ( proportional=0.000145 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 36s 198ms realtime (5 times)
    XO_shutdown.MPSS: 35s 848ms realtime (19 times)
    XO_shutdown.ADSP: 36s 250ms realtime (0 times)
    wlan.Active: 57ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock GnssLocationProvider: 1ms partial (1 times) max=1 realtime
    Fg Service for: 36s 331ms 
    Total running: 36s 331ms 
    Total cpu time: u=1s 97ms s=1s 170ms 
    Total cpu time per freq: 1260 80 40 70 50 10 60 10 30 60 30 60 0 20 10 40 10 10 20 0 20 100 20 0 20 0 10 0 0 0 0 0 20 10 0 0 0 0 0 0 0 0 0 0 0 10 0 10 0 0 0 0 250
    Total screen-off cpu time per freq: 1260 80 40 70 50 10 60 10 30 60 30 60 0 20 10 40 10 10 20 0 20 100 20 0 20 0 10 0 0 0 0 0 20 10 0 0 0 0 0 0 0 0 0 0 0 10 0 10 0 0 0 0 250
    Proc servicemanager:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 20ms usr + 40ms krn ; 0ms fg
    Proc system:
      CPU: 420ms usr + 370ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a140:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 1ms (0.0%)
    Wifi Scan (blamed): 0ms (0.0%) 0x
    Wifi Scan (actual): 0ms (0.0%) 0x
    Background Wifi Scan: 0ms (0.0%) 0x
    Wake lock NlpWakeLock: 7ms partial (2 times) max=8 actual=9 realtime
    Wake lock fiid-sync: 11ms partial (1 times) max=11 realtime
    TOTAL wake: 18ms blamed partial, 20ms actual partial realtime
    Foreground services: 35s 627ms realtime (1 times)
    Fg Service for: 35s 627ms 
    Cached for: 99ms 
    Total running: 35s 726ms 
    Total cpu time: u=47s 934ms s=2s 14ms 
    Total cpu time per freq: 490 60 30 50 10 0 40 0 0 0 30 30 10 0 0 0 0 20 0 0 0 30 920 40 30 30 20 50 20 30 70 50 30 60 40 10 30 30 30 30 10 50 60 50 20 70 70 50 30 20 0 30 47330
    Total screen-off cpu time per freq: 490 60 30 50 10 0 40 0 0 0 30 30 10 0 0 0 0 20 0 0 0 30 920 40 30 30 20 50 20 30 70 50 30 60 40 10 30 30 30 30 10 50 60 50 20 70 70 50 30 20 0 30 47330
    Proc cgeo.geocaching:
      CPU: 38s 890ms usr + 1s 540ms krn ; 0ms fg
      1 starts

