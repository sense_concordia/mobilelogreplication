Daily stats:
  Current start time: 2018-07-31-03-04-09
  Next min deadline: 2018-08-01-01-00-00
  Next max deadline: 2018-08-01-03-00-00
  Current daily steps:
    Discharge total time: 9h 16m 30s 200ms  (from 100 steps)
    Discharge screen doze time: 9h 12m 55s 700ms  (from 98 steps)
    Charge total time: 7h 30m 11s 900ms  (from 119 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 15m 4s 500ms  (from 75 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 4s 147ms (99.9%) realtime, 1m 4s 146ms (100.0%) uptime
  Time on battery screen off: 1m 4s 147ms (100.0%) realtime, 1m 4s 146ms (100.0%) uptime
  Time on battery screen doze: 1m 4s 147ms (100.0%)
  Total run time: 1m 4s 180ms realtime, 1m 4s 180ms uptime
  Discharge: 7.80 mAh
  Screen off discharge: 7.80 mAh
  Screen doze discharge: 7.80 mAh
  Start clock time: 2018-07-31-16-26-43
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 4s 147ms (100.0%) 0x
  Idle mode light time: 1m 4s 147ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 273ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 4s 147ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 4s 147ms (100.0%) 
     Cellular Sleep time:  1m 2s 150ms (96.9%)
     Cellular Idle time:   1s 334ms (2.1%)
     Cellular Rx time:     663ms (1.0%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 4s 147ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 3s 674ms (99.3%)
     WiFi Idle time:   466ms (0.7%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     8ms (0.0%)
     WiFi Battery drain: 0.000685mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 4s 148ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 6.72, actual drain: 0
    Uid u0a142: 5.48 ( cpu=5.46 wake=0.0000127 wifi=0.000618 gps=0.0209 ) Including smearing: 6.51 ( proportional=1.03 )
    Uid 0: 0.498 ( cpu=0.448 wake=0.0507 ) Excluded from smearing
    Uid 1000: 0.236 ( cpu=0.236 wake=0.0000325 ) Excluded from smearing
    Cell standby: 0.196 ( radio=0.196 ) Excluded from smearing
    Idle: 0.175 Excluded from smearing
    Uid u0a47: 0.0879 ( cpu=0.0878 wake=0.0000983 ) Excluded from smearing
    Uid 2000: 0.0291 ( cpu=0.0291 ) Excluded from smearing
    Uid u0a22: 0.00634 ( cpu=0.00625 wake=0.0000206 wifi=0.0000672 ) Excluded from smearing
    Uid 1036: 0.00563 ( cpu=0.00563 ) Excluded from smearing
    Uid u0a127: 0.00127 ( cpu=0.00127 ) Including smearing: 0.00150 ( proportional=0.000238 )
    Uid 1001: 0.00122 ( cpu=0.00117 wake=0.0000515 ) Excluded from smearing
    Wifi: 0.00109 ( cpu=0.00108 wifi=0.00000028 ) Including smearing: 0.00129 ( proportional=0.000203 )
    Uid 1021: 0.00100 ( cpu=0.00100 ) Excluded from smearing
    Uid u0a40: 0.000940 ( cpu=0.000940 ) Including smearing: 0.00112 ( proportional=0.000176 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 3s 478ms realtime (5 times)
    XO_shutdown.MPSS: 1m 0s 662ms realtime (28 times)
    XO_shutdown.ADSP: 1m 4s 51ms realtime (2 times)
    wlan.Active: 581ms realtime (5 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 39ms partial (2 times) max=58 actual=68 realtime
    Wake lock GnssLocationProvider: 3ms partial (2 times) max=4 actual=5 realtime
    TOTAL wake: 42ms blamed partial, 73ms actual partial realtime
    Fg Service for: 1m 4s 141ms 
    Total running: 1m 4s 141ms 
    Total cpu time: u=2s 113ms s=2s 250ms 
    Total cpu time per freq: 2340 50 10 90 50 20 40 70 10 30 40 30 20 10 0 40 0 0 30 0 10 110 20 0 0 0 0 0 10 0 0 0 0 10 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 300
    Total screen-off cpu time per freq: 2340 50 10 90 50 20 40 70 10 30 40 30 20 10 0 40 0 0 30 0 10 110 20 0 0 0 0 0 10 0 0 0 0 10 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 300
    Proc servicemanager:
      CPU: 10ms usr + 20ms krn ; 0ms fg
    Proc sensors.qcom:
      CPU: 10ms usr + 0ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 30ms krn ; 0ms fg
    Proc system:
      CPU: 480ms usr + 580ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
    Proc android.hardware.sensors@1.0-service:
      CPU: 0ms usr + 10ms krn ; 0ms fg
  u0a142:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 12ms (0.0%)
    Wifi Scan (blamed): 508ms (0.8%) 1x
    Wifi Scan (actual): 508ms (0.8%) 1x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 3s 942ms (99.6%)
       WiFi Idle time:   223ms (0.3%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     8ms (0.0%)
    Wake lock NlpWakeLock: 12ms partial (2 times) max=13 actual=15 realtime
    Wake lock fiid-sync: 4ms partial (1 times) max=4 realtime
    TOTAL wake: 16ms blamed partial, 19ms actual partial realtime
    Sensor GPS: 2s 505ms realtime (1 times)
    Foreground services: 1m 3s 579ms realtime (1 times)
    Fg Service for: 1m 3s 579ms 
    Cached for: 38ms 
    Total running: 1m 3s 617ms 
    Total cpu time: u=1m 18s 717ms s=13s 397ms 
    Total cpu time per freq: 400 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 60 990 10 10 0 10 10 10 0 10 20 110 230 630 850 600 530 470 460 320 110 140 70 80 80 10 40 40 30 40 50 83100
    Total screen-off cpu time per freq: 400 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 60 990 10 10 0 10 10 10 0 10 20 110 230 630 850 600 530 470 460 320 110 140 70 80 80 10 40 40 30 40 50 83100
    Proc cgeo.geocaching:
      CPU: 1m 11s 590ms usr + 12s 320ms krn ; 0ms fg
      1 starts

