#!/bin/bash
#debug output
#set -x
mergePerf()
{
    paste -d "\n" withlog_runtime.csv nolog_runtime.csv > runtime.csv
    paste -d "\n" withlog_compare_to_necessarylog_runtime.csv necessarylog_runtime.csv > necessary_runtime.csv

    paste -d "\n" withlog_cpu_time.csv nolog_cpu_time.csv > cpu_time.csv
    paste -d "\n" withlog_cpu_percentage.csv nolog_cpu_percentage.csv > cpu_percentage.csv
    paste -d "\n" withlog_compare_to_necessarylog_cpu_time.csv necessarylog_cpu_time.csv > necessary_cpu_time.csv
    paste -d "\n" withlog_compare_to_necessarylog_cpu_percentage.csv necessarylog_cpu_percentage.csv > necessary_cpu_percentage.csv

    paste -d "\n" withlog_battery.csv nolog_battery.csv > battery.csv
    paste -d "\n" withlog_compare_to_necessarylog_battery.csv necessarylog_battery.csv > necessary_battery.csv

}
mergePerf
