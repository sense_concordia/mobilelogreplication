Daily stats:
  Current start time: 2018-08-03-04-18-55
  Next min deadline: 2018-08-04-01-00-00
  Next max deadline: 2018-08-04-03-00-00
  Current daily steps:
    Discharge total time: 3d 13h 48m 56s 900ms  (from 6 steps)
    Discharge screen doze time: 9h 27m 36s 0ms  (from 5 steps)
  Daily from 2018-08-02-03-08-54 to 2018-08-03-04-18-55:
    Discharge total time: 1d 17h 15m 31s 900ms  (from 17 steps)
    Discharge screen doze time: 1d 1h 38m 56s 900ms  (from 15 steps)
    Charge total time: 2h 46m 58s 600ms  (from 4 steps)
    Charge screen doze time: 2h 46m 58s 600ms  (from 4 steps)
  Daily from 2018-08-01-07-27-20 to 2018-08-02-03-08-54:
    Discharge total time: 7h 28m 11s 400ms  (from 119 steps)
    Discharge screen doze time: 7h 21m 15s 400ms  (from 115 steps)
    Charge total time: 8h 3m 16s 600ms  (from 91 steps)
    Charge screen off time: 1h 39m 45s 200ms  (from 36 steps)
    Charge screen doze time: 12h 16m 47s 800ms  (from 54 steps)
  Daily from 2018-07-31-03-04-09 to 2018-08-01-07-27-20:
    Discharge total time: 10h 10m 35s 300ms  (from 126 steps)
    Discharge screen doze time: 10h 5m 11s 400ms  (from 123 steps)
    Charge total time: 6h 58m 29s 600ms  (from 132 steps)
    Charge screen off time: 1h 39m 15s 300ms  (from 42 steps)
    Charge screen doze time: 3h 5m 12s 400ms  (from 88 steps)
  Daily from 2018-07-30-03-01-01 to 2018-07-31-03-04-09:
    Discharge total time: 12h 27m 37s 0ms  (from 108 steps)
    Discharge screen doze time: 10h 58m 30s 600ms  (from 93 steps)
    Charge total time: 9h 36m 16s 500ms  (from 58 steps)
    Charge screen doze time: 1h 43m 24s 600ms  (from 55 steps)
  Daily from 2018-07-29-03-15-41 to 2018-07-30-03-01-01:
    Discharge total time: 14h 50m 18s 100ms  (from 97 steps)
    Discharge screen doze time: 14h 25m 25s 300ms  (from 80 steps)
    Charge total time: 8h 46m 59s 800ms  (from 126 steps)
    Charge screen doze time: 4h 26m 14s 900ms  (from 119 steps)
  Daily from 2018-07-28-21-09-05 to 2018-07-29-03-15-41:
    Discharge total time: 16h 30m 9s 900ms  (from 31 steps)
    Discharge screen doze time: 15h 41m 7s 600ms  (from 26 steps)
    Charge total time: 3h 25m 40s 200ms  (from 1 steps)
    Charge screen doze time: 3h 25m 40s 200ms  (from 1 steps)
  Daily from 2018-07-28-15-37-34 to 2018-07-28-21-09-05:
    Discharge total time: 17h 23m 15s 900ms  (from 18 steps)
    Discharge screen doze time: 12h 42m 13s 400ms  (from 17 steps)

Statistics since last charge:
  System starts: 0, currently on battery: true
  Estimated battery capacity: 3520 mAh
  Min learned battery capacity: 3761 mAh
  Max learned battery capacity: 3761 mAh
  Time on battery: 1m 2s 271ms (100.0%) realtime, 1m 2s 271ms (100.0%) uptime
  Time on battery screen off: 1m 2s 271ms (100.0%) realtime, 1m 2s 271ms (100.0%) uptime
  Time on battery screen doze: 1m 2s 271ms (100.0%)
  Total run time: 1m 2s 296ms realtime, 1m 2s 296ms uptime
  Discharge: 8.75 mAh
  Screen off discharge: 8.75 mAh
  Screen doze discharge: 8.75 mAh
  Start clock time: 2018-08-03-09-38-46
  Screen on: 0ms (0.0%) 0x, Interactive: 0ms (0.0%)
  Screen brightnesses: (no activity)
  Device light idling: 1m 2s 271ms (100.0%) 0x
  Idle mode light time: 1m 2s 271ms (100.0%) 0x -- longest 0ms 
  Total partial wakelock time: 245ms

  CONNECTIVITY POWER SUMMARY START
  Logging duration for connectivity statistics: 1m 2s 271ms 
  Cellular Statistics:
     Cellular kernel active time: 0ms (0.0%)
     Cellular data received: 0B
     Cellular data sent: 0B
     Cellular packets received: 0
     Cellular packets sent: 0
     Cellular Radio Access Technology: (no activity)
     Cellular Rx signal strength (RSRP):
       very poor (less than -128dBm):  1m 2s 271ms (100.0%) 
     Cellular Sleep time:  39s 232ms (63.0%)
     Cellular Idle time:   22s 588ms (36.3%)
     Cellular Rx time:     452ms (0.7%)
     Cellular Tx time:     
       less than 0dBm:  0ms (0.0%)
       0dBm to 8dBm:  0ms (0.0%)
       8dBm to 15dBm:  0ms (0.0%)
       15dBm to 20dBm:  0ms (0.0%)
       above 20dBm:  0ms (0.0%)
  Wifi Statistics:
     Wifi data received: 0B
     Wifi data sent: 0B
     Wifi packets received: 0
     Wifi packets sent: 0
     Wifi states:
       scanning 1m 2s 271ms (100.0%) 
     Wifi supplicant states: (no activity)
     Wifi Rx signal strength (RSSI): (no activity)
     WiFi Sleep time:  1m 1s 292ms (98.4%)
     WiFi Idle time:   964ms (1.5%)
     WiFi Rx time:     0ms (0.0%)
     WiFi Tx time:     16ms (0.0%)
     WiFi Battery drain: 0.00138mAh
  CONNECTIVITY POWER SUMMARY END

  Bluetooth total received: 0B, sent: 0B
  Bluetooth scan time: 0ms 
     Bluetooth Sleep time:  1m 2s 272ms (100.0%)
     Bluetooth Idle time:   0ms (0.0%)
     Bluetooth Rx time:     0ms (0.0%)
     Bluetooth Tx time:     0ms (0.0%)

  Device battery use since last full charge
    Amount discharged (lower bound): 0
    Amount discharged (upper bound): 0
    Amount discharged while screen on: 0
    Amount discharged while screen off: 0
    Amount discharged while screen doze: 0

  Estimated power use (mAh):
    Capacity: 3520, Computed drain: 9.31, actual drain: 0
    Uid u0a144: 5.64 ( cpu=5.13 wake=0.0000508 wifi=0.00138 gps=0.511 ) Including smearing: 9.03 ( proportional=3.39 )
    Uid 1036: 2.50 ( cpu=2.50 ) Excluded from smearing
    Uid 0: 0.660 ( cpu=0.611 wake=0.0492 ) Excluded from smearing
    Cell standby: 0.190 ( radio=0.190 ) Excluded from smearing
    Idle: 0.170 Excluded from smearing
    Uid 1000: 0.0786 ( cpu=0.0742 wake=0.00000952 sensor=0.00432 ) Excluded from smearing
    Uid 2000: 0.0242 ( cpu=0.0242 ) Excluded from smearing
    Uid u0a47: 0.0174 ( cpu=0.0173 wake=0.000102 ) Excluded from smearing
    Uid u0a22: 0.00915 ( cpu=0.00913 wake=0.0000190 ) Excluded from smearing
    Uid 1021: 0.00903 ( cpu=0.00903 ) Excluded from smearing
    Wifi: 0.00189 ( cpu=0.00189 ) Including smearing: 0.00303 ( proportional=0.00114 )
    Uid 1001: 0.00151 ( cpu=0.00150 wake=0.0000111 ) Excluded from smearing
    Uid u0a35: 0.00135 ( cpu=0.00135 ) Including smearing: 0.00216 ( proportional=0.000812 )
    Uid u0a127: 0.00102 ( cpu=0.00102 ) Including smearing: 0.00163 ( proportional=0.000613 )
    Uid u0a139: 0.000660 ( cpu=0.000660 ) Including smearing: 0.00106 ( proportional=0.000396 )
    Uid u0a40: 0.000421 ( cpu=0.000421 ) Including smearing: 0.000674 ( proportional=0.000253 )

  Resource Power Manager Stats
    wlan.Deep-Sleep: 1m 1s 15ms realtime (6 times)
    XO_shutdown.MPSS: 718ms realtime (2 times)
    XO_shutdown.ADSP: 1m 2s 144ms realtime (4 times)
    wlan.Active: 1s 147ms realtime (6 times)

  CPU freqs: 300000 364800 441600 518400 595200 672000 748800 825600 883200 960000 1036800 1094400 1171200 1248000 1324800 1401600 1478400 1555200 1670400 1747200 1824000 1900800 300000 345600 422400 499200 576000 652800 729600 806400 902400 979200 1056000 1132800 1190400 1267200 1344000 1420800 1497600 1574400 1651200 1728000 1804800 1881600 1958400 2035200 2112000 2208000 2265600 2323200 2342400 2361600 2457600

  1000:
    Wake lock *alarm*: 10ms partial (1 times) max=17 actual=17 realtime
    Wake lock GnssLocationProvider: 3ms partial (2 times) max=2 realtime
    TOTAL wake: 13ms blamed partial, 20ms actual partial realtime
    Sensor 17: 1m 2s 265ms realtime (0 times)
    Fg Service for: 1m 2s 265ms 
    Total running: 1m 2s 265ms 
    Total cpu time: u=553ms s=746ms 
    Total cpu time per freq: 220 30 40 70 80 50 70 90 20 30 60 80 10 30 30 20 10 40 0 0 10 60 10 10 10 0 10 20 0 0 20 20 0 0 10 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 300
    Total screen-off cpu time per freq: 220 30 40 70 80 50 70 90 20 30 60 80 10 30 30 20 10 40 0 0 10 60 10 10 10 0 10 20 0 0 20 20 0 0 10 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 300
    Proc servicemanager:
      CPU: 0ms usr + 20ms krn ; 0ms fg
    Proc android.hardware.graphics.composer@2.1-service:
      CPU: 10ms usr + 50ms krn ; 0ms fg
    Proc system:
      CPU: 420ms usr + 550ms krn ; 0ms fg
    Proc android.hardware.power@1.1-service.wahoo:
      CPU: 10ms usr + 10ms krn ; 0ms fg
  u0a144:
    Wifi Running: 0ms (0.0%)
    Full Wifi Lock: 48ms (0.1%)
    Wifi Scan (blamed): 1s 53ms (1.7%) 2x
    Wifi Scan (actual): 1s 53ms (1.7%) 2x
    Background Wifi Scan: 0ms (0.0%) 0x
       WiFi Sleep time:  1m 1s 301ms (98.4%)
       WiFi Idle time:   964ms (1.5%)
       WiFi Rx time:     0ms (0.0%)
       WiFi Tx time:     16ms (0.0%)
    Wake lock NlpWakeLock: 37ms partial (7 times) max=16 actual=57, 8ms background partial (2 times) max=7 realtime
    Wake lock *alarm*: 16ms partial (2 times) max=13 actual=20 realtime
    Wake lock fiid-sync: 12ms partial (1 times) max=12 realtime
    TOTAL wake: 65ms blamed partial, 80ms actual partial, 8ms actual background partial realtime
    Sensor GPS: 1m 1s 362ms realtime (1 times), 19ms background (0 times)
    Foreground services: 1m 1s 610ms realtime (1 times)
    Fg Service for: 1m 1s 610ms 
    Cached for: 119ms 
    Total running: 1m 1s 729ms 
    Total cpu time: u=1m 13s 810ms s=11s 130ms 
    Total cpu time per freq: 260 20 20 150 150 10 140 60 10 30 20 100 0 60 0 0 10 30 40 10 20 260 1650 60 110 80 40 10 20 40 70 90 220 310 330 470 220 140 70 100 30 40 80 60 40 30 10 80 20 140 10 20 78410
    Total screen-off cpu time per freq: 260 20 20 150 150 10 140 60 10 30 20 100 0 60 0 0 10 30 40 10 20 260 1650 60 110 80 40 10 20 40 70 90 220 310 330 470 220 140 70 100 30 40 80 60 40 30 10 80 20 140 10 20 78410
    Proc cgeo.geocaching:
      CPU: 1m 5s 900ms usr + 10s 750ms krn ; 0ms fg
      1 starts
    Apk com.google.android.gms:
      Wakeup alarm *walarm*:com.google.android.location.ALARM_WAKEUP_LOCATOR: 3 times

