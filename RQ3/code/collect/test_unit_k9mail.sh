#!/bin/bash
set -xi

DIR_APP=k-9

IsMonitorResponseTime=true
IsMonitorCPU=false

gradleTest()
{
TEST_CONFIG=k9mail_unit.log
OUT_RUNTIME=runtime.out
OUT_PERF=performance.out
lines=($(cat ${TEST_CONFIG}))
for line in ${lines[@]}
do
	COMMIT=${line%#*}
	echo $COMMIT >> $OUT_RUNTIME
	echo $COMMIT >> $OUT_PERF
	cd $DIR_APP
	cp ../perfsys_unit.py perfsys_unit.py
	TEST_JAVA=${line#*#}
	echo $TEST_JAVA >> ../$OUT_RUNTIME
	echo $TEST_JAVA >> ../$OUT_PERF
	#execute 30 times
	./gradlew :k9mail:testDebugUnitTest --rerun-tasks --tests="*.$TEST_JAVA"
	TestExecution=`(./gradlew :k9mail:testDebugUnitTest --rerun-tasks --tests="*.$TEST_JAVA" | grep 'Total time' | cut -d ' ' -f 3,5)`
	for i in $( seq 1 30 )
	do
		echo $i" test"
		echo $TestExecution
		if [ "$IsMonitorCPU" = true ]
        	then
			(python perfsys_unit.py $TestExecution &)
    	fi
		TestTime=`(./gradlew :k9mail:testDebugUnitTest --rerun-tasks --tests="*.$TEST_JAVA" | grep 'Total time')`
        echo $i "test finished"
		TestExecution=$TestTime
		if [ "$IsMonitorResponseTime" = true ]  
			then
				echo $i"."$TestTime >> ../$OUT_RUNTIME
    	fi
	done
	cd ..
done < $TEST_CONFIG
}

gradleTest