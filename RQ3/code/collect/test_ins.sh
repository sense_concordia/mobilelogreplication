#!/bin/bash
# set -xi

# Config target app

# APP="net.osmand"
# APP_TESTS="net.osmand.test"
# DIR_APP=osmandapp/android
# TEST_CONFIG=osmand_espresso.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

# APP="com.nextcloud.client"
# APP_TESTS="com.nextcloud.client.test"
# DIR_APP=nextcloud
# TEST_CONFIG=nextcloud_espresso.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

# APP="cgeo.geocaching"
# APP_TESTS="cgeo.geocaching.test"
# DIR_APP=cgeo
# TEST_CONFIG=cgeo_ins.log
# TEST_CONFIG=cgeo_necessary_ins.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

# APP="org.wordpress.android"
# APP_TESTS="org.wordpress.android.test"
# DIR_APP=WordPress-Android
# TEST_CONFIG=wordpress_ins.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

# APP="com.fsck.k9.debug"
# APP_TESTS="com.fsck.k9.tests"
# DIR_APP=k-9
# TEST_CONFIG=k9mail_espresso.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

# APP="com.ichi2.anki"
# APP_TESTS="com.ichi2.anki.tests"
# DIR_APP=Anki-Android
# TEST_CONFIG=anki_espresso.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

APP="de.danoeh.antennapod.debug"
APP_TESTS="de.test.antennapod"
DIR_APP=AntennaPod
TEST_CONFIG=antennapod_espresso.log
TEST_RUNNER=de.test.antennapod.AntennaPodTestRunner

# APP="org.sufficientlysecure.keychain.debug"
# APP_TESTS="org.sufficientlysecure.keychain.debug.test"
# DIR_APP=open-keychain
# TEST_CONFIG=openkeychain_espresso.log
# TEST_RUNNER=android.support.test.runner.AndroidJUnitRunner

# Config experiment type 
IsMonitorBattery=false
IsMonitorResponseTime=false
IsMonitorCPU=true

gradleTest()
{

OUT_RUNTIME=${APP}_runtime.out
OUT_PERF=${APP}_cpu.out

echo $APP
echo $TEST_RUNNER
echo $OUT_PERF
echo $OUT_RUNTIME

lines=($(cat ${TEST_CONFIG}))
for line in ${lines[@]}
do
	COMMIT=${line%#*}
	echo $COMMIT >> $OUT_RUNTIME
	echo $COMMIT >> $OUT_PERF
	cd $DIR_APP
	cp ../perfsys_ins.py perfsy_ins.py

	# TEST_JAVA=${line#*#}
	TEST_JAVA=`(echo $line | cut -d '#' -f 2)`
	# PACKAGE=`(echo $line | cut -d '#' -f 3)`
	
	#execute 30 times
	adb shell am instrument -w -r -e debug false -e class $TEST_JAVA $APP_TESTS/$TEST_RUNNER
	TestExecution=`(adb shell am instrument -w -r -e debug false -e class $TEST_JAVA $APP_TESTS/$TEST_RUNNER | grep 'Time:' | cut -d ' ' -f 2)`
	for i in $( seq 1 30 )
	do
		echo $i" test"
		echo $TestExecution
		if [ "$IsMonitorBattery" = true ]
		then
			adb shell dumpsys batterystats --reset
		fi
		if [ "$IsMonitorCPU" = true ]
		then
			(python3 perfsys.py $TestExecution &)
		fi
		TestTime=`(adb shell am instrument -w -r -e debug false -e class $TEST_JAVA $APP_TESTS/$TEST_RUNNER | grep 'Time:' )`
        echo $i "test finished"
		if [ "$IsMonitorResponseTime" = true ]  
        then
            echo $i"."$TestTime >> ../$OUT_RUNTIME
    	fi
		if [ "$IsMonitorBattery" = true ]
		then
			adb shell dumpsys batterystats --charged ${APP} >> ../${APP}_${TEST_JAVA}_${i}.txt
		fi

		# adb shell pm clear ${APP}
		sleep 10
	done
	cd ..
done < $TEST_CONFIG
}

gradleTest
