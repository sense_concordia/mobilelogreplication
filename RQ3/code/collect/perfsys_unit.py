import subprocess
import time
import sys
import re
import psutil
import commands

# package = 'nextcloud'
# package = 'osmand'
package = 'k9mail'

cpu_log = '../' + package + '_cpu.out'
process_cpu_list = []
total_cpu_list = []
file_log = open(cpu_log, 'a')
INTERVAL = 1

def get_pid(process_name):
    cmd = f"jps | grep '{process_name}'"
    info = commands.getoutput(cmd)
    infos = info.split()
    if len(infos) > 5:
        return infos[4]
    else:
        return -1


def perf(process_id):
    p = psutil.Process(process_id)
    file_log.write(str(p.cpu_times()) + '\n' + str(psutil.cpu_times()) + '\n')


if __name__ == "__main__":
    process_name="GradleWorkerMain"
    runtime = sys.argv[1]
    if("m" in runtime):
        minute = runtime.split('m')[0]
        second = runtime.split('m')[1][:-2]
        runtime = int(minute) * 60 + int(second)
    else:
        second = runtime.split('s')[0]
        runtime = int(second)

    time.sleep(10)
    pid = get_pid(process_name)

    counter = 0
    while counter < (runtime - 20):
        perf(pid)
        counter = counter + INTERVAL
        time.sleep(INTERVAL)


