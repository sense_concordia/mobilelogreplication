import subprocess
import time
import sys
import re

# package = 'net.osmand'
# package = 'com.nextcloud.client'
# package = 'org.wordpress.android'
# package = 'cgeo.geocaching'
# package = 'com.fsck.k9.debug'
# package = "com.ichi2.anki"
package = 'de.danoeh.antennapod.debug'
# package = 'org.sufficientlysecure.keychain.debug'

cpu_log = '../' + package + '_cpu.out'
cpu_dumpsys_list = []
process_cpu_list = []
total_cpu_list = []
cpu_file_log = open(cpu_log, 'a')
INTERVAL = 1

def get_cpu_dumpsys(pkg_name):
    cmd = f'adb shell dumpsys cpuinfo | grep {pkg_name}'
    output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.readlines()
    for info in output:
        if info.split()[1].decode().split("/")[1][:-1] == pkg_name:
            cpu_dumpsys_list.append(float(info.split()[2].decode().split("%")[0]))


def get_pid(package_name):
	cmd = f'adb shell ps | grep {package_name}'
	pid_output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
						   stderr=subprocess.PIPE).stdout.readlines()

	for item in pid_output:
		if item.split()[-1].decode() == package_name:
			pid = item.split()[1].decode()
			return pid

	return None

def total_cpu_time():
	cmd = 'adb shell cat /proc/stat'

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
						 stderr=subprocess.PIPE,
						 stdin=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	res = output.split()

	for info in res:
		if info.decode() == "cpu":
			if len(res) < 8:
				return None
			user = res[1].decode()
			nice = res[2].decode()
			system = res[3].decode()
			idle = res[4].decode()
			iowait = res[5].decode()
			irq = res[6].decode()
			softirq = res[7].decode()
			result = int(user) + int(nice) + int(system) + int(idle) + int(iowait) + int(irq) + int(softirq)
			return result
	
	return None


def process_cpu_time(pid):
	cmd = f'adb shell cat /proc/{pid}/stat'

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE,
						 stderr=subprocess.PIPE,
						 stdin=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	
	res = output.split()
	if len(res) < 17:
		return None

	utime = res[13].decode()
	stime = res[14].decode()
	cutime = res[15].decode()
	cstime = res[16].decode()
	result = int(utime) + int(stime) + int(cutime) + int(cstime)
	return result


def get_cpu_kernels():
	cmd = 'adb shell cat /proc/cpuinfo'
	output = subprocess.check_output(cmd, shell=True).split()
	s_item = ".".join([x.decode() for x in output])
	return len(re.findall("processor", s_item))


def get_cpu(pid):
	process_time = process_cpu_time(pid)
	total_time = total_cpu_time()
	if process_time is not None and total_time is not None:
		process_cpu_list.append(process_time)
		total_cpu_list.append(total_time)


def monitor_cpu(package_name, pid):
	while get_pid(package_name) is not None:
		get_cpu(pid)
		time.sleep(INTERVAL)
	cpu_file_log.write(str(process_cpu_list)[1:-1] + '\n' + str(total_cpu_list)[1:-1] + '\n')


if __name__ == "__main__":
	runtime = sys.argv[1]
	second = runtime.strip().split('s')[0]
	runtime = float(second)

	time.sleep(10)
	pid = get_pid(package)

	counter = 0
	while counter < (int(runtime) - 20):
		get_cpu(pid)
		counter = counter + INTERVAL
		time.sleep(INTERVAL)
	cpu_file_log.write(str(process_cpu_list)[1:-1] + '\n' + str(total_cpu_list)[1:-1] + '\n')