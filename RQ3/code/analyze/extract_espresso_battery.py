#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng


from pathlib import Path
from decimal import Decimal
import config


def get_battery_consumption(p):
    with p.open('r') as f:
        uid = get_uid(p)
        for line in f:
            line = line.strip()
            if line.startswith(f'Uid {uid}'):
                consumption = line.split(':')[1].split('(')[0].strip()
                return float(consumption)


def get_uid(file_path):
    with file_path.open('r') as f:
        lines = f.readlines()
    lines.reverse()
    for line in lines:
        # print(line)
        line = line.strip()
        if line.startswith('u') and line.endswith(':'):
            uid = line[0:-1]
            return uid


def _getmtime(entry):
    return entry.stat().st_mtime


def append_line(line, output_file_path: str):
    path = Path(output_file_path)
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open('a', encoding='utf-8') as f:
        print(line, file=f)


def extract(test_type):
    input_dir = f'./data/{config.DATE}/espresso/{config.TARGET_APP}/{test_type}/battery'
    output_file = f'./data/{config.DATE}/espresso/{config.TARGET_APP}/{test_type}_battery.csv'

    p = Path(input_dir)
    test_dirs = [x for x in p.iterdir() if x.is_dir()]
    test_dirs = sorted(test_dirs)
    for test_dir in test_dirs:
        print(test_dir)

        battery_consumption_list = []
        test_files = Path(test_dir).iterdir()
        sorted_test_files = sorted(test_files, key=_getmtime)
        for test_p in sorted_test_files:
            battery_consumption_list.append(get_battery_consumption(test_p))
        append_line(str(battery_consumption_list)[1:-1], output_file)


def main():
    extract('withlog')
    extract('nolog')
    extract('necessarylog')


if __name__ == '__main__':
    main()
