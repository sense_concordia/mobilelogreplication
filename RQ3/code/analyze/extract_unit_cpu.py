#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng


from pathlib import Path
from decimal import Decimal
import config


def generate_cpu(input_file, output_file, metric_type):
    cpu_list = []
    process_cpu_list = []
    total_cpu_list = []
    p = Path(input_file)
    with p.open('r', encoding='utf-8') as f:
        lines = f.readlines()
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        if line[-1].isalpha() and line != 'test started':
            # New test file begin
            i += 1
            if len(process_cpu_list) > 0:
                period_cpu = get_period_cpu_strip(process_cpu_list, total_cpu_list, metric_type)
                cpu_list.append(period_cpu)
                process_cpu_list = []
                total_cpu_list = []

            if len(cpu_list) > 0:
                append_line(str(cpu_list)[1:-1], output_file)
                cpu_list = []

            continue
        if line == 'test started':
            # New test round begin
            i += 1
            if len(process_cpu_list) > 0:
                period_cpu = get_period_cpu_strip(process_cpu_list, total_cpu_list, metric_type)
                cpu_list.append(period_cpu)
                process_cpu_list = []
                total_cpu_list = []
            continue
        else:
            process_cpu_utime = line.split('(user=')[1].split(',')[0]
            process_cpu_stime = line.split(', system=')[1].split(',')[0]
            process_cpu_cutime = line.split(', children_user=')[1].split(',')[0]
            process_cpu_cstime = line.split(', children_system=')[1].split(')')[0]
            process_cpu = Decimal(process_cpu_utime) + Decimal(process_cpu_stime) + Decimal(process_cpu_cutime) + Decimal(process_cpu_cstime)

            process_cpu_list.append(process_cpu)

            total_cpu_line = lines[i + 1]
            total_cpu_user = total_cpu_line.split('(user=')[1].split(',')[0]
            total_cpu_nice = total_cpu_line.split(', nice=')[1].split(',')[0]
            total_cpu_system = total_cpu_line.split(', system=')[1].split(',')[0]
            total_cpu_idle = total_cpu_line.split(', idle=')[1].split(',')[0]
            total_cpu_iowait = total_cpu_line.split(', iowait=')[1].split(',')[0]
            total_cpu_irq = total_cpu_line.split(', irq=')[1].split(',')[0]
            total_cpu_steal = total_cpu_line.split(', steal=')[1].split(',')[0]
            total_cpu_guest = total_cpu_line.split(', guest=')[1].split(',')[0]
            total_cpu_guest_nice = total_cpu_line.split(', guest_nice=')[1].split(')')[0]

            total_cpu = Decimal(total_cpu_user) + Decimal(total_cpu_nice) + Decimal(total_cpu_system) + \
                        Decimal(total_cpu_idle) + Decimal(total_cpu_iowait) + Decimal(total_cpu_irq) + \
                        Decimal(total_cpu_steal) + Decimal(total_cpu_guest) + Decimal(total_cpu_guest_nice)

            total_cpu_list.append(total_cpu)

            i += 2

    period_cpu = get_period_cpu_strip(process_cpu_list, total_cpu_list, metric_type)
    cpu_list.append(period_cpu)
    append_line(str(cpu_list)[1:-1], output_file)


def append_line(line, output_file_path: str):
    path = Path(output_file_path)
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open('a', encoding='utf-8') as f:
        print(line, file=f)


def get_period_cpu(process_cpu_list, total_cpu_list, metric_type):
    process_delta = process_cpu_list[-1] - process_cpu_list[0]
    total_delta = total_cpu_list[-1] - total_cpu_list[0]
    percentage_cpu = process_delta / total_delta

    if metric_type == 'time':
        return float(process_delta)
    else:
        return float(percentage_cpu)


def get_period_cpu_strip(process_cpu_list, total_cpu_list, metric_type):
    process_diff = get_diff(process_cpu_list)
    process_diff_index = [i for i, e in enumerate(process_diff) if e > 0.4]
    process_diff_index.append(process_diff_index[-1] + 1)
    process_strip = [process_cpu_list[i] for i in process_diff_index]
    total_strip = [total_cpu_list[i] for i in process_diff_index]

    return get_period_cpu(process_strip, total_strip, metric_type)


def get_diff(a_list):
    """
    Get a list of delta between consecutive elements in a list.
    """
    if len(a_list) < 2:
        return None

    return [int(a_list[i]) - int(a_list[i - 1]) for i in range(1, len(a_list))]


def extract_cpu_time(test_type):
    input_file = f'./data/{config.DATE}/unit/{config.TARGET_APP}/{test_type}/cpu/{config.APP_PACKAGE}_cpu.out'
    output_file = f'./data/{config.DATE}/unit/{config.TARGET_APP}/{test_type}_cpu_time.csv'
    generate_cpu(input_file, output_file, 'time')


def extract_cpu_percentage(test_type):
    input_file = f'./data/{config.DATE}/unit/{config.TARGET_APP}/{test_type}/cpu/{config.APP_PACKAGE}_cpu.out'
    output_file = f'./data/{config.DATE}/unit/{config.TARGET_APP}/{test_type}_cpu_percentage.csv'
    generate_cpu(input_file, output_file, 'percentage')


def main():
    extract_cpu_time('withlog')
    extract_cpu_time('nolog')
    extract_cpu_percentage('withlog')
    extract_cpu_percentage('nolog')


if __name__ == '__main__':
    main()
