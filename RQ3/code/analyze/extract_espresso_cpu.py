#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng


from pathlib import Path
import config


CPU_KERNELS = 8  # Google Pixel2 XL


def generate_cpu(input_file, output_file, metric_type):
    cpu_list = []
    p = Path(input_file)
    with p.open('r', encoding='utf-8') as f:
        lines = f.readlines()
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        if len(line) < 1:
            continue
        if line[-1].isalpha():
            # New test file begin
            i += 1
            if len(cpu_list) > 0:
                append_line(str(cpu_list)[1:-1], output_file)
                cpu_list = []
            continue
        else:
            process_cpu = line.strip().replace(' ', '').split(',')
            total_cpu = lines[i + 1].strip().replace(' ', '').split(',')
            period_cpu = get_period_cpu_strip(process_cpu, total_cpu, metric_type)
            cpu_list.append(period_cpu)
            i += 2

    append_line(str(cpu_list)[1:-1], output_file)


def append_line(line, output_file_path: str):
    path = Path(output_file_path)
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open('a', encoding='utf-8') as f:
        print(line, file=f)


def get_period_cpu(process_cpu_list, total_cpu_list, metric_type):
    process_delta = int(process_cpu_list[-1]) - int(process_cpu_list[0])
    total_delta = int(total_cpu_list[-1]) - int(total_cpu_list[0])
    percentage_cpu = process_delta / total_delta

    if metric_type == 'time':
        return process_delta
    else:
        return percentage_cpu


def get_period_cpu_strip(process_cpu_list, total_cpu_list, metric_type):
    process_diff = get_diff(process_cpu_list)
    process_diff_index = [i for i, e in enumerate(process_diff) if e > 7]
    process_diff_index.append(process_diff_index[-1] + 1)
    # process_diff_index = range(5, (len(process_cpu_list) - 5))
    process_strip = [process_cpu_list[i] for i in process_diff_index]
    total_strip = [total_cpu_list[i] for i in process_diff_index]

    return get_period_cpu(process_strip, total_strip, metric_type)


def get_diff(a_list):
    """
    Get a list of delta between consecutive elements in a list.
    """
    if len(a_list) < 2:
        return None

    return [int(a_list[i]) - int(a_list[i - 1]) for i in range(1, len(a_list))]


def extract_cpu_time(test_type):
    input_file = f'./data/{config.DATE}/espresso/{config.TARGET_APP}/{test_type}/cpu/{config.APP_PACKAGE}_cpu.out'
    output_file = f'./data/{config.DATE}/espresso/{config.TARGET_APP}/{test_type}_cpu_time.csv'
    generate_cpu(input_file, output_file, 'time')


def extract_cpu_percentage(test_type):
    input_file = f'./data/{config.DATE}/espresso/{config.TARGET_APP}/{test_type}/cpu/{config.APP_PACKAGE}_cpu.out'
    output_file = f'./data/{config.DATE}/espresso/{config.TARGET_APP}/{test_type}_cpu_percentage.csv'
    generate_cpu(input_file, output_file, 'percentage')


def main():
    extract_cpu_time('withlog')
    extract_cpu_time('nolog')
    extract_cpu_time('necessarylog')

    extract_cpu_percentage('withlog')
    extract_cpu_percentage('nolog')
    extract_cpu_percentage('necessarylog')


if __name__ == '__main__':
    main()
