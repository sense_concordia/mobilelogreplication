#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng


from pathlib import Path
import config
import statistics


def generate_mem_median(input_file, output_file):
    median_mem = []
    p = Path(input_file)
    with p.open('r', encoding='utf-8') as f:
        lines = f.readlines()
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        if len(line) < 1:
            continue
        if line[-1].isalpha():
            # New test file begin
            i += 1
            if len(median_mem) > 0:
                append_line(str(median_mem)[1:-1], output_file)
                median_mem = []
            continue
        else:
            mem_list = line.strip().replace(' ', '').split(',')
            mem_list = [int(n) for n in mem_list]
            median_mem.append(int(statistics.mean(mem_list)))
            # median_mem.append(int(statistics.median(mem_list)))
            i += 1

    append_line(str(median_mem)[1:-1], output_file)


def append_line(line, output_file_path: str):
    path = Path(output_file_path)
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open('a', encoding='utf-8') as f:
        print(line, file=f)


def main():
    input_file = f'./data/{config.DATE}/instrumented/{config.TARGET_APP}/withlog/memory/{config.APP_PACKAGE}_memory.out'
    output_file = f'./data/{config.DATE}/instrumented/{config.TARGET_APP}/withlog_mem.csv'
    generate_mem_median(input_file, output_file)

    input_file = f'./data/{config.DATE}/instrumented/{config.TARGET_APP}/nolog/memory/{config.APP_PACKAGE}_memory.out'
    output_file = f'./data/{config.DATE}/instrumented/{config.TARGET_APP}/nolog_mem.csv'
    generate_mem_median(input_file, output_file)


if __name__ == '__main__':
    main()
