#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng


from pathlib import Path
import config
import statistics


def generate_runtime(input_file, output_file):
    runtime_list = []
    p = Path(input_file)
    with p.open('r', encoding='utf-8') as f:
        lines = f.readlines()
    i = 0
    while i < len(lines):
        line = lines[i].strip()
        if len(line) < 1:
            continue
        if line[-1].isalpha() and ' ' not in line:
            # New test file begin
            i += 1
            if len(runtime_list) > 0:
                append_line(str(runtime_list)[1:-1], output_file)
                runtime_list = []
            continue
        else:
            runtime = line.strip().split()[2]
            runtime_list.append(float(runtime))
            i += 1

    append_line(str(runtime_list)[1:-1], output_file)


def append_line(line, output_file_path: str):
    path = Path(output_file_path)
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open('a', encoding='utf-8') as f:
        print(line, file=f)


def extract(test_type):
    """
    Generate csv files for further R analysis
    :param type: can be "withlog", "nolog", "necessarylog"
    :return: csv files
    """
    input_file = f'./data/{config.DATE}/unit/{config.TARGET_APP}/{test_type}/runtime/{config.APP_PACKAGE}_runtime.out'
    output_file = f'./data/{config.DATE}/unit/{config.TARGET_APP}/{test_type}_runtime.csv'
    generate_runtime(input_file, output_file)


def main():
    extract('withlog')
    extract('nolog')


if __name__ == '__main__':
    main()
