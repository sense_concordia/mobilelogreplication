#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# @Author : Yi(Joy) Zeng

DATE = '10-22'
# TARGET_APP = 'wordpress'
# APP_PACKAGE = 'org.wordpress.android'

# TARGET_APP = 'cgeo'
# APP_PACKAGE = 'cgeo.geocaching'

# TARGET_APP = 'osmand'
# APP_PACKAGE = 'net.osmand'

# TARGET_APP = 'nextcloud'
# APP_PACKAGE = 'com.nextcloud.client'


# TARGET_APP = 'k9'
# APP_PACKAGE = 'com.fsck.k9.debug'

# TARGET_APP = 'anki'
# APP_PACKAGE = 'com.ichi2.anki'

# TARGET_APP = 'antennapod'
# APP_PACKAGE = 'de.danoeh.antennapod.debug'

TARGET_APP = 'openkeychain'
APP_PACKAGE = 'org.sufficientlysecure.keychain.debug'

APPS_HAVE_NECESSARY_LOGS = ['k9', 'cgeo', 'anki', 'antennapod']
